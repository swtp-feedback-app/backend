# Backend der THM Feedback App

Dieses Projekt beinhaltet das Backend für die THM Feedback App, welche im Rahmen des Softwaretechnik-Projekts von Studierenden der THM im Sommersemester 2020 entwickelt wurde.


Am Backend beteiligt waren:

Claude Stephane Manace Kouame, Jonas-Ian Kuche und Silas Greeb und Daniela Claudia Coenen (Projektleitung).

## Dokumentation

Eine umfangreiche Dokumentation in Hinblick auf die Entwicklung des Backends ist der [Wiki](https://git.thm.de/swtp-feedback-app/backend/-/wikis/home) dieses Projektes zu entnehmen.

Informationen rund um das Frontend dieser Anwendung sind in einem extra [Repository](https://git.thm.de/swtp-feedback-app/frontend) und dessen [Wiki](https://git.thm.de/swtp-feedback-app/frontend/-/wikis/home) zu finden.
