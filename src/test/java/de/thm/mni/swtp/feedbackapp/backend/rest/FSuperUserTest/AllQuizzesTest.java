package de.thm.mni.swtp.feedbackapp.backend.rest.FSuperUserTest;

import de.thm.mni.swtp.feedbackapp.backend.rest.TestBase;
import io.vertx.core.Future;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.json.JsonArray;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import org.junit.Test;

public class AllQuizzesTest extends TestBase {
  @Test
  public void allQuizzes(TestContext context) {
    getAllQuizzes(context, "su", 200, 7);
  }

  @Test
  public void allQuizzesWithoutSuperuser(TestContext context) {
    getAllQuizzes(context, "user", 403, 0);
  }

  @Test
  public void allQuizzesWithoutLogin(TestContext context) {
    getAllQuizzes(context, null, 401, 0);
  }

  private void getAllQuizzes(TestContext context, String user, int expectedStatus, int expectedCount) {
    Async async = context.async();
    request("/api/su/quizzes", HttpMethod.GET, user, null).compose(res -> {
      context.assertEquals(expectedStatus, res.statusCode());
      if (res.statusCode() == 200) {
        JsonArray response = res.bodyAsJsonArray();
        context.assertEquals(expectedCount, response.size());
      }
      return Future.succeededFuture();
    }).onSuccess(res -> async.complete()).onFailure(context::fail);
  }
}
