package de.thm.mni.swtp.feedbackapp.backend.rest.CSessionControllerTest;

import de.thm.mni.swtp.feedbackapp.backend.rest.TestBase;
import io.vertx.ext.unit.TestContext;
import org.junit.Test;

public class XCreateAuthenticatedSessionTest extends TestBase {
  @Test
  public void createAuthenticatedSessionForUser4(TestContext context) {
    createSession(context, "user4Quiz", true, "user4", 200,
        "user4Session");
  }

  @Test
  public void createAuthenticatedSessionForUser1(TestContext context) {
    createSession(context, "user1Quiz", false, "user1", 200,
        "user1Session");
  }

  @Test
  public void createAuthenticatedSessionForUser(TestContext context) {
    createSession(context,"quizWithoutDescription", false, "user", 200,
        "userSession");
  }

  @Test
  public void createAuthenticatedSessionForUnownedQuiz(TestContext context) {
    createSession(context, "user1Quiz", false, "user", 403,
        "unOwnedUserSession");
  }

  private void createSession(TestContext context, String quizIDKey, boolean showCorrect, String user, int expectedStatus,
                             String hashKey) {
    CreateSessionTest.createSession(context, quizIDKey, "authenticated", showCorrect, user, expectedStatus, hashKey);
  }
}
