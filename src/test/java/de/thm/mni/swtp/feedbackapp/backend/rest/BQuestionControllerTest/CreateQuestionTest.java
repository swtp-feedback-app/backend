package de.thm.mni.swtp.feedbackapp.backend.rest.BQuestionControllerTest;

import de.thm.mni.swtp.feedbackapp.backend.rest.AQuizControllerTest.CreateQuizTest;
import de.thm.mni.swtp.feedbackapp.backend.rest.TestBase;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import org.junit.Test;

public class CreateQuestionTest extends TestBase {
  static final JsonObject question = new JsonObject()
      .put("question", "What is OpenApi?")
      .put("instantFeedback", true)
      .put("answers", new JsonObject()
        .put("options", new JsonArray().add(new JsonObject()
          .put("answer", "A").put("correct", true)).add(new JsonObject()
          .put("answer", "B").put("correct", false)).add(new JsonObject()
          .put("answer", "C").put("correct", false)).add(new JsonObject()
          .put("answer", "D").put("correct", false)))
        .put("possibleAnswers", 1));

  @Test
  public void createQuestionWithEmptyBody(TestContext context) {
    createQuestion(context, CreateQuizTest.quizIDs.get("quizWithoutDescription"), "user", new JsonObject(), 400);
  }

  @Test
  public void createQuestionWithoutType(TestContext context) {
    JsonObject body = new JsonObject()
        .put("question", question);
    createQuestion(context, CreateQuizTest.quizIDs.get("quizWithoutDescription"), "user", body, 400);
  }

  @Test
  public void createQuestionWithoutQuestion(TestContext context) {
    JsonObject body = new JsonObject()
        .put("type", "multipleChoice");
    createQuestion(context, CreateQuizTest.quizIDs.get("quizWithoutDescription"), "user", body, 400);
  }

  static final JsonObject questionWithTypeAndQuestion = new JsonObject()
      .put("type", "multipleChoice")
      .put("question", question);

  @Test
  public void createQuestion(TestContext context) {
    createQuestion(context, CreateQuizTest.quizIDs.get("quizWithoutDescription"), "user", questionWithTypeAndQuestion, 200);
  }

  @Test
  public void createQuestionWithWrongUser(TestContext context) {
    createQuestion(context, CreateQuizTest.quizIDs.get("quizWithoutDescription"), "user2", questionWithTypeAndQuestion, 403);
  }

  @Test
  public void createQuestionForAuthenticatedSession(TestContext context) {
    createQuestion(context, CreateQuizTest.quizIDs.get("user4Quiz"), "user4", questionWithTypeAndQuestion, 200);
  }

  private void createQuestion(TestContext context, int quizID, String user, JsonObject body, int expectedStatus) {
    Async async = context.async();
    request("/api/quizzes/" + quizID + "/questions", HttpMethod.POST, user, body).onSuccess(res -> {
      context.assertEquals(expectedStatus, res.statusCode());
      if (res.statusCode() == 200) {
        JsonObject resBody = res.bodyAsJsonObject();
        context.assertNotNull(resBody.getInteger("number"));
      }
      async.complete();
    }).onFailure(context::fail);
  }
}
