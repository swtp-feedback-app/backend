package de.thm.mni.swtp.feedbackapp.backend.rest.DCourseControllerTest;

import de.thm.mni.swtp.feedbackapp.backend.rest.TestBase;
import io.vertx.core.http.HttpMethod;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import org.junit.Test;

public class YRemoveAllAuthorsTest extends TestBase {
  @Test
  public void removeAuthors(TestContext context) {
    removeAuthors(context, "allAuthorCourse", "user4", 200);
  }

  @Test
  public void removeAuthorsNonDocent(TestContext context) {
    removeAuthors(context, "allAuthorCourse", "user", 403);
  }

  @Test
  public void removeAdminsNonExistingCourse(TestContext context) {
    removeAuthors(context, 0, "user4", 404);
  }

  private void removeAuthors(TestContext context, int courseID, String user, int expectedStatus) {
    Async async = context.async();
    request("/api/courses/" + courseID + "/all/authors", HttpMethod.DELETE, user, null)
        .onSuccess(res -> {
          context.assertEquals(expectedStatus, res.statusCode());
          async.complete();
        }).onFailure(context::fail);
  }
  private void removeAuthors(TestContext context, String courseIDKey, String user, int expectedStatus) {
    int courseID = CreateCourseTest.courseIDs.get(courseIDKey);
    removeAuthors(context, courseID, user, expectedStatus);
  }
}
