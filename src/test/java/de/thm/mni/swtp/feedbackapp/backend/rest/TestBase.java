package de.thm.mni.swtp.feedbackapp.backend.rest;

import de.thm.mni.swtp.feedbackapp.backend.MainVerticle;
import io.vertx.core.Future;
import io.vertx.core.Promise;
import io.vertx.core.Vertx;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.RunTestOnContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;
import io.vertx.ext.web.client.HttpRequest;
import io.vertx.ext.web.client.HttpResponse;
import io.vertx.ext.web.client.WebClient;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.runner.RunWith;

@RunWith(VertxUnitRunner.class)
abstract public class TestBase {
  @ClassRule
  public static final RunTestOnContext rule = new RunTestOnContext();

  public static WebClient client;

  @BeforeClass
  public static void createWebClient() {
    client = WebClient.create(rule.vertx());
  }

  @BeforeClass
  public static void startVertx(TestContext context) {
    Async async = context.async();
    TestBase.startTestServer().onSuccess(res -> async.complete()).onFailure(context::fail);
  }

  protected static Future<HttpResponse<Buffer>> request(String path, HttpMethod method, String authorized, Object body) {
    HttpRequest<Buffer> req = client.request(method, 8080, "localhost", path);
    if (authorized != null) {
      String token = tokens.get(authorized);
      if (token == null) {
        throw new IllegalArgumentException("User \"" + authorized + "\" not found");
      }
      req.putHeader("Authorization", "Bearer " + token);
    }
    Promise<HttpResponse<Buffer>> resPromise = Promise.promise();
    if (body != null) {
      req.putHeader("Content-Type", "application/json")
          .sendJson(body, resPromise);
    } else {
      req.send(resPromise);
    }
    return resPromise.future();
  }


  private static final List<String[]> credentials = new ArrayList<>(List.of(
      new String[]{"user", "password"},
      new String[]{"user1", "password"},
      new String[]{"user2", "password"},
      new String[]{"user3", "password"},
      new String[]{"user4", "password"},
      new String[]{"su", "password"}
  ));
  private static boolean started;
  private static Throwable failed;
  private static Future<Void> startTestServer() {
    if (started) {
      return Future.succeededFuture();
    }
    if (failed != null) {
      return Future.failedFuture(failed);
    }
    Promise<String> deploymentVerticle = Promise.promise();
    Vertx.vertx().deployVerticle(new MainVerticle(), deploymentVerticle);
    return deploymentVerticle.future().compose(res -> getToken(credentials)).compose(res -> {
      started = true;
      return Future.succeededFuture();
    }, err -> {
      failed = err;
      return Future.failedFuture(err);
    });
  }

  private static final HashMap<String, String> tokens = new HashMap<>();
  private static Future<Void> getToken(List<String[]> credentials) {
    if (credentials.isEmpty()) {
      return Future.succeededFuture();
    }
    String[] credential = credentials.remove(0);
    Promise<HttpResponse<Buffer>> responsePromise = Promise.promise();
    client.post(8080, "localhost", "/api/auth")
        .putHeader("Content-Type", "application/json")
        .sendJsonObject(new JsonObject()
            .put("username", credential[0])
            .put("password", credential[1]), responsePromise);
    return responsePromise.future().compose(res -> {
      if (res.statusCode() != 200) {
        return Future.failedFuture("non 200 status code: " + res.statusCode());
      }
      tokens.put(credential[0], res.bodyAsJsonObject().getString("token"));
      return getToken(credentials);
    });
  }
}
