package de.thm.mni.swtp.feedbackapp.backend.rest.DCourseControllerTest;

import de.thm.mni.swtp.feedbackapp.backend.rest.TestBase;
import io.vertx.core.Future;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import org.junit.Test;

public class YListSessionTest extends TestBase {
    @Test
    public void listSession(TestContext context){
        this.listSessions(context, "courseWithNameAndSemester", "user", 1, 200);
    }

    @Test
    public void listSessionOtherSession(TestContext context){
        this.listSessions(context, "courseWithNameAndDescriptionAndSemester", "user", 1, 200);
    }

    @Test
    public void listSessionForCourseOwnedByOtherUser(TestContext context){
        this.listSessions(context, "courseWithNameAndSemester", "user1", 1, 200);
    }

    @Test
    public void listSessionsForNonExistingCourse(TestContext context){
        this.listSessions(context, 10000000, "user", 1, 404);
    }

    private void listSessions(TestContext context, int courseID, String user,
        int expectedSessionCount, int expectedStatus){
        Async async = context.async();
        request("/api/courses/" + courseID + "/sessions", HttpMethod.GET, user, null).compose(res -> {
            context.assertEquals(expectedStatus, res.statusCode());
            if (res.statusCode() == 200) {
                JsonArray body = res.bodyAsJsonArray();
                context.assertEquals(expectedSessionCount, body.size());
                for (int i = 0; i < body.size(); i++) {
                    JsonObject session = body.getJsonObject(i);
                    context.assertNotNull(session.getInteger("id"));
                    context.assertNotNull(session.getString("type"));
                    context.assertNotNull(session.getLong("start"));
                    context.assertNotNull(session.getLong("end"));
                    context.assertNotNull(session.getJsonObject("quiz"));
                    context.assertNotNull(session.getBoolean("complete"));
                    JsonObject creator = session.getJsonObject("creator");
                    context.assertNotNull(creator);
                    context.assertNotNull(creator.getInteger("id"));
                    context.assertNotNull(creator.getString("username"));
                    context.assertNotNull(creator.getBoolean("docent"));
                }
            }
            return Future.succeededFuture();
        }).onSuccess(res -> async.complete()).onFailure(context::fail);
    }
    private void listSessions(TestContext context, String courseIDKey, String user,
        int expectedSessionCount, int expectedStatus) {
        int courseID = CreateCourseTest.courseIDs.get(courseIDKey);
        listSessions(context, courseID, user, expectedSessionCount, expectedStatus);
    }
}
