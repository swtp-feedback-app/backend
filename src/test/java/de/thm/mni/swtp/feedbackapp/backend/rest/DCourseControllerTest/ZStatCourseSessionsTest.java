package de.thm.mni.swtp.feedbackapp.backend.rest.DCourseControllerTest;

import de.thm.mni.swtp.feedbackapp.backend.rest.CSessionControllerTest.CreateSessionTest;
import de.thm.mni.swtp.feedbackapp.backend.rest.TestBase;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import org.junit.Assert;
import org.junit.Test;

public class ZStatCourseSessionsTest extends TestBase {
  @Test
  public void getCourseSessionStatistics(TestContext context) {
    getCourseSessionsStatistics(context, "courseWithNameAndDescriptionAndSemester", "user4", 200);
  }

  @Test
  public void getCourseSessionStatisticsAsNonLecturer(TestContext context) {
    getCourseSessionsStatistics(context, "courseWithNameAndDescriptionAndSemester", "user", 403);
  }

  @Test
  public void getCourseSessionStatisticsForNonExistingCourse(TestContext context) {
    getCourseSessionsStatistics(context, 10000000, "user4", 404);
  }

  private static void getCourseSessionsStatistics(TestContext context, int courseID, String user, int expectedStatus) {
    Async async = context.async();
    request("/api/courses/" + courseID + "/stats/sessions", HttpMethod.GET, user, null)
        .onSuccess(res -> {
          context.assertEquals(expectedStatus, res.statusCode());
          if (res.statusCode() == 200) {
            JsonArray body = res.bodyAsJsonArray();
            Assert.assertEquals(1, body.size());
            for (int i = 0; i < body.size(); i++) {
              JsonObject entry = body.getJsonObject(i);
              JsonObject session = entry.getJsonObject("session");
              context.assertNotNull(session);
              context.assertNotNull(session.getString("type"));
              context.assertNotNull(session.getJsonObject("quiz"));
              context.assertNotNull(session.getLong("start"));
              context.assertNotNull(session.getLong("end"));
              context.assertNotNull(session.getJsonObject("course"));
              context.assertEquals("A new Session", session.getString("description"));
              JsonObject stats = entry.getJsonObject("stats");
              context.assertNotNull(stats);
              context.assertNotNull(stats.getInteger("participantCount"));
              context.assertNotNull(stats.getInteger("completeCount"));
              context.assertNotNull(stats.getInteger("allWrong"));
              context.assertNotNull(stats.getInteger("zeroPlus"));
              context.assertNotNull(stats.getInteger("fortyPlus"));
              context.assertNotNull(stats.getInteger("sixtyPlus"));
              context.assertNotNull(stats.getInteger("eightyPlus"));
            }
          }
          async.complete();
        }).onFailure(context::fail);
  }
  private static void getCourseSessionsStatistics(TestContext context, String courseIDKey, String user, int expectedStatus) {
    int courseID = CreateCourseTest.courseIDs.get(courseIDKey);
    getCourseSessionsStatistics(context, courseID, user, expectedStatus);
  }
}
