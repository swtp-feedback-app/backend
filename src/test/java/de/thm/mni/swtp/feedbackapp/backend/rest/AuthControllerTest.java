package de.thm.mni.swtp.feedbackapp.backend.rest;

import io.vertx.core.http.HttpMethod;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import org.junit.Test;

public class AuthControllerTest extends TestBase {
  @Test
  public void authSuccess(TestContext context) {
    authTest(context, "user", "password", 200, true);
  }

  @Test
  public void authFailWrongPassword(TestContext context) {
    authTest(context, "user", "wrongPassword", 401, false);
  }

  @Test
  public void authFailWrongUsername(TestContext context) {
    authTest(context, "userWrong", "password", 401, false);
  }

  @Test
  public void authFailWrongUsernameAndPassword(TestContext context) {
    authTest(context, "userWrong", "wrongPassword", 401, false);
  }

  private void authTest(TestContext context, String username, String password, int status, boolean expectToken) {
    Async async = context.async();
    request("/api/auth", HttpMethod.POST, null, new JsonObject()
        .put("username", username).put("password", password)).onSuccess(res -> {
          context.assertEquals(status, res.statusCode());
          if (expectToken) {
            JsonObject resBody = res.bodyAsJsonObject();
            String token = resBody.getString("token");
            context.assertNotNull(token);
            JsonObject user = resBody.getJsonObject("user");
            context.assertNotNull(user);
            context.assertEquals(username, user.getString("username"));
          }
          async.complete();
    }).onFailure(context::fail);
  }
}
