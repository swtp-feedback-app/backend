package de.thm.mni.swtp.feedbackapp.backend.rest.DCourseControllerTest;

import de.thm.mni.swtp.feedbackapp.backend.rest.CSessionControllerTest.ZGetSessionStatisticsTest;
import de.thm.mni.swtp.feedbackapp.backend.rest.TestBase;
import io.vertx.ext.unit.TestContext;
import org.junit.Test;

public class ZStatCourseSessionTest extends TestBase {
  @Test
  public void getSessionStatisticsAsSessionOwner(TestContext context) {
    ZGetSessionStatisticsTest.getSessionStatistics(context, "user1Session",
        "user1", 200);
  }
  @Test
  public void getSessionStatisticsAsCourseAdmin(TestContext context) {
    ZGetSessionStatisticsTest.getSessionStatistics(context, "user1Session",
        "user2", 200);
  }
  @Test
  public void getSessionStatisticsAsCourseOwner(TestContext context) {
    ZGetSessionStatisticsTest.getSessionStatistics(context, "user1Session",
    "user4", 200);
  }
  @Test
  public void getSessionStatisticsAsSomebodyElse(TestContext context) {
    ZGetSessionStatisticsTest.getSessionStatistics(context, "user1Session",
        "user3", 403);
  }
}
