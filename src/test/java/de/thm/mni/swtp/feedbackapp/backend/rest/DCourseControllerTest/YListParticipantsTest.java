package de.thm.mni.swtp.feedbackapp.backend.rest.DCourseControllerTest;

import de.thm.mni.swtp.feedbackapp.backend.rest.TestBase;
import io.netty.handler.codec.http.QueryStringEncoder;
import io.vertx.core.Future;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import org.junit.Test;

public class YListParticipantsTest extends TestBase {
  @Test
  public void listParticipants(TestContext context) {
    this.listParticipants(context, "courseWithNameAndDescriptionAndSemester", false, "user4", 200, 4);
  }

  @Test
  public void listParticipantsWithoutAdmins(TestContext context) {
    this.listParticipants(context, "courseWithNameAndDescriptionAndSemester", true, "user4", 200, 1);
  }

  @Test
  public void listParticipantsAsAdmin(TestContext context) {
    this.listParticipants(context, "courseWithNameAndDescriptionAndSemester", false, "user1", 200, 4);
  }

  @Test
  public void listParticipantsForNonOwnedCourse(TestContext context) {
    this.listParticipants(context, "courseWithNameAndDescriptionAndSemester", false, "user", 403, 0);
  }

  @Test
  public void listParticipantsForNonExistingCourse(TestContext context) {
    this.listParticipants(context, 10000000, false, "user4", 404, 0);
  }

  private void listParticipants(TestContext context, int courseID, boolean hideAdmins, String user,
      int expectedStatus, int expectedParticipantCount) {
    Async async = context.async();
    QueryStringEncoder queryStringEncoder = new QueryStringEncoder("/api/courses/" + courseID + "/users");
    if (hideAdmins) {
      queryStringEncoder.addParam("role", "participant");
    }
    request(queryStringEncoder.toString(), HttpMethod.GET, user, null).compose(res -> {
      context.assertEquals(expectedStatus, res.statusCode());
      if (res.statusCode() == 200) {
        JsonArray resBody = res.bodyAsJsonArray();
        context.assertEquals(expectedParticipantCount, resBody.size());
        for (int i = 0; i < resBody.size(); i++) {
          JsonObject person = resBody.getJsonObject(i);
          context.assertNotNull(person.getInteger("id"));
          context.assertNotNull(person.getString("username"));
          context.assertNotNull(person.getBoolean("docent"));
        }
      }
      return Future.succeededFuture();
    }).onSuccess(res -> async.complete()).onFailure(context::fail);
  }
  private void listParticipants(TestContext context, String courseIDKey, boolean hideAdmins, String user,
      int expectedStatus, int expectedParticipantCount) {
    int courseID = CreateCourseTest.courseIDs.get(courseIDKey);
    listParticipants(context, courseID, hideAdmins, user, expectedStatus, expectedParticipantCount);
  }
}
