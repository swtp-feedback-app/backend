package de.thm.mni.swtp.feedbackapp.backend.rest.DCourseControllerTest;

import de.thm.mni.swtp.feedbackapp.backend.rest.TestBase;
import io.netty.handler.codec.http.QueryStringEncoder;
import io.vertx.core.Future;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import org.junit.Test;

public class ZListAdminTest extends TestBase {
  @Test
  public void listAdmins(TestContext context) {
    this.listAdmins(context, "courseWithNameAndDescriptionAndSemester", "user4", 200);
  }

  @Test
  public void listAdminsForNonOwnedCourse(TestContext context) {
    this.listAdmins(context, "courseWithNameAndDescriptionAndSemester", "user", 403);
  }

  @Test
  public void listAdminsForNonExistingCourse(TestContext context) {
    this.listAdmins(context, 10000000, "user", 404);
  }

  private void listAdmins(TestContext context, int courseID, String user, int expectedStatus) {
    Async async = context.async();
    QueryStringEncoder queryStringEncoder = new QueryStringEncoder("/api/courses/" + courseID + "/users");
    queryStringEncoder.addParam("role", "admin");
    request(queryStringEncoder.toString(), HttpMethod.GET, user, null).compose(res -> {
      context.assertEquals(expectedStatus, res.statusCode());
      if (res.statusCode() == 200) {
        JsonArray resBody = res.bodyAsJsonArray();
        for (int i = 0; i < resBody.size(); i++) {
          JsonObject person = resBody.getJsonObject(i);
          context.assertNotNull(person.getInteger("id"));
          context.assertNotNull(person.getString("username"));
          context.assertNotNull(person.getBoolean("docent"));
        }
      }
      return Future.succeededFuture();
    }).onSuccess(res -> async.complete()).onFailure(context::fail);
  }
  private void listAdmins(TestContext context, String courseIDKey, String user, int expectedStatus) {
    int courseID = CreateCourseTest.courseIDs.get(courseIDKey);
    listAdmins(context, courseID, user, expectedStatus);
  }
}
