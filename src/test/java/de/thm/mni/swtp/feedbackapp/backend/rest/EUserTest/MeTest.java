package de.thm.mni.swtp.feedbackapp.backend.rest.EUserTest;

import de.thm.mni.swtp.feedbackapp.backend.rest.TestBase;
import io.vertx.core.Future;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import org.junit.Test;

public class MeTest extends TestBase {
  @Test
  public void getMe(TestContext context) {
    getMe(context, "user", 200, "user", 1, null, false, false);
  }

  @Test
  public void getMeDocent(TestContext context) {
    getMe(context, "user4", 200, "user4", 5, null, true, false);
  }

  @Test
  public void getMeSuperUser(TestContext context) {
    getMe(context, "su", 200, "su", 6, null, false, true);
  }

  @Test
  public void getMeWithoutLogin(TestContext context) {
    getMe(context, null, 401, null, null, null, false, false);
  }

  private void getMe(TestContext context, String user, int expectedStatus, String expectedUsername,
      Integer expectedID, String expectedName, boolean expectedDocent, boolean expectedSuperUser) {
    Async async = context.async();
    request("/api/users/me", HttpMethod.GET, user, null).compose(res -> {
      context.assertEquals(expectedStatus, res.statusCode());
      if (res.statusCode() == 200) {
        JsonObject response = res.bodyAsJsonObject();
        context.assertEquals(expectedID, response.getInteger("id"));
        context.assertEquals(expectedUsername, response.getString("username"));
        context.assertEquals(expectedName, response.getString("name"));
        context.assertEquals(expectedDocent, response.getBoolean("docent"));
        context.assertEquals(expectedSuperUser, response.getBoolean("superUser"));
      }
      return Future.succeededFuture();
    }).onSuccess(res -> async.complete()).onFailure(context::fail);
  }
}
