package de.thm.mni.swtp.feedbackapp.backend.rest.DCourseControllerTest;

import de.thm.mni.swtp.feedbackapp.backend.rest.TestBase;
import io.netty.handler.codec.http.QueryStringEncoder;
import io.vertx.core.Future;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import org.junit.Test;

public class ZBrowseCoursesTest extends TestBase {
  @Test
  public void browseCourse(TestContext context) {
    browseCourses(context, null, null, null, false, "user", 200, 4);
  }

  @Test
  public void browseCourseWithSearch(TestContext context) {
    browseCourses(context, "Course", null, null, false, "user", 200, 4);
  }

  @Test
  public void browseCourseWithSearchWithoutResult(TestContext context) {
    browseCourses(context, "Kurs", null, null, false, "user", 200, 0);
  }

  @Test
  public void browseCourseWithPage(TestContext context) {
    browseCourses(context, null, 2, 0, false, "user", 200, 2);
  }

  @Test
  public void browseCourseWithPageOne(TestContext context) {
    browseCourses(context, null, 2, 1, false, "user", 200, 2);
  }

  @Test
  public void browseCourseWithPageTwo(TestContext context) {
    browseCourses(context, null, 2, 2, false, "user", 200, 0);
  }

  @Test
  public void browseCourseHideJoinedWithoutJoined(TestContext context) {
    browseCourses(context, null, null, null, true, "user3", 200, 4);
  }

  @Test
  public void browseCourseHideJoined(TestContext context) {
    browseCourses(context, null, null, null, true, "user1", 200, 2);
  }

  private void browseCourses(TestContext context, String search, Integer perPage, Integer page,
      boolean hideJoined, String user, int expectedStatus, int expectedCount) {
    Async async = context.async();
    QueryStringEncoder queryStringEncoder = new QueryStringEncoder("/api/courses");
    if (search != null) {
      queryStringEncoder.addParam("search", search);
    }
    if (perPage != null) {
      queryStringEncoder.addParam("perPage", perPage.toString());
    }
    if (page != null) {
      queryStringEncoder.addParam("page", page.toString());
    }
    if (hideJoined) {
      queryStringEncoder.addParam("hideJoined", "true");
    }
    request(queryStringEncoder.toString(), HttpMethod.GET, user, null).compose(res -> {
      context.assertEquals(expectedStatus, res.statusCode());
      if (res.statusCode() == 200) {
        JsonArray resBody = res.bodyAsJsonArray();
        context.assertEquals(expectedCount, resBody.size());
        for (int i = 0; i < resBody.size(); i++) {
          JsonObject course = resBody.getJsonObject(i);
          context.assertNotNull(course.getInteger("id"));
          context.assertNotNull(course.getString("name"));
          context.assertNotNull(course.getString("semester"));
          context.assertNotNull(course.getBoolean("protected"));
          context.assertNotNull(course.getBoolean("allAuthor"));
          JsonObject docent = course.getJsonObject("docent");
          context.assertNotNull(docent);
          context.assertNotNull(docent.getString("username"));
          context.assertEquals(true, docent.getBoolean("docent"));
        }
      }
      return Future.succeededFuture();
    }).onSuccess(res -> async.complete()).onFailure(context::fail);
  }
}
