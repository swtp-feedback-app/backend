package de.thm.mni.swtp.feedbackapp.backend.rest.CSessionControllerTest;

import io.vertx.core.http.HttpMethod;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import org.junit.Test;

public class ZGetMyStatisticsTest extends SessionTestBase {

  @Test
  public void getMyStatistics(TestContext context) {
    getMyStatistics(context, "session", "sessionToken", 200, 3, 3, 2);
  }

  @Test
  public void getMyStatisticsWithOtherUser(TestContext context) {
    getMyStatistics(context, "session", "sessionTokenAgain", 200, 3, 1, 1);
  }

  @Test
  public void getMyStatisticsForNonExistingSession(TestContext context) {
    getMyStatistics(context, 10000000, "sessionToken", 404);
  }

  private void getMyStatistics(TestContext context, int sessionID, String sessionTokenKey,
      int expectedStatus, int expectedQuestionCount, int expectedAnsweredCount,
      int expectedCorrectCount) {
    Async async = context.async();
    request("/api/sessions/" + sessionID + "/myStatistics", HttpMethod.GET, sessionTokenKey,
        null).onSuccess(res -> {
      context.assertEquals(expectedStatus, res.statusCode());
      if (res.statusCode() == 200) {
        JsonObject body = res.bodyAsJsonObject();
        context.assertEquals(expectedQuestionCount, body.getInteger("questionCount"));
        context.assertEquals(expectedAnsweredCount, body.getInteger("answeredQuestions"));
        context.assertEquals(expectedCorrectCount, body.getInteger("correctQuestions"));
      }
      async.complete();
    }).onFailure(context::fail);
  }

  private void getMyStatistics(TestContext context, String sessionIDKey, String sessionTokenKey,
      int expectedStatus, int expectedQuestionCount, int expectedAnsweredCount,
      int expectedCorrectCount) {
    int sessionID = CreateSessionTest.sessionIDs.get(sessionIDKey);
    getMyStatistics(context, sessionID, sessionTokenKey, expectedStatus, expectedQuestionCount,
        expectedAnsweredCount, expectedCorrectCount);
  }

  private void getMyStatistics(TestContext context, int sessionID, String sessionTokenKey,
      int expectedStatus) {
    getMyStatistics(context, sessionID, sessionTokenKey, expectedStatus, -1, -1, -1);
  }
}
