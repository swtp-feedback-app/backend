package de.thm.mni.swtp.feedbackapp.backend.rest.CSessionControllerTest;

import de.thm.mni.swtp.feedbackapp.backend.rest.TestBase;
import io.vertx.core.Future;
import io.vertx.core.http.HttpMethod;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import org.junit.Test;

public class DeleteSessionTest extends TestBase {
  @Test
  public void deleteSession(TestContext context) {
    deleteSession(context, CreateSessionTest.sessionIDs.get("deleteMeSession"), "user", 200);
  }

  @Test
  public void deleteNonExistingSession(TestContext context) {
    deleteSession(context, 10000000, "user", 404);
  }

  @Test
  public void deleteNonOwnedSession(TestContext context) {
    deleteSession(context, CreateSessionTest.sessionIDs.get("session"), "user4", 403);
  }

  private void deleteSession(TestContext context, int sessionID, String user, int expectedStatus) {
    Async async = context.async();
    request("/api/sessions/" + sessionID, HttpMethod.DELETE, user, null).compose(res -> {
      context.assertEquals(expectedStatus, res.statusCode());
      return Future.succeededFuture();
    }).onSuccess(res -> async.complete()).onFailure(context::fail);
  }
}
