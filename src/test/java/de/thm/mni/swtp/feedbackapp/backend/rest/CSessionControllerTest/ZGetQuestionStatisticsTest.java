package de.thm.mni.swtp.feedbackapp.backend.rest.CSessionControllerTest;

import de.thm.mni.swtp.feedbackapp.backend.rest.TestBase;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import org.junit.Test;

public class ZGetQuestionStatisticsTest extends TestBase {

  @Test
  public void getQuestionStatistics(TestContext context) {
    getQuestionStatistics(context, "session", 1, "user", 200, 2, 2);
  }

  @Test
  public void getQuestionStatisticsForWrongAnswered(TestContext context) {
    getQuestionStatistics(context, "session", 3, "user", 200, 1, 0);
  }

  @Test
  public void getQuestionStatisticsForNonExistingSession(TestContext context) {
    getQuestionStatistics(context, 10000000, 1, "user", 404);
  }

  @Test
  public void getQuestionStatisticsForNonOwnedSession(TestContext context) {
    getQuestionStatistics(context, "session", 1, "user1", 403);
  }

  @Test
  public void getQuestionStatisticsForNonExistingQuestion(TestContext context) {
    getQuestionStatistics(context, "session", 5, "user", 404);
  }

  private void getQuestionStatistics(TestContext context, int sessionID, int questionNumber,
      String user, int expectedStatus, int expectedAnswered, int expectedCorrect) {
    Async async = context.async();
    request("/api/sessions/" + sessionID + "/statistics/" + questionNumber, HttpMethod.GET, user,
        null).onSuccess(res -> {
      context.assertEquals(expectedStatus, res.statusCode());
      if (res.statusCode() == 200) {
        JsonObject body = res.bodyAsJsonObject();
        context.assertEquals(expectedAnswered, body.getInteger("answered"));
        context.assertEquals(expectedCorrect, body.getInteger("correct"));
      }
      async.complete();
    }).onFailure(context::fail);
  }
  private void getQuestionStatistics(TestContext context, String sessionIDKey, int questionNumber,
      String user, int expectedStatus, int expectedAnswered, int expectedCorrect) {
    int sessionID = CreateSessionTest.sessionIDs.get(sessionIDKey);
    getQuestionStatistics(context, sessionID, questionNumber, user, expectedStatus,
        expectedAnswered, expectedCorrect);
  }

  public void getQuestionStatistics(TestContext context, int sessionID, int questionNumber,
      String user, int expectedStatus) {
    getQuestionStatistics(context, sessionID, questionNumber, user, expectedStatus, 0, 0);
  }
  public void getQuestionStatistics(TestContext context, String sessionIDKey, int questionNumber,
      String user, int expectedStatus) {
    int sessionID = CreateSessionTest.sessionIDs.get(sessionIDKey);
    getQuestionStatistics(context, sessionID, questionNumber, user, expectedStatus, 0, 0);
  }
}
