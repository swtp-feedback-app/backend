package de.thm.mni.swtp.feedbackapp.backend.rest.FSuperUserTest;

import de.thm.mni.swtp.feedbackapp.backend.rest.TestBase;
import io.vertx.core.Future;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.json.JsonArray;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import org.junit.Test;

public class AllUsersTest extends TestBase {
  @Test
  public void allUsers(TestContext context) {
    getAllUsers(context, "su", 200, 6);
  }

  @Test
  public void allUsersWithoutSuperuser(TestContext context) {
    getAllUsers(context, "user", 403, 0);
  }

  @Test
  public void allUsersWithoutLogin(TestContext context) {
    getAllUsers(context, null, 401, 0);
  }

  private void getAllUsers(TestContext context, String user, int expectedStatus, int expectedCount) {
    Async async = context.async();
    request("/api/su/users", HttpMethod.GET, user, null).compose(res -> {
      context.assertEquals(expectedStatus, res.statusCode());
      if (res.statusCode() == 200) {
        JsonArray response = res.bodyAsJsonArray();
        context.assertEquals(expectedCount, response.size());
      }
      return Future.succeededFuture();
    }).onSuccess(res -> async.complete()).onFailure(context::fail);
  }
}
