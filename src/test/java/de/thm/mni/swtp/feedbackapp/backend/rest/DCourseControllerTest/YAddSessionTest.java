package de.thm.mni.swtp.feedbackapp.backend.rest.DCourseControllerTest;

import de.thm.mni.swtp.feedbackapp.backend.rest.CSessionControllerTest.CreateSessionTest;
import de.thm.mni.swtp.feedbackapp.backend.rest.TestBase;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import org.junit.Test;

public class YAddSessionTest extends TestBase {

    @Test
    public void addSessionWithEmptyBodyBadRequest(TestContext context) {
        addSession(context, "courseWithNameAndSemester", new JsonObject(), "user4", 400);
    }

    @Test
    public void addSession(TestContext context) {
        JsonObject body = new JsonObject()
            .put("sessionID", CreateSessionTest.sessionIDs.get("user4Session"));
        addSession(context, "courseWithNameAndSemester", body, "user4", 200);
    }

    @Test
    public void addSessionToAllAuthorCourse(TestContext context) {
        JsonObject body = new JsonObject()
            .put("sessionID", CreateSessionTest.sessionIDs.get("userSession"));
        addSession(context, "allAuthorCourse", body, "user", 200);
    }

    @Test
    public void addAnonymSession(TestContext context) {
        JsonObject body = new JsonObject()
            .put("sessionID", CreateSessionTest.sessionIDs.get("anonymSession"));
        addSession(context, "courseWithNameAndSemester", body, "user4", 403);
    }

    @Test
    public void addSessionToNonExistingCourse(TestContext context) {
        JsonObject body = new JsonObject()
            .put("sessionID", CreateSessionTest.sessionIDs.get("user4Session"));
        addSession(context, 10000000, body, "user4", 404);
    }

    @Test
    public void addSessionAsAdmin(TestContext context) {
        JsonObject body = new JsonObject()
            .put("sessionID", CreateSessionTest.sessionIDs.get("user1Session"));
        addSession(context, "courseWithNameAndDescriptionAndSemester", body, "user1", 200);
    }

    @Test
    public void addSessionAsAdminToOtherCourse(TestContext context) {
        JsonObject body = new JsonObject()
            .put("sessionID", CreateSessionTest.sessionIDs.get("user1Session"));
        addSession(context, "courseWithNameAndSemester", body, "user1", 403);
    }

    @Test
    public void addSessionToNonOwnedCourse(TestContext context) {
        JsonObject body = new JsonObject()
            .put("sessionID", CreateSessionTest.sessionIDs.get("userSession"));
        addSession(context, "courseWithNameAndSemester", body, "user", 403);
    }

    @Test
    public void addNonOwnedSession(TestContext context) {
        JsonObject body = new JsonObject()
            .put("sessionID", CreateSessionTest.sessionIDs.get("userSession"));
        addSession(context, "courseWithNameAndSemester", body, "user4", 403);
    }


    private void addSession(TestContext context, int courseID, JsonObject body, String user,
        int expectedStatus) {
        Async async = context.async();
        request("/api/courses/" + courseID + "/sessions", HttpMethod.POST, user, body)
            .onSuccess(res -> {
                context.assertEquals(expectedStatus, res.statusCode());
                if (res.statusCode() == 200) {
                    JsonObject resBody = res.bodyAsJsonObject();
                    JsonObject quiz = resBody.getJsonObject("quiz");
                    context.assertNotNull(resBody.getInteger("id"));
                    context.assertNotNull(resBody.getString("type"));
                    context.assertNotNull(resBody.getLong("start"));
                    context.assertNotNull(resBody.getLong("end"));
                    context.assertNotNull(quiz.getInteger("questionCount"));
                    context.assertEquals("quiz", quiz.getString("type"));
                    context.assertNotNull(quiz.getBoolean("instantFeedback"));
                }
                async.complete();
            }).onFailure(context::fail);
    }

    private void addSession(TestContext context, String courseIDKey, JsonObject body, String user,
        int expectedStatus) {
        int courseID = CreateCourseTest.courseIDs.get(courseIDKey);
        addSession(context, courseID, body, user, expectedStatus);
    }
}
