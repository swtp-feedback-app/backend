package de.thm.mni.swtp.feedbackapp.backend.rest.DCourseControllerTest;

import de.thm.mni.swtp.feedbackapp.backend.rest.TestBase;
import io.vertx.core.Future;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import org.junit.Test;

public class ZMyCourseStatsTest extends TestBase {
  @Test
  public void getMyCourseStats(TestContext context) {
    getMyCourseStats(context, "user", "courseWithNameAndSemester",
        200, 1, 1, 1);
  }

  @Test
  public void getMyCourseStatsWithNonAnswers(TestContext context) {
    getMyCourseStats(context, "user1", "courseWithNameAndDescriptionAndSemester",
        200, 0, 0,0);
  }

  @Test
  public void getMyCourseStatsWithNonParticipant(TestContext context) {
    getMyCourseStats(context, "user3", "courseWithNameAndDescriptionAndSemester",
        403, 0, 0,0);
  }

  @Test
  public void getMyCourseStatsForNonExistingCourse(TestContext context) {
    getMyCourseStats(context, "user", null, 404,
        0, 0,0);
  }


  private void getMyCourseStats(TestContext context, String user, String course, int expectedStatus,
      int expectedSessionCount, int expectedAnsweredQuestion, int expectedCorrectQuestions) {
    Async async = context.async();
    int courseID = 10000000;
    if (course != null) {
      courseID = CreateCourseTest.courseIDs.get(course);
    }
    request("/api/courses/" + courseID + "/stats/user", HttpMethod.GET, user, null).compose(res -> {
      context.assertEquals(expectedStatus, res.statusCode());
      if (res.statusCode() == 200) {
        JsonObject response = res.bodyAsJsonObject();
        context.assertEquals(expectedSessionCount, response.getInteger("sessionCount"));
        context.assertEquals(expectedAnsweredQuestion, response.getInteger("answeredQuestions"));
        context.assertEquals(expectedCorrectQuestions, response.getInteger("correctQuestions"));
      }
      return Future.succeededFuture();
    }).onSuccess(res -> async.complete()).onFailure(context::fail);
  }
}
