package de.thm.mni.swtp.feedbackapp.backend.rest.EUserTest;

import de.thm.mni.swtp.feedbackapp.backend.rest.TestBase;
import io.vertx.core.Future;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import org.junit.Test;

public class MySessionsTest extends TestBase {
  @Test
  public void listSessions(TestContext context) {
    listSessions(context, "user", 200, 1);
  }

  private void listSessions(TestContext context, String user, int expectedStatus, int expectedCount) {
    Async async = context.async();
    request("/api/users/me/sessions", HttpMethod.GET, user, null).compose(res -> {
      context.assertEquals(expectedStatus, res.statusCode());
      if (res.statusCode() == 200) {
        JsonArray body = res.bodyAsJsonArray();
        context.assertEquals(expectedCount, body.size());
        for (int i = 0; i < body.size(); i++) {
          JsonObject session = body.getJsonObject(i);
          context.assertNotNull(session.getInteger("id"));
          context.assertNotNull(session.getString("type"));
          context.assertNotNull(session.getLong("start"));
          context.assertNotNull(session.getLong("end"));
          context.assertNotNull(session.getString("description"));
          context.assertNotNull(session.getJsonObject("quiz"));
          JsonObject course = session.getJsonObject("course");
          if (course != null) {
            context.assertNotNull(course.getString("name"));
            context.assertNotNull(course.getJsonObject("docent"));
            context.assertNotNull(course.getJsonObject("docent").getString("username"));
          }
        }
      }
      return Future.succeededFuture();
    }).onSuccess(res -> async.complete()).onFailure(context::fail);
  }
}
