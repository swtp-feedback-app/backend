package de.thm.mni.swtp.feedbackapp.backend.rest.CSessionControllerTest;

import de.thm.mni.swtp.feedbackapp.backend.rest.AQuizControllerTest.CreateQuizTest;
import de.thm.mni.swtp.feedbackapp.backend.rest.TestBase;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import org.junit.Test;

public class CreateSessionTest extends TestBase {
  private static final long now = new Date().getTime() / 1000;
  private static final long inAHour = now+60*60;
  private static final long inTwoHours = now+60*60*2;
  private static final long aHourAgo = now-60*60;
  private static final long twoHoursAgo = now-60*60*2;

  @Test
  public void createSession(TestContext context) {
    createSession(context, "quizWithoutDescription", "user",
        200, "session");
  }

  @Test
  public void createSessionAsOtherUser(TestContext context) {
    createSession(context, "quizWithoutDescription", "user1", 403);
  }

  @Test
  public void createSessionForNonExistingQuiz(TestContext context) {
    createSession(context, "quizWithoutDescription", "user1", 403);
  }

  @Test
  public void createSessionAgain(TestContext context) {
    createSession(context, "quizWithoutDescription", "user",
        200, "againSession");
  }

  @Test
  public void createNotStartedSession(TestContext context) {
    createSession(context, "quizWithoutDescription", inAHour, inTwoHours, "user",
        200, "nonStartedSession");
  }

  @Test
  public void createEndedSession(TestContext context) {
    createSession(context, "quizWithoutDescription", twoHoursAgo, aHourAgo, "user",
        200, "endedSession");
  }

  @Test
  public void newSession(TestContext context) {
    createSession(context, "quiz", aHourAgo, inAHour, "user",
        200, "newSession");
  }

  @Test
  public void anotherNewSession(TestContext context) {
    createSession(context, "quiz", aHourAgo, inAHour, "user",
        200, "anotherSession");
  }

  @Test
  public void editMeSession(TestContext context) {
    createSession(context, "quizWithoutDescription", twoHoursAgo, aHourAgo, "user",
        200, "editMeSession");
  }

  @Test
  public void anonymSession(TestContext context) {
    createSession(context, "user4Quiz", "user4",
        200, "anonymSession");
  }

  @Test
  public void deleteMeSession(TestContext context) {
    createSession(context, "quizWithoutDescription", "user",
        200, "deleteMeSession");
  }

  public final static Map<String, Integer> sessionIDs = new HashMap<>();

  static void createSession(TestContext context, String quizIDKey, String type, long start, long end, boolean showCorrect,
                            String user, int expectedStatus, String sessionIDKey) {
    Async async = context.async();
    JsonObject body = new JsonObject()
        .put("type", type)
        .put("start", start)
        .put("end", end)
        .put("description", "A new Session")
        .put("showCorrectAnswers", showCorrect);
    int quizID = CreateQuizTest.quizIDs.get(quizIDKey);
    request("/api/quizzes/" + quizID + "/sessions", HttpMethod.POST, user, body).onSuccess(res -> {
      context.assertEquals(expectedStatus, res.statusCode());
      if (res.statusCode() == 200) {
        JsonObject resBody = res.bodyAsJsonObject();
        Integer id = resBody.getInteger("id");
        context.assertNotNull(id);
        context.assertNotNull(resBody.getJsonObject("quiz"));
        context.assertEquals(type, resBody.getString("type"));
        context.assertEquals(start, resBody.getLong("start"));
        context.assertEquals(end, resBody.getLong("end"));
        context.assertEquals("A new Session", resBody.getString("description"));
        sessionIDs.put(sessionIDKey, id);
      }
      async.complete();
    }).onFailure(context::fail);
  }
  static void createSession(TestContext context, String quizIDKey, String type, boolean showCorrect, String user,
                            int expectedStatus, String sessionIDKey) {
    createSession(context, quizIDKey, type, now, inAHour, showCorrect, user, expectedStatus, sessionIDKey);
  }
  private void createSession(TestContext context, String quizIDKey, long start, long end,
      String user, int expectedStatus, String sessionIDKey) {
    createSession(context, quizIDKey, "anonym", start, end, false, user, expectedStatus, sessionIDKey);
  }
  private void createSession(TestContext context, String quizIDKey, String user,
      int expectedStatus, String sessionIDKey) {
    createSession(context, quizIDKey, now, inAHour, user, expectedStatus, sessionIDKey);
  }
  private void createSession(TestContext context, String quizIDKey, String user,
      int expectedStatus) {
    createSession(context, quizIDKey, now, inAHour, user, expectedStatus, "");
  }
}
