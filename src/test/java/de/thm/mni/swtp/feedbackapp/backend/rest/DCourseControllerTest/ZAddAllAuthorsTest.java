package de.thm.mni.swtp.feedbackapp.backend.rest.DCourseControllerTest;

import de.thm.mni.swtp.feedbackapp.backend.rest.TestBase;
import io.vertx.core.http.HttpMethod;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import org.junit.Test;

public class ZAddAllAuthorsTest extends TestBase {
  @Test
  public void addAuthors(TestContext context) {
    addAuthors(context, "allAuthorCourse", "user4", 200);
  }

  @Test
  public void addAuthorsNonDocent(TestContext context) {
    addAuthors(context, "allAuthorCourse", "user", 403);
  }

  @Test
  public void addAuthorsNonExistingCourse(TestContext context) {
    addAuthors(context, 0, "user4", 404);
  }

  private void addAuthors(TestContext context, int courseID, String user, int expectedStatus) {
    Async async = context.async();
    request("/api/courses/" + courseID + "/all/authors", HttpMethod.POST, user, null)
        .onSuccess(res -> {
          context.assertEquals(expectedStatus, res.statusCode());
          async.complete();
        }).onFailure(context::fail);
  }
  private void addAuthors(TestContext context, String courseIDKey, String user, int expectedStatus) {
    int courseID = CreateCourseTest.courseIDs.get(courseIDKey);
    addAuthors(context, courseID, user, expectedStatus);
  }
}
