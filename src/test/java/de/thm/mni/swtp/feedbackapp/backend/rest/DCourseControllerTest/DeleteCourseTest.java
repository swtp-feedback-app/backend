package de.thm.mni.swtp.feedbackapp.backend.rest.DCourseControllerTest;

import de.thm.mni.swtp.feedbackapp.backend.rest.TestBase;
import io.vertx.core.Future;
import io.vertx.core.http.HttpMethod;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import org.junit.Test;

public class DeleteCourseTest extends TestBase {
  @Test
  public void deleteCourse(TestContext context) {
    deleteCourse(context, "deleteMeCourse", "user4", 200);
  }

  @Test
  public void deleteNonExistingCourse(TestContext context) {
    deleteCourse(context, 10000000, "user4", 404);
  }

  @Test
  public void deleteNonOwnedCourse(TestContext context) {
    deleteCourse(context, "courseWithNameAndDescriptionAndSemesterAndPassword", "user3", 403);
  }

  private void deleteCourse(TestContext context, int courseID, String user, int expectedStatus) {
    Async async = context.async();
    request("/api/courses/" + courseID, HttpMethod.DELETE, user, null).compose(res -> {
      context.assertEquals(expectedStatus, res.statusCode());
      return Future.succeededFuture();
    }).onSuccess(res -> async.complete()).onFailure(context::fail);
  }
  private void deleteCourse(TestContext context, String courseIDKey, String user, int expectedStatus) {
    int courseID = CreateCourseTest.courseIDs.get(courseIDKey);
    deleteCourse(context, courseID, user, expectedStatus);
  }
}
