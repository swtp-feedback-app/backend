package de.thm.mni.swtp.feedbackapp.backend.rest.AQuizControllerTest;

import de.thm.mni.swtp.feedbackapp.backend.rest.TestBase;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import org.junit.Test;

public class ListQuizTest extends TestBase {
  @Test
  public void listQuiz(TestContext context) {
    listQuiz(context, "user", new JsonArray()
      .add(CreateQuizTest.quizWithNameAndDescription)
      .add(CreateQuizTest.quizWithName)
      .add(CreateQuizTest.quizWithNameAndDescription)
      .add(CreateQuizTest.surveyQuiz)
      .add(DeleteQuizTest.deleteThisQuiz)
    );
  }
  @Test
  public void listQuizOtherUser(TestContext context) {
    listQuiz(context, "user2", new JsonArray());
  }

  private void listQuiz(TestContext context, String user, JsonArray expected) {
    Async async = context.async();
    request("/api/quizzes", HttpMethod.GET, user, null).onSuccess(res -> {
      context.assertEquals(200, res.statusCode());
      JsonArray body = res.bodyAsJsonArray();
      context.assertEquals(expected.size(), body.size());
      for (int i = 0; i < expected.size(); i++) {
        JsonObject expectedObject = expected.getJsonObject(i);
        JsonObject givenObject = body.getJsonObject(i);
        context.assertNotNull(givenObject.getInteger("id"));
        context.assertEquals(expectedObject.getString("name"), givenObject.getString("name"));
        context.assertEquals(expectedObject.getString("description"), givenObject.getString("description"));
        context.assertEquals(expectedObject.getString("type", "quiz"), givenObject.getString("type"));
        context.assertEquals(expectedObject.getBoolean("instantFeedback", true), givenObject.getBoolean("instantFeedback"));
        context.assertEquals(0, givenObject.getInteger("questionCount"));
      }
      async.complete();
    }).onFailure(context::fail);
  }
}
