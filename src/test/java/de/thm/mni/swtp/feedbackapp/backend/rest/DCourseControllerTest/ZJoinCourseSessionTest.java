package de.thm.mni.swtp.feedbackapp.backend.rest.DCourseControllerTest;

import de.thm.mni.swtp.feedbackapp.backend.rest.CSessionControllerTest.SessionTestBase;
import de.thm.mni.swtp.feedbackapp.backend.rest.CSessionControllerTest.XJoinAuthenticatedSessionTest;
import io.vertx.ext.unit.TestContext;
import org.junit.Test;

public class ZJoinCourseSessionTest extends SessionTestBase {
  @Test
  public void joinSession(TestContext context) {
    XJoinAuthenticatedSessionTest.joinSession(context, "user4Session", "user1", 200, "user1@user4Session");
  }
  @Test
  public void joinSessionWithNonParticpant(TestContext context) {
    XJoinAuthenticatedSessionTest.joinSession(context, "user4Session", "user2", 403, "user2@user4Session");
  }
}
