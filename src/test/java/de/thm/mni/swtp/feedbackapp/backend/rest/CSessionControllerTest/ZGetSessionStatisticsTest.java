package de.thm.mni.swtp.feedbackapp.backend.rest.CSessionControllerTest;

import de.thm.mni.swtp.feedbackapp.backend.rest.TestBase;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import org.junit.Test;

public class ZGetSessionStatisticsTest extends TestBase {

  @Test
  public void getSessionStatistics(TestContext context) {
    getSessionStatistics(context, "session", "user", 200);
  }

  @Test
  public void getSessionStatisticsForNonExistingSession(TestContext context) {
    getSessionStatistics(context, 10000000, "user", 404);
  }

  @Test
  public void getSessionStatisticsForNonOwnedSession(TestContext context) {
    getSessionStatistics(context, "session", "user1", 403);
  }

  public static void getSessionStatistics(TestContext context, int sessionID, String user, int expectedStatus) {
    Async async = context.async();
    request("/api/sessions/" + sessionID + "/statistics", HttpMethod.GET, user, null)
        .onSuccess(res -> {
          context.assertEquals(expectedStatus, res.statusCode());
          if (res.statusCode() == 200) {
            JsonObject body = res.bodyAsJsonObject();
            context.assertNotNull(body.getInteger("participantCount"));
            context.assertNotNull(body.getInteger("completeCount"));
            context.assertNotNull(body.getInteger("allWrong"));
            context.assertNotNull(body.getInteger("zeroPlus"));
            context.assertNotNull(body.getInteger("fortyPlus"));
            context.assertNotNull(body.getInteger("sixtyPlus"));
            context.assertNotNull(body.getInteger("eightyPlus"));
          }
          async.complete();
        }).onFailure(context::fail);
  }
  public static void getSessionStatistics(TestContext context, String sessionIDKey, String user, int expectedStatus) {
    int sessionID = CreateSessionTest.sessionIDs.get(sessionIDKey);
    getSessionStatistics(context, sessionID, user, expectedStatus);
  }
}
