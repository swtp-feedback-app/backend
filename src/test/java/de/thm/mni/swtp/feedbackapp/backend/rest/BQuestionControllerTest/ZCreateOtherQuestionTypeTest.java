package de.thm.mni.swtp.feedbackapp.backend.rest.BQuestionControllerTest;

import de.thm.mni.swtp.feedbackapp.backend.rest.AQuizControllerTest.CreateQuizTest;
import de.thm.mni.swtp.feedbackapp.backend.rest.TestBase;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import java.util.HashMap;
import java.util.Map;
import org.junit.Test;

public class ZCreateOtherQuestionTypeTest extends TestBase {
  static final JsonObject openQuestion = new JsonObject()
      .put("question", "What is OpenApi?");

  static final JsonObject sortQuestion = new JsonObject()
      .put("question", "Sort these numbers")
      .put("answers", new JsonArray()
          .add("1")
          .add("2")
          .add("3")
          .add("4"));

  static final JsonObject pairsQuestion = new JsonObject()
      .put("question", "Match these paris")
      .put("answers", new JsonArray()
          .add(new JsonArray().add("Train").add("Rail"))
          .add(new JsonArray().add("Bus").add("Road"))
          .add(new JsonArray().add("Ship").add("Sea"))
          .add(new JsonArray().add("Plane").add("Sky"))
      );

  static final JsonObject blankQuestion = new JsonObject()
      .put("question", "Fill the blanks")
      .put("answers", new JsonObject()
          .put("text", new JsonArray()
              .add("The OpenAPI Specification, originally known as the ")
              .add(" Specification, is a specification for machine-readable interface files for describing, producing, consuming, and visualizing RESTful web services. It became a separate project in ")
              .add(" overseen by the OpenAPI Initiative, an open-source collaboration project of the ")
              .add("."))
          .put("blanks", new JsonArray()
            .add("Swagger")
            .add("2016")
            .add("Linux Foundation"))
      );

  static final JsonObject wordCloudQuestion = new JsonObject()
      .put("question", "What is OpenApi?").put("answers", new JsonObject().put("max", 10));

  static final JsonObject ratingQuestion = new JsonObject()
      .put("question", "How much do you like this API-Documentation??")
      .put("answers", new JsonObject()
          .put("min", 0)
          .put("max", 5)
      );

  static final JsonObject invalidRatingQuestion = new JsonObject()
      .put("question", "How much do you like this API-Documentation??")
      .put("answers", new JsonObject()
          .putNull("min")
          .putNull("max")
      );

  static final JsonObject invalidWordCloudQuestion = new JsonObject()
      .put("question", "What is OpenApi?").put("answers", new JsonObject().putNull("max"));

  static final JsonObject multipleChoiceWithMultipleCorrectAnswers = new JsonObject()
      .put("question", "What is OpenApi?")
      .put("answers", new JsonObject()
          .put("options", new JsonArray().add(new JsonObject()
              .put("answer", "A").put("correct", true)).add(new JsonObject()
              .put("answer", "B").put("correct", true)).add(new JsonObject()
              .put("answer", "C").put("correct", false)).add(new JsonObject()
              .put("answer", "D").put("correct", false)))
          .put("possibleAnswers", 2));

  @Test
  public void createOpenQuestionTest(TestContext context) {
    createQuestion(context, CreateQuizTest.quizIDs.get("quiz"), "user", "open", openQuestion);
  }

  @Test
  public void createSortQuestionTest(TestContext context) {
    createQuestion(context, CreateQuizTest.quizIDs.get("quiz"), "user", "sort", sortQuestion);
  }

  @Test
  public void creatPairsQuestionTest(TestContext context) {
    createQuestion(context, CreateQuizTest.quizIDs.get("quiz"), "user", "pairs", pairsQuestion);
  }

  @Test
  public void createBlankQuestionTest(TestContext context) {
    createQuestion(context, CreateQuizTest.quizIDs.get("quiz"), "user", "blankText", blankQuestion);
  }

  @Test
  public void createWordCloudQuestionTest(TestContext context) {
    createQuestion(context, CreateQuizTest.quizIDs.get("quiz"), "user", "wordCloud", wordCloudQuestion);
  }

  @Test
  public void createRatingQuestionTest(TestContext context) {
    createQuestion(context, CreateQuizTest.quizIDs.get("quiz"), "user", "rating", ratingQuestion);
  }

  @Test
  public void createMultipleChoiceQuestionWithMultipleCorrectAnswers(TestContext context) {
    createQuestion(context, CreateQuizTest.quizIDs.get("quiz"), "user", "multipleChoice",
        multipleChoiceWithMultipleCorrectAnswers);
  }

  @Test
  public void createRatingQuestionWithIncorrectBodyTest(TestContext context) {
    createQuestion(context, CreateQuizTest.quizIDs.get("quiz"), "user", "rating", openQuestion, 400);
  }

  @Test
  public void creatInvalidRating(TestContext context) {
    createQuestion(context, CreateQuizTest.quizIDs.get("quiz"), "user", "rating", invalidRatingQuestion, 400);
  }

  @Test
  public void creatInvalidWordCloud(TestContext context) {
    createQuestion(context, CreateQuizTest.quizIDs.get("quiz"), "user", "wordCloud", invalidWordCloudQuestion, 400);
  }

  public final static Map<String, Integer> questionIDs = new HashMap<>();
  private void createQuestion(TestContext context, int quizID, String user, String type,
      JsonObject question, int status) {
    Async async = context.async();
    JsonObject body = new JsonObject()
        .put("type", type)
        .put("question", question);
    request("/api/quizzes/" + quizID + "/questions", HttpMethod.POST, user, body).onSuccess(res -> {
      context.assertEquals(status, res.statusCode());
      if (res.statusCode() == 200) {
        JsonObject resBody = res.bodyAsJsonObject();
        questionIDs.put(type, resBody.getInteger("number"));
      }
      async.complete();
    }).onFailure(context::fail);
  }

  private void createQuestion(TestContext context, int quizID, String user, String type,
      JsonObject question) {
    createQuestion(context, quizID, user, type, question, 200);
  }
}
