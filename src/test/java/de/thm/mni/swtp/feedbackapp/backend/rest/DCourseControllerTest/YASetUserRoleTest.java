package de.thm.mni.swtp.feedbackapp.backend.rest.DCourseControllerTest;

import de.thm.mni.swtp.feedbackapp.backend.rest.TestBase;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import org.junit.Test;

public class YASetUserRoleTest extends TestBase {
  @Test
  public void addAdminToCourse(TestContext context) {
    addAdmin(context, "courseWithNameAndDescriptionAndSemester", "user4", "user1", 200);
  }

  @Test
  public void addOtherAdminToCourse(TestContext context) {
    addAdmin(context, "courseWithNameAndDescriptionAndSemester", "user4", "user2", 200);
  }

  @Test
  public void addAdminToNonExistingCourse(TestContext context) {
    addAdmin(context, 10000000, "user", "user4", 404);
  }

  @Test
  public void addAdminToNonOwnedCourse(TestContext context) {
    addAdmin(context, "courseWithNameAndDescriptionAndSemester", "user3", "user1", 403);
  }

  @Test
  public void addAdminAgainToCourse(TestContext context) {
    addAdmin(context, "courseWithNameAndDescriptionAndSemester", "user4", "user1", 200);
  }

  private void addAdmin(TestContext context, int courseID, String user, String newAdmin,
      int expectedStatus) {
    Async async = context.async();
    JsonObject requestBody = new JsonObject().put("role", "admin");
    request("/api/courses/" + courseID + "/users/" + newAdmin, HttpMethod.PUT, user, requestBody)
        .onSuccess(res -> {
          context.assertEquals(expectedStatus, res.statusCode());
          async.complete();
        }).onFailure(context::fail);
  }

  private void addAdmin(TestContext context, String courseIDKey, String user, String newAdmin,
      int expectedStatus) {
    int courseID = CreateCourseTest.courseIDs.get(courseIDKey);
    addAdmin(context, courseID, user, newAdmin, expectedStatus);
  }
}
