package de.thm.mni.swtp.feedbackapp.backend.rest.CSessionControllerTest;

import de.thm.mni.swtp.feedbackapp.backend.rest.AQuizControllerTest.CreateQuizTest;
import de.thm.mni.swtp.feedbackapp.backend.rest.TestBase;
import io.vertx.core.Future;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import org.junit.Test;

public class ListQuizSessionsTest extends TestBase {
  @Test
  public void listSessions(TestContext context) {
    listSessions(context, CreateQuizTest.quizIDs.get("quizWithoutDescription"), "user", 200);
  }

  @Test
  public void listSessionsForNonExistingQuiz(TestContext context) {
    listSessions(context, 10000000, "user", 404);
  }

  @Test
  public void listSessionsForNonOwnedQuiz(TestContext context) {
    listSessions(context, CreateQuizTest.quizIDs.get("quizWithoutDescription"), "user1", 403);
  }

  private void listSessions(TestContext context, int quizID, String user, int expectedStatus) {
    Async async = context.async();
    request("/api/quizzes/" + quizID + "/sessions", HttpMethod.GET, user, null).compose(res -> {
      context.assertEquals(expectedStatus, res.statusCode());
      if (res.statusCode() == 200) {
        JsonArray body = res.bodyAsJsonArray();
        context.assertEquals(5, body.size());
        for (int i = 0; i < body.size(); i++) {
          JsonObject session = body.getJsonObject(i);
          context.assertNotNull(session.getString("type"));
          context.assertNotNull(session.getJsonObject("quiz"));
          context.assertNotNull(session.getLong("start"));
          context.assertNotNull(session.getLong("end"));
          context.assertNull(session.getJsonObject("course"));
          context.assertEquals("A new Session", session.getString("description"));
          context.assertEquals(false, session.getBoolean("showCorrectAnswers"));
        }
      }
      return Future.succeededFuture();
    }).onSuccess(res -> async.complete()).onFailure(context::fail);
  }
}
