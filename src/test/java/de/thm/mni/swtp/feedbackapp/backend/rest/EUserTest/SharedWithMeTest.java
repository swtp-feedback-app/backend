package de.thm.mni.swtp.feedbackapp.backend.rest.EUserTest;

import de.thm.mni.swtp.feedbackapp.backend.rest.TestBase;
import io.vertx.core.Future;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import org.junit.Test;

public class SharedWithMeTest extends TestBase {
  @Test
  public void sharedWithMe(TestContext context) {
    sharedWithMe(context, "user4", 200, 1);
  }

  private void sharedWithMe(TestContext context, String user, int expectedStatus, int expectedCount) {
    Async async = context.async();
    request("/api/users/me/sharedWithMe", HttpMethod.GET, user, null).compose(res -> {
      context.assertEquals(expectedStatus, res.statusCode());
      if (res.statusCode() == 200) {
        JsonArray body = res.bodyAsJsonArray();
        context.assertEquals(expectedCount, body.size());
      }
      return Future.succeededFuture();
    }).onSuccess(res -> async.complete()).onFailure(context::fail);
  }
}
