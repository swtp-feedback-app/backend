package de.thm.mni.swtp.feedbackapp.backend.rest.BQuestionControllerTest;

import de.thm.mni.swtp.feedbackapp.backend.rest.AQuizControllerTest.CreateQuizTest;
import de.thm.mni.swtp.feedbackapp.backend.rest.TestBase;
import io.vertx.core.Future;
import io.vertx.core.http.HttpMethod;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import org.junit.Test;

public class GetQuestionTest extends TestBase {
  @Test
  public void getQuestion(TestContext context) {
    getQuestion(context, CreateQuizTest.quizIDs.get("quizWithoutDescription"), 1,"user", 200);
  }

  @Test
  public void getQuestionFromNonExistingQuiz(TestContext context) {
    getQuestion(context, 10000000, 1, "user", 404);
  }
  @Test
  public void getNonExistingQuestion(TestContext context) {
    getQuestion(context, CreateQuizTest.quizIDs.get("quizWithoutDescription"), 4,"user", 404);
  }


  @Test
  public void getQuestionOfOtherUser(TestContext context) {
    getQuestion(context, CreateQuizTest.quizIDs.get("quizWithoutDescription"), 1,"user2", 403);
  }

  private void getQuestion(TestContext context, int quizID, int questionNumber, String user, int expectedStatus) {
    Async async = context.async();
    request("/api/quizzes/" + quizID + "/questions/" + questionNumber, HttpMethod.GET, user, null).compose(res -> {
      context.assertEquals(expectedStatus, res.statusCode());
      return Future.succeededFuture();
    }).onSuccess(res -> async.complete()).onFailure(context::fail);
  }
}
