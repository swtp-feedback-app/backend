package de.thm.mni.swtp.feedbackapp.backend.rest.AQuizControllerTest;

import de.thm.mni.swtp.feedbackapp.backend.rest.TestBase;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import org.junit.Test;

public class GetQuizTest extends TestBase {
  @Test
  public void getExistingQuiz(TestContext context) {
    getQuiz(context, CreateQuizTest.quizIDs.get("quiz"), "user", 200, CreateQuizTest.quizWithNameAndDescription);
  }
  @Test
  public void getNonExistingQuiz(TestContext context) {
    getQuiz(context, 1000000, "user", 404, null);
  }
  @Test
  public void getNonOwnedQuiz(TestContext context) {
    getQuiz(context, CreateQuizTest.quizIDs.get("quiz"), "user2", 403, null);
  }

  private void getQuiz(TestContext context, int quizID, String user, int expectedStatus, JsonObject expectedBody) {
    Async async = context.async();
    request("/api/quizzes/" + quizID, HttpMethod.GET, user, null).onSuccess(res -> {
      context.assertEquals(expectedStatus, res.statusCode());
      JsonObject resBody = res.bodyAsJsonObject();
      if (expectedBody != null) {
        context.assertNotNull(resBody.getInteger("id"));
        context.assertEquals(expectedBody.getString("name"), resBody.getString("name"));
        context.assertEquals(expectedBody.getString("description"), resBody.getString("description"));
        context.assertEquals(expectedBody.getString("name"), resBody.getString("name"));
        context.assertEquals(expectedBody.getString("description"), resBody.getString("description"));
        context.assertEquals(expectedBody.getString("type", "quiz"), resBody.getString("type"));
        context.assertEquals(expectedBody.getBoolean("instantFeedback", true), resBody.getBoolean("instantFeedback"));
        context.assertEquals(0, resBody.getInteger("questionCount"));
      }
      async.complete();
    }).onFailure(context::fail);
  }
}
