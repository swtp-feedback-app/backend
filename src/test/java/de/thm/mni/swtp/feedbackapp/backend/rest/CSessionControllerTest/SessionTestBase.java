package de.thm.mni.swtp.feedbackapp.backend.rest.CSessionControllerTest;

import de.thm.mni.swtp.feedbackapp.backend.rest.TestBase;
import io.vertx.core.Future;
import io.vertx.core.Promise;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.http.HttpMethod;
import io.vertx.ext.web.client.HttpRequest;
import io.vertx.ext.web.client.HttpResponse;

public abstract class SessionTestBase extends TestBase {
  protected static Future<HttpResponse<Buffer>> request(String path, HttpMethod method,
      String sessionTokenKey, Object body) {
    HttpRequest<Buffer> req = client.request(method, 8080, "localhost", path);
    if (sessionTokenKey != null) {
      String sessionToken = JoinSessionTest.sessionTokens.get(sessionTokenKey);
      if (sessionToken == null) {
        throw new IllegalArgumentException("SessionToken \"" + sessionTokenKey + "\" not found");
      }
      req.putHeader("Authorization", "Bearer " + sessionToken);
    }
    Promise<HttpResponse<Buffer>> resPromise = Promise.promise();
    if (body != null) {
      req.putHeader("Content-Type", "application/json")
          .sendJson(body, resPromise);
    } else {
      req.send(resPromise);
    }
    return resPromise.future();
  }
}
