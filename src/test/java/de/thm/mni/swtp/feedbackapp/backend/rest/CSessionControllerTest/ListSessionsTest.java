package de.thm.mni.swtp.feedbackapp.backend.rest.CSessionControllerTest;

import de.thm.mni.swtp.feedbackapp.backend.rest.TestBase;
import io.netty.handler.codec.http.QueryStringEncoder;
import io.vertx.core.Future;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import org.junit.Test;

public class ListSessionsTest extends TestBase {
  @Test
  public void listSessions(TestContext context) {
    listSessions(context, "user", false, false, 200, 7);
  }

  @Test
  public void listHideEnded(TestContext context) {
    listSessions(context, "user", true, false, 200, 5);
  }

  @Test
  public void listHideNonStarted(TestContext context) {
    listSessions(context, "user", false, true, 200, 6);
  }

  @Test
  public void listHideEndedAndNonStarted(TestContext context) {
    listSessions(context, "user", true, true, 200,  4);
  }

  private void listSessions(TestContext context, String user, boolean hideEnded, boolean hideNonStarted,
      int expectedStatus, int expectedSessionCount) {
    Async async = context.async();
    QueryStringEncoder queryStringEncoder = new QueryStringEncoder("/api/sessions");
    if (hideEnded) {
      queryStringEncoder.addParam("hideEnded", "true");
    }
    if (hideNonStarted) {
      queryStringEncoder.addParam("hideNonStarted", "true");
    }
    request(queryStringEncoder.toString(), HttpMethod.GET, user, null).compose(res -> {
      context.assertEquals(expectedStatus, res.statusCode());
      if (res.statusCode() == 200) {
        JsonArray body = res.bodyAsJsonArray();
        context.assertEquals(expectedSessionCount, body.size());
        for (int i = 0; i < body.size(); i++) {
          JsonObject session = body.getJsonObject(i);
          context.assertNotNull(session.getString("type"));
          context.assertNotNull(session.getJsonObject("quiz"));
          context.assertNotNull(session.getLong("start"));
          context.assertNotNull(session.getLong("end"));
          context.assertNull(session.getJsonObject("course"));
          context.assertEquals("A new Session", session.getString("description"));
          context.assertEquals(false, session.getBoolean("showCorrectAnswers"));
        }
      }
      return Future.succeededFuture();
    }).onSuccess(res -> async.complete()).onFailure(context::fail);
  }
}
