package de.thm.mni.swtp.feedbackapp.backend.rest.CSessionControllerTest;

import io.vertx.core.http.HttpMethod;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import org.junit.Test;

public class YGetQuestionsTest extends SessionTestBase {
    @Test
    public void getQuestions(TestContext context){
        getQuestions(context, "session", "sessionToken", 200, 3);
    }

  @Test
  public void getQuestionsForOtherSession(TestContext context){
    getQuestions(context, "againSession", "againSessionToken", 200, 3);
  }

  @Test
  public void getQuestionsForNonExistingSession(TestContext context){
    getQuestions(context, 10000000, "sessionToken", 404, 0);
  }

  private void getQuestions(TestContext context, int sessionID, String sessionTokenKey, int expectedStatus,
      int expectedCount) {
    Async async = context.async();
    request("/api/sessions/" + sessionID + "/questions", HttpMethod.GET, sessionTokenKey, null).onSuccess(res -> {
      context.assertEquals(expectedStatus, res.statusCode());
      if (res.statusCode() == 200) {
        JsonArray resBody = res.bodyAsJsonArray();
        context.assertEquals(expectedCount, resBody.size());
        for (int i = 0; i < resBody.size(); i++) {
          JsonObject question = resBody.getJsonObject(i);
          context.assertNotNull(question.getInteger("number"));
          context.assertNotNull(question.getString("type"));
          context.assertNotNull(question.getJsonObject("question"));
        }
      }
      async.complete();
    }).onFailure(context::fail);
    }

  private void getQuestions(TestContext context, String sessionKey, String sessionTokenKey, int expectedStatus,
      int expectedCount) {
      int sessionID = CreateSessionTest.sessionIDs.get(sessionKey);
    getQuestions(context, sessionID, sessionTokenKey, expectedStatus, expectedCount);
  }
}
