package de.thm.mni.swtp.feedbackapp.backend.rest.CSessionControllerTest;

import de.thm.mni.swtp.feedbackapp.backend.rest.TestBase;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import java.util.Date;
import org.junit.Test;

public class UpdateSessionTest extends TestBase {
  private static final long now = new Date().getTime() / 1000;
  private static final long inAHour = now+60*60;

  @Test
  public void updateSessionTest(TestContext context) {
    updateSession(context, CreateSessionTest.sessionIDs.get("editMeSession"), "user", now, inAHour, 200);
  }

  @Test
  public void updateSessionTestNonOwnedSession(TestContext context) {
    updateSession(context, CreateSessionTest.sessionIDs.get("editMeSession"), "user1", now, inAHour, 403);
  }

  @Test
  public void updateSessionTestNonExistingSession(TestContext context) {
    updateSession(context, 10000000, "user", now, inAHour, 404);
  }

  private void updateSession(TestContext context, int sessionID, String user,
      long newStart, long newEnd, int expectedStatus) {
    Async async = context.async();
    JsonObject body = new JsonObject().put("start", newStart).put("end", newEnd).put("description", "A edited Session");
    request("/api/sessions/" + sessionID, HttpMethod.PUT,
        user, body).onSuccess(res -> {
      context.assertEquals(expectedStatus, res.statusCode());
      if (res.statusCode() == 200) {
        JsonObject resBody = res.bodyAsJsonObject();
        context.assertEquals(sessionID, resBody.getInteger("id"));
        context.assertNotNull(resBody.getString("type"));
        context.assertEquals(newStart, resBody.getLong("start"));
        context.assertEquals(newEnd, resBody.getLong("end"));
        context.assertEquals("A edited Session", resBody.getString("description"));
      }
      async.complete();
    }).onFailure(context::fail);
  }

}
