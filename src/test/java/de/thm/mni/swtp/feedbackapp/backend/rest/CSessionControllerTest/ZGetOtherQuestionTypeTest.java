package de.thm.mni.swtp.feedbackapp.backend.rest.CSessionControllerTest;

import de.thm.mni.swtp.feedbackapp.backend.rest.BQuestionControllerTest.ZCreateOtherQuestionTypeTest;
import io.vertx.core.http.HttpMethod;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import org.junit.Test;

public class ZGetOtherQuestionTypeTest extends SessionTestBase {
  @Test
  public void getOpenQuestionTest(TestContext context) {
    getQuestion(context, "anotherSession", "open",
        "anotherSessionToken", 200);
  }

  @Test
  public void getSortQuestionTest(TestContext context) {
    getQuestion(context, "anotherSession", "sort",
        "anotherSessionToken", 200);
  }

  @Test
  public void getPairsQuestionTest(TestContext context) {
    getQuestion(context, "anotherSession", "pairs",
        "anotherSessionToken", 200);
  }

  @Test
  public void getBlankQuestionTest(TestContext context) {
    getQuestion(context, "anotherSession", "blankText",
        "anotherSessionToken", 200);
  }

  @Test
  public void getWordCloudQuestionTest(TestContext context) {
    getQuestion(context, "anotherSession", "wordCloud",
        "anotherSessionToken", 200);
  }

  @Test
  public void getRatingQuestion(TestContext context) {
    getQuestion(context, "anotherSession", "rating",
        "anotherSessionToken", 200);
  }

  private void getQuestion(TestContext context, String sessionIDKey, String questionNumberKey,
      String sessionTokenKey, int expectedStatus) {
    Async async = context.async();
    int sessionID = CreateSessionTest.sessionIDs.get(sessionIDKey);
    int questionNumber = ZCreateOtherQuestionTypeTest.questionIDs.get(questionNumberKey);
    request("/api/sessions/" + sessionID + "/questions/" + questionNumber, HttpMethod.GET,
        sessionTokenKey, null).onSuccess(res -> {
      context.assertEquals(expectedStatus, res.statusCode());
      async.complete();
    }).onFailure(context::fail);
  }
}
