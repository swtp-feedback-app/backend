package de.thm.mni.swtp.feedbackapp.backend.rest.CSessionControllerTest;

import io.vertx.core.http.HttpMethod;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import org.junit.Test;

public class SubmitAnswerTest extends SessionTestBase {
    final JsonArray answer = new JsonArray().add("A");
    final JsonArray wrongAnswer = new JsonArray().add("B");

    @Test
    public void submitAnswer(TestContext context) {
        submitAnswer(context, CreateSessionTest.sessionIDs.get("session"), 1, answer,
            "sessionToken", 200, true);
    }

    @Test
    public void submitAnswerAgain(TestContext context) {
        submitAnswer(context, CreateSessionTest.sessionIDs.get("session"), 1, answer,
            "sessionToken", 200, true);
    }

    @Test
    public void submitAnswerForOtherQuestion(TestContext context) {
        submitAnswer(context, CreateSessionTest.sessionIDs.get("session"), 2, answer,
            "sessionToken", 200, true);
    }

    @Test
    public void submitWrongAnswer(TestContext context) {
        submitAnswer(context, CreateSessionTest.sessionIDs.get("session"), 3, wrongAnswer,
            "sessionToken", 200, false);
    }

    @Test
    public void submitAnswerAsAnotherUser(TestContext context) {
        submitAnswer(context, CreateSessionTest.sessionIDs.get("session"), 1, answer,
            "sessionTokenAgain", 200, true);
    }

    @Test
    public void submitAnswerWithoutAnswer(TestContext context) {
        submitAnswer(context, CreateSessionTest.sessionIDs.get("session"), 1, null,
            "sessionToken", 400, false);
    }

    @Test
    public void submitAnswerForNonExistingSession(TestContext context) {
        submitAnswer(context, 10000000, 1, answer,
            "sessionToken", 404, false);
    }

    @Test
    public void submitAnswerForNonExistingQuestion(TestContext context) {
        submitAnswer(context, CreateSessionTest.sessionIDs.get("againSession"), 4, answer,
            "againSessionToken", 404, false);
    }

    @Test
    public void submitAnswerWithoutSessionToken(TestContext context) {
        submitAnswer(context, CreateSessionTest.sessionIDs.get("session"), 1, answer,
            null, 401, false);
    }

    @Test
    public void submitAnswerWithWrongSessionToken(TestContext context) {
        submitAnswer(context, CreateSessionTest.sessionIDs.get("session"), 1, answer,
            "againSessionToken", 403, false);
    }

    static void submitAnswer(TestContext context, int sessionID, int questionNumber, JsonArray answer,
        String sessionTokenKey, int expectedStatus, boolean expectedCorrect) {
        Async async = context.async();
        request("/api/sessions/" + sessionID + "/questions/" + questionNumber, HttpMethod.POST,
            sessionTokenKey, answer).onSuccess(res -> {
                context.assertEquals(expectedStatus, res.statusCode());
                if (res.statusCode() == 200) {
                    JsonObject body = res.bodyAsJsonObject();
                    context.assertEquals(expectedCorrect, body.getBoolean("correct"));
                }
                async.complete();
            }).onFailure(context::fail);
    }
}
