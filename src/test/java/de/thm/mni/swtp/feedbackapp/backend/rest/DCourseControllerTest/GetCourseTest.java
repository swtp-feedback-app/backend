package de.thm.mni.swtp.feedbackapp.backend.rest.DCourseControllerTest;

import de.thm.mni.swtp.feedbackapp.backend.rest.TestBase;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import org.junit.Test;

public class GetCourseTest extends TestBase {
  @Test
  public void getExistingCourseWithOnlyNameAndSemester(TestContext context) {
    int id = CreateCourseTest.courseIDs.get("courseWithNameAndSemester");
    JsonObject expectedCourse = CreateCourseTest.courseWithNameAndSemester.copy()
        .put("id", id).put("role", "none");
    getCourse(context, id, "user", 200, expectedCourse);
  }

  @Test
  public void getExistingCourse(TestContext context) {
    int id = CreateCourseTest.courseIDs.get("courseWithNameAndDescriptionAndSemester");
    JsonObject expectedCourse = CreateCourseTest.courseWithNameAndDescriptionAndSemester.copy()
        .put("id", id).put("role", "none");
    getCourse(context, id, "user", 200, expectedCourse);
  }

  @Test
  public void getCourseWithPassword(TestContext context) {
    int id = CreateCourseTest.courseIDs.get("courseWithNameAndDescriptionAndSemesterAndPassword");
    JsonObject expectedCourse = CreateCourseTest.courseWithNameAndDescriptionAndSemesterAndPassword.copy();
    expectedCourse.remove("password");
    expectedCourse.put("id", id).put("protected", true).put("role", "none");
    getCourse(context, id, "user", 200, expectedCourse);
  }

  @Test
  public void getNonExistingCourse(TestContext context) {
    getCourse(context, 10000000, "user", 404, null);
  }

  @Test
  public void getNonOwnedCourse(TestContext context) {
    int id = CreateCourseTest.courseIDs.get("courseWithNameAndDescriptionAndSemester");
    JsonObject expectedCourse = CreateCourseTest.courseWithNameAndDescriptionAndSemester.copy()
        .put("id", id).put("role", "none");
    getCourse(context, id, "user1", 200, expectedCourse);
  }

  @Test
  public void getOwnedCourse(TestContext context) {
    int id = CreateCourseTest.courseIDs.get("courseWithNameAndDescriptionAndSemester");
    JsonObject expectedCourse = CreateCourseTest.courseWithNameAndDescriptionAndSemester.copy()
        .put("id", id).put("role", "lecturer");
    getCourse(context, id, "user4", 200, expectedCourse);
  }

  @Test
  public void getAllAuthorCourse(TestContext context) {
    int id = CreateCourseTest.courseIDs.get("allAuthorCourse");
    JsonObject expectedCourse = CreateCourseTest.allAuthorCourse.copy().put("id", id).put("role", "lecturer");
    getCourse(context, id, "user4", 200, expectedCourse);
  }

  private void getCourse(TestContext context, int courseID, String user, int expectedStatus, JsonObject expectedBody) {
    Async async = context.async();
    request("/api/courses/" + courseID, HttpMethod.GET, user, null).onSuccess(res -> {
      context.assertEquals(expectedStatus, res.statusCode());
      JsonObject body = res.bodyAsJsonObject();
      if (expectedBody != null) {
        context.assertEquals(expectedBody.getInteger("id"), body.getInteger("id"));
        context.assertEquals(expectedBody.getString("name"), body.getString("name"));
        context.assertEquals(expectedBody.getString("description", ""), body.getString("description"));
        context.assertEquals(expectedBody.getBoolean("protected", false), body.getBoolean("protected"));
        context.assertEquals(expectedBody.getBoolean("allAuthor", false), body.getBoolean("allAuthor"));
        context.assertEquals(expectedBody.getString("role"), body.getString("role"));
        JsonObject docent = body.getJsonObject("docent");
        context.assertNotNull(docent);
        context.assertEquals(true, docent.getBoolean("docent"));
      }
      async.complete();
    }).onFailure(context::fail);
  }
}
