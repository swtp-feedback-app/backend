package de.thm.mni.swtp.feedbackapp.backend.rest.EUserTest;

import de.thm.mni.swtp.feedbackapp.backend.rest.TestBase;
import io.netty.handler.codec.http.QueryStringEncoder;
import io.vertx.core.Future;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import org.junit.Test;

import java.util.List;

public class MyCoursesTest extends TestBase {
  @Test
  public void listCourses(TestContext context) {
    listCourses(context, "participant", "user", 200, 3);
  }

  @Test
  public void listAuthorCourses(TestContext context) {
    listCourses(context, "author", "user", 200, 1);
  }

  @Test
  public void listCoursesOtherUser(TestContext context) {
    listCourses(context, "participant", "user1", 200, 1);
  }


  @Test
  public void listCoursesForUserWithoutJoinedCourses(TestContext context) {
    listCourses(context, "participant", "user2", 200, 0);
  }

  @Test
  public void listCoursesAll(TestContext context) {
    listCourses(context, List.of(), "user1", 200, 2);
  }

  @Test
  public void listAdminCourses(TestContext context) {
    listCourses(context, "admin", "user1", 200, 0);
  }

  @Test
  public void listOwnerCoursesWithoutOwningCourses(TestContext context) {
    listCourses(context, "lecturer", "user3", 200, 0);
  }

  @Test
  public void listOwnerCourses(TestContext context) {
    listCourses(context, "lecturer", "user4", 200, 4);
  }

  @Test
  public void listAdminOrOwnerCourses(TestContext context) {
    listCourses(context, List.of("admin", "lecturer"), "user4", 200, 4);
  }

  private void listCourses(TestContext context, List<String> roles, String user, int expectedStatus, int expectedCount) {
    Async async = context.async();
    QueryStringEncoder queryStringEncoder = new QueryStringEncoder("/api/users/me/courses");
    for (String role: roles) {
      queryStringEncoder.addParam("role", role);
    }
    request(queryStringEncoder.toString(), HttpMethod.GET, user, null).compose(res -> {
      context.assertEquals(expectedStatus, res.statusCode());
      if (res.statusCode() == 200) {
        JsonArray resBody = res.bodyAsJsonArray();
        context.assertEquals(expectedCount, resBody.size());
        for (int i = 0; i < resBody.size(); i++) {
          JsonObject course = resBody.getJsonObject(i);
          context.assertNotNull(course.getString("name"));
          context.assertNotNull(course.getString("semester"));
          context.assertNotNull(course.getBoolean("protected"));
          context.assertNotNull(course.getBoolean("allAuthor"));
          String role = course.getString("role");
          context.assertNotNull(role);
          if (!roles.isEmpty()) {
            context.assertTrue(roles.contains(role));
          }
          JsonObject docent = course.getJsonObject("docent");
          context.assertNotNull(docent);
          context.assertNotNull(docent.getString("username"));
          context.assertEquals(true, docent.getBoolean("docent"));
        }
      }
      return Future.succeededFuture();
    }).onSuccess(res -> async.complete()).onFailure(context::fail);
  }

  private void listCourses(TestContext context, String role, String user, int expectedStatus, int expectedCount) {
    listCourses(context, List.of(role), user, expectedStatus, expectedCount);
  }
}
