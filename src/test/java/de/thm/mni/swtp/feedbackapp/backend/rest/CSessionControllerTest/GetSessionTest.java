package de.thm.mni.swtp.feedbackapp.backend.rest.CSessionControllerTest;

import de.thm.mni.swtp.feedbackapp.backend.rest.TestBase;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import org.junit.Test;

public class GetSessionTest extends TestBase {
  @Test
  public void getSession(TestContext context) {
    getSession(context, CreateSessionTest.sessionIDs.get("session"), "user", 200);
  }

  @Test
  public void getSessionWithOtherUser(TestContext context) {
    getSession(context, CreateSessionTest.sessionIDs.get("session"), "user1", 200);
  }

  @Test
  public void getNonExistingSession(TestContext context) {
    getSession(context, 10000000, "user", 404);
  }

  private void getSession(TestContext context, int sessionID, String user, int expectedStatus) {
    Async async = context.async();
    request("/api/sessions/" + sessionID, HttpMethod.GET, user, null).onSuccess(res -> {
      context.assertEquals(expectedStatus, res.statusCode());
      if (res.statusCode() == 200) {
        JsonObject body = res.bodyAsJsonObject();
        JsonObject quiz = body.getJsonObject("quiz");
        context.assertEquals(sessionID, body.getInteger("id"));
        context.assertNotNull(body.getString("type"));
        context.assertNotNull(body.getLong("start"));
        context.assertNotNull(body.getLong("end"));
        context.assertEquals("A new Session", body.getString("description"));
        context.assertEquals(false, body.getBoolean("showCorrectAnswers"));
        context.assertEquals(3, quiz.getInteger("questionCount"));
        context.assertEquals("quiz", quiz.getString("type"));
        context.assertNotNull(quiz.getBoolean("instantFeedback"));
        context.assertNull(body.getJsonObject("course"));
        JsonObject creator = body.getJsonObject("creator");
        context.assertNotNull(creator);
        context.assertNotNull(creator.getString("username"));
        context.assertNotNull(creator.getInteger("id"));
        context.assertNotNull(creator.getBoolean("docent"));
      }
      async.complete();
    }).onFailure(context::fail);
  }
}
