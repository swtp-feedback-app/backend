package de.thm.mni.swtp.feedbackapp.backend.rest.CSessionControllerTest;

import de.thm.mni.swtp.feedbackapp.backend.rest.TestBase;
import io.vertx.core.json.JsonArray;
import io.vertx.ext.unit.TestContext;
import org.junit.Test;

public class XSubmitAnswerAuthenticatedSessionTest extends TestBase {
  @Test
  public void submitAnswer(TestContext context) {
    submitAnswer(context, CreateSessionTest.sessionIDs.get("user4Session"),
        1, new JsonArray().add("A"), "user@user4Session",
        200, true);
  }

  private void submitAnswer(
      TestContext context, int sessionID, int questionNumber, JsonArray answer,
      String sessionTokenName, int expectedStatus, boolean expectedCorrect) {
    SubmitAnswerTest.submitAnswer(context, sessionID, questionNumber, answer, sessionTokenName,
        expectedStatus, expectedCorrect);
  }
}
