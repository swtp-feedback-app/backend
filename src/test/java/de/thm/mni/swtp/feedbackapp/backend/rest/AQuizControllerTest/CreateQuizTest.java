package de.thm.mni.swtp.feedbackapp.backend.rest.AQuizControllerTest;

import de.thm.mni.swtp.feedbackapp.backend.rest.TestBase;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import java.util.HashMap;
import java.util.Map;
import org.junit.Test;

public class CreateQuizTest extends TestBase {
  @Test
  public void createQuizWithEmptyBodyBadRequest(TestContext context) {
    createQuiz(context, new JsonObject(), 400);
  }

  @Test
  public void createQuizWithMissingName(TestContext context) {
    JsonObject body = new JsonObject()
        .put("description", "A Quiz");
    createQuiz(context, body, 400);
  }

  static final JsonObject quizWithName = new JsonObject()
        .put("name", "quiz");

  @Test
  public void createQuizWithMissingDescription(TestContext context) {
    createQuiz(context, quizWithName, 200, "quizWithoutDescription");
  }

  static final JsonObject quizWithNameAndDescription = new JsonObject()
      .put("name", "quiz")
      .put("description", "A Quiz");

  @Test
  public void createQuiz(TestContext context) {
    createQuiz(context, quizWithNameAndDescription, 200, "quiz");
  }

  static final JsonObject surveyQuiz = quizWithNameAndDescription.copy().put("type", "survey");

  @Test
  public void createSurvey(TestContext context) {
    createQuiz(context, surveyQuiz, 200, "survey");
  }

  @Test
  public void createQuizWithInvalidType(TestContext context) {
    createQuiz(context, quizWithNameAndDescription.copy().put("type", "foo"), 400);
  }

  @Test
  public void createQuizForUser4(TestContext context) {
    createQuiz(context, quizWithNameAndDescription, "user4", 200, "user4Quiz");
  }

  @Test
  public void createQuizForUser1(TestContext context) {
    createQuiz(context, quizWithNameAndDescription, "user1", 200, "user1Quiz");
  }

  @Test
  public void createQuizForSharing(TestContext context) {
    createQuiz(context, quizWithNameAndDescription, 200, "shareQuiz");
  }

  public final static Map<String, Integer> quizIDs = new HashMap<>();

  private void createQuiz(TestContext context, JsonObject body, String user, int expectedStatus,
      String quizIDKey) {
    Async async = context.async();
    request("/api/quizzes", HttpMethod.POST, user, body).onSuccess(res -> {
      context.assertEquals(expectedStatus, res.statusCode());
      if (res.statusCode() == 200) {
        JsonObject resBody = res.bodyAsJsonObject();
        context.assertNotNull(resBody.getInteger("id"));
        context.assertEquals(body.getString("name"), resBody.getString("name"));
        context.assertEquals(body.getString("description"), resBody.getString("description"));
        context.assertEquals(body.getString("type", "quiz"), resBody.getString("type"));
        context.assertEquals(body.getBoolean("instantFeedback", true), resBody.getBoolean("instantFeedback"));
        context.assertEquals(0, resBody.getInteger("questionCount"));
        quizIDs.put(quizIDKey, resBody.getInteger("id"));
      }
      async.complete();
    }).onFailure(context::fail);
  }

  private void createQuiz(TestContext context, JsonObject body, int expectedStatus, String quizIDKey) {
    createQuiz(context, body, "user", expectedStatus, quizIDKey);
  }
  private void createQuiz(TestContext context, JsonObject body, int expectedStatus) {
    createQuiz(context, body, expectedStatus, "");
  }
}
