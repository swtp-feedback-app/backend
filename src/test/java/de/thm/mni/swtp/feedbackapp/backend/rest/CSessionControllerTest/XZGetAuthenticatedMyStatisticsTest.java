package de.thm.mni.swtp.feedbackapp.backend.rest.CSessionControllerTest;

import de.thm.mni.swtp.feedbackapp.backend.rest.TestBase;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import org.junit.Test;

public class XZGetAuthenticatedMyStatisticsTest extends TestBase {
  @Test
  public void getMyStatistics(TestContext context) {
    getMyStatistics(context, CreateSessionTest.sessionIDs.get("user4Session"),
        "user", 200, 1, 1, 1);
  }

  @Test
  public void getMyStatisticsForNonExistingSesion(TestContext context) {
    getMyStatistics(context, 10000000,
        "user", 404, 0, 0, 0);
  }

  @Test
  public void getMyStatisticsWithoutAuthorization(TestContext context) {
    getMyStatistics(context, CreateSessionTest.sessionIDs.get("user4Session"),
        null, 401, 0, 0, 0);
  }

  private void getMyStatistics(TestContext context, int sessionID, String username,
      int expectedStatus, int expectedQuestionCount, int expectedAnsweredCount,
      int expectedCorrectCount) {
    Async async = context.async();
    request("/api/sessions/" + sessionID + "/myStatistics/user", HttpMethod.GET, username,
        null).onSuccess(res -> {
      context.assertEquals(expectedStatus, res.statusCode());
      if (res.statusCode() == 200) {
        JsonObject body = res.bodyAsJsonObject();
        context.assertEquals(expectedQuestionCount, body.getInteger("questionCount"));
        context.assertEquals(expectedAnsweredCount, body.getInteger("answeredQuestions"));
        context.assertEquals(expectedCorrectCount, body.getInteger("correctQuestions"));
      }
      async.complete();
    }).onFailure(context::fail);
  }

}
