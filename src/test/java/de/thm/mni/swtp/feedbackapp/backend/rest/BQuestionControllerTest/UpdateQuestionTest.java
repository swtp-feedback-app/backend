package de.thm.mni.swtp.feedbackapp.backend.rest.BQuestionControllerTest;

import de.thm.mni.swtp.feedbackapp.backend.rest.AQuizControllerTest.CreateQuizTest;
import de.thm.mni.swtp.feedbackapp.backend.rest.TestBase;
import io.vertx.core.Future;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import org.junit.Test;

public class UpdateQuestionTest extends TestBase {
  @Test
  public void updateQuestion(TestContext context) {
    updateQuestion(context, CreateQuizTest.quizIDs.get("quizWithoutDescription"), 1, "user",
        CreateQuestionTest.questionWithTypeAndQuestion, 200);
  }

  @Test
  public void updateQuestionFromNonExistingQuiz(TestContext context) {
    updateQuestion(context, 10000000, 1, "user",
        CreateQuestionTest.questionWithTypeAndQuestion, 404);
  }

  @Test
  public void updateNonExistingQuestion(TestContext context) {
    updateQuestion(context, CreateQuizTest.quizIDs.get("quizWithoutDescription"), 4, "user",
        CreateQuestionTest.questionWithTypeAndQuestion, 404);
  }

  @Test
  public void updateQuestionOfOtherUser(TestContext context) {
    updateQuestion(context, CreateQuizTest.quizIDs.get("quizWithoutDescription"), 1, "user2",
        CreateQuestionTest.questionWithTypeAndQuestion, 403);
  }

  private void updateQuestion(TestContext context, int quizID, int questionNumber, String user, JsonObject updated, int expectedStatus) {
    Async async = context.async();
    request("/api/quizzes/" + quizID + "/questions/" + questionNumber, HttpMethod.PUT, user, updated).compose(res -> {
      context.assertEquals(expectedStatus, res.statusCode());
      if (res.statusCode() == 200) {
        JsonObject resBody = res.bodyAsJsonObject();
        context.assertNotNull(resBody.getInteger("number"));
      }
      return Future.succeededFuture();
    }).onSuccess(res -> async.complete()).onFailure(context::fail);
  }
}
