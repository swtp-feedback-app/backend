package de.thm.mni.swtp.feedbackapp.backend.rest.BQuestionControllerTest;

import de.thm.mni.swtp.feedbackapp.backend.rest.AQuizControllerTest.CreateQuizTest;
import de.thm.mni.swtp.feedbackapp.backend.rest.TestBase;
import io.vertx.core.Future;
import io.vertx.core.http.HttpMethod;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import org.junit.Test;

public class DeleteQuestionTest extends TestBase {
  @Test
  public void deleteQuestion(TestContext context) {
    deleteQuestion(context, CreateQuizTest.quizIDs.get("quizWithoutDescription"), "user", 200);
  }

  @Test
  public void deleteQuestionWithOtherUser(TestContext context) {
    deleteQuestion(context, CreateQuizTest.quizIDs.get("quizWithoutDescription"), "user1", 403);
  }

  private void deleteQuestion(TestContext context, int quizID, String deletingUser, int expectedStatus) {
    Async async = context.async();
    request("/api/quizzes/" + quizID + "/questions", HttpMethod.POST, "user",
        CreateQuestionTest.questionWithTypeAndQuestion).compose(createRes -> {
      context.assertEquals(200, createRes.statusCode());
      return request("/api/quizzes/" + quizID + "/questions/" + 3, HttpMethod.DELETE, deletingUser, null).compose(res -> {
        context.assertEquals(expectedStatus, res.statusCode());
        return Future.succeededFuture();
      });
    }).onSuccess(res -> async.complete()).onFailure(context::fail);
  }
}
