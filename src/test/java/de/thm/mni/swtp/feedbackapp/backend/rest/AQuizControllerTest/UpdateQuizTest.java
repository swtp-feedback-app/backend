package de.thm.mni.swtp.feedbackapp.backend.rest.AQuizControllerTest;

import de.thm.mni.swtp.feedbackapp.backend.rest.TestBase;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import org.junit.Test;

public class UpdateQuizTest extends TestBase {
  @Test
  public void updateQuizSuccessful(TestContext context) {
    updateQuiz(context, CreateQuizTest.quizIDs.get("quizWithoutDescription"), "user", CreateQuizTest.quizWithNameAndDescription, 200);
  }
  @Test
  public void updateNonExistingQuiz(TestContext context) {
    updateQuiz(context, 1000000, "user", CreateQuizTest.quizWithNameAndDescription, 404);
  }
  @Test
  public void updateNonOwnedQuiz(TestContext context) {
    updateQuiz(context, CreateQuizTest.quizIDs.get("quizWithoutDescription"), "user1", CreateQuizTest.quizWithNameAndDescription, 403);
  }

  private void updateQuiz(TestContext context, int quizID, String user, JsonObject updateQuiz, int expectedStatus) {
    Async async = context.async();
    request("/api/quizzes/" + quizID, HttpMethod.PUT, user, updateQuiz).onSuccess(res -> {
      context.assertEquals(expectedStatus, res.statusCode());
      if (res.statusCode() == 200) {
        JsonObject resBody = res.bodyAsJsonObject();
        context.assertNotNull(resBody.getInteger("id"));
        context.assertEquals(updateQuiz.getString("name"), resBody.getString("name"));
        context.assertEquals(updateQuiz.getString("description"), resBody.getString("description"));
        context.assertEquals(updateQuiz.getString("name"), resBody.getString("name"));
        context.assertEquals(updateQuiz.getString("description"), resBody.getString("description"));
        context.assertEquals(updateQuiz.getString("type", "quiz"), resBody.getString("type"));
        context.assertEquals(updateQuiz.getBoolean("instantFeedback", true), resBody.getBoolean("instantFeedback"));
        context.assertEquals(0, resBody.getInteger("questionCount"));
      }
      async.complete();
    }).onFailure(context::fail);
  }
}
