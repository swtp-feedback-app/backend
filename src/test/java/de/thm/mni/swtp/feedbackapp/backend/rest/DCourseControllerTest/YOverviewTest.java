package de.thm.mni.swtp.feedbackapp.backend.rest.DCourseControllerTest;

import de.thm.mni.swtp.feedbackapp.backend.rest.TestBase;
import io.vertx.core.Future;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import org.junit.Assert;
import org.junit.Test;

public class YOverviewTest extends TestBase {
  @Test
  public void overview(TestContext context) {
    getOverview(context, "user4", "courseWithNameAndSemester", 200);
  }

  @Test
  public void overviewFromNonExistingCourse(TestContext context) {
    getOverview(context, "user4", null, 404);
  }

  @Test
  public void overviewFromNonOwnedCourse(TestContext context) {
    getOverview(context, "user", "courseWithNameAndSemester", 403);
  }

  @Test
  public void overviewForAdminCourse(TestContext context) {
    getOverview(context, "user1", "courseWithNameAndDescriptionAndSemester", 403);
  }

  private void getOverview(TestContext context, String user, String course, int expectedStatus) {
    Async async = context.async();
    int courseID = 10000000;
    if (course != null) {
      courseID = CreateCourseTest.courseIDs.get(course);
    }
    request("/api/courses/" + courseID + "/stats/overview", HttpMethod.GET, user, null).compose(res -> {
      context.assertEquals(expectedStatus, res.statusCode());
      if (res.statusCode() == 200) {
        JsonArray response = res.bodyAsJsonArray();
        for (int i = 0; i < response.size(); i++) {
          JsonObject content = response.getJsonObject(i);
          JsonObject session = content.getJsonObject("session");
          Assert.assertNotNull(session);
          Assert.assertNotNull(session.getInteger("id"));
          Assert.assertNotNull(session.getString("description"));
          JsonObject quiz = session.getJsonObject("quiz");
          Assert.assertNotNull(quiz);
          Assert.assertNotNull(quiz.getInteger("id"));
          Assert.assertNotNull(quiz.getString("name"));
          Assert.assertNotNull(content.getJsonObject("session"));
          Assert.assertEquals(1, content.getJsonObject("students").size());
        }
      }
      return Future.succeededFuture();
    }).onSuccess(res -> async.complete()).onFailure(context::fail);
  }
}
