package de.thm.mni.swtp.feedbackapp.backend.rest.DCourseControllerTest;

import de.thm.mni.swtp.feedbackapp.backend.rest.CSessionControllerTest.CreateSessionTest;
import de.thm.mni.swtp.feedbackapp.backend.rest.TestBase;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import org.junit.Assert;
import org.junit.Test;

public class ZSessionCorrectTest extends TestBase {
  @Test
  public void getCorrect(TestContext context) {
    getCorrect(context, "user4Session", 1, "user", 200, true, new JsonArray().add("A"));
  }

  @Test
  public void getCorrectQuestion(TestContext context) {
    getCorrect(context, "user4Session", 2, "user", 404, null, null);
  }

  @Test
  public void getCorrectForNonExistingSession(TestContext context) {
    getCorrect(context, 10000000, 1, "user", 404, null, null);
  }

  @Test
  public void getCorrectWithoutAnswer(TestContext context) {
    getCorrect(context, "user4Session", 1, "user1", 200, false, new JsonArray());
  }

  @Test
  public void getCorrectOnNotEnabledSession(TestContext context) {
    getCorrect(context, "session", 1, "user", 403, false, new JsonArray());
  }

  private void getCorrect(TestContext context, int sessionID, int questionNumber, String user,
                          int expectedStatus, Boolean expectedCorrect, JsonArray expectedAnswer) {
    Async async = context.async();
    request("/api/sessions/" + sessionID + "/correct/" + questionNumber, HttpMethod.GET, user,
        null).onSuccess(res -> {
      context.assertEquals(expectedStatus, res.statusCode());
      if (res.statusCode() == 200) {
        JsonObject body = res.bodyAsJsonObject();
        context.assertEquals(expectedCorrect, body.getBoolean("correct"));
        context.assertEquals(expectedAnswer, body.getJsonArray("answer"));
      }
      async.complete();
    }).onFailure(context::fail);
  }

  private void getCorrect(TestContext context, String sessionIDKey, int questionNumber, String user,
                               int expectedStatus, Boolean expectedCorrect, JsonArray expectedAnswer) {
    int sessionID = CreateSessionTest.sessionIDs.get(sessionIDKey);
    getCorrect(context, sessionID, questionNumber, user, expectedStatus, expectedCorrect, expectedAnswer);
  }
}
