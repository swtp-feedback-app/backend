package de.thm.mni.swtp.feedbackapp.backend.rest.AQuizControllerTest;

import de.thm.mni.swtp.feedbackapp.backend.rest.TestBase;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class QuizShareTest extends TestBase {
  @Test
  public void share(TestContext context) {
    createQuiz(context, "user4", "user", 200, "shareQuiz");
  }

  @Test
  public void shareAgain(TestContext context) {
    createQuiz(context, "user4", "user", 409, "shareQuiz");
  }

  @Test
  public void shareRe(TestContext context) {
    createQuiz(context, "user2", "user4", 403, "shareQuiz");
  }

  private void createQuiz(TestContext context, String shareWithUser, String user, int expectedStatus,
                          String quizIDKey) {
    Async async = context.async();
    int quizID = CreateQuizTest.quizIDs.get(quizIDKey);
    JsonObject body = new JsonObject().put("username", shareWithUser);
    request("/api/quizzes/" + quizID + "/share", HttpMethod.POST, user, body).onSuccess(res -> {
      context.assertEquals(expectedStatus, res.statusCode());
      async.complete();
    }).onFailure(context::fail);
  }
}
