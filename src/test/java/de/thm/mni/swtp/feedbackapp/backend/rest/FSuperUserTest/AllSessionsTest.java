package de.thm.mni.swtp.feedbackapp.backend.rest.FSuperUserTest;

import de.thm.mni.swtp.feedbackapp.backend.rest.TestBase;
import io.vertx.core.Future;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.json.JsonArray;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import org.junit.Test;

public class AllSessionsTest extends TestBase {
  @Test
  public void allSessions(TestContext context) {
    getAllSessions(context, "su", 200, 11);
  }

  @Test
  public void allSessionsWithoutSuperuser(TestContext context) {
    getAllSessions(context, "user", 403, 0);
  }

  @Test
  public void allSessionsWithoutLogin(TestContext context) {
    getAllSessions(context, null, 401, 0);
  }

  private void getAllSessions(TestContext context, String user, int expectedStatus, int expectedCount) {
    Async async = context.async();
    request("/api/su/sessions", HttpMethod.GET, user, null).compose(res -> {
      context.assertEquals(expectedStatus, res.statusCode());
      if (res.statusCode() == 200) {
        JsonArray response = res.bodyAsJsonArray();
        context.assertEquals(expectedCount, response.size());
      }
      return Future.succeededFuture();
    }).onSuccess(res -> async.complete()).onFailure(context::fail);
  }
}
