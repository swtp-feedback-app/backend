package de.thm.mni.swtp.feedbackapp.backend.rest.CSessionControllerTest;

import de.thm.mni.swtp.feedbackapp.backend.rest.TestBase;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import java.util.HashMap;
import java.util.Map;
import org.junit.Test;

public class JoinSessionTest extends TestBase {
  @Test
  public void joinSession(TestContext context) {
    joinSession(context, "session", 200, "sessionToken");
  }

  @Test
  public void joinSessionAgain(TestContext context) {
    joinSession(context, "session", 200, "sessionTokenAgain");
  }

  @Test
  public void joinAgainSession(TestContext context) {
    joinSession(context, "againSession", 200, "againSessionToken");
  }

  @Test
  public void joinNonExistingSession(TestContext context) {
    joinSession(context, 10000000, null,404, "");
  }

  @Test
  public void joinNonStartedSession(TestContext context) {
    joinSession(context, "nonStartedSession", 403);
  }

  @Test
  public void joinEndedSession(TestContext context) {
    joinSession(context, "endedSession", 403);
  }

  @Test
  public void joinOtherSession(TestContext context) {
    joinSession(context, "newSession", 200, "newSessionToken");
  }

  @Test
  public void joinYetAnotherSession(TestContext context) {
    joinSession(context, "anotherSession", 200, "anotherSessionToken");
  }

  public final static Map<String, String> sessionTokens = new HashMap<>();

  static void joinSession(TestContext context, int sessionID, String asUser, int expectedStatus,
      String sessionTokenKey) {
    Async async = context.async();
    JsonObject body = new JsonObject();
    if (asUser == null) {
      body.put("anonym", true);
    }
    request("/api/sessions/" + sessionID + "/join", HttpMethod.POST, asUser, body).onSuccess(res -> {
      context.assertEquals(expectedStatus, res.statusCode());
      if (res.statusCode() == 200) {
        JsonObject resBody = res.bodyAsJsonObject();
        String token = resBody.getString("token");
        context.assertNotNull(token);
        sessionTokens.put(sessionTokenKey, token);
      }
      async.complete();
    }).onFailure(context::fail);
  }

  static void joinSession(TestContext context, String sessionIDKey, int expectedStatus,
      String sessionTokenKey) {
    int sessionID = CreateSessionTest.sessionIDs.get(sessionIDKey);
    joinSession(context, sessionID, null, expectedStatus, sessionTokenKey);
  }

  static void joinSession(TestContext context, String sessionIDKey, int expectedStatus) {
    joinSession(context, sessionIDKey, expectedStatus, "");
  }
}
