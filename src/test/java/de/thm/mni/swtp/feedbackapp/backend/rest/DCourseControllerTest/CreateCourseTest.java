package de.thm.mni.swtp.feedbackapp.backend.rest.DCourseControllerTest;

import de.thm.mni.swtp.feedbackapp.backend.rest.TestBase;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import java.util.HashMap;
import java.util.Map;
import org.junit.Test;

public class CreateCourseTest extends TestBase {
  static final JsonObject courseWithName = new JsonObject().put("name", "Course");
  static final JsonObject courseWithSemester = new JsonObject().put("semester", "sose2020");
  static final JsonObject courseWithNameAndSemester = new JsonObject()
      .mergeIn(courseWithName).mergeIn(courseWithSemester);
  static final JsonObject courseWithDescription = new JsonObject().put("description", "A Course");
  static final JsonObject courseWithPassword = new JsonObject().put("password", 10000000);
  static final JsonObject courseWithNameAndDescriptionAndSemester = new JsonObject()
      .mergeIn(courseWithName).mergeIn(courseWithDescription).mergeIn(courseWithSemester);
  static final JsonObject courseWithNameAndDescriptionAndSemesterAndPassword = new JsonObject()
      .mergeIn(courseWithNameAndDescriptionAndSemester).mergeIn(courseWithPassword);
  static final JsonObject allAuthorCourse = new JsonObject().mergeIn(courseWithNameAndSemester).put("allAuthor", true);
  static final JsonObject courseDeleteMe = new JsonObject().put("name", "Delete me").mergeIn(courseWithSemester);

  @Test
  public void createWithName(TestContext context) {
    createCourse(context, courseWithName, 400);
  }

  @Test
  public void createWithSemester(TestContext context) {
    createCourse(context, courseWithSemester, 400);
  }

  @Test
  public void createWithNameAndSemester(TestContext context) {
    createCourse(context, courseWithNameAndSemester, 200,
        "courseWithNameAndSemester");
  }

  @Test
  public void createWithDescription(TestContext context) {
    createCourse(context, courseWithDescription, 400);
  }

  @Test
  public void createWithPassword(TestContext context) {
    createCourse(context, courseWithPassword, 400);
  }

  @Test
  public void createAsNonDocent(TestContext context) {
    createCourse(context, courseWithNameAndDescriptionAndSemester, "user", 403, "");
  }

  @Test
  public void createWithNameAndDescriptionAndSemester(TestContext context) {
    createCourse(context, courseWithNameAndDescriptionAndSemester, 200,
        "courseWithNameAndDescriptionAndSemester");
  }

  @Test
  public void createWithNameAndDescriptionAndPassword(TestContext context) {
    createCourse(context, courseWithNameAndDescriptionAndSemesterAndPassword, 200,
        "courseWithNameAndDescriptionAndSemesterAndPassword");
  }

  @Test
  public void creatDeleteMe(TestContext context) {
    createCourse(context, courseDeleteMe, 200, "deleteMeCourse");
  }

  @Test
  public void createAllAuthorCourse(TestContext context) {
    createCourse(context, allAuthorCourse, 200, "allAuthorCourse");
  }

  public final static Map<String, Integer> courseIDs = new HashMap<>();

  private void createCourse(TestContext context, JsonObject body, String user, int expectedStatus,
      String courseIDKey) {
    Async async = context.async();
    request("/api/courses", HttpMethod.POST, user, body).onSuccess(res -> {
      context.assertEquals(expectedStatus, res.statusCode());
      if (res.statusCode() == 200) {
        JsonObject resBody = res.bodyAsJsonObject();
        int id = resBody.getInteger("id");
        context.assertNotNull(id);
        context.assertEquals(body.getString("name"), resBody.getString("name"));
        context.assertEquals(body.getString("semester"), resBody.getString("semester"));
        context.assertEquals(body.getString("description", ""), resBody.getString("description"));
        context.assertEquals(body.getInteger("password") != null, resBody.getBoolean("protected"));
        courseIDs.put(courseIDKey, id);
      }
      async.complete();
    }).onFailure(context::fail);
  }
  private void createCourse(TestContext context, JsonObject body, int expectedStatus, String courseIDKey) {
    createCourse(context, body, "user4", expectedStatus, courseIDKey);
  }
  private void createCourse(TestContext context, JsonObject body, int expectedStatus) {
    createCourse(context, body, "user4", expectedStatus, "");
  }
}
