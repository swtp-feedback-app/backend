package de.thm.mni.swtp.feedbackapp.backend.rest.FSuperUserTest;

import de.thm.mni.swtp.feedbackapp.backend.rest.TestBase;
import io.vertx.core.Future;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.json.JsonArray;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import org.junit.Test;

public class AllCoursesTest extends TestBase {
  @Test
  public void allCourses(TestContext context) {
    getAllCourses(context, "su", 200, 4);
  }

  @Test
  public void allCoursesWithoutSuperuser(TestContext context) {
    getAllCourses(context, "user", 403, 0);
  }

  @Test
  public void allCoursesWithoutLogin(TestContext context) {
    getAllCourses(context, null, 401, 0);
  }

  private void getAllCourses(TestContext context, String user, int expectedStatus, int expectedCount) {
    Async async = context.async();
    request("/api/su/courses", HttpMethod.GET, user, null).compose(res -> {
      context.assertEquals(expectedStatus, res.statusCode());
      if (res.statusCode() == 200) {
        JsonArray response = res.bodyAsJsonArray();
        context.assertEquals(expectedCount, response.size());
      }
      return Future.succeededFuture();
    }).onSuccess(res -> async.complete()).onFailure(context::fail);
  }
}
