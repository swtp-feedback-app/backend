package de.thm.mni.swtp.feedbackapp.backend.rest.CSessionControllerTest;

import de.thm.mni.swtp.feedbackapp.backend.rest.TestBase;
import io.vertx.ext.unit.TestContext;
import org.junit.Test;

public class XJoinAuthenticatedSessionTest extends TestBase {
  @Test
  public void joinSession(TestContext context) {
    joinSession(context, "user4Session", "user", 200, "user@user4Session");
  }

  @Test
  public void joinEmptySession(TestContext context) {
    joinSession(context, "user1Session", "user", 409, "user@user1Session");
  }

  public static void joinSession(TestContext context, String sessionID, String asUser, int expectedStatus,
      String hashKey) {
    JoinSessionTest.joinSession(context, CreateSessionTest.sessionIDs.get(sessionID),
        asUser, expectedStatus, hashKey);
  }
}
