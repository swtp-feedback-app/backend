package de.thm.mni.swtp.feedbackapp.backend.rest.CSessionControllerTest;

import de.thm.mni.swtp.feedbackapp.backend.rest.BQuestionControllerTest.ZCreateOtherQuestionTypeTest;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import org.junit.Test;

public class ZSubmitOtherQuestionTypeTest extends SessionTestBase {
  private static final JsonArray openAnswer = new JsonArray().add("An API definition language");

  private static final JsonArray correctSortAnswer = new JsonArray().add("1").add("2").add("3").add("4");

  private static final JsonArray correctPairsAnswer = new JsonArray()
      .add(new JsonArray().add("Train").add("Rail"))
      .add(new JsonArray().add("Bus").add("Road"))
      .add(new JsonArray().add("Ship").add("Sea"))
      .add(new JsonArray().add("Plane").add("Sky"));

  private static final JsonArray correctBlankAnswer = new JsonArray()
      .add("Swagger").add("2016").add("Linux Foundation");

  private static final JsonArray correctRatingAnswer = new JsonArray()
      .add(5);

  private static final JsonArray correctMultipleMultipleChoiceAnswer = new JsonArray().add("A").add("B");

  private static final JsonArray wrongSortAnswer = new JsonArray();

  private static final JsonArray wrongPairsAnswer = new JsonArray()
      .add(new JsonArray().add("Train").add("Road"))
      .add(new JsonArray().add("Bus").add("Rail"))
      .add(new JsonArray().add("Ship").add("Sea"))
      .add(new JsonArray().add("Plane").add("Sky"));

  private static final JsonArray wrongBlankAnswer = new JsonArray()
      .add("Swagger").add("2016").add("Windows Foundation");

  private static final JsonArray wrongRatingAnswer = new JsonArray().add(6);

  private static final JsonArray wrongMultipleMultipleChoiceAnswer = new JsonArray().add("A");

  private static final JsonArray wordCloudAnswer = new JsonArray().add("api").add("rest").add("schema");

  @Test
  public void submitOpenAnswerTest(TestContext context) {
    submitAnswer(context, "newSession", ZCreateOtherQuestionTypeTest.questionIDs.get("open"),
        openAnswer, "newSessionToken", 200, true);
  }

  @Test
  public void submitSortAnswerTest(TestContext context) {
    submitAnswer(context, "newSession", ZCreateOtherQuestionTypeTest.questionIDs.get("sort"),
        correctSortAnswer, "newSessionToken", 200, true);
  }

  @Test
  public void submitPairsAnswerTest(TestContext context) {
    submitAnswer(context, "newSession", ZCreateOtherQuestionTypeTest.questionIDs.get("pairs"),
        correctPairsAnswer, "newSessionToken", 200, true);
  }

  @Test
  public void submitBlankAnswerTest(TestContext context) {
    submitAnswer(context, "newSession", ZCreateOtherQuestionTypeTest.questionIDs.get("blankText"),
        correctBlankAnswer, "newSessionToken", 200,true);
  }

  @Test
  public void submitRatingAnswerTest(TestContext context) {
    submitAnswer(context, "newSession", ZCreateOtherQuestionTypeTest.questionIDs.get("rating"),
        correctRatingAnswer, "newSessionToken", 200,true);
  }

  @Test
  public void submitMultipleChoiceQuestionWithMultipleCorrectAnswersTest(TestContext context) {
    submitAnswer(context, "newSession", ZCreateOtherQuestionTypeTest.questionIDs.get("multipleChoice"),
        correctMultipleMultipleChoiceAnswer, "newSessionToken", 200,true);
  }

  @Test
  public void submitWrongSortAnswerTest(TestContext context) {
    submitAnswer(context, "anotherSession", ZCreateOtherQuestionTypeTest.questionIDs.get("sort"),
        wrongSortAnswer, "anotherSessionToken", 200, false);
  }

  @Test
  public void submitWrongPairsAnswerTest(TestContext context) {
    submitAnswer(context, "anotherSession", ZCreateOtherQuestionTypeTest.questionIDs.get("pairs"),
        wrongPairsAnswer, "anotherSessionToken", 200, false);
  }

  @Test
  public void submitWrongBlankAnswerTest(TestContext context) {
    submitAnswer(context, "anotherSession", ZCreateOtherQuestionTypeTest.questionIDs.get("blankText"),
        wrongBlankAnswer, "anotherSessionToken", 200, false);
  }

  @Test
  public void submitWrongRatingAnswerTest(TestContext context) {
    submitAnswer(context, "anotherSession", ZCreateOtherQuestionTypeTest.questionIDs.get("rating"),
        wrongRatingAnswer, "anotherSessionToken", 200,false);
  }

  @Test
  public void submitWrongMultipleChoiceQuestionWithMultipleCorrectAnswersTest(TestContext context) {
    submitAnswer(context, "anotherSession", ZCreateOtherQuestionTypeTest.questionIDs.get("multipleChoice"),
        wrongMultipleMultipleChoiceAnswer, "anotherSessionToken", 200,false);
  }

  @Test
  public void submitWordCloudAnswerTest(TestContext context) {
    submitAnswer(context,"anotherSession", ZCreateOtherQuestionTypeTest.questionIDs.get("wordCloud"),
        wordCloudAnswer, "anotherSessionToken", 200, true);
  }

  @Test
  public void submitWrongWordCloudAnswerTest(TestContext context) {
    submitAnswer(context, "anotherSession", ZCreateOtherQuestionTypeTest.questionIDs.get("wordCloud"),
        new JsonArray().add("").add("").add("").add(""), "anotherSessionToken", 200, false);
  }

  @Test
  public void submitWrongOpenAnswerTest(TestContext context) {
    submitAnswer(context, "anotherSession", ZCreateOtherQuestionTypeTest.questionIDs.get("open"),
        new JsonArray().add("        "), "anotherSessionToken", 200, false);
  }

  @Test
  public void submitBadWordOpenAnswerTest(TestContext context) {
    submitAnswer(context, "anotherSession", ZCreateOtherQuestionTypeTest.questionIDs.get("open"),
        new JsonArray().add("arschloch"), "anotherSessionToken", 403, true);
  }

  @Test
  public void submitBadWordWordCloudAnswerTest(TestContext context) {
    submitAnswer(context, "newSession", ZCreateOtherQuestionTypeTest.questionIDs.get("wordCloud"),
        new JsonArray().add("anus"), "newSessionToken", 403, true);
  }

  private void submitAnswer(TestContext context, String sessionIDKey, int questionNumber, JsonArray answer,
      String sessionTokenKey, int expectedStatus, boolean expectedCorrect) {
    Async async = context.async();
    int sessionID = CreateSessionTest.sessionIDs.get(sessionIDKey);
    request("/api/sessions/" + sessionID + "/questions/" + questionNumber, HttpMethod.POST,
        sessionTokenKey, answer).onSuccess(res -> {
      context.assertEquals(expectedStatus, res.statusCode());
      if (res.statusCode() == 200) {
        JsonObject body = res.bodyAsJsonObject();
        context.assertEquals(expectedCorrect, body.getBoolean("correct"));
      }
      async.complete();
    }).onFailure(context::fail);
  }
}
