package de.thm.mni.swtp.feedbackapp.backend.rest.AQuizControllerTest;

import de.thm.mni.swtp.feedbackapp.backend.rest.TestBase;
import io.vertx.core.Future;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import org.junit.Test;

public class DeleteQuizTest extends TestBase {
  @Test
  public void deleteQuiz(TestContext context) {
    deleteQuiz(context, "user", 200);
  }

  @Test
  public void deleteQuizWithOtherUser(TestContext context) {
    deleteQuiz(context, "user1", 403);
  }

  final static JsonObject deleteThisQuiz = new JsonObject().put("name", "Delete this");
  private void deleteQuiz(TestContext context, String deletingUser, int expectedStatus) {
    Async async = context.async();
    request("/api/quizzes", HttpMethod.POST, "user", deleteThisQuiz).compose(createRes -> {
      context.assertEquals(200, createRes.statusCode());
      int id = createRes.bodyAsJsonObject().getInteger("id");
      return request("/api/quizzes/" + id, HttpMethod.DELETE, deletingUser, null).compose(res -> {
        context.assertEquals(expectedStatus, res.statusCode());
        return Future.succeededFuture();
      });
    }).onSuccess(res -> async.complete()).onFailure(context::fail);
  }
}
