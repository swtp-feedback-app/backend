package de.thm.mni.swtp.feedbackapp.backend.rest.DCourseControllerTest;

import de.thm.mni.swtp.feedbackapp.backend.rest.TestBase;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import org.junit.Test;

public class ZRSetUserRoleTest extends TestBase {
  @Test
  public void removeAdmin(TestContext context) {
    removeAdmin(context, "courseWithNameAndDescriptionAndSemester", "user1", "user4", 200);
  }

  @Test
  public void removeAdminFromNonExistingCourse(TestContext context) {
    removeAdmin(context, 10000000, "user1", "user4", 404);
  }

  @Test
  public void removeAdminFromNonOwnedCourse(TestContext context) {
    removeAdmin(context, "courseWithNameAndDescriptionAndSemester", "user1", "user3", 403);
  }

  private void removeAdmin(TestContext context, int courseID, String username, String user, int expectedStatus) {
    Async async = context.async();
    JsonObject requestBody = new JsonObject().put("role", "none");
    request("/api/courses/" + courseID + "/users/" + username, HttpMethod.PUT, user, requestBody).onSuccess(res -> {
      context.assertEquals(expectedStatus, res.statusCode());
      async.complete();
    }).onFailure(context::fail);
  }
  private void removeAdmin(TestContext context, String courseIDKey, String username, String user, int expectedStatus) {
    int courseID = CreateCourseTest.courseIDs.get(courseIDKey);
    removeAdmin(context, courseID, username, user, expectedStatus);
  }
}
