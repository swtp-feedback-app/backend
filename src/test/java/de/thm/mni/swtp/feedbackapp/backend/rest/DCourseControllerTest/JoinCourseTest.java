package de.thm.mni.swtp.feedbackapp.backend.rest.DCourseControllerTest;

import de.thm.mni.swtp.feedbackapp.backend.rest.TestBase;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class JoinCourseTest extends TestBase {

  @Test
  public void joinCourse(TestContext context) {
    joinCourse(context, "courseWithNameAndDescriptionAndSemester", null, "user", 200);
  }

  @Test
  public void joinCourseWithUserOne(TestContext context) {
    joinCourse(context, "courseWithNameAndDescriptionAndSemester", null, "user1", 200);
  }

  @Test
  public void joinAnother(TestContext context) {
    joinCourse(context, "courseWithNameAndSemester", null, "user", 200);
  }

  @Test
  public void joinAnotherCourseWithUserOne(TestContext context) {
    joinCourse(context, "courseWithNameAndSemester", null, "user1", 200);
  }

  @Test
  public void joinPasswordCourseWithPassword(TestContext context) {
    joinCourse(context, "courseWithNameAndDescriptionAndSemesterAndPassword",
        CreateCourseTest.courseWithPassword.getInteger("password"),
        "user", 200);
  }

  @Test
  public void joinAllAuthorCourse(TestContext context) {
    joinCourse(context, "allAuthorCourse", null, "user", 200);
  }

  @Test
  public void joinPasswordCourseWithWrongPassword(TestContext context) {
    joinCourse(context, "courseWithNameAndDescriptionAndSemesterAndPassword", 0, "user", 403);
  }

  @Test
  public void joinPasswordCourseWithoutPassword(TestContext context) {
    joinCourse(context, "courseWithNameAndDescriptionAndSemesterAndPassword", null, "user", 401);
  }

  @Test
  public void joinNonExistingSession(TestContext context) {
    joinCourse(context, 10000000, null, "user", 404);
  }

  @Test
  public void zJoinCourseAgain(TestContext context) {
    joinCourse(context, "courseWithNameAndDescriptionAndSemester", null, "user", 409);
  }

  @Test
  public void zJoinCourseWithUser2(TestContext context) {
    joinCourse(context, "courseWithNameAndDescriptionAndSemester", null, "user2", 200);
  }

  private void joinCourse(TestContext context, int courseID, Integer password, String asUser,
      int expectedStatus) {
    Async async = context.async();
    JsonObject body = null;
    if (password != null) {
      body = new JsonObject().put("password", password);
    }
    request("/api/courses/" + courseID + "/join", HttpMethod.POST, asUser, body).onSuccess(res -> {
      context.assertEquals(expectedStatus, res.statusCode());
      if (res.statusCode() == 200) {
        JsonObject resBody = res.bodyAsJsonObject();
        context.assertEquals(courseID, resBody.getInteger("id"));
        context.assertNotNull(resBody.getString("name"));
        context.assertNotNull(resBody.getString("semester"));
        context.assertNotNull(resBody.getString("description"));
        context.assertNotNull(resBody.getBoolean("protected"));
      }
      async.complete();
    }).onFailure(context::fail);
  }

  private void joinCourse(TestContext context, String courseIDKey, Integer password, String asUser,
      int expectedStatus) {
    int courseID = CreateCourseTest.courseIDs.get(courseIDKey);
    joinCourse(context, courseID, password, asUser, expectedStatus);
  }
}
