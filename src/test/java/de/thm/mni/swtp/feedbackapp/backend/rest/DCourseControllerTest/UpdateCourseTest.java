package de.thm.mni.swtp.feedbackapp.backend.rest.DCourseControllerTest;

import de.thm.mni.swtp.feedbackapp.backend.rest.TestBase;
import io.vertx.core.Future;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import org.junit.Test;

public class UpdateCourseTest extends TestBase {
  @Test
  public void updateCourse(TestContext context) {
    updateCourse(context, "courseWithNameAndDescriptionAndSemester", "user4",
        CreateCourseTest.courseWithNameAndDescriptionAndSemester, 200);
  }

  @Test
  public void updateNonExistingCourse(TestContext context) {
    updateCourse(context, 10000000, "user4",
        CreateCourseTest.courseWithNameAndSemester, 404);
  }

  @Test
  public void updateNonOwnedCourse(TestContext context) {
    updateCourse(context, "courseWithNameAndSemester", "user3",
        CreateCourseTest.courseWithNameAndSemester, 403);
  }

  private void updateCourse(TestContext context, int courseID, String user,
      JsonObject updated, int expectedStatus) {
    Async async = context.async();
    request("/api/courses/" + courseID, HttpMethod.PUT, user, updated).compose(res -> {
      context.assertEquals(expectedStatus, res.statusCode());
      if (res.statusCode() == 200) {
        JsonObject resBody = res.bodyAsJsonObject();
        context.assertEquals(courseID, resBody.getInteger("id"));
        context.assertEquals(updated.getString("name"), resBody.getString("name"));
        context.assertEquals(updated.getString("semester"), resBody.getString("semester"));
        context.assertEquals(updated.getString("description", ""), resBody.getString("description"));
        context.assertEquals(updated.getString("password") != null, resBody.getBoolean("protected"));
      }
      return Future.succeededFuture();
    }).onSuccess(res -> async.complete()).onFailure(context::fail);
  }
  private void updateCourse(TestContext context, String courseIDKey, String user,
      JsonObject updated, int expectedStatus) {
    int courseID = CreateCourseTest.courseIDs.get(courseIDKey);
    updateCourse(context, courseID, user, updated, expectedStatus);
  }
}
