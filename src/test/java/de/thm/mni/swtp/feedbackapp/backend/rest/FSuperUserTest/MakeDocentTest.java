package de.thm.mni.swtp.feedbackapp.backend.rest.FSuperUserTest;

import de.thm.mni.swtp.feedbackapp.backend.rest.TestBase;
import io.vertx.core.Future;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import org.junit.Test;

public class MakeDocentTest  extends TestBase {
  @Test
  public void makeDocent(TestContext context) {
    makeUserDocent(context, "su", 200);
  }

  @Test
  public void makeDocentWithoutSuperuser(TestContext context) {
    makeUserDocent(context, "user", 403);
  }

  @Test
  public void makeDocentWithoutLogin(TestContext context) {
    makeUserDocent(context, null, 401);
  }

  private void makeUserDocent(TestContext context, String user, int expectedStatus) {
    Async async = context.async();
    JsonObject body = new JsonObject().put("username", "user4");
    request("/api/su/docent", HttpMethod.POST, user, body).compose(res -> {
      context.assertEquals(expectedStatus, res.statusCode());
      return Future.succeededFuture();
    }).onSuccess(res -> async.complete()).onFailure(context::fail);
  }
}
