package de.thm.mni.swtp.feedbackapp.backend.rest.BQuestionControllerTest;

import de.thm.mni.swtp.feedbackapp.backend.rest.AQuizControllerTest.CreateQuizTest;
import de.thm.mni.swtp.feedbackapp.backend.rest.TestBase;
import io.vertx.core.Future;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import org.junit.Test;

public class ListQuestionsTest extends TestBase {
  @Test
  public void listQuestions(TestContext context) {
    listQuestions(context, CreateQuizTest.quizIDs.get("quizWithoutDescription"), "user", 200);
  }

  @Test
  public void getQuestionForNonExistingQuiz(TestContext context) {
    listQuestions(context, 10000000, "user", 404);
  }

  @Test
  public void listQuestionsForOtherUser(TestContext context) {
    listQuestions(context, CreateQuizTest.quizIDs.get("quizWithoutDescription"), "user2", 403);
  }

  private void listQuestions(TestContext context, int quizID, String user, int expectedStatus) {
    Async async = context.async();
    request("/api/quizzes/" + quizID + "/questions", HttpMethod.GET, user, null).compose(res -> {
      context.assertEquals(expectedStatus, res.statusCode());
      if (res.statusCode() == 200) {
        JsonArray resBody = res.bodyAsJsonArray();
        context.assertEquals(3, resBody.size());
        for (int i = 0; i < resBody.size(); i++) {
          JsonObject question = resBody.getJsonObject(i);
          context.assertNotNull(question.getInteger("number"));
          context.assertNotNull(question.getString("type"));
          context.assertNotNull(question.getJsonObject("question"));
        }
      }
      return Future.succeededFuture();
    }).onSuccess(res -> async.complete()).onFailure(context::fail);
  }
}
