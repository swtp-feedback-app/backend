package de.thm.mni.swtp.feedbackapp.backend.rest.EUserTest;

import de.thm.mni.swtp.feedbackapp.backend.rest.TestBase;
import io.vertx.core.Future;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import org.junit.Test;

public class MyToDoSessionsTest extends TestBase {
  @Test
  public void listToDoSessions(TestContext context) {
    listSessions(context, "user1", 200, 2);
  }

  private void listSessions(TestContext context, String user, int expectedStatus, int expectedCount) {
    Async async = context.async();
    request("/api/users/me/todoSessions", HttpMethod.GET, user, null).compose(res -> {
      context.assertEquals(expectedStatus, res.statusCode());
      if (res.statusCode() == 200) {
        JsonArray body = res.bodyAsJsonArray();
        context.assertEquals(expectedCount, body.size());
        for (int i = 0; i < body.size(); i++) {
          JsonObject session = body.getJsonObject(i);
          context.assertNotNull(session.getInteger("id"));
          context.assertNotNull(session.getString("type"));
          context.assertNotNull(session.getLong("start"));
          context.assertNotNull(session.getLong("end"));
          context.assertNotNull(session.getString("description"));
          context.assertNotNull(session.getJsonObject("quiz"));
        }
      }
      return Future.succeededFuture();
    }).onSuccess(res -> async.complete()).onFailure(context::fail);
  }
}
