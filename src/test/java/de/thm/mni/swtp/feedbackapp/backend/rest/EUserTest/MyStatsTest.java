package de.thm.mni.swtp.feedbackapp.backend.rest.EUserTest;

import de.thm.mni.swtp.feedbackapp.backend.rest.TestBase;
import io.vertx.core.Future;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import org.junit.Test;

public class MyStatsTest extends TestBase {
  @Test
  public void getMyStats(TestContext context) {
    getMyStats(context, "user", 200, 4, 1, 1, 1);
  }

  @Test
  public void getMyStatsWithOtherUser(TestContext context) {
    getMyStats(context, "user1", 200, 2, 0, 0, 0);
  }

  @Test
  public void getMyStatsWithoutUser(TestContext context) {
    getMyStats(context, null, 401, 0, 0, 0, 0);
  }

  private void getMyStats(TestContext context, String user, int expectedStatus,
      int expectedCourseCount, int expectedSessionCount, int expectedAnsweredQuestion,
      int expectedCorrectQuestions) {
    Async async = context.async();
    request("/api/users/me/stats", HttpMethod.GET, user, null).compose(res -> {
      context.assertEquals(expectedStatus, res.statusCode());
      if (res.statusCode() == 200) {
        JsonObject response = res.bodyAsJsonObject();
        context.assertEquals(expectedCourseCount, response.getInteger("courseCount"));
        context.assertEquals(expectedSessionCount, response.getInteger("sessionCount"));
        context.assertEquals(expectedAnsweredQuestion, response.getInteger("answeredQuestions"));
        context.assertEquals(expectedCorrectQuestions, response.getInteger("correctQuestions"));
      }
      return Future.succeededFuture();
    }).onSuccess(res -> async.complete()).onFailure(context::fail);
  }
}
