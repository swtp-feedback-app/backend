package de.thm.mni.swtp.feedbackapp.backend.rest.DCourseControllerTest;

import de.thm.mni.swtp.feedbackapp.backend.rest.TestBase;
import io.vertx.core.http.HttpMethod;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import org.junit.Test;

public class LeaveCourseTest extends TestBase {
  @Test
  public void leaveCourse(TestContext context) {
    joinCourse(context, "courseWithNameAndDescriptionAndSemester", "user2", 200);
  }

  @Test
  public void leaveNonExistingCourse(TestContext context) {
    joinCourse(context, 10000000, "user2", 404);
  }

  @Test
  public void leaveCourseNotMemberOf(TestContext context) {
    joinCourse(context, "courseWithNameAndDescriptionAndSemester", "user2", 403);
  }

  private void joinCourse(TestContext context, int courseID, String asUser, int expectedStatus) {
    Async async = context.async();
    request("/api/courses/" + courseID + "/leave", HttpMethod.POST, asUser, null).onSuccess(res -> {
      context.assertEquals(expectedStatus, res.statusCode());
      async.complete();
    }).onFailure(context::fail);
  }
  private void joinCourse(TestContext context, String courseIDKey, String asUser, int expectedStatus) {
    int courseID = CreateCourseTest.courseIDs.get(courseIDKey);
    joinCourse(context, courseID, asUser, expectedStatus);
  }
}
