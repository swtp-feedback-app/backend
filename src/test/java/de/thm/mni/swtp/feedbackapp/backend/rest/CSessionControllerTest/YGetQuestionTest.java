package de.thm.mni.swtp.feedbackapp.backend.rest.CSessionControllerTest;

import io.vertx.core.http.HttpMethod;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import org.junit.Test;

public class YGetQuestionTest extends SessionTestBase {
    @Test
    public void getQuestion(TestContext context){
      getQuestion(context, "session", 1, "sessionToken", 200);
    }

    @Test
    public void getNonExistingQuestion(TestContext context){
        getQuestion(context, "session", 4, "sessionToken", 404);
    }

    @Test
    public void getQuestionWithOtherUser(TestContext context){
        getQuestion(context, "session", 1, "sessionToken", 200);
    }

    private void getQuestion(TestContext context, int sessionID, int questionID, String sessionTokenKey,
        int expectedStatus) {
        Async async = context.async();
        request("/api/sessions/" + sessionID + "/questions/" + questionID, HttpMethod.GET, sessionTokenKey, null).onSuccess(res -> {
          context.assertEquals(expectedStatus, res.statusCode());
          if (res.statusCode() == 200) {
              JsonObject body = res.bodyAsJsonObject();
              context.assertNotNull(body.getString("type"));
              context.assertNotNull(body.getInteger("number"));
              JsonObject question = body.getJsonObject("question");
              context.assertNotNull(question);
              context.assertNotNull(question.getString("question"));
              if (body.getString("type").equals("multipleChoice")) {
                  int options = question.getJsonObject("answers").getJsonArray("options").size();
                  context.assertEquals(options, 4);
              }
          }
          async.complete();
        }).onFailure(context::fail);
    }

    private void getQuestion(TestContext context, String sessionIDKey, int questionID, String sessionTokenKey,
        int expectedStatus) {
        int sessionID = CreateSessionTest.sessionIDs.get(sessionIDKey);
        getQuestion(context, sessionID, questionID, sessionTokenKey, expectedStatus);
    }
}
