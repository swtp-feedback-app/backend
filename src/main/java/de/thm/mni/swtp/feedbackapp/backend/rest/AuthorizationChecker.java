package de.thm.mni.swtp.feedbackapp.backend.rest;

public class AuthorizationChecker {
  private final boolean adminAllowed;
  private final boolean studentRequired;
  private final int resourceOwnerID;

  public AuthorizationChecker(boolean adminAllowed, boolean studentAllowed, int resourceOwnerID) {
    this.adminAllowed = adminAllowed;
    this.studentRequired = studentAllowed;
    this.resourceOwnerID = resourceOwnerID;
  }

  public static class Builder {
    private boolean adminAllowed = false;
    private boolean studentAllowed = false;

    public Builder allowAdmin(boolean allow) {
      this.adminAllowed = allow;
      return this;
    }
    public Builder allowStudent(boolean allow) {
      this.studentAllowed = allow;
      return this;
    }
    public AuthorizationChecker build(int resourceOwnerID) {
      return new AuthorizationChecker(this.adminAllowed, this.studentAllowed, resourceOwnerID);
    }
  }

  public boolean check(int userID, boolean isSuperUser, boolean isAdmin, boolean isStudent) {
    return isSuperUser || this.resourceOwnerID == userID ||
        this.adminAllowed && isAdmin || this.studentRequired && isStudent;

  }
  public boolean check(int userID, boolean isSuperUser) {
    return check(userID, isSuperUser, false, false);
  }
}
