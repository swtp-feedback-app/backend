package de.thm.mni.swtp.feedbackapp.backend.rest.questionTypes;

import de.thm.mni.swtp.feedbackapp.backend.rest.StatisticUtils;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

public class WordCloud extends NonEmpty {
  WordCloud(JsonObject options) {
    super(options);
  }

  @Override
  public Object computeStats(JsonArray answers) {
    return StatisticUtils.transformWordCloudQuestion(answers);
  }
}
