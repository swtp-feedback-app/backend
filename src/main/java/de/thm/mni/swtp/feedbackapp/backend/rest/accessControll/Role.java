package de.thm.mni.swtp.feedbackapp.backend.rest.accessControll;

import java.util.Set;

public abstract class Role {
  public abstract String getName();
  public abstract Set<Permission> getPermissions();

  public boolean hasPermission(Permission permission) {
    return this.getPermissions().contains(permission);
  }

  @Override
  public boolean equals(Object obj) {
    if (!(obj instanceof Role)) {
      return false;
    }
    Role other = (Role) obj;
    return this.getName().equals(other.getName());
  }

  @Override
  public int hashCode() {
    return this.getName().hashCode();
  }

  @Override
  public String toString() {
    return "Role("+ this.getName() +")";
  }
}
