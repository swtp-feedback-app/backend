package de.thm.mni.swtp.feedbackapp.backend.rest.accessControll;

import de.thm.mni.swtp.feedbackapp.backend.rest.RestVerticle;
import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.Promise;

public class RoleUtils {
  public static void getRole(int userID, int courseID, Handler<AsyncResult<Role>> result) {
    Promise<String> rolePromise = Promise.promise();
    RestVerticle.db.getRole(userID, courseID, rolePromise);
    rolePromise.future().onSuccess(roleName -> {
        Role role;
        if (roleName != null) {
          role = Roles.fromName(roleName);
        } else {
          role = Roles.NONE;
        }
        result.handle(Future.succeededFuture(role));
    }).onFailure(err -> result.handle(Future.failedFuture(err)));
  }
  public static void setRole(int userID, int courseID, Role role, Handler<AsyncResult<Void>> result) {
    RestVerticle.db.setRole(userID, courseID, role.getName(), result);
  }
}
