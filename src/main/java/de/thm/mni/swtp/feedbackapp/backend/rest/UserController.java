package de.thm.mni.swtp.feedbackapp.backend.rest;

import io.vertx.core.Promise;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;

import java.util.List;

public class UserController {
  public static void me(RoutingContext context) {
    String username = (String) context.data().get("username");
    String firstname = (String) context.data().get("firstname");
    String lastname = (String) context.data().get("lastname");
    int userID = (int) context.data().get("userID");
    Integer fbsID = (Integer) context.data().get("fbsID");
    boolean docent = (boolean) context.data().get("docent");
    boolean superUser = (boolean) context.data().get("superUser");

    JsonObject jsonResponse = new JsonObject()
        .put("id", userID)
        .put("fbsID", fbsID)
        .put("username", username)
        .put("firstname", firstname)
        .put("lastname", lastname)
        .put("docent", docent)
        .put("superUser", superUser);

    ResponseUtils.jsonResponse(context, 200, jsonResponse);
  }

  public static void myStats(RoutingContext context) {
    int userID = (int) context.data().get("userID");

    Promise<JsonObject> statsPromise = Promise.promise();
    RestVerticle.db.getUserStatistics(userID, statsPromise);

    statsPromise.future().onSuccess(stats -> {
      ResponseUtils.jsonResponse(context, 200, stats);
    }).onFailure(err -> ResponseUtils.internalServerErrorResponse(context, err));
  }

  public static void myCourses(RoutingContext context) {
    int userID = (int) context.data().get("userID");
    List<String> roles = RequestUtils.getQueryParamAsStringArray(context, "role");

    Promise<JsonArray> coursesPromise = Promise.promise();
    RestVerticle.db.getCoursesForUser(userID, roles, coursesPromise);

    coursesPromise.future().onSuccess(courses -> {
      ResponseUtils.jsonResponse(context, 200, DBMapper.mapMany(courses, DBMapper::mapCourse));
    }).onFailure(err -> ResponseUtils.internalServerErrorResponse(context, err));
  }

  public static void mySessions(RoutingContext context) {
    int userID = (int) context.data().get("userID");

    Promise<JsonArray> sessionPromise = Promise.promise();
    RestVerticle.db.getSessionsForUser(userID, sessionPromise);

    sessionPromise.future().onSuccess(sessions -> {
      ResponseUtils.jsonResponse(context, 200, DBMapper.mapMany(sessions, DBMapper::mapSession));
    }).onFailure(err -> ResponseUtils.internalServerErrorResponse(context, err));
  }

  public static void toDoSessions(RoutingContext context) {
    int userID = (int) context.data().get("userID");

    Promise<JsonArray> sessionPromise = Promise.promise();
    RestVerticle.db.listToDoSessions(userID, sessionPromise);

    sessionPromise.future().onSuccess(sessions -> {
      ResponseUtils.jsonResponse(context, 200, DBMapper.mapMany(sessions, DBMapper::mapSession));
    }).onFailure(err -> ResponseUtils.internalServerErrorResponse(context, err));
  }

  public static void sharedWithMe(RoutingContext context) {
    int userID = (int) context.data().get("userID");

    Promise<JsonArray> quizPromise = Promise.promise();
    RestVerticle.db.sharedWith(userID, quizPromise);

    quizPromise.future().onSuccess(quizzes -> {
      ResponseUtils.jsonResponse(context, 200, DBMapper.mapMany(quizzes, DBMapper::mapQuiz));
    }).onFailure(err -> ResponseUtils.internalServerErrorResponse(context, err));
  }
}
