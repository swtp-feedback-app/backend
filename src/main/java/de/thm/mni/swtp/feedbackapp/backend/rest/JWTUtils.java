package de.thm.mni.swtp.feedbackapp.backend.rest;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;

import java.security.SecureRandom;
import java.time.Duration;
import java.time.Instant;
import java.util.Date;

public class JWTUtils {
  private static final String AUTH_TOKEN_ISSUER = "feedbackAppBackend/auth";
  private static final String SESSION_TOKEN_ISSUER = "feedbackAppBackend/session";
  private static final String ONE_DAY_ISO_8601 = "P1D";
  private static final String ONE_WEEK_ISO_8601 = "P7D";

  private static Algorithm algorithm;
  private static JWTVerifier authVerifier;
  private static JWTVerifier sessionVerifier;
  private static Duration authTokenExpiration = Duration.parse(ONE_WEEK_ISO_8601);
  private static Duration sessionTokenExpiration = Duration.parse(ONE_DAY_ISO_8601);

  public static void setSecret(String secret) {
    if (secret == null) {
      setRandomSecret();
    } else {
      algorithm = Algorithm.HMAC256(secret);
    }
    authVerifier = JWT.require(algorithm).withIssuer(AUTH_TOKEN_ISSUER).build();
    sessionVerifier = JWT.require(algorithm).withIssuer(SESSION_TOKEN_ISSUER).build();
  }

  public static void setAuthTokenExpiration(Duration authTokenExpiration) {
    JWTUtils.authTokenExpiration = authTokenExpiration;
  }

  public static void setSessionTokenExpiration(Duration sessionTokenExpiration) {
    JWTUtils.sessionTokenExpiration = sessionTokenExpiration;
  }

  public static void setAuthTokenExpiration(String authTokenExpiration) {
    if (authTokenExpiration == null) {
      return;
    }
    setAuthTokenExpiration(Duration.parse(authTokenExpiration));
  }

  public static void setSessionTokenExpiration(String sessionTokenExpiration) {
    if (sessionTokenExpiration == null) {
      return;
    }
    setSessionTokenExpiration(Duration.parse(sessionTokenExpiration));
  }

  public static String genAuthToken(String username, String firstname, String lastname,
      int internalUserID, Integer fbsUserID, boolean docent, boolean superUser, String feedbackSystemToken) {
    return JWT.create()
        .withJWTId(Long.toString(Utils.getRandomLongID()))
        .withIssuer(AUTH_TOKEN_ISSUER)
        .withIssuedAt(new Date())
        .withExpiresAt(getAuthTokenExpirationDate())
        .withSubject(username)
        .withClaim("firstname", firstname)
        .withClaim("lastname", lastname)
        .withClaim("internalUserID", internalUserID)
        .withClaim("fbsUserID", fbsUserID)
        .withClaim("docent", docent)
        .withClaim("superUser", superUser)
        .withClaim("feedbackSystemToken", feedbackSystemToken)
        .sign(algorithm);
  }

  public static AuthTokenVerificationResult verifyAuthToken(String token) {
    DecodedJWT decodedJWT;
    try {
      decodedJWT = authVerifier.verify(token);
    } catch (JWTVerificationException exception) {
      return null;
    }
    return new AuthTokenVerificationResult(Long.parseLong(decodedJWT.getId()),
        decodedJWT.getSubject(),
        decodedJWT.getClaim("firstname").asString(),
        decodedJWT.getClaim("lastname").asString(),
        decodedJWT.getClaim("internalUserID").asInt(),
        decodedJWT.getClaim("fbsUserID").asInt(),
        decodedJWT.getClaim("docent").asBoolean(),
        decodedJWT.getClaim("superUser").asBoolean(),
        decodedJWT.getClaim("feedbackSystemToken").asString());
  }

  public static String genSessionToken(String userIdentifier, int sessionID, int quizID,
      boolean identConfirmed, long sessionEnds) {
    return JWT.create()
        .withIssuer(SESSION_TOKEN_ISSUER)
        .withIssuedAt(new Date())
        .withExpiresAt(getSessionTokenExpirationDate())
        .withSubject(userIdentifier)
        .withClaim("sessionID", sessionID)
        .withClaim("quizID", quizID)
        .withClaim("identifierConfirmed", identConfirmed)
        .sign(algorithm);
  }

  public static SessionTokenVerificationResult verifySessionToken(String token) {
    DecodedJWT decodedJWT;
    try {
      decodedJWT = sessionVerifier.verify(token);
    } catch (JWTVerificationException exception) {
      return null;
    }
  return new SessionTokenVerificationResult(decodedJWT.getSubject(),
      decodedJWT.getClaim("sessionID").asInt(), decodedJWT.getClaim("quizID").asInt(),
      decodedJWT.getClaim("identifierConfirmed").asBoolean());
  }

  public static FBSTokenDecodingResult decodeFBSToken(String token) {
    DecodedJWT decodedJWT = JWT.decode(token);
    return new FBSTokenDecodingResult(
            decodedJWT.getClaim("id").asInt(),
            decodedJWT.getClaim("username").asString(),
            decodedJWT.getClaim("courseRoles").asString(),
            decodedJWT.getClaim("globalRole").asString(),
            decodedJWT.getClaim("exp").asString(),
            decodedJWT.getClaim("iat").asString()
    );
  }

  static class AuthTokenVerificationResult {
    final long tokenID;
    final String username;
    final String firstname;
    final String lastname;
    final int internalUserID;
    final Integer fbsUserID;
    final boolean docent;
    final boolean superUser;
    final String feedbackSystemToken;

    public AuthTokenVerificationResult(long tokenID, String username, String firstname, String lastname,
        int internalUserID, Integer fbsUserID, boolean docent, boolean superUser, String feedbackSystemToken) {
      this.tokenID = tokenID;
      this.username = username;
      this.firstname = firstname;
      this.lastname = lastname;
      this.internalUserID = internalUserID;
      this.fbsUserID = fbsUserID;
      this.docent = docent;
      this.superUser = superUser;
      this.feedbackSystemToken = feedbackSystemToken;
    }
  }

  static class SessionTokenVerificationResult {
    final String userIdentifier;
    final int sessionID;
    final int quizID;
    final boolean identifierConfirmed;

    public SessionTokenVerificationResult(String userIdentifier, int sessionID, int quizID, boolean identifierConfirmed) {
      this.userIdentifier = userIdentifier;
      this.sessionID = sessionID;
      this.quizID = quizID;
      this.identifierConfirmed = identifierConfirmed;
    }
  }

  static class FBSTokenDecodingResult {
    final int    userID;
    final String username;
    final String courseRoles;
    final String globalRole;
    final String exp;
    final String iat;

    public FBSTokenDecodingResult(int userID, String username, String courseRoles, String globalRole, String exp, String iat) {
      this.userID = userID;
      this.username = username;
      this.courseRoles = courseRoles;
      this.globalRole = globalRole;
      this.exp = exp;
      this.iat = iat;
    }
  }

  private static Date getAuthTokenExpirationDate() {
    return Date.from(Instant.now().plus(authTokenExpiration));
  }

  private static Date getSessionTokenExpirationDate() {
    return Date.from(Instant.now().plus(sessionTokenExpiration));
  }

  private static final SecureRandom secureRandom = new SecureRandom();
  private static void setRandomSecret() {
    byte[] randomSecret = new byte[32];
    secureRandom.nextBytes(randomSecret);
    algorithm = Algorithm.HMAC256(randomSecret);
  }
}
