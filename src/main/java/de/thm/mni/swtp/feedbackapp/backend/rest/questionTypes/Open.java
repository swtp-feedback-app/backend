package de.thm.mni.swtp.feedbackapp.backend.rest.questionTypes;

import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

public class Open extends NonEmpty {
  Open() {
    super(new JsonObject().put("max", 1));
  }

  @Override
  public Object computeStats(JsonArray answers) {
    JsonArray answersGiven = new JsonArray();
    for (int i = 0; i < answers.size(); i++) {
      JsonObject answer = answers.getJsonObject(i);
      if (!answer.getBoolean("correct")) {
        continue;
      }
      String answerGiven = answer.getJsonArray("answers").getString(0);
      answersGiven.add(answerGiven);
    }
    return answersGiven;
  }
}
