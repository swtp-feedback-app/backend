package de.thm.mni.swtp.feedbackapp.backend.wordcloud;

import com.kennycason.kumo.CollisionMode;
import com.kennycason.kumo.WordCloud;
import com.kennycason.kumo.WordFrequency;
import com.kennycason.kumo.bg.CircleBackground;
import com.kennycason.kumo.font.scale.LinearFontScalar;
import com.kennycason.kumo.palette.ColorPalette;
import de.thm.mni.swtp.feedbackapp.backend.rest.StatisticUtils;
import de.thm.mni.swtp.feedbackapp.backend.services.WordcloudService;
import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.Promise;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

import java.awt.*;
import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class WordCloudController implements WordcloudService {
    public void getDataForWordcloud(int idsession, int questionNumber, Handler<AsyncResult<String>> wordcloudResult){
        Promise<JsonObject> promisegetSession = Promise.promise();
        WordCloudVerticle.db.getSession(idsession,promisegetSession);
        promisegetSession.future().onSuccess(ar -> {
            Promise<JsonObject> promiseQuestion = Promise.promise();
            WordCloudVerticle.db.getQuestion(ar.getJsonObject("quiz").getInteger("idquiz"),questionNumber, promiseQuestion);
            promiseQuestion.future().onSuccess(at ->{
                if (!at.getString("type").equals("wordCloud")) {
                    wordcloudResult.handle(Future.failedFuture("Wrong Questiontype"));
                    return;
                }
                Promise<JsonArray> promiseListAnswers = Promise.promise();
                WordCloudVerticle.db.listAnswers(idsession, at.getInteger("id"),promiseListAnswers);
                promiseListAnswers.future().onSuccess(ay -> {
                    JsonObject res = StatisticUtils.transformWordCloudQuestion(ay);
                    List<WordFrequency> result = new ArrayList<>();
                    for (Entry<String, Object> entry : res){
                        result.add(new WordFrequency(entry.getKey(), (int) entry.getValue()));
                    }
                    String g = makeWordCloud(result);
                    wordcloudResult.handle(Future.succeededFuture(g));
                }).onFailure(r ->{
                    wordcloudResult.handle(Future.failedFuture(r));
                }).onFailure(r ->{
                    wordcloudResult.handle(Future.failedFuture(r));
                });

            }).onFailure(r ->{
                    wordcloudResult.handle(Future.failedFuture(r));
            });

        }).onFailure(r ->{
                    wordcloudResult.handle(Future.failedFuture(r));
        });


    }


    private static String makeWordCloud(List<WordFrequency> result){
        List<WordFrequency> wordFrequencies = result;
        final Dimension dimension = new Dimension(1080, 1080);
        final WordCloud wordCloud = new WordCloud(dimension, CollisionMode.PIXEL_PERFECT);
        wordCloud.setPadding(2);
        wordCloud.setBackgroundColor(Color.WHITE);
        wordCloud.setBackground(new CircleBackground(540));
        wordCloud.setColorPalette(new ColorPalette(new Color(0xFF80BA24), new Color(0xFF4A5C66), new Color(0xFFF9D47F)));
        wordCloud.setFontScalar(new LinearFontScalar(10, 40));
        wordCloud.build(wordFrequencies);
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        wordCloud.writeToStreamAsPNG(output);
        return Base64.getEncoder().encodeToString(output.toByteArray());
    }
}
