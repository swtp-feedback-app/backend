package de.thm.mni.swtp.feedbackapp.backend.services;

import io.vertx.codegen.annotations.ProxyGen;
import io.vertx.codegen.annotations.VertxGen;
import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

import java.util.List;

@ProxyGen
@VertxGen
public interface DBService {

  void listQuestions(int idQuiz, Handler<AsyncResult<JsonArray>> result);

  void createQuestion(String questiontext, String type, int idquiz, JsonObject answersObject,
      JsonArray answersArray, Handler<AsyncResult<JsonObject>> result);

  void deleteQuestion(int idquiz, int position, Handler<AsyncResult<Void>> result);

  void getQuestion(int idquiz, int position, Handler<AsyncResult<JsonObject>> result);

  void updateQuestion(int idquiz, int position, String questiontext, String type,
      JsonObject answersObject, JsonArray answersArray, Handler<AsyncResult<Void>> result);

  void listQuizzes(int userID, Handler<AsyncResult<JsonArray>> result);

  void createQuiz(int idquiz, String name, int password, String description, String type,
      boolean instantFeedback, int idperson, Handler<AsyncResult<Void>> result);

  void deleteQuiz(int idquiz, Handler<AsyncResult<Void>> result);

  void getQuiz(int idquiz, Handler<AsyncResult<JsonObject>> result);

  void updateQuiz(int idquiz, String name, int password, String description, String type,
      boolean instantFeedback, int idperson, Handler<AsyncResult<Void>> result);

  void listQuizSessions(int idquiz, Handler<AsyncResult<JsonArray>> result);

  void createQuizSession(int id, long start, long end, boolean anonym, String description, boolean showCorrect,
      int idquiz, Integer idcourse, int idperson, Handler<AsyncResult<Void>> result);

  void createCourse(int courseID, String name, String description, String semester, int lecturerID,
                    Integer password, boolean fbs, boolean allAuthor, Handler<AsyncResult<Void>> result);

  void browseCourse(String search, int perPage, int page, Integer hideJoined, Handler<AsyncResult<JsonArray>> result);

  void getCourse(int idcourse, Handler<AsyncResult<JsonObject>> result);

  void updateCourse(int idcourse, String name, String description, String semester,
                    int lecturer, Integer password, boolean fbs, boolean allAuthor, Handler<AsyncResult<Void>> result);

  void deleteCourse(int idcourse, Handler<AsyncResult<Void>> result);

  void joinCourse(int idcourse, int idperson, Handler<AsyncResult<Void>> result);

  //Die Methode klappt noch nicht so ganz, wegen den OffserDateTime Sachen
  void addSession(int idsession, int idcourse, Handler<AsyncResult<Void>> result);

  void listSessions(int idcourse, int idperson, boolean hideNonActive, Handler<AsyncResult<JsonArray>> result);

  void listParticipants(int idcourse, List<String> roles, Handler<AsyncResult<JsonArray>> result);

  void isParticipant(int idcourse, int idperson, Handler<AsyncResult<Boolean>> result);

  void getSession(int idsession, Handler<AsyncResult<JsonObject>> result);

  void getquestion(int idsession, int idquestion, Handler<AsyncResult<Void>> result);

  void submitAnswer(int idquestion, Integer idperson, String anonymIdentifier, int idsession, boolean correct,
      JsonArray answers, long necessarytime, Handler<AsyncResult<Void>> result);

  void getMyStatistics(int idsession, Integer idperson, String anonymIdentity,
      Handler<AsyncResult<JsonObject>> result);

  void getMyCourses(int idperson, Handler<AsyncResult<JsonArray>> result);

  void updateSession(int idsession, long start, long ends, String description, boolean showCorrect, Handler<AsyncResult<Void>> result);

  void deleteSession(int idsession, Handler<AsyncResult<Void>> result);

  void getPersonByUsername(String username, Handler<AsyncResult<JsonObject>> result);

  void getPersonByID(int idperson, Handler<AsyncResult<JsonObject>> result);

  void getQuestionStatistics(int idsession, int idquestion, Handler<AsyncResult<JsonObject>> result);
  void getSessionStatistics(int idsession, Handler<AsyncResult<JsonObject>> result);

  void createPerson(int userID, String username, String firstName, String lastName, boolean docent, Handler<AsyncResult<JsonObject>> result);

  void getCoursesForUser(int userID, List<String> roles, Handler<AsyncResult<JsonArray>> result);
  void getSessionsForUser(int userID, Handler<AsyncResult<JsonArray>> result);

  void getBadWords(Handler<AsyncResult<JsonArray>> result);

  void countAnswersFromPerson(int idsession,int idperson, Handler<AsyncResult<Integer>> result);

  void listSessionsForUser(int iduser, boolean hideEnded, boolean hideNonStarted, Handler<AsyncResult<JsonArray>> result);
  void listToDoSessions(int userID, Handler<AsyncResult<JsonArray>> result);

  void getUserStatistics(int userID, Handler<AsyncResult<JsonObject>> result);

  void getMyCourseStatistics(int idcourse, int idperson, Handler<AsyncResult<JsonObject>> result);

  void listAnswers(int idsession, int idquestion, Handler<AsyncResult<JsonArray>> result);

  void getPersonByFbsIdOrCreate(int fbsID, String firstName, String lastName, boolean docent, Handler<AsyncResult<Integer>> result);

  void setPersonUsername(int idperson, String username, Handler<AsyncResult<Void>> result);

  void revokeToken(long tokenID, Handler<AsyncResult<Void>> result);
  void checkRevoked(long tokenID, Handler<AsyncResult<Boolean>> result);

  void getAllCourses(Handler<AsyncResult<JsonArray>> result);
  void getAllQuizzes(Handler<AsyncResult<JsonArray>> result);
  void getAllSessions(Handler<AsyncResult<JsonArray>> result);
  void getAllPersons(Handler<AsyncResult<JsonArray>> result);

  void setDocent(int idperson, boolean docent, Handler<AsyncResult<Void>> result);

  void share(int idquiz, int idperson, Handler<AsyncResult<Void>> result);
  void unShare(int idquiz, int idperson, Handler<AsyncResult<Void>> result);
  void sharedWith(int idperson, Handler<AsyncResult<JsonArray>> result);
  void isSharedWith(int idquiz, int idperson, Handler<AsyncResult<Boolean>> result);

  void getCorrect(int idperson, int idsession, int position, Handler<AsyncResult<JsonObject>> result);

  void getRole(int idperson, int idcourse, Handler<AsyncResult<String>> result);
  void setRole(int idperson, int idcourse, String role, Handler<AsyncResult<Void>> result);
  void deleteRole(int idperson, int idcourse, Handler<AsyncResult<Void>> result);

  void getBarChartForSingleMultipleChoiceQuiz1(int idsession, Handler<AsyncResult<JsonArray>> result);
  void getBarChartForSingleMultipleChoiceQuiz2(int idquestion, Handler<AsyncResult<JsonArray>> result);
  void getBarChartForSingleMultipleChoiceQuiz3(int idquestion, int idsession,Handler<AsyncResult<JsonArray>> result);
  void getBarChartForSingleMultipleChoiceQuiz4(int idsession, Handler<AsyncResult<JsonArray>> result);

  void getBarChartForMultipleQuizzes1(int idsession, Handler<AsyncResult<JsonArray>> result);
  void getBarChartForMultipleQuizzes2(int idquestion, Handler<AsyncResult<JsonArray>> result);
  void getBarChartForMultipleQuizzes3(int idsession, int questionCount, Handler<AsyncResult<JsonArray>> result);
  void getBarChartForMultipleQuizzes4(int idcourse, Handler<AsyncResult<JsonArray>> result);
  void getBarChartForMultipleQuizzes5(int idcourse, Handler<AsyncResult<JsonArray>> result);

  void getCurseOverview(int courseID, Handler<AsyncResult<JsonArray>> result);
}
