package de.thm.mni.swtp.feedbackapp.backend.rest.questionTypes;

import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import java.util.LinkedHashMap;
import java.util.Map;

public class MultipleChoice extends QuestionType {
  private final JsonArray correctAnswers;
  private final Integer possibleAnswers;

  MultipleChoice(JsonObject question) {
    this.correctAnswers = question.getJsonArray("options");
    this.possibleAnswers = question.getInteger("possibleAnswers");
  }

  @Override
  public boolean validateAnswer(JsonArray givenAnswers) {
    for (int i = 0; i < correctAnswers.size(); i++) {
      JsonObject correctAnswer = correctAnswers.getJsonObject(i);
      String answer = correctAnswer.getString("answer");
      boolean correct = correctAnswer.getBoolean("correct");
      if (correct) {
        if (!givenAnswers.contains(answer)) {
          return false;
        }
      } else {
        if (givenAnswers.contains(answer)) {
          return false;
        }
      }
    }
    return true;
  }

  @Override
  public JsonArray getCorrectAnswers() {
    JsonArray res = new JsonArray();
    for (int i = 0; i < correctAnswers.size(); i++) {
      JsonObject correctAnswer = correctAnswers.getJsonObject(i);
      if (correctAnswer.getBoolean("correct")) {
        res.add(correctAnswer.getString("answer"));
      }
    }
    return res;
  }

  @Override
  public Object getPossibleAnswers() {
    JsonObject output = new JsonObject();
    output.put("possibleAnswers", possibleAnswers);
    JsonArray newOptions = new JsonArray();
    for (int i = 0; i < correctAnswers.size(); i++) {
      newOptions.add(new JsonObject().put("answer", correctAnswers.getJsonObject(i).getString("answer")));
    }
    output.put("options", newOptions);
    return output;
  }

  @Override
  public Object computeStats(JsonArray answers) {
    Map<String, Integer> answerMap = new LinkedHashMap<>();
    for (int i = 0; i < correctAnswers.size(); i++) {
      answerMap.put(correctAnswers.getJsonObject(i).getString("answer"), 0);
    }
    for (int i = 0; i < answers.size(); i++) {
      JsonArray answersGiven = answers.getJsonObject(i).getJsonArray("answers");
      for (int j = 0; j < answersGiven.size(); j++) {
        String answer = answersGiven.getString(j);
        Integer answerCount = answerMap.get(answer);
        if (answerCount == null) {
          continue;
        }
        answerMap.put(answer, answerCount + 1);
      }
    }
    JsonArray stats = new JsonArray();
    for (Integer answerCount : answerMap.values()) {
      stats.add(answerCount);
    }
    return stats;
  }
}
