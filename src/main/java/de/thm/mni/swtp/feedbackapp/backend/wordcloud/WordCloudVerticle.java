package de.thm.mni.swtp.feedbackapp.backend.wordcloud;

import de.thm.mni.swtp.feedbackapp.backend.charts.ChartController;
import de.thm.mni.swtp.feedbackapp.backend.services.ChartsService;
import de.thm.mni.swtp.feedbackapp.backend.services.DBService;
import de.thm.mni.swtp.feedbackapp.backend.services.DBServiceVertxEBProxy;
import de.thm.mni.swtp.feedbackapp.backend.services.WordcloudService;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Promise;
import io.vertx.serviceproxy.ServiceBinder;

import static de.thm.mni.swtp.feedbackapp.backend.db.DatabaseVerticle.DATABASE_SERVICE_VERTX_ADDRESS;

public class WordCloudVerticle extends AbstractVerticle {
    public static final String WORDCLOUD_SERVICE_VERTX_ADDRESS = "feedbackapp.backend.wordcloud";
    static DBService db;

    @Override
    public void start(Promise<Void> startPromise){
        db = new DBServiceVertxEBProxy(vertx, DATABASE_SERVICE_VERTX_ADDRESS);

        WordCloudController dbController = new WordCloudController();
        ServiceBinder serviceBinder = new ServiceBinder(vertx);
        serviceBinder
                .setAddress(WORDCLOUD_SERVICE_VERTX_ADDRESS)
                .register(WordcloudService.class, dbController);

        startPromise.complete();
    }
}

