package de.thm.mni.swtp.feedbackapp.backend.rest;

import io.vertx.core.Promise;
import io.vertx.ext.web.RoutingContext;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

import java.util.Optional;

public class QuizController {
  private QuizController() {}

  public static void listQuizzes(RoutingContext context) {
    int userID = (int) context.data().get("userID");

    Promise<JsonArray> listQuizzesPromise = Promise.promise();
    RestVerticle.db.listQuizzes(userID, listQuizzesPromise);
    listQuizzesPromise.future().onSuccess(quizzes -> {
      ResponseUtils.jsonResponse(context, 200, DBMapper.mapMany(quizzes, DBMapper::mapQuiz));
    }).onFailure(err -> ResponseUtils.internalServerErrorResponse(context, err));
  }
  public static void createQuiz(RoutingContext context) {
    JsonObject jsonRequest = context.getBodyAsJson();
    JsonObject jsonResponse = new JsonObject();
    int userID = (int) context.data().get("userID");

    String name = jsonRequest.getString("name");
    String description = jsonRequest.getString("description");
    String type = jsonRequest.getString("type", "quiz");
    boolean instantFeedback = jsonRequest.getBoolean("instantFeedback", true);
    int password = jsonRequest.getInteger("password", 0);

    int quizID = Utils.getRandomID();

    Promise<Void> createQuizPromise = Promise.promise();
    RestVerticle.db.createQuiz(quizID, name, password, description, type, instantFeedback, userID, createQuizPromise);

    createQuizPromise.future().onSuccess(res -> {
      jsonResponse
          .put("id", quizID).put("name", name).put("description", description).put("type", type)
          .put("instantFeedback", instantFeedback).put("questionCount", 0);

      ResponseUtils.jsonResponse(context, 200, jsonResponse);
    }).onFailure(err -> ResponseUtils.internalServerErrorResponse(context, err));
  }
  public static void deleteQuiz(RoutingContext context) {
    AuthorizationUtils.checkQuizAuthorization(context).onSuccess(quiz -> {
      int quizID = RequestUtils.getPathParamAsInt(context, "quizID");
      Promise<Void> deleteQuiz = Promise.promise();
      RestVerticle.db.deleteQuiz(quizID, deleteQuiz);

      deleteQuiz.future().onSuccess(res -> {
        ResponseUtils.okResponse(context, "Quiz deleted");
      }).onFailure(err -> ResponseUtils.internalServerErrorResponse(context, err));
    });
  }
  public static void getQuiz(RoutingContext context) {
    AuthorizationUtils.checkQuizAuthorization(context).onSuccess(quiz -> {
      ResponseUtils.jsonResponse(context, 200, DBMapper.mapQuiz(quiz));
    });
  }
  public static void updateQuiz(RoutingContext context) {
    AuthorizationUtils.checkQuizAuthorization(context).onSuccess(quiz -> {
      JsonObject jsonRequest = context.getBodyAsJson();
      int quizID = RequestUtils.getPathParamAsInt(context, "quizID");
      int userID = (int) context.data().get("userID");

      String name = jsonRequest.getString("name");
      String description = jsonRequest.getString("description");
      String type = jsonRequest.getString("type", "quiz");
      boolean instantFeedback = jsonRequest.getBoolean("instantFeedback", true);
      int password = jsonRequest.getInteger("password", 0);

      Promise<Void> updatePromise = Promise.promise();
      RestVerticle.db.updateQuiz(quizID, name, password, description, type, instantFeedback, userID, updatePromise);

      updatePromise.future().onSuccess(res -> {
        JsonObject quizResponse = jsonRequest.copy();
        quizResponse
            .put("id", quizID)
            .put("questionCount", quiz.getInteger("question_count"))
            .put("type", jsonRequest.getString("type", "quiz"))
            .put("instantFeedback", jsonRequest.getBoolean("instant_feedback", true)
        );
        ResponseUtils.jsonResponse(context, 200, quizResponse);
      }).onFailure(err -> ResponseUtils.internalServerErrorResponse(context, err));
    });
  }
  public static void listQuizSessions(RoutingContext context) {
    AuthorizationUtils.checkQuizAuthorization(context).onSuccess(quiz -> {
      JsonObject mappedQuiz = DBMapper.mapQuiz(quiz);
      int quizID = RequestUtils.getPathParamAsInt(context, "quizID");

      Promise<JsonArray> quizSession = Promise.promise();
      RestVerticle.db.listQuizSessions(quizID, quizSession);

      quizSession.future().onSuccess(sessions -> {
        JsonArray mappedSessions = DBMapper.mapMany(sessions, DBMapper::mapSession);
        for (int i = 0; i < sessions.size(); i++) {
          mappedSessions.getJsonObject(i).put("quiz", mappedQuiz);
        }
        ResponseUtils.jsonResponse(context, 200, mappedSessions);
      }).onFailure(err -> ResponseUtils.internalServerErrorResponse(context, err));
    });
  }
  public static void createQuizSession(RoutingContext context) {
    AuthorizationUtils.checkQuizAuthorization(context).onSuccess(quiz -> {
      int quizID = RequestUtils.getPathParamAsInt(context, "quizID");
      JsonObject jsonRequest = context.getBodyAsJson();
      int userID = (int) context.data().get("userID");

      Long start = jsonRequest.getLong("start");
      Long end = jsonRequest.getLong("end");
      String type = jsonRequest.getString("type");
      String description = jsonRequest.getString("description");
      boolean showCorrect = Optional.ofNullable(jsonRequest.getBoolean("showCorrectAnswers", null)).orElse(false);
      boolean anonym = type.equals("anonym");

      int id = Utils.getRandomID();

      Promise<Void> createSessionPromise = Promise.promise();
      RestVerticle.db.createQuizSession(id, start, end, anonym, description, showCorrect, quizID, null, userID, createSessionPromise);

      createSessionPromise.future().onSuccess(res -> {
        String responseType = "authenticated";
        if (anonym) {
          responseType = "anonym";
        }
        JsonObject responseBody = new JsonObject()
            .put("id", id).put("type", type).put("start", start).put("end", end)
            .put("quiz", new JsonObject()
                .put("id", quiz.getInteger("idquiz")).put("name", quiz.getString("name"))
                .put("description", quiz.getString("description")).put("type", quiz.getString("type"))
                .put("instantFeedback", quiz.getBoolean("instantFeedback"))
                .put("questionCount", quiz.getInteger("questionCount"))
            )
            .put("type", responseType)
            .put("start", start)
            .put("end", end)
            .put("description", description);
        ResponseUtils.jsonResponse(context, 200, responseBody);
      }).onFailure(err -> ResponseUtils.internalServerErrorResponse(context, err));
    });
  }
  public static void shareQuiz(RoutingContext context) {
    AuthorizationUtils.checkQuizAuthorization(context, true).onSuccess(quiz -> {
      int quizID = quiz.getInteger("idquiz");
      String shareWith = context.getBodyAsJson().getString("username");

      Promise<JsonObject> userPromise = Promise.promise();
      RestVerticle.db.getPersonByUsername(shareWith, userPromise);

      userPromise.future().onSuccess(user -> {
        int userID = user.getInteger("idperson");

        Promise<Void> sharePromise = Promise.promise();
        RestVerticle.db.share(quizID, userID, sharePromise);

        sharePromise.future().onSuccess(res -> {
          ResponseUtils.okResponse(context, "quiz shared");
        }).onFailure(err -> ResponseUtils.conflictResponse(context, "Already shared with user"));
      }).onFailure(err -> ResponseUtils.internalServerErrorResponse(context, err));
    });
  }
  public static void unShareQuiz(RoutingContext context) {
    AuthorizationUtils.checkQuizAuthorization(context, true).onSuccess(quiz -> {
      int quizID = quiz.getInteger("idquiz");
      String username = context.pathParam("username");

      Promise<JsonObject> userPromise = Promise.promise();
      RestVerticle.db.getPersonByUsername(username, userPromise);

      userPromise.future().onSuccess(user -> {
        int userID = user.getInteger("idperson");

        Promise<Void> sharePromise = Promise.promise();
        RestVerticle.db.unShare(quizID, userID, sharePromise);

        sharePromise.future().onSuccess(res -> {
          ResponseUtils.okResponse(context, "quiz shared");
        }).onFailure(err -> ResponseUtils.internalServerErrorResponse(context, err));
      }).onFailure(err -> ResponseUtils.internalServerErrorResponse(context, err));
    });
  }
}
