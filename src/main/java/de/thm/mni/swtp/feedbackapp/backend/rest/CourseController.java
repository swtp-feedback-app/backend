package de.thm.mni.swtp.feedbackapp.backend.rest;

import de.thm.mni.swtp.feedbackapp.backend.rest.accessControll.Permission;
import de.thm.mni.swtp.feedbackapp.backend.rest.accessControll.Role;
import de.thm.mni.swtp.feedbackapp.backend.rest.accessControll.RoleUtils;
import de.thm.mni.swtp.feedbackapp.backend.rest.accessControll.Roles;
import io.vertx.core.CompositeFuture;
import io.vertx.core.Future;
import io.vertx.core.Promise;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;

import java.util.ArrayList;
import java.util.List;

public class CourseController {
  public static void browseCourses(RoutingContext context) {
    String search = RequestUtils.getQueryParamAsString(context, "search", null);
    int perPage = RequestUtils.getQueryParamAsInteger(context, "perPage", 50);
    int page = RequestUtils.getQueryParamAsInteger(context, "page", 0);
    boolean hideJoined = RequestUtils.getQueryParamAsBoolean(context, "hideJoined", false);
    int userID = (int) context.data().get("userID");

    Integer hideJoinedUserID = null;
    if (hideJoined) {
      hideJoinedUserID = userID;
    }

    Promise<JsonArray> coursesPromise = Promise.promise();
    RestVerticle.db.browseCourse(search, perPage, page, hideJoinedUserID, coursesPromise);

    coursesPromise.future().onSuccess(courses -> {
      ResponseUtils.jsonResponse(context, 200, DBMapper.mapMany(courses, DBMapper::mapCourse));
    }).onFailure(err -> ResponseUtils.internalServerErrorResponse(context, err));
  }
  public static void createCourse(RoutingContext context) {
    if (RestVerticle.internCoursesDisabled) {
      ResponseUtils.forbiddenResponse(context, "Intern course creation is disabled");
      return;
    }

    boolean docent = (boolean) context.data().get("docent");
    if (!docent) {
      ResponseUtils.forbiddenResponse(context, "Only docents are allowed to creates courses");
      return;
    }

    JsonObject jsonRequest = context.getBodyAsJson();
    JsonObject jsonResponse = new JsonObject();
    int userID = (int) context.data().get("userID");

    String name = jsonRequest.getString("name");
    String semester = jsonRequest.getString("semester");
    String description = jsonRequest.getString("description", "");
    Integer password = jsonRequest.getInteger("password", null);
    boolean allAuthor = jsonRequest.getBoolean("allAuthor", false);

    int courseID = Utils.getRandomID();

    Promise<Void> createPromise = Promise.promise();
    RestVerticle.db.createCourse(courseID, name, description, semester, userID, password, false, allAuthor, createPromise);

    createPromise.future().onSuccess(res -> {
      Promise<Void> joinCoursePromise = Promise.promise();
      RoleUtils.setRole(userID, courseID, Roles.LECTURER, joinCoursePromise);

      joinCoursePromise.future().onSuccess(joinRes -> {
        jsonResponse.put("id", courseID)
            .put("name", name)
            .put("semester", semester)
            .put("description", description)
            .put("protected", password != null)
            .put("allAuthor", allAuthor);

        ResponseUtils.jsonResponse(context, 200, jsonResponse);
      }).onFailure(err -> ResponseUtils.internalServerErrorResponse(context, err));
    }).onFailure(err -> ResponseUtils.internalServerErrorResponse(context, err));
  }
  public static void getCourse(RoutingContext context) {
    int courseID = RequestUtils.getPathParamAsInt(context, "courseID");
    int userID = (int) context.data().get("userID");

    Promise<JsonObject> coursePromise = Promise.promise();
    RestVerticle.db.getCourse(courseID, coursePromise);

    coursePromise.future().onSuccess(course -> {
      if (course == null) {
        ResponseUtils.notFoundResponse(context, "Course not found");
        return;
      }

      Promise<Role> rolePromise = Promise.promise();
      RoleUtils.getRole(userID, courseID, rolePromise);

      rolePromise.future().onSuccess(role -> {
        course.put("role", role.getName());
        ResponseUtils.jsonResponse(context, 200, DBMapper.mapCourse(course));
      }).onFailure(err -> ResponseUtils.internalServerErrorResponse(context, err));
    }).onFailure(err -> ResponseUtils.internalServerErrorResponse(context, err));
  }
  public static void updateCourse(RoutingContext context) {
    AuthorizationUtils.checkCourseAuthorization(context, Permission.MANAGE_COURSE).compose(course -> {
      JsonObject jsonRequest = context.getBodyAsJson();
      int userID = (int) context.data().get("userID");

      String name = jsonRequest.getString("name");
      String semester = jsonRequest.getString("semester");
      String description = jsonRequest.getString("description");
      Integer password = jsonRequest.getInteger("password", null);
      boolean allAuthor = jsonRequest.getBoolean("allAuthor", false);

      Promise<Void> updatePromise = Promise.promise();
      RestVerticle.db.updateCourse(course.getInteger("idcourse"), name, description, semester,
          userID, password, false, allAuthor, updatePromise);

      return updatePromise.future().onSuccess(res -> {
        JsonObject jsonResponse = new JsonObject();
        jsonResponse.put("id", course.getInteger("idcourse"))
            .put("name", name)
            .put("semester", semester)
            .put("description", description)
            .put("protected", password != null)
            .put("allAuthor", allAuthor);
        ResponseUtils.jsonResponse(context, 200, jsonResponse);
      }).onFailure(err -> ResponseUtils.internalServerErrorResponse(context, err));
    });
  }
  public static void deleteCourse(RoutingContext context) {
    AuthorizationUtils.checkCourseAuthorization(context, Permission.MANAGE_COURSE).compose(course -> {
      Promise<Void> deletePromise = Promise.promise();
      RestVerticle.db.deleteCourse(course.getInteger("idcourse"), deletePromise);

      return deletePromise.future().onSuccess(res -> {
        ResponseUtils.okResponse(context, "course deleted");
      }).onFailure(err -> ResponseUtils.internalServerErrorResponse(context, err));
    });
  }
  public static void joinCourse(RoutingContext context) {
    int courseID = RequestUtils.getPathParamAsInt(context, "courseID");
    int userID = (int) context.data().get("userID");
    JsonObject request = context.getBodyAsJson();

    Promise<JsonObject> coursePromise = Promise.promise();
    RestVerticle.db.getCourse(courseID, coursePromise);

    coursePromise.future().compose(course -> {
      if (course == null) {
        ResponseUtils.notFoundResponse(context, "Course not found");
        return Future.succeededFuture();
      }
      Integer password = course.getInteger("course_password");
      if (password != null) {
        if (request == null || request.getInteger("password") == null) {
          ResponseUtils.unauthorizedResponse(context, "Missing course password");
          return Future.succeededFuture();
        }
        if (!password.equals(request.getInteger("password"))) {
          ResponseUtils.forbiddenResponse(context, "Invalid course password");
          return Future.succeededFuture();
        }
      }

      Promise<Void> joinPromise = Promise.promise();
      RestVerticle.db.joinCourse(courseID, userID, joinPromise);

      return joinPromise.future().compose(res -> {
        if (course.getBoolean("all_author")) {
          Promise<Void> adminPromise = Promise.promise();
          RoleUtils.setRole(userID, courseID, Roles.AUTHOR, adminPromise);

          return adminPromise.future().onSuccess(res2 -> {
            ResponseUtils.jsonResponse(context, 200, DBMapper.mapCourse(course));
          });
        }
        ResponseUtils.jsonResponse(context, 200, DBMapper.mapCourse(course));
        return Future.succeededFuture();
      }, err -> {
        ResponseUtils.conflictResponse(context, "user already joined course");
        return Future.succeededFuture();
      });
    }).onFailure(err -> ResponseUtils.internalServerErrorResponse(context, err));
  }
  public static void leaveCourse(RoutingContext context) {
    AuthorizationUtils.checkCourseAuthorization(context, Permission.LEAVE_COURSE).compose(course -> {
      int courseID = RequestUtils.getPathParamAsInt(context, "courseID");
      int userID = (int) context.data().get("userID");

      Promise<Void> leavePromise = Promise.promise();
      RoleUtils.setRole(userID, courseID, Roles.NONE, leavePromise);

      return leavePromise.future().onSuccess(res -> {
        ResponseUtils.okResponse(context, "Course left");
      }).onFailure(err -> ResponseUtils.internalServerErrorResponse(context, err));
    });
  }
  public static void addSession(RoutingContext context) {
    AuthorizationUtils.checkCourseAuthorization(context, Permission.CREATE_SESSIONS).onSuccess(course -> {
      int courseID = RequestUtils.getPathParamAsInt(context, "courseID");
      int userID = (int) context.data().get("userID");
      JsonObject jsonRequest = context.getBodyAsJson();

      int sessionID = jsonRequest.getInteger("sessionID");

      Promise<JsonObject> sessionPromise = Promise.promise();
      RestVerticle.db.getSession(sessionID, sessionPromise);

      sessionPromise.future().onSuccess(session -> {
        if (session == null) {
          ResponseUtils.notFoundResponse(context, "session not found");
          return;
        }
        if (session.getInteger("session_creator_idperson") != userID) {
          ResponseUtils.forbiddenResponse(context, "session is owned by another user");
          return;
        }
        if (session.getBoolean("anonym")) {
          ResponseUtils.forbiddenResponse(context, "anonym session can't be added to courses");
          return;
        }
        Promise<JsonObject> quizPromise = Promise.promise();
        RestVerticle.db.getQuiz(session.getJsonObject("quiz").getInteger("idquiz"), quizPromise);
        quizPromise.future().onSuccess(quiz -> {
          Promise<Void> addSessionPromise = Promise.promise();
          RestVerticle.db.addSession(sessionID, courseID, addSessionPromise);

          addSessionPromise.future().onSuccess(res -> {
            ResponseUtils.jsonResponse(context, 200,
                DBMapper.mapSession(
                    session.put("quiz", quiz)));
          }).onFailure(err -> ResponseUtils.internalServerErrorResponse(context, err));
        }).onFailure(err -> ResponseUtils.internalServerErrorResponse(context, err));
      }).onFailure(err -> ResponseUtils.internalServerErrorResponse(context, err));
    });
  }
  public static void listSessions(RoutingContext context) {
    int courseID = RequestUtils.getPathParamAsInt(context, "courseID");
    boolean hideNonActive = RequestUtils.getQueryParamAsBoolean(context, "hideNonActive", false);
    int userID = (int) context.data().get("userID");

    Promise<JsonObject> coursePromise = Promise.promise();
    RestVerticle.db.getCourse(courseID, coursePromise);

    coursePromise.future().onSuccess(course -> {
      if (course == null) {
        ResponseUtils.notFoundResponse(context, "Course not found");
        return;
      }

      Promise<JsonArray> listSessionPromise = Promise.promise();
      RestVerticle.db.listSessions(courseID, userID, hideNonActive, listSessionPromise);

      listSessionPromise.future().onSuccess(sessions -> {
        ResponseUtils.jsonResponse(context, 200, DBMapper.mapMany(sessions, DBMapper::mapSession));
      }).onFailure(err -> ResponseUtils.internalServerErrorResponse(context, err));
    });
  }
  public static void listUsers(RoutingContext context) {
    AuthorizationUtils.checkCourseAuthorization(context, Permission.LIST_USERS).onSuccess(course -> {
      int courseID = RequestUtils.getPathParamAsInt(context, "courseID");
      List<String> roles = RequestUtils.getQueryParamAsStringArray(context, "role");

      Promise<JsonObject> coursePromise = Promise.promise();
      RestVerticle.db.getCourse(courseID, coursePromise);

      Promise<JsonArray> listParticipantsPromise = Promise.promise();
      RestVerticle.db.listParticipants(courseID, roles, listParticipantsPromise);

      listParticipantsPromise.future().onSuccess(participants -> {
        ResponseUtils
            .jsonResponse(context, 200, DBMapper.mapMany(participants, DBMapper::mapUser));
      }).onFailure(err -> ResponseUtils.internalServerErrorResponse(context, err));
    });
  }
  public static void setUserRole(RoutingContext context) {
    AuthorizationUtils.checkCourseAuthorization(context, Permission.MANAGE_USERS).onSuccess(course -> {
      int courseID = RequestUtils.getPathParamAsInt(context, "courseID");
      String username = RequestUtils.getPathParamAsString(context, "username");

      Promise<JsonObject> userPromise = Promise.promise();
      RestVerticle.db.getPersonByUsername(username, userPromise);

      userPromise.future().onSuccess(user -> {
        if (user == null) {
          ResponseUtils.notFoundResponse(context, "user not found");
          return;
        }
        int userID = user.getInteger("idperson");

        JsonObject jsonRequest = context.getBodyAsJson();
        String roleName = jsonRequest.getString("role");
        Role role;
        try {
          role = Roles.fromName(roleName);
        } catch (IllegalArgumentException e) {
          ResponseUtils.badRequestResponse(context, "invalid role");
          return;
        }

        Promise<Void> setRolePromise = Promise.promise();
        RoleUtils.setRole(userID, courseID, role, setRolePromise);
        setRolePromise.future().onSuccess(res -> {
          ResponseUtils.okResponse(context, "role set");
        }).onFailure(err -> ResponseUtils.internalServerErrorResponse(context, err));
      }).onFailure(err -> ResponseUtils.internalServerErrorResponse(context, err));
    });
  }

  public static void addAllToAuthors(RoutingContext context) {
    AuthorizationUtils.checkCourseAuthorization(context, Permission.MANAGE_USERS).onSuccess(course -> {
      int courseID = RequestUtils.getPathParamAsInt(context, "courseID");

      Promise<JsonArray> authorsPromise = Promise.promise();
      RestVerticle.db.listParticipants(courseID, List.of(Roles.PARTICIPANT.getName()), authorsPromise);

      authorsPromise.future().onSuccess(users -> {
        List<Future> addAuthorFutures = new ArrayList<>();
        for (int i = 0; i < users.size(); i++) {
          Promise<Void> addAuthorPromise = Promise.promise();
          int userID = users.getJsonObject(i).getInteger("idperson");
          RoleUtils.setRole(userID, courseID, Roles.AUTHOR, addAuthorPromise);
          addAuthorFutures.add(addAuthorPromise.future());
        }

        CompositeFuture.all(addAuthorFutures).onSuccess(res -> {
          ResponseUtils.okResponse(context, "All users added as authors");
        }).onFailure(err -> ResponseUtils.internalServerErrorResponse(context, err));
      }).onFailure(err -> ResponseUtils.internalServerErrorResponse(context, err));
    });
  }

  public static void removeAllFromAuthors(RoutingContext context) {
    AuthorizationUtils.checkCourseAuthorization(context, Permission.MANAGE_USERS).onSuccess(course -> {
      int courseID = RequestUtils.getPathParamAsInt(context, "courseID");

      Promise<JsonArray> authorsPromise = Promise.promise();
      RestVerticle.db.listParticipants(courseID, List.of(Roles.AUTHOR.getName()), authorsPromise);

      authorsPromise.future().onSuccess(users -> {
        List<Future> removeAuthorFutures = new ArrayList<>();
        for (int i = 0; i < users.size(); i++) {
          Promise<Void> removeAuthorPromise = Promise.promise();
          int userID = users.getJsonObject(i).getInteger("idperson");
          RoleUtils.setRole(userID, courseID, Roles.PARTICIPANT, removeAuthorPromise);
          removeAuthorFutures.add(removeAuthorPromise.future());
        }

        CompositeFuture.all(removeAuthorFutures).onSuccess(res -> {
          ResponseUtils.okResponse(context, "All authors removed");
        }).onFailure(err -> ResponseUtils.internalServerErrorResponse(context, err));
      }).onFailure(err -> ResponseUtils.internalServerErrorResponse(context, err));
    });
  }

  public static void myCourseStats(RoutingContext context) {
    AuthorizationUtils.checkCourseAuthorization(context, Permission.VIEW_OWN_STATISTICS).onSuccess(course -> {
      int courseID = RequestUtils.getPathParamAsInt(context, "courseID");
      int userID = (int) context.data().get("userID");

      Promise<JsonObject> statsPromise = Promise.promise();
      RestVerticle.db.getMyCourseStatistics(courseID, userID, statsPromise);

      statsPromise.future().onSuccess(stats -> {
        ResponseUtils.jsonResponse(context, 200, stats);
      }).onFailure(err -> ResponseUtils.internalServerErrorResponse(context, err));
    });
  }
  public static void courseOverview(RoutingContext context) {
    AuthorizationUtils.checkCourseAuthorization(context, Permission.VIEW_RESULTS).onSuccess(course -> {
      int courseID = RequestUtils.getPathParamAsInt(context, "courseID");
      Promise<JsonArray> overviewPromise = Promise.promise();
      RestVerticle.db.getCurseOverview(courseID, overviewPromise);

      overviewPromise.future().onSuccess(overview -> {
        ResponseUtils.jsonResponse(context, 200, StatisticUtils.transformOverview(overview));
      }).onFailure(err -> ResponseUtils.internalServerErrorResponse(context, err));
    });
  }
  public static void coursesSessionStatistics(RoutingContext context) {
    AuthorizationUtils.checkCourseAuthorization(context, Permission.VIEW_STATISTICS).onSuccess(course -> {
      int courseID = RequestUtils.getPathParamAsInt(context, "courseID");
      int userID = (int) context.data().get("userID");

      Promise<JsonArray> sessionsPromise = Promise.promise();
      RestVerticle.db.listSessions(courseID, userID, false, sessionsPromise);

      sessionsPromise.future().onSuccess(sessions -> {
        JsonArray results = new JsonArray();
        List<Future> futures = new ArrayList<Future>();
        for (int i = 0; i < sessions.size(); i++) {
          JsonObject input = sessions.getJsonObject(i);
          Promise<JsonObject> sessionStatsPromise = Promise.promise();
          RestVerticle.db.getSessionStatistics(input.getInteger("idsession"), sessionStatsPromise);
          Future res = sessionStatsPromise.future().compose(sessionStats -> {
            JsonObject session = DBMapper.mapSession(input);
            JsonObject stats = StatisticUtils.calculateSessionStatistics(sessionStats);
            results.add(new JsonObject().put("session", session).put("stats", stats));
            return Future.succeededFuture();
          });
          futures.add(res);
        }
        CompositeFuture.all(futures).onSuccess(res -> {
          ResponseUtils.jsonResponse(context, 200, results);
        }).onFailure(err -> ResponseUtils.internalServerErrorResponse(context, err));
      }).onFailure(err -> ResponseUtils.internalServerErrorResponse(context, err));
    });
  }
}
