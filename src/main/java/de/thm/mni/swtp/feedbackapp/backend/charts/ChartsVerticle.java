package de.thm.mni.swtp.feedbackapp.backend.charts;

import de.thm.mni.swtp.feedbackapp.backend.services.ChartsService;
import de.thm.mni.swtp.feedbackapp.backend.services.DBService;
import de.thm.mni.swtp.feedbackapp.backend.services.DBServiceVertxEBProxy;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Promise;
import io.vertx.serviceproxy.ServiceBinder;

import static de.thm.mni.swtp.feedbackapp.backend.db.DatabaseVerticle.DATABASE_SERVICE_VERTX_ADDRESS;

public class ChartsVerticle extends AbstractVerticle {
    public static final String CHART_SERVICE_VERTX_ADDRESS = "feedbackapp.backend.charts";
    static DBService db;

    @Override
    public void start(Promise<Void> startPromise){
        db = new DBServiceVertxEBProxy(vertx, DATABASE_SERVICE_VERTX_ADDRESS);

        ChartController dbController = new ChartController(vertx);
        ServiceBinder serviceBinder = new ServiceBinder(vertx);
        serviceBinder
                .setAddress(CHART_SERVICE_VERTX_ADDRESS)
                .register(ChartsService.class, dbController);

        startPromise.complete();
    }
}
