package de.thm.mni.swtp.feedbackapp.backend.rest;

import de.thm.mni.swtp.feedbackapp.backend.fbsIntegration.FBSIntegrationVerticle;
import de.thm.mni.swtp.feedbackapp.backend.rest.JWTUtils.AuthTokenVerificationResult;
import io.vertx.config.ConfigRetriever;
import io.vertx.core.Future;
import io.vertx.core.Promise;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Set;

public class AuthController {
  static String fbsUsername;
  static String fbsPassword;
  private static final Logger LOGGER = LoggerFactory.getLogger(AuthController.class);

  private AuthController() {
  }

  private static final Set<String> dummyUsers = Set.of("user", "user1", "user2", "user3", "user4", "su",
      "user5", "user6", "user7", "user8", "user9", "user10", "user11", "user12");
  private static final Set<String> docentRoles = Set.of("tutor", "TUTOR", "docent", "DOCENT", "moderator", "MODERATOR", "admin", "ADMIN");

  public static void auth(RoutingContext context) {
    JsonObject jsonRequest = context.getBodyAsJson();

    String username;
    String password = jsonRequest.getString("password");

    boolean localLogin;
    if (jsonRequest.getString("username").startsWith("fbs_")) {
      username = jsonRequest.getString("username").substring(4);
      localLogin = true;
    } else {
      username = jsonRequest.getString("username");
      localLogin = false;
    }

    Promise<String> fbsLoginPromise = Promise.promise();
    if (username.startsWith("user") || username.equals("su")) {
     fbsLoginPromise.complete(null);
    } else {
      RestVerticle.fbsIntegration.getToken(username, password, localLogin, fbsLoginPromise);
    }

    fbsLoginPromise.future().compose(fbsToken -> {
      if (fbsToken == null && !(dummyUsers.contains(username) && password.equals("password") &&
                                    RestVerticle.demoUsersEnabled)) {
        ResponseUtils.unauthorizedResponse(context, "Invalid username or password");
        return Future.succeededFuture();
      }
      return fbsTokenToFbaToken(fbsToken, username).compose(jsonResponse -> {
        ResponseUtils.jsonResponse(context, 200, jsonResponse);
        return Future.succeededFuture();
      });
    }).onFailure(err -> ResponseUtils.internalServerErrorResponse(context, err));
  }

  public static void fbsAuth(RoutingContext context) {
    String fbsToken = RequestUtils.getBearerToken(context);
    if (fbsToken == null) {
      return;
    }
    String username = JWTUtils.decodeFBSToken(fbsToken).username;

    fbsTokenToFbaToken(fbsToken, username).onSuccess(jsonResponse -> {
      ResponseUtils.jsonResponse(context, 200, jsonResponse);
    }).onFailure(err -> ResponseUtils.internalServerErrorResponse(context, err));
  }

  public static void revoke(RoutingContext context) {
    long tokenID = (long) context.data().get("tokenID");

    Promise<Void> revokeTokenPromise = Promise.promise();
    RestVerticle.db.revokeToken(tokenID, revokeTokenPromise);

    revokeTokenPromise.future().onSuccess(res -> {
      ResponseUtils.okResponse(context, "token revoked");
    }).onFailure(err -> ResponseUtils.internalServerErrorResponse(context, err));
  }

  public static void verify(RoutingContext context) {
    String token = RequestUtils.getBearerToken(context);
    if (token == null) {
      return;
    }
    AuthTokenVerificationResult res = JWTUtils.verifyAuthToken(token);
    if (res == null) {
      ResponseUtils.unauthorizedResponse(context, "Invalid bearer token");
      return;
    }

    Promise<Boolean> checkRevokedPromise = Promise.promise();
    RestVerticle.db.checkRevoked(res.tokenID, checkRevokedPromise);

    checkRevokedPromise.future().onSuccess(revoked -> {
      if (revoked) {
        ResponseUtils.unauthorizedResponse(context, "Revoked bearer token");
        return;
      }

      context.data().put("tokenID", res.tokenID);
      context.data().put("username", res.username);
      context.data().put("firstname", res.firstname);
      context.data().put("lastname", res.lastname);
      context.data().put("userID", res.internalUserID);
      context.data().put("fbsID", res.fbsUserID);
      context.data().put("docent", res.docent);
      context.data().put("superUser", res.superUser);
      context.data().put("feedbackSystemToken", res.feedbackSystemToken);
      context.next();
    }).onFailure(err -> ResponseUtils.internalServerErrorResponse(context, err));
  }

  private static Future<JsonObject> fbsTokenToFbaToken(String fbsToken, String username) {
    Promise<JsonObject> userPromise = Promise.promise();
    RestVerticle.db.getPersonByUsername(username, userPromise);

    return userPromise.future().compose(user -> {
      Promise<JsonObject> newUserPromise = Promise.promise();
      if (user == null) {
        JWTUtils.FBSTokenDecodingResult fbsTokenDecoded = JWTUtils.decodeFBSToken(fbsToken);
        Promise<JsonObject> userInfos = Promise.promise();
        RestVerticle.fbsIntegration.getUserInfosFromFBS(fbsTokenDecoded.userID, userInfos);
        userInfos.future().compose(uinfos -> {
          Promise<Integer> idPromise = Promise.promise();
          RestVerticle.db.getPersonByFbsIdOrCreate(fbsTokenDecoded.userID, uinfos.getString("prename"),
              uinfos.getString("surname"), docentRoles.contains(fbsTokenDecoded.globalRole), idPromise);
          return idPromise.future();
        }).compose(id -> {
          Promise<Void> usernamePromise = Promise.promise();
          RestVerticle.db.setPersonUsername(id, fbsTokenDecoded.username, usernamePromise);
          return usernamePromise.future();
        }).onSuccess(res -> RestVerticle.db.getPersonByUsername(fbsTokenDecoded.username, newUserPromise)).onFailure(newUserPromise::fail);
      } else {
        newUserPromise.complete(user);
      }
      return newUserPromise.future();
    }).compose(user -> {
      String token = JWTUtils.genAuthToken(username,
          user.getString("firstname"),
          user.getString("lastname"),
          user.getInteger("idperson"),
          user.getInteger("fbsID"),
          user.getBoolean("docent"),
          user.getBoolean("superuser"),
          fbsToken
      );
      JsonObject jsonResponse = new JsonObject();
      jsonResponse.put("token", token);
      jsonResponse.put("user", DBMapper.mapUser(user));
      return Future.succeededFuture(jsonResponse);
    });
  }
}
