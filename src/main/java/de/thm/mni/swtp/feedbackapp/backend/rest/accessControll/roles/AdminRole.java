package de.thm.mni.swtp.feedbackapp.backend.rest.accessControll.roles;

import de.thm.mni.swtp.feedbackapp.backend.rest.accessControll.Permission;
import de.thm.mni.swtp.feedbackapp.backend.rest.accessControll.Role;

import java.util.Set;

public class AdminRole extends AuthorRole {
  public String getName() {
    return "admin";
  }

  @Override
  public Set<Permission> getPermissions() {
    Set<Permission> permissions = super.getPermissions();
    permissions.add(Permission.MANAGE_SESSIONS);
    permissions.add(Permission.LIST_USERS);
    permissions.add(Permission.VIEW_STATISTICS);
    return permissions;
  }
}
