package de.thm.mni.swtp.feedbackapp.backend.rest;

import io.vertx.core.Promise;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;

public class SuperUserController {
  public static void getCourses(RoutingContext context) {
    if (!enforceSuperUser(context)) {
      return;
    }
    Promise<JsonArray> coursesPromise = Promise.promise();
    RestVerticle.db.getAllCourses(coursesPromise);
    
    coursesPromise.future().onSuccess(courses -> { 
      ResponseUtils.jsonResponse(context, 200, DBMapper.mapMany(courses, DBMapper::mapCourse));
    }).onFailure(err -> ResponseUtils.internalServerErrorResponse(context, err));
  }

  public static void getQuizzes(RoutingContext context) {
    if (!enforceSuperUser(context)) {
      return;
    }
    Promise<JsonArray> quizzesPromise = Promise.promise();
    RestVerticle.db.getAllQuizzes(quizzesPromise);

    quizzesPromise.future().onSuccess(quizzes -> {
      ResponseUtils.jsonResponse(context, 200, DBMapper.mapMany(quizzes, DBMapper::mapQuiz));
    }).onFailure(err -> ResponseUtils.internalServerErrorResponse(context, err));
  }

  public static void getSessions(RoutingContext context) {
    if (!enforceSuperUser(context)) {
      return;
    }
    Promise<JsonArray> sessionsPromise = Promise.promise();
    RestVerticle.db.getAllSessions(sessionsPromise);

    sessionsPromise.future().onSuccess(sessions -> {
      ResponseUtils.jsonResponse(context, 200, DBMapper.mapMany(sessions, DBMapper::mapSession));
    }).onFailure(err -> ResponseUtils.internalServerErrorResponse(context, err));
  }

  public static void getUsers(RoutingContext context) {
    if (!enforceSuperUser(context)) {
      return;
    }
    Promise<JsonArray> usersPromise = Promise.promise();
    RestVerticle.db.getAllPersons(usersPromise);

    usersPromise.future().onSuccess(users -> {
      ResponseUtils.jsonResponse(context, 200, DBMapper.mapMany(users, DBMapper::mapUser));
    }).onFailure(err -> ResponseUtils.internalServerErrorResponse(context, err));
  }

  public static void makeDocent(RoutingContext context) {
    if (!enforceSuperUser(context)) {
      return;
    }
    String username = context.getBodyAsJson().getString("username");

    Promise<JsonObject> userPromise = Promise.promise();
    RestVerticle.db.getPersonByUsername(username, userPromise);

    userPromise.future().onSuccess(user -> {
      int userID = user.getInteger("idperson");
      Promise<Void> makeDocentPromise = Promise.promise();
      RestVerticle.db.setDocent(userID, true, makeDocentPromise);
      makeDocentPromise.future()
          .onSuccess(v -> ResponseUtils.okResponse(context, "user made to docent"))
          .onFailure(err -> ResponseUtils.internalServerErrorResponse(context, err));;
    }).onFailure(err -> ResponseUtils.internalServerErrorResponse(context, err));
  }

  public static void removeDocent(RoutingContext context) {
    if (!enforceSuperUser(context)) {
      return;
    }
    String username = context.pathParam("username");

    Promise<JsonObject> userPromise = Promise.promise();
    RestVerticle.db.getPersonByUsername(username, userPromise);

    userPromise.future().onSuccess(user -> {
      int userID = user.getInteger("idperson");
      Promise<Void> removeDocentPromise = Promise.promise();
      RestVerticle.db.setDocent(userID, false, removeDocentPromise);
      removeDocentPromise.future()
          .onSuccess(v -> ResponseUtils.okResponse(context, "user removed as docent"))
          .onFailure(err -> ResponseUtils.internalServerErrorResponse(context, err));;
    }).onFailure(err -> ResponseUtils.internalServerErrorResponse(context, err));
  }

  private static boolean enforceSuperUser(RoutingContext context) {
    boolean isSuperUser = (boolean) context.data().get("superUser");
    if (!isSuperUser) {
      ResponseUtils.forbiddenResponse(context, "Only a super user can use su routes");
    }
    return isSuperUser;
  }
}
