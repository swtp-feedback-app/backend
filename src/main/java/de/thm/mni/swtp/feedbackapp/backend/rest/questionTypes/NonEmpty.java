package de.thm.mni.swtp.feedbackapp.backend.rest.questionTypes;

import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

public abstract class NonEmpty extends QuestionType {
  private final JsonObject options;
  NonEmpty(JsonObject options) {
    this.options = options;
  }

  @Override
  public boolean validateAnswer(JsonArray givenAnswers) {
    if (options != null) {
      if (options.getInteger("max") < givenAnswers.size()) {
        return false;
      }
    }
    int invalidCount = 0;
    for (int i = 0; i < givenAnswers.size(); i++) {
      String answer = givenAnswers.getString(i);
      if (answer.trim().equals("")) {
        invalidCount++;
      }
    }
    return invalidCount != givenAnswers.size();
  }

  @Override
  public JsonArray getCorrectAnswers() {
    return new JsonArray();
  }

  @Override
  public Object getPossibleAnswers() {
    return options;
  }

  @Override
  public void validate() {
    if (options.getInteger("max") == null) {
      throw new IllegalArgumentException("max must not be null for non empty question");
    }
  }
}
