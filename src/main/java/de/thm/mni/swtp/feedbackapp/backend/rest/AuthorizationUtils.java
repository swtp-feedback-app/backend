package de.thm.mni.swtp.feedbackapp.backend.rest;

import de.thm.mni.swtp.feedbackapp.backend.rest.accessControll.Permission;
import de.thm.mni.swtp.feedbackapp.backend.rest.accessControll.Role;
import de.thm.mni.swtp.feedbackapp.backend.rest.accessControll.RoleUtils;
import io.vertx.core.Future;
import io.vertx.core.Promise;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import java.util.Date;

public class AuthorizationUtils {
  public static Future<JsonObject> checkQuizAuthorization(RoutingContext context, boolean preventShared) {
    int quizID = RequestUtils.getPathParamAsInt(context, "quizID");
    int userID = (int) context.data().get("userID");
    boolean isSuperUser = (boolean) context.data().get("superUser");
    Promise<JsonObject> quizPromise = Promise.promise();
    RestVerticle.db.getQuiz(quizID, quizPromise);

    return quizPromise.future().compose(quiz -> {
      if (quiz == null) {
        ResponseUtils.notFoundResponse(context, "Quiz not found");
        return Future.failedFuture("quiz not found");
      }
      Promise<Boolean> isSharedPromise = Promise.promise();
      RestVerticle.db.isSharedWith(quizID, userID, isSharedPromise);
      return isSharedPromise.future().compose(isSharedWith -> {
        boolean ok =  new AuthorizationChecker.Builder()
            .allowAdmin(!preventShared)
            .build(quiz.getInteger("quiz_creator_idperson"))
            .check(userID, isSuperUser, isSharedWith, false);
        if (!ok) {
          ResponseUtils.forbiddenResponse(context, "Quiz owned by another user");
          return Future.failedFuture("quiz not owned");
        }
        return Future.succeededFuture(quiz);
      });
    }, err -> {
      ResponseUtils.internalServerErrorResponse(context, err);
      return Future.failedFuture(err);
    });
  }
  public static Future<JsonObject> checkQuizAuthorization(RoutingContext context) {
    return checkQuizAuthorization(context, false);
  }
  public static Future<JsonObject> checkCourseAuthorization(RoutingContext context, Permission required, boolean preventFeedbackChanges) {
    int courseID = RequestUtils.getPathParamAsInt(context, "courseID");
    int userID = (int) context.data().get("userID");
    boolean isSuperUser = (boolean) context.data().get("superUser");
    Promise<JsonObject> coursePromise = Promise.promise();
    RestVerticle.db.getCourse(courseID, coursePromise);

    return coursePromise.future().compose(course -> {
      if (course == null) {
        ResponseUtils.notFoundResponse(context, "Course not found");
        return Future.failedFuture("course not found");
      }
      Promise<Role> rolePromise = Promise.promise();
      RoleUtils.getRole(userID, courseID, rolePromise);
      return rolePromise.future().compose(role -> {
        course.put("role", role.getName());
        if (!isSuperUser && !role.hasPermission(required)) {
          ResponseUtils.forbiddenResponse(context, "You are not authorized to perform this action");
          return Future.failedFuture("forbidden");
        }
        if (preventFeedbackChanges && course.getBoolean("fbs", false)) {
          ResponseUtils.forbiddenResponse(context, "Course imported from feedback system");
          return Future.failedFuture("feedback systemcourse");
        }
        return Future.succeededFuture(course);
      }, err -> {
        ResponseUtils.internalServerErrorResponse(context, err);
        return Future.failedFuture(err);
      });
    }, err -> {
      ResponseUtils.internalServerErrorResponse(context, err);
      return Future.failedFuture(err);
    });
  }
  public static Future<JsonObject> checkCourseAuthorization(RoutingContext context, Permission required) {
    return checkCourseAuthorization(context, required, false);
  }

  public static Future<JsonObject> checkSessionAuthorization(RoutingContext context) {
    int sessionID = RequestUtils.getPathParamAsInt(context, "sessionID");

    int tokenSessonID = (int) context.data().get("sessionID");

    Promise<JsonObject> sessionPromise = Promise.promise();
    RestVerticle.db.getSession(sessionID, sessionPromise);

    return sessionPromise.future().compose(session -> {
      if (session == null) {
        ResponseUtils.notFoundResponse(context, "session not found");
        return Future.failedFuture("session not found");
      }

      if (sessionID != tokenSessonID) {
        ResponseUtils.forbiddenResponse(context, "token and path session id differ");
        return Future.failedFuture("token and session missmatch");
      }

      Long now = new Date().getTime() / 1000;
      if (session.getLong("start") > now) {
        ResponseUtils.forbiddenResponse(context, "session has not started yet");
        return Future.failedFuture("session not started");
      }
      if (session.getLong("end") < now) {
        ResponseUtils.forbiddenResponse(context, "session has already ended");
        return Future.failedFuture("session already ended");
      }

      return Future.succeededFuture(session);
    }, err -> {
      ResponseUtils.internalServerErrorResponse(context, err);
      return Future.failedFuture(err);
    });
  }
  public static Future<JsonObject> checkUserSessionAuthorization(RoutingContext context, boolean allowCourseAdmins) {
    int sessionID = RequestUtils.getPathParamAsInt(context, "sessionID");
    int userID = (int) context.data().get("userID");
    boolean isSuperUser = (boolean) context.data().get("superUser");

    Promise<JsonObject> sessionPromise = Promise.promise();
    RestVerticle.db.getSession(sessionID, sessionPromise);

    return sessionPromise.future().compose(session -> {
      if (session == null) {
        ResponseUtils.notFoundResponse(context, "session not found");
        return Future.failedFuture("session not found");
      }

      int ownerID = session.getInteger("session_creator_idperson");
      boolean ok = new AuthorizationChecker.Builder().build(ownerID).check(userID, isSuperUser);
      if (ok) {
        return Future.succeededFuture(session);
      }

      JsonObject course = session.getJsonObject("course");
      if (course != null) {
          Promise<Role> rolePromise = Promise.promise();
          RoleUtils.getRole(userID, course.getInteger("idcourse"), rolePromise);
          return rolePromise.future().compose(role -> {
            int courseOwnerID = course.getJsonObject("docent").getInteger("idperson");
            boolean isAdmin = role.hasPermission(Permission.MANAGE_SESSIONS);
            boolean courseOk = new AuthorizationChecker.Builder().allowAdmin(allowCourseAdmins).build(courseOwnerID)
                .check(userID, isSuperUser, isAdmin, true);
            if (!courseOk) {
              ResponseUtils.forbiddenResponse(context, "session owned by another user");
              return Future.failedFuture("session not owned");
            }
            return Future.succeededFuture(session);
          }, err -> {
            ResponseUtils.internalServerErrorResponse(context, err);
            return Future.failedFuture(err);
          });
      }
      ResponseUtils.forbiddenResponse(context, "session owned by another user");
      return Future.failedFuture("session not owned");
    }, err -> {
      ResponseUtils.internalServerErrorResponse(context, err);
      return Future.failedFuture(err);
    });
  }
  private static boolean isAdmin(int userID, JsonArray admins) {
    for (int i = 0; i < admins.size(); i++) {
      int adminID = admins.getJsonObject(i).getInteger("idperson");
      if (adminID == userID) {
        return true;
      }
    }
    return false;
  }
}
