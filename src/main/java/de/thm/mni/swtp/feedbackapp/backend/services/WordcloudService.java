package de.thm.mni.swtp.feedbackapp.backend.services;

import io.vertx.codegen.annotations.ProxyGen;
import io.vertx.codegen.annotations.VertxGen;
import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;

@ProxyGen
@VertxGen
public interface WordcloudService {

    void getDataForWordcloud(int idsession, int questionNumber, Handler<AsyncResult<String>> wordcloudResult);

}
