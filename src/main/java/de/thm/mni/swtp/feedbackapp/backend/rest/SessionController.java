package de.thm.mni.swtp.feedbackapp.backend.rest;

import de.thm.mni.swtp.feedbackapp.backend.rest.JWTUtils.AuthTokenVerificationResult;
import de.thm.mni.swtp.feedbackapp.backend.rest.JWTUtils.SessionTokenVerificationResult;
import de.thm.mni.swtp.feedbackapp.backend.rest.questionTypes.QuestionType;
import io.vertx.core.CompositeFuture;
import io.vertx.core.Future;
import io.vertx.core.Promise;
import io.vertx.ext.web.RoutingContext;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class SessionController {
  private SessionController() {}

  public static void listSessions(RoutingContext context) {
    int userID = (int) context.data().get("userID");
    boolean hideEnded = RequestUtils.getQueryParamAsBoolean(context, "hideEnded", false);
    boolean hideNonStarted = RequestUtils.getQueryParamAsBoolean(context, "hideNonStarted", false);

    Promise<JsonArray> sessionPromise = Promise.promise();
    RestVerticle.db.listSessionsForUser(userID, hideEnded, hideNonStarted, sessionPromise);

    sessionPromise.future().onSuccess(sessions -> {
      JsonArray response = DBMapper.mapMany(sessions, DBMapper::mapSession);
      ResponseUtils.jsonResponse(context, 200, response);
    }).onFailure(err -> ResponseUtils.internalServerErrorResponse(context, err));

  }


  public static void getSession(RoutingContext context) {
    int sessionID = RequestUtils.getPathParamAsInt(context, "sessionID");

    Promise<JsonObject> sessionPromise = Promise.promise();
    RestVerticle.db.getSession(sessionID, sessionPromise);

    sessionPromise.future().onSuccess(session -> {
      if (session == null) {
        ResponseUtils.notFoundResponse(context, "session not found");
        return;
      }
      ResponseUtils.jsonResponse(context, 200,
          DBMapper.mapSession(
              session));
    }).onFailure(err -> ResponseUtils.internalServerErrorResponse(context, err));
  }

  public static void updateSession(RoutingContext context) {
    AuthorizationUtils.checkUserSessionAuthorization(context, false).onSuccess(session -> {
      int sessionID = RequestUtils.getPathParamAsInt(context, "sessionID");

      JsonObject jsonRequest = context.getBodyAsJson();
      Long start = jsonRequest.getLong("start");
      Long end = jsonRequest.getLong("end");
      String description = jsonRequest.getString("description");
      boolean showCorrect = jsonRequest.getBoolean("showCorrectAnswers", false);

      Promise<Void> updatePromise = Promise.promise();
      RestVerticle.db.updateSession(sessionID, start, end, description, showCorrect, updatePromise);

      updatePromise.future().onSuccess(res -> {
        JsonObject jsonResponse = DBMapper.mapSession(session)
            .put("start", start).put("end", end).put("description", description);
        ResponseUtils.jsonResponse(context, 200, jsonResponse);
      }).onFailure(err -> ResponseUtils.internalServerErrorResponse(context, err));
    });
  }

  public static void deleteSession(RoutingContext context) {
    AuthorizationUtils.checkUserSessionAuthorization(context, false).onSuccess(session -> {
      int sessionID = RequestUtils.getPathParamAsInt(context, "sessionID");

      Promise<Void> deletePromise = Promise.promise();
      RestVerticle.db.deleteSession(sessionID, deletePromise);

      deletePromise.future().onSuccess(res -> {
        ResponseUtils.okResponse(context, "session deleted");
      }).onFailure(err -> ResponseUtils.internalServerErrorResponse(context, err));
    });
  }

  public static void joinSession(RoutingContext context) {
    int sessionID = RequestUtils.getPathParamAsInt(context, "sessionID");
    JsonObject jsonRequest = context.getBodyAsJson();
    JsonObject jsonResponse = new JsonObject();

    Boolean anonym = jsonRequest.getBoolean("anonym");
    String pseudonym = jsonRequest.getString("pseudonym");

    Promise<JsonObject> sessionPromise = Promise.promise();
    RestVerticle.db.getSession(sessionID, sessionPromise);

    sessionPromise.future().onSuccess(session -> {
      if (session == null) {
        ResponseUtils.notFoundResponse(context, "session not found");
        return;
      }

      Long now = new Date().getTime() / 1000;
      if (session.getLong("start") > now) {
        ResponseUtils.forbiddenResponse(context, "session has not started yet");
        return;
      }
      if (session.getLong("end") < now) {
        ResponseUtils.forbiddenResponse(context, "session has already ended");
        return;
      }

      String userIdent;
      boolean identConfirmed = false;
      if (anonym != null && anonym) {
        if (!session.getBoolean("anonym")) {
          ResponseUtils.forbiddenResponse(context, "session does not allow anonym participation");
          return;
        }
        userIdent = "anonym-" + Utils.getRandomID();
      } else if (pseudonym != null) {
        //userIdent = pseudonym;
        ResponseUtils.badRequestResponse(context, "pseudonym not supported");
        return;
      } else {
        String token = RequestUtils.getBearerToken(context);
        if (token == null) {
          return;
        }
        AuthTokenVerificationResult result = JWTUtils.verifyAuthToken(token);
        if (result == null) {
          ResponseUtils.unauthorizedResponse(context, "Invalid bearer token");
          return;
        }
        userIdent = Integer.toString(result.internalUserID);
        identConfirmed = true;
      }
      final boolean finalIdentConfirmed = identConfirmed;


      Promise<Boolean> participantPromise = Promise.promise();
      JsonObject course = session.getJsonObject("course");
      if (course != null) {
        RestVerticle.db.isParticipant(course.getInteger("idcourse"), Integer.parseInt(userIdent),
            participantPromise);
      } else {
        participantPromise.complete(true);
      }

      participantPromise.future().onSuccess(particpant -> {
        if (!particpant) {
          ResponseUtils.forbiddenResponse(context, "you need to be a participant to join");
          return;
        }

        int quizID = session.getJsonObject("quiz").getInteger("idquiz");

        Promise<JsonObject> quizPromise = Promise.promise();
        RestVerticle.db.getQuiz(quizID, quizPromise);

        Future<Integer> continueFuture;
        if (finalIdentConfirmed) {
          continueFuture = quizPromise.future().compose(quiz -> {
            int questionCount = quiz.getInteger("question_count");
            Promise<Integer> answerCountPromise = Promise.promise();
            RestVerticle.db
                .countAnswersFromPerson(sessionID, Integer.parseInt(userIdent), answerCountPromise);
            return answerCountPromise.future().compose(answerCount -> {
              if (answerCount == questionCount) {
                return Future.succeededFuture(-1);
              }
              return Future.succeededFuture(answerCount);
            });
          });
        } else {
          continueFuture = Future.succeededFuture(0);
        }

        continueFuture.onSuccess(answerCount -> {
          if (answerCount < 0) {
            ResponseUtils.conflictResponse(context, "session already answered");
            return;
          }
          String token = JWTUtils.genSessionToken(userIdent, sessionID, quizID, finalIdentConfirmed,
              session.getLong("end"));
          jsonResponse.put("token", token);
          if (answerCount > 0) {
            jsonResponse.put("continue", answerCount);
          }
          ResponseUtils.jsonResponse(context, 200, jsonResponse);
        }).onFailure(err -> ResponseUtils.internalServerErrorResponse(context, err));
      }).onFailure(err -> ResponseUtils.internalServerErrorResponse(context, err));
    }).onFailure(err -> ResponseUtils.internalServerErrorResponse(context, err));
  }
  public static void getQuestions(RoutingContext context) {
    int sessionID = RequestUtils.getPathParamAsInt(context, "sessionID");

    Promise<JsonObject> sessionPromise = Promise.promise();
    RestVerticle.db.getSession(sessionID, sessionPromise);

    sessionPromise.future().compose(session -> {
      if (session == null) {
        ResponseUtils.notFoundResponse(context, "session not found");
        return Future.succeededFuture();
      }

      Promise<JsonArray> questionPromise = Promise.promise();
      RestVerticle.db.listQuestions(session.getJsonObject("quiz").getInteger("idquiz"), questionPromise);
      return questionPromise.future().onSuccess(questions -> {
        JsonArray newQuestions = DBMapper.mapMany(questions, DBMapper::mapQuestionWithoutAnswer);
        ResponseUtils.jsonResponse(context, 200, newQuestions);
      }).onFailure(err -> ResponseUtils.internalServerErrorResponse(context, err));
    }).onFailure(err -> ResponseUtils.internalServerErrorResponse(context, err));
  }
  public static void getQuestion(RoutingContext context) {
    int sessionID = RequestUtils.getPathParamAsInt(context, "sessionID");
    int questionNumber = RequestUtils.getPathParamAsInt(context, "questionNumber");

    Promise<JsonObject> sessionPromise = Promise.promise();
    RestVerticle.db.getSession(sessionID, sessionPromise);

    sessionPromise.future().compose(session -> {
      if (session == null) {
        ResponseUtils.notFoundResponse(context, "session not found");
        return Future.succeededFuture();
      }

      Promise<JsonObject> questionPromise = Promise.promise();
      RestVerticle.db.getQuestion(session.getJsonObject("quiz").getInteger("idquiz"), questionNumber, questionPromise);
      return questionPromise.future();
    }).onSuccess(question -> {
      if (question == null) {
        ResponseUtils.notFoundResponse(context, "question not found");
        return;
      }

      ResponseUtils.jsonResponse(context, 200, DBMapper.mapQuestionWithoutAnswer(question));
    }).onFailure(err -> ResponseUtils.internalServerErrorResponse(context, err));
  }
  public static void submitAnswer(RoutingContext context) {
    AuthorizationUtils.checkSessionAuthorization(context).onSuccess(session -> {
      int sessionID = RequestUtils.getPathParamAsInt(context, "sessionID");
      int questionNumber = RequestUtils.getPathParamAsInt(context, "questionNumber");

      String userIdentifier = (String) context.data().get("userIdentifier");
      boolean identifierConfirmed = (boolean) context.data().get("identifierConfirmed");
      int quizID = (int) context.data().get("quizID");

      JsonArray answers = context.getBodyAsJsonArray();

      Promise<JsonObject> quizPromise = Promise.promise();
      RestVerticle.db.getQuiz(quizID, quizPromise);

      quizPromise.future().onSuccess(quiz -> {
        Promise<JsonObject> questionPromise = Promise.promise();
        RestVerticle.db.getQuestion(quizID, questionNumber, questionPromise);
        questionPromise.future().onSuccess(question -> {
          if (question == null) {
            ResponseUtils.notFoundResponse(context, "question not found");
            return;
          }
          String type = question.getString("type");

          Promise<Boolean> isBad = Promise.promise();
          if (type.equals("open") || type.equals("wordCloud")) {
            List<Future> futures = new ArrayList<>();
            for (int i = 0; i < answers.size(); i++) {
              String answer = answers.getString(i);
              Promise<Boolean> isBadWordPromise = Promise.promise();
              RestVerticle.wordFilterService.isBadWord(answer, isBadWordPromise);
              futures.add(isBadWordPromise.future());
            }
            CompositeFuture.all(futures).onSuccess(res -> {
              for (Object isBadObject : res.list()) {
                if ((Boolean) isBadObject) {
                  isBad.complete(true);
                  return;
                }
              }
              isBad.complete(false);
            }).onFailure(isBad::fail);
          } else {
            isBad.complete(false);
          }

          isBad.future().onSuccess(bad -> {
            if (bad) {
              ResponseUtils.forbiddenResponse(context, "swear words are not allowed in answers");
              return;
            }

            QuestionType answerValidator = QuestionType.from(type, question.getValue("answers"));
            boolean correct = answerValidator.validateAnswer(answers);

            Promise<Void> answerPromise = Promise.promise();
            if (identifierConfirmed) {
              Integer userID = Integer.parseInt(userIdentifier);
              RestVerticle.db.submitAnswer(question.getInteger("id"), userID, null, sessionID,
                  correct, answers, 0, answerPromise);
            } else {
              RestVerticle.db.submitAnswer(question.getInteger("id"),null, userIdentifier, sessionID,
                  correct, answers, 0, answerPromise);
            }
            answerPromise.future().onSuccess(res -> {
              JsonObject response = new JsonObject();
              if (quiz.getBoolean("instant_feedback")) {
                response.put("correct", correct);
                response.put("correctAnswers", answerValidator.getCorrectAnswers());
              } else {
                response.put("correct", (Boolean) null);
              }
              ResponseUtils.jsonResponse(context, 200, response);
            }).onFailure(err -> ResponseUtils.internalServerErrorResponse(context, err));
          }).onFailure(err -> ResponseUtils.internalServerErrorResponse(context, err));
        }).onFailure(err -> ResponseUtils.internalServerErrorResponse(context, err));
      }).onFailure(err -> ResponseUtils.internalServerErrorResponse(context, err));
    });
  }
  public static void getMyStatistics(RoutingContext context) {
    String userIdentifier = (String) context.data().get("userIdentifier");
    Boolean identifierConfirmed = (Boolean) context.data().get("identifierConfirmed");
    if (identifierConfirmed != null && identifierConfirmed) {
      int userID = Integer.parseInt(userIdentifier);
      getMyStatisticsInternal(context, userID, null);
    } else {
      getMyStatisticsInternal(context, null, userIdentifier);
    }
  }
  public static void getMyStatisticsAuthed(RoutingContext context) {
    Integer userID = (Integer) context.data().get("userID");
    getMyStatisticsInternal(context, userID, null);
  }
  private static void getMyStatisticsInternal(RoutingContext context, Integer userID, String userIdentifier) {
    int sessionID = RequestUtils.getPathParamAsInt(context, "sessionID");

    Promise<JsonObject> statisticsPromise = Promise.promise();
    if (userID != null) {
      RestVerticle.db.getMyStatistics(sessionID, userID, null, statisticsPromise);
    } else {
      RestVerticle.db.getMyStatistics(sessionID, null, userIdentifier, statisticsPromise);
    }

    Promise<JsonObject> sessionPromise = Promise.promise();
    RestVerticle.db.getSession(RequestUtils.getPathParamAsInt(context, "sessionID"), sessionPromise);

    sessionPromise.future().onSuccess(session -> {
      if (session == null) {
        ResponseUtils.notFoundResponse(context, "session not found");
        return;
      }
      statisticsPromise.future().onSuccess(statistics -> {
        Promise<JsonArray> questionPromise = Promise.promise();
        RestVerticle.db.listQuestions(session.getJsonObject("quiz").getInteger("idquiz"), questionPromise);

        questionPromise.future().onSuccess(questions -> {
          statistics.put("questionCount", questions.size());
          ResponseUtils.jsonResponse(context, 200, statistics);
        }).onFailure(err -> ResponseUtils.internalServerErrorResponse(context, err));
      }).onFailure(err -> ResponseUtils.internalServerErrorResponse(context, err));
    }).onFailure(err -> ResponseUtils.internalServerErrorResponse(context, err));
  }
  public static void getSessionStatistics(RoutingContext context) {
    AuthorizationUtils.checkUserSessionAuthorization(context, true).onSuccess(session -> {
      int sessionID = RequestUtils.getPathParamAsInt(context, "sessionID");

      Promise<JsonObject> statisticsPromise = Promise.promise();
      RestVerticle.db.getSessionStatistics(sessionID, statisticsPromise);

      statisticsPromise.future().onSuccess(statistics -> {
        ResponseUtils.jsonResponse(context, 200,
            StatisticUtils.calculateSessionStatistics(statistics));
      }).onFailure(err -> ResponseUtils.internalServerErrorResponse(context, err));
    });
  }
  public static void getQuestionStatistics(RoutingContext context) {
    AuthorizationUtils.checkUserSessionAuthorization(context, true).onSuccess(session -> {
      int sessionID = RequestUtils.getPathParamAsInt(context, "sessionID");
      int questionNumber = RequestUtils.getPathParamAsInt(context, "questionNumber");

      Promise<JsonObject> questionPromise = Promise.promise();
      RestVerticle.db.getQuestion(session.getJsonObject("quiz").getInteger("idquiz"), questionNumber, questionPromise);

      questionPromise.future().onSuccess(question -> {
        if (question == null) {
          ResponseUtils.notFoundResponse(context, "question not found");
          return;
        }

        Promise<JsonObject> statisticsPromise = Promise.promise();
        RestVerticle.db.getQuestionStatistics(sessionID, question.getInteger("id"), statisticsPromise);

        statisticsPromise.future().onSuccess(statistics -> {
          ResponseUtils.jsonResponse(context, 200, statistics);
        }).onFailure(err -> ResponseUtils.internalServerErrorResponse(context, err));
      }).onFailure(err -> ResponseUtils.internalServerErrorResponse(context, err));
    });
  }
  public static void getResultStatistics(RoutingContext context) {
    AuthorizationUtils.checkUserSessionAuthorization(context, true).onSuccess(session -> {
      int sessionID = RequestUtils.getPathParamAsInt(context, "sessionID");

      Promise<JsonArray> questionPromise = Promise.promise();
      RestVerticle.db.listQuestions(session.getJsonObject("quiz").getInteger("idquiz"), questionPromise);

      questionPromise.future().onSuccess(questions -> {
        Promise<JsonArray> statisticsPromise = Promise.promise();
        StatisticUtils.calculateResultStatistics(questions, sessionID, statisticsPromise);

        statisticsPromise.future().onSuccess(statistics -> {
          ResponseUtils.jsonResponse(context, 200, DBMapper.mapMany(statistics, DBMapper::mapStatistics));
        }).onFailure(err -> ResponseUtils.internalServerErrorResponse(context, err));
      }).onFailure(err -> ResponseUtils.internalServerErrorResponse(context, err));
    });
  }
  public static void getCorrect(RoutingContext context) {
    int sessionID = RequestUtils.getPathParamAsInt(context, "sessionID");
    int questionNumber = RequestUtils.getPathParamAsInt(context, "questionNumber");

    int userID = (int) context.data().get("userID");

    Promise<JsonObject> correctPromise = Promise.promise();
    RestVerticle.db.getCorrect(userID, sessionID, questionNumber, correctPromise);

    correctPromise.future().onSuccess(res -> {
      if (res == null) {
        ResponseUtils.notFoundResponse(context, "session or question not found");
        return;
      }
      if (!res.getBoolean("show_correct")) {
        ResponseUtils.forbiddenResponse(context, "display of correct answers not enabled for this session");
        return;
      }
      JsonObject question = DBMapper.mapQuestion(res);
      JsonObject given = res.getJsonObject("given");
      Boolean correct = given.getBoolean("correct");
      if (correct == null) {
        correct = false;
      }
      JsonArray answer = given.getJsonArray("given_answers");
      if (answer == null) {
        answer = new JsonArray();
      }
      ResponseUtils.jsonResponse(context, 200, new JsonObject()
          .put("question", question).put("correct", correct).put("answer", answer));
    }).onFailure(err -> ResponseUtils.internalServerErrorResponse(context, err));
  }
  public static void verify(RoutingContext context) {
    String token = RequestUtils.getBearerToken(context);
    if (token == null) {
      return;
    }
    SessionTokenVerificationResult result = JWTUtils.verifySessionToken(token);
    if (result == null) {
      ResponseUtils.unauthorizedResponse(context, "Invalid bearer token");
      return;
    }
    context.data().put("userIdentifier", result.userIdentifier);
    context.data().put("identifierConfirmed", result.identifierConfirmed);
    context.data().put("sessionID", result.sessionID);
    context.data().put("quizID", result.quizID);
    context.next();
  }
}
