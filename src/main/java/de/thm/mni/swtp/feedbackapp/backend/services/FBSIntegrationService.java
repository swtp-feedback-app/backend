package de.thm.mni.swtp.feedbackapp.backend.services;

import io.vertx.codegen.annotations.ProxyGen;
import io.vertx.codegen.annotations.VertxGen;
import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.json.JsonObject;

@ProxyGen
@VertxGen
public interface FBSIntegrationService {
    void getToken(String username, String password, boolean localLogin, Handler<AsyncResult<String>> result);
    void getAndUpdateCourses();
    void getUserInfosFromFBS(int userID, Handler<AsyncResult<JsonObject>> result);
}
