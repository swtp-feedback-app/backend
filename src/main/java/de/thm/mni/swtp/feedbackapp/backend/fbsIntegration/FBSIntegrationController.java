package de.thm.mni.swtp.feedbackapp.backend.fbsIntegration;

import de.thm.mni.swtp.feedbackapp.backend.services.FBSIntegrationService;
import io.vertx.core.*;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.client.HttpResponse;
import io.vertx.ext.web.client.WebClient;
import io.vertx.ext.web.client.WebClientOptions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class FBSIntegrationController implements FBSIntegrationService {
  private static final Logger LOGGER = LoggerFactory.getLogger(FBSIntegrationController.class);
  private final WebClient client;

  FBSIntegrationController(Vertx vertx) {
    WebClientOptions options = new WebClientOptions().setSsl(true);
    if (FBSIntegrationVerticle.insecureTlsTrustAll) {
          options.setTrustAll(true);
          options.setVerifyHost(false);
    }

    client = WebClient.create(vertx, options);
  }

  @Override
  public void getToken(String username, String password, boolean localLogin, Handler<AsyncResult<String>> result) {
    JsonObject jsonRequest = new JsonObject()
        .put("username", username)
        .put("password", password);

    String url_lastPart = localLogin ? "local" : "ldap";
    client.post(443, FBSIntegrationVerticle.fbsHostname, "/api/v1/login/" + url_lastPart)
        .ssl(true)
        .putHeader("Accept", "application/json")
        .putHeader("Content-Type", "application/json")
        .sendJsonObject(jsonRequest, jr -> {
          if (jr.succeeded()) {
            LOGGER.debug("Received response with status code {}", jr.result().statusCode());
            HttpResponse<Buffer> res = jr.result();
            if (res.statusCode() != 200) {
              result.handle(Future.succeededFuture(null));
              return;
            }

            HttpResponse<Buffer> response = jr.result();
            JsonObject responseBody = response.bodyAsJsonObject();
            if (responseBody != null) {
              if (!responseBody.getBoolean("success", false)) {
                result.handle(Future.succeededFuture(null));
                return;
              }
            }

            String authorizationHeader = response.getHeader("Authorization");
            String[] authorizationHeaderSplit = authorizationHeader.split(" ");
            if (authorizationHeaderSplit.length != 2 || !authorizationHeaderSplit[0].equals("Bearer")) {
              result.handle(Future.succeededFuture(null));
              return;
            }
            LOGGER.debug("FBSIntegration succeeded");
            result.handle(Future.succeededFuture(authorizationHeaderSplit[1]));
          } else {
            result.handle(Future.failedFuture(jr.cause()));
          }
        });
  }

  @Override
  public void getAndUpdateCourses() {
    Promise<String> fbsLoginPromise = Promise.promise();
    Promise<JsonArray> oldCourses = Promise.promise();

    Date date = new Date();
    List<Integer> summerCourses = List.of(4, 5, 6, 7, 8, 9);
    String year = date.toString().split(" ")[5];
    int month = date.getMonth();
    String semester = summerCourses.contains(month) ? "sose" + year : "wise" + year;

    getToken(FBSIntegrationVerticle.fbsUsername, FBSIntegrationVerticle.fbsPassword, true, fbsLoginPromise);
    FBSIntegrationVerticle.db.browseCourse(null, 10000, 0, null, oldCourses);

    fbsLoginPromise.future().onSuccess(fbsToken -> {
      oldCourses.future().onSuccess(listOfOldCourses -> {
        if (fbsToken != null) {
          client.get(443, FBSIntegrationVerticle.fbsHostname, "/api/v1/courses/")
              .ssl(true)
              .putHeader("Accept", "application/json")
              .putHeader("Content-Type", "application/json")
              .putHeader("Authorization", "Bearer " + fbsToken.trim())
              .send(ar -> {
                if (ar.succeeded()) {
                  HttpResponse<Buffer> response = ar.result();
                  LOGGER.debug("Received response with status code {}", response.statusCode());
                  JsonArray listOfCourses = response.bodyAsJsonArray();

                  if (listOfCourses != null && listOfOldCourses != null) {

                    for (int i = 0; i < listOfCourses.size(); i++) {
                      JsonObject tmpJson = listOfCourses.getJsonObject(i);

                      Promise<Integer> personIDPromise = Promise.promise();
                      getDocentForCourse(tmpJson.getInteger("id"), fbsToken, personIDPromise);

                      personIDPromise.future().onSuccess(personID -> {
                        if (tmpJson.getBoolean("visible")) {
                          Promise<Void> courseReadyPromise = Promise.promise();
                          if (isCourseAlreadyInside(listOfOldCourses, tmpJson)) {
                            LOGGER.debug("Updates Courses in the DB");

                            FBSIntegrationVerticle.db.updateCourse(
                                tmpJson.getInteger("id"),
                                tmpJson.getString("name"),
                                tmpJson.getString("description"),
                                semester,
                                personID,
                                null,
                                true,
                                false,
                                courseReadyPromise);
                          } else {
                            LOGGER.debug("Insert new Courses in the DB");
                            FBSIntegrationVerticle.db.createCourse(
                                tmpJson.getInteger("id"),
                                tmpJson.getString("name"),
                                tmpJson.getString("description"),
                                semester,
                                personID,
                                null,
                                true,
                                false,
                                courseReadyPromise);
                          }
                          courseReadyPromise.future().compose(res ->
                              importRoles(tmpJson.getInteger("id"), fbsToken)
                          ).onFailure(err -> LOGGER.error("Error during role import: {}", err.getMessage()));
                        } else {
                          LOGGER.info("Course cannot create or update because is not visible\n");
                        }
                      }).onFailure(System.out::println);
                    }
                  }

                } else {
                  LOGGER.error("Something went wrong {}", ar.cause().getMessage());
                }
              });
        } else {
          LOGGER.error("fbs Token is NULL");
        }


      }).onFailure(err -> {
        LOGGER.error("Something went wrong {}", err.getMessage());
      });


    }).onFailure(err -> {
      LOGGER.error("Something went wrong {}", err.getMessage());
    });

  }

  public void getDocentForCourse(int courseID, String fbsToken, Handler<AsyncResult<Integer>> result) {
      client.get(443, FBSIntegrationVerticle.fbsHostname, "/api/v1/courses/" + courseID + "/participants")
          .ssl(true)
          .putHeader("Accept", "application/json")
          .putHeader("Content-Type", "application/json")
          .putHeader("Authorization", "Bearer " + fbsToken.trim())
          .send(ar -> {
              if (ar.succeeded()) {
                  JsonArray users = ar.result().bodyAsJsonArray();
                  JsonObject docent = null;
                  for (int i = 0; i < users.size(); i++) {
                      JsonObject user = users.getJsonObject(i);
                      if (user.getJsonObject("role").getString("value").equals("DOCENT")) {
                          docent = user;
                          break;
                      }
                  }
                  if (docent == null) {
                      result.handle(Future.failedFuture("course does not have a docent"));
                      return;
                  }

                  JsonObject docentUser = docent.getJsonObject("user");
                  int userID = docentUser.getInteger("id");
                  FBSIntegrationVerticle.db.getPersonByFbsIdOrCreate(userID,
                      docentUser.getString("prename"), docentUser.getString("surname"), true, result);
              } else {
                  result.handle(Future.failedFuture(ar.cause()));
              }
          });
  }

  @Override
  public void getUserInfosFromFBS(int userID, Handler<AsyncResult<JsonObject>> result) {
    if (FBSIntegrationVerticle.fbsUsername == null || FBSIntegrationVerticle.fbsPassword == null) {
      LOGGER.warn("FBS_ADMIN_USERNAME or FBS_ADMIN_PASSWORD not set. Won't be able to add a new user!");
      result.handle(Future.failedFuture("FBS_ADMIN_USERNAME and FBS_ADMIN_PASSWORD required"));
      return;
    }


    Promise<String> tokenPromise = Promise.promise();

    this.getToken(FBSIntegrationVerticle.fbsUsername, FBSIntegrationVerticle.fbsPassword, true, tokenPromise);

    tokenPromise.future().compose(token -> {
      if (userID <= 0) {
        result.handle(Future.failedFuture("invalid userid"));
      }

      Promise<HttpResponse<Buffer>> responsePromise = Promise.promise();
      client.get(443, FBSIntegrationVerticle.fbsHostname, "/api/v1/users/" + userID)
          .ssl(true)
          .putHeader("Authorization", "Bearer " + token.trim())
          .send(responsePromise);
      return responsePromise.future();
    }).onSuccess(response -> {
      if (response.statusCode() != 200) {
        result.handle(Future.failedFuture("unexpected status code: " + response.statusCode()));
        return;
      }
      JsonObject responseBody = response.bodyAsJsonObject();
      result.handle(Future.succeededFuture(responseBody));
    }).onFailure(err -> result.handle(Future.failedFuture(err)));
  }

  private boolean isCourseAlreadyInside(JsonArray listOfCourses, JsonObject course) {
    boolean res = false;
    for (int i = 0; i < listOfCourses.size(); i++) {
      if (listOfCourses.getJsonObject(i) != null && course != null) {
        if (listOfCourses.getJsonObject(i).getInteger("idcourse").equals(course.getInteger("id")))
          res = true;
      }
    }
    return res;
  }

  private Future<Void> importRoles(int courseID, String token) {
    Promise<HttpResponse<Buffer>> responsePromise = Promise.promise();

    client.get(443, FBSIntegrationVerticle.fbsHostname, "/api/v1/courses/" + courseID + "/participants")
        .ssl(true)
        .putHeader("Authorization", "Bearer " + token.trim())
        .send(responsePromise);

    return responsePromise.future().compose(res -> {
      JsonArray body = res.bodyAsJsonArray();

      List<Future> futures = new ArrayList<>();
      for (int i = 0; i < body.size(); i++) {
        JsonObject roleObject = body.getJsonObject(i);
        JsonObject user = roleObject.getJsonObject("user");
        String role = roleObject.getJsonObject("role").getString("value");

        Promise<Integer> personPromise = Promise.promise();
        FBSIntegrationVerticle.db.getPersonByFbsIdOrCreate(user.getInteger("id"),
            user.getString("prename"), user.getString("surname"),
            user.getString("globalRole").equals("ADMIN"), personPromise);
        Future<Void> future = personPromise.future().compose(personID -> {
          Promise<Void> setRolePromise = Promise.promise();
          FBSIntegrationVerticle.db.setRole(personID, courseID, convertRole(role), setRolePromise);
          return setRolePromise.future();
        });
        futures.add(future);
      }

      return CompositeFuture.all(futures).compose(v -> Future.succeededFuture());
    });
  }

  private String convertRole(String role) {
    switch (role) {
      case "STUDENT":
        return "participant";
      case "TUTOR":
        return "admin";
      case "DOCENT":
        return "lecturer";
      default:
        LOGGER.warn("Unknown fbs role \"" + role + "\" defaulting to participant");
        return "participant";
    }
  }
}
