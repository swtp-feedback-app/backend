package de.thm.mni.swtp.feedbackapp.backend.rest;

import de.thm.mni.swtp.feedbackapp.backend.charts.ChartsVerticle;
import de.thm.mni.swtp.feedbackapp.backend.db.DatabaseVerticle;
import de.thm.mni.swtp.feedbackapp.backend.fbsIntegration.FBSIntegrationVerticle;
import de.thm.mni.swtp.feedbackapp.backend.services.ChartsService;
import de.thm.mni.swtp.feedbackapp.backend.services.ChartsServiceVertxEBProxy;
import de.thm.mni.swtp.feedbackapp.backend.services.DBService;
import de.thm.mni.swtp.feedbackapp.backend.services.DBServiceVertxEBProxy;
import de.thm.mni.swtp.feedbackapp.backend.services.FBSIntegrationService;
import de.thm.mni.swtp.feedbackapp.backend.services.FBSIntegrationServiceVertxEBProxy;
import de.thm.mni.swtp.feedbackapp.backend.services.WordFilterService;
import de.thm.mni.swtp.feedbackapp.backend.services.WordFilterServiceVertxEBProxy;
import de.thm.mni.swtp.feedbackapp.backend.services.WordcloudService;
import de.thm.mni.swtp.feedbackapp.backend.services.WordcloudServiceVertxEBProxy;
import de.thm.mni.swtp.feedbackapp.backend.wordcloud.WordCloudVerticle;
import de.thm.mni.swtp.feedbackapp.backend.wordfilter.WordFilterVerticle;
import io.vertx.config.ConfigRetriever;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Promise;
import io.vertx.core.http.HttpServer;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.api.contract.openapi3.OpenAPI3RouterFactory;
import io.vertx.core.Handler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RestVerticle extends AbstractVerticle {

  private static final Logger LOGGER = LoggerFactory.getLogger(RestVerticle.class);

  private OpenAPI3RouterFactory routerFactory;
  private int port;

  public static DBService db;
  static FBSIntegrationService fbsIntegration;
  static WordFilterService wordFilterService;
  static ChartsService chartService;
  static WordcloudService wordCloudService;
  static boolean internCoursesDisabled;
  static boolean demoUsersEnabled;

  @Override
  public void start(Promise<Void> startPromise) {
    HttpServer server = vertx.createHttpServer();

    Promise<JsonObject> configPromise = Promise.promise();
    ConfigRetriever.create(vertx).getConfig(configPromise);
    configPromise.future().compose(config -> {
      this.port = config.getInteger("PORT", 8080);
      db = new DBServiceVertxEBProxy(vertx, DatabaseVerticle.DATABASE_SERVICE_VERTX_ADDRESS);
      fbsIntegration = new FBSIntegrationServiceVertxEBProxy(vertx, FBSIntegrationVerticle.FBS_INTEGRATION_SERVICE_VERTX_ADDRESS);
      wordFilterService = new WordFilterServiceVertxEBProxy(vertx, WordFilterVerticle.WORDFILTER_SERVICE_VERTX_ADDRESS);
      chartService = new ChartsServiceVertxEBProxy(vertx, ChartsVerticle.CHART_SERVICE_VERTX_ADDRESS);
      wordCloudService = new WordcloudServiceVertxEBProxy(vertx, WordCloudVerticle.WORDCLOUD_SERVICE_VERTX_ADDRESS);
      JWTUtils.setSecret(config.getString("JWT_SECRET"));
      JWTUtils.setAuthTokenExpiration(config.getString("JWT_AUTH_TOKEN_EXPIRATION"));
      JWTUtils.setSessionTokenExpiration(config.getString("JWT_SESSION_TOKEN_EXPIRATION"));
      internCoursesDisabled = config.getBoolean("INTERN_COURSES_DISABLED", false);
      demoUsersEnabled = !config.getBoolean("DEMO_USERS_DISABLED", false);
      Promise<OpenAPI3RouterFactory> routerPromise = Promise.promise();
      OpenAPI3RouterFactory.create(vertx, "api-docs/openapi.yaml", routerPromise);
      return routerPromise.future();
    }).compose(routerFactory -> {
      this.routerFactory = routerFactory;
      setupRoutes();
      Router router = routerFactory.getRouter();
      router.get("/api/health").handler(context -> ResponseUtils.okResponse(context, "Ok"));
      Promise<HttpServer> listenPromise = Promise.promise();
      server
          .requestHandler(router)
          .listen(port, listenPromise);
      return listenPromise.future();
    }).onFailure(err -> {
      LOGGER.error("Could not start a HTTP server", err);
      startPromise.fail(err);
    }).onSuccess(result -> {
      LOGGER.info("HTTP server running on port {}", port);
      startPromise.complete();
    });
  }

  private void setupRoutes() {
    routerFactory.addSecurityHandler("userBearer", AuthController::verify);
    routerFactory.addSecurityHandler("sessionBearer", SessionController::verify);
    routerFactory.addGlobalHandler(this::timeoutHandler);

    setupRoute("auth", AuthController::auth);
    setupRoute("revoke", AuthController::revoke);
    setupRoute("fbsAuth", AuthController::fbsAuth);

    setupRoute("listQuizzes", QuizController::listQuizzes);
    setupRoute("createQuiz", QuizController::createQuiz);
    setupRoute("deleteQuiz", QuizController::deleteQuiz);
    setupRoute("getQuiz", QuizController::getQuiz);
    setupRoute("updateQuiz", QuizController::updateQuiz);
    setupRoute("listQuizSessions", QuizController::listQuizSessions);
    setupRoute("createQuizSession", QuizController::createQuizSession);
    setupRoute("shareQuiz", QuizController::shareQuiz);
    setupRoute("unShareQuiz", QuizController::unShareQuiz);

    setupRoute("listQuizQuestions", QuestionController::listQuestions);
    setupRoute("createQuizQuestion", QuestionController::createQuestion);
    setupRoute("deleteQuizQuestion", QuestionController::deleteQuestion);
    setupRoute("getQuizQuestion", QuestionController::getQuestion);
    setupRoute("updateQuizQuestion", QuestionController::updateQuestion);

    setupRoute("listSessions", SessionController::listSessions);
    setupRoute("getSession", SessionController::getSession);
    setupRoute("updateSession", SessionController::updateSession);
    setupRoute("deleteSession", SessionController::deleteSession);
    setupRoute("joinSession", SessionController::joinSession);
    setupRoute("getSessionQuestions", SessionController::getQuestions);
    setupRoute("getSessionQuestion", SessionController::getQuestion);
    setupRoute("answerSessionQuestion", SessionController::submitAnswer);
    setupRoute("getMyStatistics", SessionController::getMyStatistics);
    setupRoute("getMyStatisticsAuthed", SessionController::getMyStatisticsAuthed);
    setupRoute("getSessionStatistics", SessionController::getSessionStatistics);
    setupRoute("getSessionQuestionStatistics", SessionController::getQuestionStatistics);
    setupRoute("getResultStatistics", SessionController::getResultStatistics);
    setupRoute("getCorrect", SessionController::getCorrect);

    setupRoute("browseCourses", CourseController::browseCourses);
    setupRoute("createCourse", CourseController::createCourse);
    setupRoute("getCourse", CourseController::getCourse);
    setupRoute("updateCourse", CourseController::updateCourse);
    setupRoute("deleteCourse", CourseController::deleteCourse);
    setupRoute("joinCourse", CourseController::joinCourse);
    setupRoute("leaveCourse", CourseController::leaveCourse);
    setupRoute("addSessionToCourse", CourseController::addSession);
    setupRoute("listCourseSessions", CourseController::listSessions);
    setupRoute("listUsers", CourseController::listUsers);
    setupRoute("setUserRole", CourseController::setUserRole);
    setupRoute("addAllToAuthors", CourseController::addAllToAuthors);
    setupRoute("removeAllFromAuthors", CourseController::removeAllFromAuthors);
    setupRoute("myCourseStats", CourseController::myCourseStats);
    setupRoute("courseOverview", CourseController::courseOverview);
    setupRoute("coursesSessionStatistics", CourseController::coursesSessionStatistics);

    setupRoute("aboutMe", UserController::me);
    setupRoute("myStats", UserController::myStats);
    setupRoute("listMyCourses", UserController::myCourses);
    setupRoute("listMySessions", UserController::mySessions);
    setupRoute("listToDoSessions", UserController::toDoSessions);
    setupRoute("sharedWithMe", UserController::sharedWithMe);

    setupRoute("getChartForSession", ChartController::getChartForSession);
    setupRoute("getChartForCourse", ChartController::getChartForCourse);
    setupRoute("getWordCloud", ChartController::getWordCloud);

    setupRoute("suCourses", SuperUserController::getCourses);
    setupRoute("suQuizzes", SuperUserController::getQuizzes);
    setupRoute("suSessions", SuperUserController::getSessions);
    setupRoute("suUsers", SuperUserController::getUsers);
    setupRoute("suMakeDocent", SuperUserController::makeDocent);
    setupRoute("suRemoveDocent", SuperUserController::removeDocent);

  }

  private void setupRoute(String operationID, Handler<RoutingContext> handler) {
    this.routerFactory.addHandlerByOperationId(operationID, handler);
    this.routerFactory.addFailureHandlerByOperationId(operationID, FailureHandler::validationErrorHandler);
  }

  private void timeoutHandler(RoutingContext context) {
    context.next();
    vertx.setTimer(10000, t -> {
      if (!context.response().headWritten()) {
        ResponseUtils.internalServerErrorResponse(context, "timeout");
      }
    });
  }
}
