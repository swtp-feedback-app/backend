package de.thm.mni.swtp.feedbackapp.backend.services;

import io.vertx.codegen.annotations.ProxyGen;
import io.vertx.codegen.annotations.VertxGen;
import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.json.JsonArray;

@ProxyGen
@VertxGen
public interface WordFilterService {
    void isBadWord(String input, Handler<AsyncResult<Boolean>> result);
}
