package de.thm.mni.swtp.feedbackapp.backend.rest.accessControll;

public enum Permission {
  DO_SESSIONS, CREATE_SESSIONS, MANAGE_SESSIONS,
  VIEW_OWN_STATISTICS, VIEW_STATISTICS, VIEW_RESULTS,
  LEAVE_COURSE, LIST_USERS, MANAGE_USERS, MANAGE_COURSE,
}
