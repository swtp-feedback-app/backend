package de.thm.mni.swtp.feedbackapp.backend.rest.accessControll.roles;

import de.thm.mni.swtp.feedbackapp.backend.rest.accessControll.Permission;
import de.thm.mni.swtp.feedbackapp.backend.rest.accessControll.Role;

import java.util.Set;

public class LecturerRole extends AdminRole {
  public String getName() {
    return "lecturer";
  }

  @Override
  public Set<Permission> getPermissions() {
    Set<Permission> permissions = super.getPermissions();
    permissions.add(Permission.VIEW_RESULTS);
    permissions.add(Permission.MANAGE_USERS);
    permissions.add(Permission.MANAGE_COURSE);
    return permissions;
  }
}
