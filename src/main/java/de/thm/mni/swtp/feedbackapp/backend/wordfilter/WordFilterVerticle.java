package de.thm.mni.swtp.feedbackapp.backend.wordfilter;

import de.thm.mni.swtp.feedbackapp.backend.db.DatabaseVerticle;
import de.thm.mni.swtp.feedbackapp.backend.services.DBService;
import de.thm.mni.swtp.feedbackapp.backend.services.DBServiceVertxEBProxy;
import de.thm.mni.swtp.feedbackapp.backend.services.WordFilterService;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Promise;
import io.vertx.core.json.JsonArray;
import io.vertx.serviceproxy.ServiceBinder;

public class WordFilterVerticle extends AbstractVerticle {

  public static final String WORDFILTER_SERVICE_VERTX_ADDRESS = "feedbackapp.backend.wortfilter";

  @Override
  public void start(Promise<Void> startPromise) {
    DBService db = new DBServiceVertxEBProxy(vertx,
        DatabaseVerticle.DATABASE_SERVICE_VERTX_ADDRESS);

    Promise<JsonArray> badWordsPromise = Promise.promise();
    db.getBadWords(badWordsPromise);
    badWordsPromise.future().onSuccess(badWordList -> {
      BadWordController badWordController = new BadWordController(badWordList);
      ServiceBinder serviceBinder = new ServiceBinder(vertx);
      serviceBinder
          .setAddress(WORDFILTER_SERVICE_VERTX_ADDRESS)
          .register(WordFilterService.class, badWordController);

      startPromise.complete();
    }).onFailure(startPromise::fail);
  }
}
