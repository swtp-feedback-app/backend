package de.thm.mni.swtp.feedbackapp.backend.db;

import io.vertx.core.AsyncResult;
import io.vertx.core.CompositeFuture;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.Promise;
import io.vertx.core.Vertx;
import io.vertx.core.buffer.Buffer;
import io.vertx.pgclient.PgConnectOptions;
import io.vertx.pgclient.PgPool;
import io.vertx.sqlclient.PoolOptions;
import io.vertx.sqlclient.Row;
import io.vertx.sqlclient.RowSet;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MigrationController {
  private static final Logger LOGGER = LoggerFactory.getLogger(MigrationController.class);

  public static void migrate(Vertx vertx, PgConnectOptions connectOptions, PoolOptions poolOptions, Handler<AsyncResult<Void>> result) {
    PgPool client = PgPool.pool(vertx, connectOptions, poolOptions);
    LOGGER.debug("Database Migration started");
    Promise<RowSet<Row>> queryPromise = Promise.promise();
    client.query("SELECT migration_number FROM _migrations").execute(queryPromise);
    queryPromise.future().compose(res -> {
      int migrationNumber = res.iterator().next().getInteger(0);
      LOGGER.debug("Migration current number found: {}", migrationNumber);
      return Future.succeededFuture(migrationNumber);
    }, f -> {
      LOGGER.debug("Migration number not found");
      return Future.succeededFuture(-1);
    }).compose(migrationNumber -> {
      Promise<List<String>> migrationList = Promise.promise();
      LOGGER.debug("Reading migrations folder");
      vertx.fileSystem().readDir("migrations", migrationList);
      return CompositeFuture.all(Future.succeededFuture(migrationNumber), migrationList.future());
    }).compose(compositeFuture -> {
      LOGGER.debug("Read migrations folder");
      int currentMigrationNumber = (int) compositeFuture.list().get(0);
      @SuppressWarnings("unchecked")
      List<String> migrations = (List<String>) compositeFuture.list().get(1);
      Collections.sort(migrations);
      LOGGER.debug("Found migrations: {}", migrations);
      List<String> toDoMigrations = new ArrayList<>();
      for (String migration : migrations) {
        String migrationFileName = Paths.get(migration).getFileName().toString();
        String migrationName = migrationFileName.split("_", 2)[0];
        int migrationNumber;
        try {
          migrationNumber = Integer.parseInt(migrationName);
        } catch (NumberFormatException e) {
          continue;
        }
        if (migrationNumber <= currentMigrationNumber) {
          continue;
        }
        toDoMigrations.add(migrationFileName);
      }
      LOGGER.debug("ToDo migrations: {}", toDoMigrations);
      Promise<Void> migrationPromise = Promise.promise();
      client.close();
      runMigrations(vertx, connectOptions, poolOptions,
          toDoMigrations, migrationPromise);
      return migrationPromise.future();
    }).onComplete(result);
  }

  private static void runMigrations(Vertx vertx, PgConnectOptions connectOptions, PoolOptions poolOptions, List<String> todo, Handler<AsyncResult<Void>> result) {
    if (todo.isEmpty()) {
      result.handle(Future.succeededFuture());
      return;
    }
    PgPool client = PgPool.pool(vertx, connectOptions, poolOptions);
    String migrationName = todo.get(0);
    LOGGER.info("Running Migration: {}", migrationName);
    String path = Paths.get("migrations", migrationName).toString();
    LOGGER.debug("Reading migration file: {}", path);
    Promise<Buffer> migrationFilePromise = Promise.promise();
    vertx.fileSystem().readFile(path, migrationFilePromise);
   migrationFilePromise.future().compose(migration -> {
      LOGGER.debug("Read migration file");
      String migrationString = migration.toString();
      LOGGER.debug("Executing Migration");
      Promise<RowSet<Row>> migrationExecPromise = Promise.promise();
      client.query(migrationString).execute(migrationExecPromise);
      return migrationExecPromise.future();
    }).onFailure(err -> {
     LOGGER.error("Error while running migration {}: {}", migrationName, err.getCause().getMessage());
     result.handle(Future.failedFuture(err));
   }).compose(res -> {
      LOGGER.info("Migration successful: {}", migrationName);
      Promise<Void> nextMigrationPromise = Promise.promise();
      client.close();
      runMigrations(vertx, connectOptions, poolOptions,
          todo.subList(1, todo.size()), nextMigrationPromise);
      return nextMigrationPromise.future();
    }).onComplete(result);
  }
}
