package de.thm.mni.swtp.feedbackapp.backend.rest.questionTypes;

import io.vertx.core.json.JsonArray;

public class Pairs extends Order {
  private final JsonArray correctAnswers;
  Pairs(JsonArray correctAnswers) {
    super(transformAnswers(correctAnswers));
    this.correctAnswers = correctAnswers;
  }

  private static JsonArray transformAnswers(JsonArray input) {
    JsonArray b = new JsonArray();
    for (int i = 0; i < input.size(); i++) {
      JsonArray element = input.getJsonArray(i);
      b.add(element.getValue(1));
    }
    return b;
  }

  @Override
  public boolean validateAnswer(JsonArray givenAnswers) {
    return super.validateAnswer(transformAnswers(givenAnswers));
  }

  @Override
  public JsonArray getCorrectAnswers() {
    return this.correctAnswers;
  }
}
