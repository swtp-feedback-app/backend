package de.thm.mni.swtp.feedbackapp.backend.rest.accessControll;

import de.thm.mni.swtp.feedbackapp.backend.rest.accessControll.roles.*;

public class Roles {
  public static Role NONE = new NoneRole();
  public static Role PARTICIPANT = new ParticipantRole();
  public static Role AUTHOR = new AuthorRole();
  public static Role ADMIN = new AdminRole();
  public static Role LECTURER = new LecturerRole();

  public static Role fromName(String role) {
    switch (role) {
      case "none":
        return Roles.NONE;
      case "participant":
        return Roles.PARTICIPANT;
      case "author":
        return Roles.AUTHOR;
      case "admin":
        return Roles.ADMIN;
      case "lecturer":
        return Roles.LECTURER;
      default:
        throw new IllegalArgumentException("Unknown role: " + role);
    }
  }
}
