package de.thm.mni.swtp.feedbackapp.backend.rest.accessControll.roles;

import de.thm.mni.swtp.feedbackapp.backend.rest.accessControll.Permission;

import java.util.Set;

public class ParticipantRole extends NoneRole {
  public String getName() {
    return "participant";
  }

  @Override
  public Set<Permission> getPermissions() {
    Set<Permission> permissions = super.getPermissions();
    permissions.add(Permission.LEAVE_COURSE);
    permissions.add(Permission.DO_SESSIONS);
    permissions.add(Permission.VIEW_OWN_STATISTICS);
    return permissions;
  }
}
