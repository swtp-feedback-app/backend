package de.thm.mni.swtp.feedbackapp.backend.rest.accessControll.roles;

import de.thm.mni.swtp.feedbackapp.backend.rest.accessControll.Permission;
import de.thm.mni.swtp.feedbackapp.backend.rest.accessControll.Role;

import java.util.HashSet;
import java.util.Set;

public class NoneRole extends Role {
  @Override
  public String getName() {
    return "none";
  }

  @Override
  public Set<Permission> getPermissions() {
    return new HashSet<>();
  }
}
