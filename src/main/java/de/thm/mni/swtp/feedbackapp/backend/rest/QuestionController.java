package de.thm.mni.swtp.feedbackapp.backend.rest;

import de.thm.mni.swtp.feedbackapp.backend.rest.questionTypes.QuestionType;
import io.vertx.core.Future;
import io.vertx.core.Promise;
import io.vertx.ext.web.RoutingContext;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

public class QuestionController {
  private QuestionController() {}

  public static void listQuestions(RoutingContext context) {
    AuthorizationUtils.checkQuizAuthorization(context).onSuccess(quiz -> {
      int quizID = RequestUtils.getPathParamAsInt(context, "quizID");

      Promise<JsonArray> questionsPromise = Promise.promise();
      RestVerticle.db.listQuestions(quizID, questionsPromise);

      questionsPromise.future().onSuccess(questions -> {
        ResponseUtils.jsonResponse(context, 200,
            DBMapper.mapMany(questions, DBMapper::mapQuestion));
      }).onFailure(err -> ResponseUtils.internalServerErrorResponse(context, err));
    });
  }
  public static void createQuestion(RoutingContext context) {
    AuthorizationUtils.checkQuizAuthorization(context).onSuccess(quiz -> {
      int quizID = RequestUtils.getPathParamAsInt(context, "quizID");
      JsonObject jsonRequest = context.getBodyAsJson();
      JsonObject jsonResponse = new JsonObject();

      String type = jsonRequest.getString("type");
      JsonObject question = jsonRequest.getJsonObject("question");

      Object answers = question.getValue("answers");
      try {
        QuestionType.from(type, answers).validate();
      } catch (IllegalArgumentException e) {
        ResponseUtils.badRequestResponse(context, e.getMessage());
        return;
      }

      JsonObject answersObject = null;
      JsonArray answersArray = null;
      if (answers instanceof JsonObject) {
        answersObject = (JsonObject) answers;
      } else if (answers instanceof JsonArray) {
        answersArray = (JsonArray) answers;
      }

      Promise<JsonObject> createQuestionPromise = Promise.promise();
      RestVerticle.db.createQuestion(question.getString("question"), type, quizID, answersObject,
          answersArray, createQuestionPromise);

      createQuestionPromise.future().onSuccess(res -> {
        jsonResponse
            .put("number", res.getInteger("position"))
            .put("type", type)
            .put("question", question);
        ResponseUtils.jsonResponse(context, 200, jsonResponse);
      }).onFailure(err -> ResponseUtils.internalServerErrorResponse(context, err));
    });
  }
  public static void deleteQuestion(RoutingContext context) {
    AuthorizationUtils.checkQuizAuthorization(context).onSuccess(quiz -> {
      int quizID = RequestUtils.getPathParamAsInt(context, "quizID");
      int questionNumber = RequestUtils.getPathParamAsInt(context, "questionNumber");
      Promise<Void> deletePromise = Promise.promise();
      RestVerticle.db.deleteQuestion(quizID, questionNumber, deletePromise);
      deletePromise.future().onSuccess(res -> {
        ResponseUtils.okResponse(context, "Question deleted");
      }).onFailure(err -> ResponseUtils.internalServerErrorResponse(context, err));
    });
  }
  public static void getQuestion(RoutingContext context) {
    AuthorizationUtils.checkQuizAuthorization(context).onSuccess(quiz -> {
      int quizID = RequestUtils.getPathParamAsInt(context, "quizID");
      int questionNumber = RequestUtils.getPathParamAsInt(context, "questionNumber");

      Promise<JsonObject> questionPromise = Promise.promise();
      RestVerticle.db.getQuestion(quizID, questionNumber, questionPromise);

      questionPromise.future().onSuccess(question -> {
        if (question == null) {
          ResponseUtils.notFoundResponse(context, "Question not found");
          return;
        }

        ResponseUtils.jsonResponse(context, 200, DBMapper.mapQuestion(question));
      }).onFailure(err -> ResponseUtils.internalServerErrorResponse(context, err));
    });
  }
  public static void updateQuestion(RoutingContext context) {
    AuthorizationUtils.checkQuizAuthorization(context).onSuccess(quiz -> {
      int quizID = RequestUtils.getPathParamAsInt(context, "quizID");
      int questionNumber = RequestUtils.getPathParamAsInt(context, "questionNumber");
      JsonObject jsonRequest = context.getBodyAsJson();

      String type = jsonRequest.getString("type");
      JsonObject question = jsonRequest.getJsonObject("question");

      Promise<JsonObject> questionPromise = Promise.promise();
      RestVerticle.db.getQuestion(quizID, questionNumber, questionPromise);

      questionPromise.future().compose(resQuestion -> {
        if (resQuestion == null) {
          ResponseUtils.notFoundResponse(context, "Question not found");
          return Future.succeededFuture();
        }

        Object answers = question.getValue("answers");
        try {
          QuestionType.from(type, answers).validate();
        } catch (IllegalArgumentException e) {
          ResponseUtils.badRequestResponse(context, e.getMessage());
          return Future.succeededFuture();
        }

        JsonObject answersObject = null;
        JsonArray answersArray = null;
        if (answers instanceof JsonObject) {
          answersObject = (JsonObject) answers;
        } else if (answers instanceof JsonArray) {
          answersArray = (JsonArray) answers;
        }

        Promise<Void> updatePromise = Promise.promise();
        RestVerticle.db.updateQuestion(quizID, questionNumber, question.getString("question"), type,
            answersObject, answersArray, updatePromise);

        return updatePromise.future().onSuccess(res -> {
          JsonObject respQuestion = jsonRequest.copy();
          respQuestion.put("number", questionNumber);
          ResponseUtils.jsonResponse(context, 200, respQuestion);
        });
      }).onFailure(err -> ResponseUtils.internalServerErrorResponse(context, err));
    });
  }
}
