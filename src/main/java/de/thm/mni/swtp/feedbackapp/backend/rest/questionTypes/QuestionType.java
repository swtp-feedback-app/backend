package de.thm.mni.swtp.feedbackapp.backend.rest.questionTypes;

import de.thm.mni.swtp.feedbackapp.backend.rest.Utils;
import io.vertx.core.json.JsonArray;

public abstract class QuestionType {
  public static QuestionType from(String type, Object correct) {
    switch (type) {
      case "multipleChoice":
        return new MultipleChoice(Utils.castToJsonObject(correct));
      case "sort":
        return new Sort(Utils.castToJsonArray(correct));
      case "pairs":
        return new Pairs(Utils.castToJsonArray(correct));
      case "blankText":
        return new BlankText(Utils.castToJsonObject(correct));
      case "open":
        return new Open();
      case "wordCloud":
        return new WordCloud(Utils.castToJsonObject(correct));
      case "rating":
        return new Rating(Utils.castToJsonObject(correct));
      default:
        throw new IllegalArgumentException("invalid question type: " + type);
    }
  }

  public abstract boolean validateAnswer(JsonArray givenAnswers);
  public abstract JsonArray getCorrectAnswers();
  public abstract Object getPossibleAnswers();
  public abstract Object computeStats(JsonArray answers);
  public void validate() {}
}
