package de.thm.mni.swtp.feedbackapp.backend.rest;

import de.thm.mni.swtp.feedbackapp.backend.rest.questionTypes.QuestionType;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import java.util.function.Function;

class DBMapper {
  public static JsonArray mapMany(JsonArray input, Function<JsonObject, JsonObject> mapper) {
    JsonArray result = new JsonArray();
    for (int i = 0; i < input.size(); i++) {
      JsonObject inpObject = input.getJsonObject(i);
      JsonObject resObject = mapper.apply(inpObject);
      result.add(resObject);
    }
    return result;
  }

  public static JsonObject mapSession(JsonObject input) {
    String type = "authenticated";
    if (input.getBoolean("anonym", true)) {
      type = "anonym";
    }

    JsonObject result = new JsonObject()
        .put("id", input.getInteger("idsession"))
        .put("type", type)
        .put("start", input.getLong("start"))
        .put("end", input.getLong("end"))
        .put("description", input.getString("session_description"))
        .put("showCorrectAnswers", input.getBoolean("show_correct"));

    JsonObject quiz = input.getJsonObject("quiz");
    if (quiz != null) {
      result.put("quiz", mapQuiz(quiz));
    }

    JsonObject course = input.getJsonObject("course");
    if (course != null) {
      result.put("course", mapCourse(course));
    }

    JsonObject creator = input.getJsonObject("creator");
    if (creator != null) {
      result.put("creator", creator);
    }

    Boolean complete = input.getBoolean("complete");
    if (complete != null) {
      result.put("complete", complete);
    }

    return result;
  }

  public static JsonObject mapQuestion(JsonObject input) {
    return new JsonObject()
        .put("type", input.getString("type"))
        .put("number", input.getInteger("position"))
        .put("question", new JsonObject()
            .put("question", input.getString("text"))
            .put("answers", input.getValue("answers")));
  }

  public static JsonObject mapQuestionWithoutAnswer(JsonObject input) {
    String type = input.getString("type");
    Object answers = QuestionType.from(type, input.getValue("answers")).getPossibleAnswers();

    return new JsonObject()
        .put("type", type)
        .put("number", input.getInteger("position"))
        .put("question", new JsonObject()
            .put("question", input.getString("text"))
            .put("answers", answers));
  }

  public static JsonObject mapQuiz(JsonObject input) {
    return new JsonObject()
        .put("id", input.getInteger("idquiz"))
        .put("name", input.getString("quiz_name"))
        .put("description", input.getString("quiz_description"))
        .put("type", input.getString("type"))
        .put("instantFeedback", input.getBoolean("instant_feedback"))
        .put("questionCount", input.getInteger("question_count"));
  }

  public static JsonObject mapCourse(JsonObject input) {
    return new JsonObject()
        .put("id", input.getInteger("idcourse"))
        .put("name", input.getString("course_name"))
        .put("semester", input.getString("semester"))
        .put("description", input.getString("course_description"))
        .put("docent", mapUser(input.getJsonObject("docent")))
        .put("protected", input.getInteger("course_password") != null)
        .put("allAuthor", input.getBoolean("all_author"))
        .put("role", input.getString("role"));
  }

  public static JsonObject mapUser(JsonObject input) {
    return new JsonObject()
        .put("id", input.getInteger("idperson"))
        .put("fbsID", input.getInteger("fbsID"))
        .put("username", input.getString("username"))
        .put("firstname", input.getString("firstname"))
        .put("lastname", input.getString("lastname"))
        .put("docent", input.getBoolean("docent"))
        .put("superUser", input.getBoolean("superuser"))
        .put("role", input.getString("role"));
  }

  public static JsonObject mapStatistics(JsonObject input) {
    return new JsonObject()
        .put("type", input.getString("type"))
        .put("number", input.getInteger("position"))
        .put("answered", input.getInteger("answered"))
        .put("question", new JsonObject()
            .put("question", input.getString("text"))
            .put("answers", input.getValue("answers")))
            .put("stats", input.getValue("stats"));
  }
}
