package de.thm.mni.swtp.feedbackapp.backend.rest;

import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.api.validation.ValidationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FailureHandler {
  private static final Logger LOGGER = LoggerFactory.getLogger(RestVerticle.class);

  public static void validationErrorHandler(RoutingContext context) {
    Throwable failure = context.failure();
    if (failure instanceof ValidationException) {
      String validationErrorMessage = failure.getMessage();
      LOGGER.debug("Request Validation failed: {}", validationErrorMessage);
      ResponseUtils.badRequestResponse(context, validationErrorMessage);
    } else {
      ResponseUtils.internalServerErrorResponse(context, failure);
    }
  }
}
