package de.thm.mni.swtp.feedbackapp.backend.rest.questionTypes;

import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

public class Rating extends QuestionType {
  private final Integer min;
  private final Integer max;

  public Rating(JsonObject options) {
    this.min = options.getInteger("min");
    this.max = options.getInteger("max");
  }

  @Override
  public boolean validateAnswer(JsonArray givenAnswers) {
    Integer rating = givenAnswers.getInteger(0);
    return rating >= min && rating <= max;
  }

  @Override
  public JsonArray getCorrectAnswers() {
    return new JsonArray();
  }

  @Override
  public Object getPossibleAnswers() {
    return new JsonObject().put("min", min).put("max", max);
  }

  @Override
  public Object computeStats(JsonArray answers) {
    int[] ratings = new int[max-min+1];
    for (int i = 0; i < answers.size(); i++) {
      JsonObject answer = answers.getJsonObject(i);
      if (!answer.getBoolean("correct")) {
        continue;
      }
      int ratingGiven = answer.getJsonArray("answers").getInteger(0);
      ratings[ratingGiven - min]++;
    }
    JsonArray stats = new JsonArray();
    for (int rating : ratings) {
      stats.add(rating);
    }
    return stats;
  }

  @Override
  public void validate() {
    if (min == null || max == null) {
      throw new IllegalArgumentException("min and max must not be null for rating question");
    }
  }
}
