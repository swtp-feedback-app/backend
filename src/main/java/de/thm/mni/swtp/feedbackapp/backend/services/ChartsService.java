package de.thm.mni.swtp.feedbackapp.backend.services;

import io.vertx.codegen.annotations.ProxyGen;
import io.vertx.codegen.annotations.VertxGen;
import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.buffer.Buffer;

@ProxyGen
@VertxGen
public interface ChartsService {

    void getDataForSingleMultipleChoiceQuiz(int idsession, Handler<AsyncResult<String>> chartResult);

    void getDataForMultipleQuizzes(int idcourse, Handler<AsyncResult<String>> chartResult);
}