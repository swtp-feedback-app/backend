package de.thm.mni.swtp.feedbackapp.backend.rest;

import de.thm.mni.swtp.feedbackapp.backend.rest.accessControll.Permission;
import io.vertx.core.Promise;
import io.vertx.core.buffer.Buffer;
import io.vertx.ext.web.RoutingContext;
import java.util.Base64;

public class ChartController {
  public static void getChartForSession(RoutingContext context) {
    AuthorizationUtils.checkUserSessionAuthorization(context, true).onSuccess(session -> {
      int sessionID = RequestUtils.getPathParamAsInt(context, "sessionID");

      Promise<String> chartPromise = Promise.promise();
      RestVerticle.chartService.getDataForSingleMultipleChoiceQuiz(sessionID, chartPromise);

      chartPromise.future().onSuccess(chartBase64 -> {
        byte[] png = Base64.getDecoder().decode(chartBase64);
        Buffer pngBuffer = Buffer.buffer(png);
        ResponseUtils.pngResponse(context, pngBuffer);
      }).onFailure(err -> ResponseUtils.internalServerErrorResponse(context, err));
    });
  }
  public static void getChartForCourse(RoutingContext context) {
    AuthorizationUtils.checkCourseAuthorization(context, Permission.VIEW_STATISTICS).onSuccess(course -> {
      int courseID = RequestUtils.getPathParamAsInt(context, "courseID");

      Promise<String> chartPromise = Promise.promise();
      RestVerticle.chartService.getDataForMultipleQuizzes(courseID, chartPromise);

      chartPromise.future().onSuccess(chartBase64 -> {
        byte[] png = Base64.getDecoder().decode(chartBase64);
        Buffer pngBuffer = Buffer.buffer(png);
        ResponseUtils.pngResponse(context, pngBuffer);
      }).onFailure(err -> ResponseUtils.internalServerErrorResponse(context, err));
    });
  }
  public static void getWordCloud(RoutingContext context) {
    AuthorizationUtils.checkUserSessionAuthorization(context, true).onSuccess(session -> {
      int sessionID = RequestUtils.getPathParamAsInt(context, "sessionID");
      int questionNumber = RequestUtils.getPathParamAsInt(context, "questionNumber");

      Promise<String> wordCloudPromise = Promise.promise();
      RestVerticle.wordCloudService.getDataForWordcloud(sessionID, questionNumber, wordCloudPromise);

      wordCloudPromise.future().onSuccess(wordCloudBase64 -> {
        byte[] png = Base64.getDecoder().decode(wordCloudBase64);
        Buffer pngBuffer = Buffer.buffer(png);
        ResponseUtils.pngResponse(context, pngBuffer);
      }).onFailure(err -> ResponseUtils.internalServerErrorResponse(context, err));
    });
  }
}
