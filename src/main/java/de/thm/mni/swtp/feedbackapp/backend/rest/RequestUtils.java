package de.thm.mni.swtp.feedbackapp.backend.rest;

import io.vertx.ext.web.RoutingContext;
import java.util.List;

public class RequestUtils {
  public static String getPathParamAsString(RoutingContext context, String name) {
    return context.pathParam(name);
  }
  public static int getPathParamAsInt(RoutingContext context, String name) {
    return Integer.parseInt(getPathParamAsString(context, name));
  }
  public static String getBearerToken(RoutingContext context) {
    String authorizationHeader = context.request().getHeader("Authorization");
    if (authorizationHeader == null) {
      ResponseUtils.unauthorizedResponse(context, "Missing authorization header");
      return null;
    }
    String[] authorizationHeaderSplit = authorizationHeader.split(" ");
    if (authorizationHeaderSplit.length != 2 || !authorizationHeaderSplit[0].equals("Bearer")) {
      ResponseUtils.badRequestResponse(context, "Invalid bearer authorization header");
      return null;
    }
    return authorizationHeaderSplit[1];
  }
  public static List<String> getQueryParamAsStringArray(RoutingContext context, String paramName) {
    return context.queryParam(paramName);
  }
  public static String getQueryParamAsString(RoutingContext context, String paramName, String defaultValue) {
    List<String> queryParam = getQueryParamAsStringArray(context, paramName);
    if (queryParam.size() < 1) {
      return defaultValue;
    }
    return queryParam.get(0);
  }
  public static Integer getQueryParamAsInteger(RoutingContext context, String paramName, Integer defaultValue) {
    String queryString = getQueryParamAsString(context, paramName, null);
    if (queryString == null) {
      return defaultValue;
    }
    return Integer.parseInt(queryString);
  }
  public static Boolean getQueryParamAsBoolean(RoutingContext context, String paramName, Boolean defaultValue) {
    String queryString = getQueryParamAsString(context, paramName, null);
    if (queryString == null) {
      return defaultValue;
    }
    return Boolean.parseBoolean(queryString);
  }
}
