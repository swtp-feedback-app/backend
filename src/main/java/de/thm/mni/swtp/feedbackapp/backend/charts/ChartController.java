package de.thm.mni.swtp.feedbackapp.backend.charts;

import de.thm.mni.swtp.feedbackapp.backend.services.ChartsService;
import io.vertx.core.AsyncResult;
import io.vertx.core.CompositeFuture;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.Promise;
import io.vertx.core.Vertx;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

import java.awt.*;
import java.io.IOException;
import java.util.Base64;
import org.knowm.xchart.BitmapEncoder;
import org.knowm.xchart.CategoryChart;
import org.knowm.xchart.CategoryChartBuilder;
import org.knowm.xchart.SwingWrapper;
import org.knowm.xchart.style.Styler;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ChartController implements ChartsService {
    private final Vertx vertx;
    public ChartController(Vertx vertx) {
        this.vertx = vertx;
    }

    public void getDataForSingleMultipleChoiceQuiz(int idsession, Handler<AsyncResult<String>> chartResult){
        Promise<JsonArray> zero = Promise.promise();
        ChartsVerticle.db.getBarChartForSingleMultipleChoiceQuiz4(idsession,zero);
        zero.future().onSuccess(lambdazero -> {
            ArrayList<String> quizname = new ArrayList<>();
            for(int i = 0; i < lambdazero.size(); i++){
                quizname.add(lambdazero.getString(i));
            }
            Promise<JsonArray> promisequery1 = Promise.promise();
            ChartsVerticle.db.getBarChartForSingleMultipleChoiceQuiz1(idsession, promisequery1); // select all idquestions for this session
            promisequery1.future().onSuccess(resultquery1 -> {
                if (resultquery1.size() < 1) {
                    blankChart(chartResult);
                    return;
                }
                Promise<JsonArray> promisequery2 = Promise.promise();
                int idquestion = resultquery1.getJsonObject(0).getInteger("idquestion");
                ChartsVerticle.db.getBarChartForSingleMultipleChoiceQuiz2(idquestion,promisequery2); //select idquestion and question text and answers
                promisequery2.future().onSuccess(resultquery2 -> {
                    List<Future> resultquery3 = new ArrayList<>();
                    for(int i = 0; i < resultquery2.size(); i++) { // get all answers for each question
                        Promise<JsonArray> promisequery3 = Promise.promise();
                        ChartsVerticle.db.getBarChartForSingleMultipleChoiceQuiz3(resultquery2.getJsonObject(i).getInteger("idquestion"), idsession, promisequery3);
                        resultquery3.add(promisequery3.future());
                    }
                    CompositeFuture.all(resultquery3).onSuccess(res -> {
                        List<JsonArray> reslist = res.list();
                        ArrayList<Double> teilnehmer = new ArrayList<>();
                        ArrayList<Double> richtigeAntworten = new ArrayList<>();
                        ArrayList<String> questiontext = new ArrayList<>();
                        for(Integer i = 0; i < res.size(); i++){
                            teilnehmer.add(reslist.get(i).getJsonObject(0).getDouble("teilnehmer"));
                            richtigeAntworten.add(reslist.get(i).getJsonObject(0).getDouble("richtigeAntworten"));
                            String question = resultquery2.getJsonObject(i).getString("questionText");
                            Integer quetsionnumber = i + 1;
                            questiontext.add(quetsionnumber.toString());
                        }
                        try {
                            String chartBase64 = getBarChartForSingleMultipleChoiceQuiz(quizname.get(0),questiontext,teilnehmer,richtigeAntworten);
                            chartResult.handle(Future.succeededFuture(chartBase64));
                        } catch (IOException e) {
                            chartResult.handle(Future.failedFuture(e));
                        }
                    });
                });
            });
        });
    }

    public void getDataForMultipleQuizzes(int idcourse, Handler<AsyncResult<String>> chartResult){
        ArrayList<String> quizname = new ArrayList<>();
        ArrayList<String> coursename = new ArrayList<>();
        ArrayList<Double> teilnehmer = new ArrayList<>();
        ArrayList<Double> richtigeAntworten = new ArrayList<>();
        Promise<JsonArray> zero = Promise.promise();
        ChartsVerticle.db.getBarChartForMultipleQuizzes4(idcourse,zero);
        zero.future().onSuccess(resultzero -> {
            for(int i = 0; i < resultzero.size(); i++){
                quizname.add(resultzero.getJsonObject(i).getString("quizname"));
            }
            Promise<JsonArray> promisequery5 = Promise.promise();
            ChartsVerticle.db.getBarChartForMultipleQuizzes5(idcourse,promisequery5);
            promisequery5.future().onSuccess(z -> {
                coursename.add(z.getJsonObject(0).getString("coursename"));
            });
            Promise<JsonArray> promisequery1 = Promise.promise();
            ChartsVerticle.db.getBarChartForMultipleQuizzes1(idcourse,promisequery1);
            promisequery1.future().onSuccess(resultquery1 -> {
                List<JsonObject> result1 = new ArrayList<>();
                for(int i = 0; i < resultquery1.size(); i++){ ;
                    result1.add(resultquery1.getJsonObject(i));
                }
                List<Future> result2 = new ArrayList<>();
                for(int j = 0; j < result1.size(); j++) {
                    Promise<JsonArray> promisequery2 = Promise.promise();
                    ChartsVerticle.db.getBarChartForMultipleQuizzes2(result1.get(j).getInteger("idsession"), promisequery2);
                    result2.add(promisequery2.future());
                }
                CompositeFuture.all(result2).onSuccess(resultcomposite2 -> {
                    List<JsonArray> resList2 = resultcomposite2.list();
                    List<Future> result3 = new ArrayList<>();
                    System.out.println(result2);
                    for(int k = 0; k< resList2.size(); k++){
                        Promise<JsonArray> promisequery3 = Promise.promise();
                        System.out.println(k);
                        System.out.println(resList2);
                        ChartsVerticle.db.getBarChartForMultipleQuizzes3(result1.get(k).getInteger("idsession"),
                                resList2.get(k).getJsonObject(0).getInteger("questioncount"),promisequery3);
                        result3.add(promisequery3.future());
                    }
                    CompositeFuture.all(result3).onSuccess(resultcomposite3 -> {
                        List<JsonArray> reslist = resultcomposite3.list();
                        for(int i = 0; i < reslist.size(); i++){
                            teilnehmer.add(reslist.get(i).getJsonObject(0).getDouble("teilnehmer"));
                            richtigeAntworten.add(reslist.get(i).getJsonObject(0).getDouble("richtigeAntworten"));
                        }
                        try {
                            if(quizname.size() == teilnehmer.size()) {
                                String chartBase64 = getBarChartForMultipleQuizzes(coursename.get(0), quizname, teilnehmer, richtigeAntworten);
                                chartResult.handle(Future.succeededFuture(chartBase64));
                            }
                        } catch (IOException e) {
                            chartResult.handle(Future.failedFuture(e));
                        }
                    });
                });

            });
        });

    }

    public static String getBarChartForSingleMultipleChoiceQuiz(String coursename, ArrayList<String> fragen,
                                                                ArrayList<Double> teilnehmer,ArrayList<Double> richtigeAntworten)
        throws IOException {


        // Create Chart
        CategoryChart chart = new CategoryChartBuilder().width(800).height(600).title(coursename).xAxisTitle("Fragen")
                .yAxisTitle("Teilnehmer").theme(Styler.ChartTheme.GGPlot2).build();

        chart.addSeries("abgegebene Antworten",fragen, teilnehmer).setYAxisGroup(0);
        chart.addSeries("richtige Antworten",fragen, richtigeAntworten);
        chart.getStyler().setOverlapped(true).setHasAnnotations(true)
                .setAnnotationsPosition((float) 0.99).setLegendPosition(Styler.LegendPosition.OutsideS);
        chart.getStyler().setLegendFont(new Font(Font.SANS_SERIF, Font.ITALIC, 20));
        chart.getStyler().setAxisTitleFont(new Font(Font.SANS_SERIF, Font.ITALIC, 20));
        chart.getStyler().setAnnotationsFont(new Font(Font.SANS_SERIF, Font.ITALIC, 20));
        chart.getStyler().setChartTitleFont(new Font(Font.SANS_SERIF, Font.ITALIC, 27));
        chart.getStyler().setAxisTickLabelsFont(new Font(Font.SANS_SERIF, Font.ITALIC, 20)).setAxisTickLabelsColor(Color.BLACK);

      byte[] chartPNG = BitmapEncoder.getBitmapBytes(chart, BitmapEncoder.BitmapFormat.PNG);

        try {
            BitmapEncoder.saveBitmapWithDPI(chart, "./Sample_Chart_300_DPI_Single", BitmapEncoder.BitmapFormat.PNG, 300);
        } catch (IOException e) {
            e.printStackTrace();
        }

      return Base64.getEncoder().encodeToString(chartPNG);
    }

    public static String getBarChartForMultipleQuizzes(String coursename, ArrayList<String> fragen,
                                                     ArrayList<Double> teilnehmer, ArrayList<Double> richtigeAntworten) throws IOException {


        CategoryChart chart = new CategoryChartBuilder().width(1280).height(720).title(coursename).theme(Styler.ChartTheme.GGPlot2)
                .xAxisTitle("Quiz").yAxisTitle("Teilnehmer").build();

        chart.addSeries("Quiz Teilnehmer",fragen, teilnehmer).setYAxisGroup(0);
        chart.addSeries("richtige Quizze",fragen, richtigeAntworten);
        chart.getStyler().setOverlapped(true).setXAxisLabelRotation(45).setHasAnnotations(true)
                .setAnnotationsPosition((float) 0.99).setLegendPosition(Styler.LegendPosition.OutsideS);
        chart.getStyler().setLegendFont(new Font(Font.SANS_SERIF, Font.ITALIC, 20));
        chart.getStyler().setAxisTitleFont(new Font(Font.SANS_SERIF, Font.ITALIC, 20));
        chart.getStyler().setAnnotationsFont(new Font(Font.SANS_SERIF, Font.ITALIC, 20));
        chart.getStyler().setChartTitleFont(new Font(Font.SANS_SERIF, Font.ITALIC, 27));
        chart.getStyler().setAxisTickLabelsFont(new Font(Font.SANS_SERIF, Font.ITALIC, 20)).setAxisTickLabelsColor(Color.BLACK);

        byte[] chartPNG = BitmapEncoder.getBitmapBytes(chart, BitmapEncoder.BitmapFormat.PNG);

        try {
            BitmapEncoder.saveBitmapWithDPI(chart, "./Sample_Chart_300_DPI_Miltiple", BitmapEncoder.BitmapFormat.PNG, 300);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return Base64.getEncoder().encodeToString(chartPNG);

    }

    public static void getBarChartForDecisionQuiz(String coursename, String [] fragen, Integer[][] antworten) {
        ArrayList<Integer> antwortWahr = new ArrayList<>();
        ArrayList<Integer> antwortFalsch = new ArrayList<>();


        for(int j = 0; j< antworten.length; j++) {
            for (int i = 0; i < antworten[0].length; i++) {
                switch (j) {
                    case 0:
                        antwortWahr.add(antworten[j][i]);
                        break;
                    case 1:
                        antwortFalsch.add(antworten[j][i]);
                        break;
                }
            }
        }

        // Create Chart
        CategoryChart chart = new CategoryChartBuilder().width(800).height(600).title(coursename).xAxisTitle("Fragen").yAxisTitle("Antworten").theme(Styler.ChartTheme.GGPlot2).build();

        // Customize Chart

        // Series
        chart.addSeries("wahr", new ArrayList<>(Arrays.asList(fragen)),
                antwortWahr);
        chart.addSeries("falsch", new ArrayList<>(Arrays.asList(fragen)),
                antwortFalsch);

        new SwingWrapper(chart).displayChart();
        /*try {
          BitmapEncoder.saveBitmapWithDPI(chart, "./Sample_Chart_300_DPI", BitmapEncoder.BitmapFormat.PNG, 300);
        } catch (IOException e) {
          e.printStackTrace();
        }*/
    }


    public static void getBarChartForSequenceQuiz(String coursename, String [] fragen, Integer[][] antworten) {
        ArrayList<Integer> teilnehmer = new ArrayList<>();
        ArrayList<Integer> richtigeAntworten = new ArrayList<>();


        for(int j = 0; j< antworten.length; j++) {
            for (int i = 0; i < antworten[0].length; i++) {
                switch (j) {
                    case 0:
                        teilnehmer.add(antworten[j][i]);
                        break;
                    case 1:
                        richtigeAntworten.add(antworten[j][i]);
                        break;
                }
            }
        }

        // Create Chart
        CategoryChart chart = new CategoryChartBuilder().width(800).height(600).title(coursename).xAxisTitle("Fragen").yAxisTitle("Personen").theme(Styler.ChartTheme.GGPlot2).build();

        // Customize Chart

        // Series
        chart.addSeries("Teilnehmer", new ArrayList<>(Arrays.asList(fragen)),
                teilnehmer);
        chart.addSeries("richtige Antworten", new ArrayList<>(Arrays.asList(fragen)),
                richtigeAntworten);

        new SwingWrapper(chart).displayChart();
        /*try {
          BitmapEncoder.saveBitmapWithDPI(chart, "./Sample_Chart_300_DPI", BitmapEncoder.BitmapFormat.PNG, 300);
        } catch (IOException e) {
          e.printStackTrace();
        }*/
    }

    private void blankChart(Handler<AsyncResult<String>> blankChartResult) {
        Promise<Buffer> blankPromise = Promise.promise();
        vertx.fileSystem().readFile("blank.png", blankPromise);
        blankPromise.future().onSuccess(blank -> {
            String base64Blank = Base64.getEncoder().encodeToString(blank.getBytes());
            blankChartResult.handle(Future.succeededFuture(base64Blank));
        }).onFailure(err -> blankChartResult.handle(Future.failedFuture(err)));
    }
}
