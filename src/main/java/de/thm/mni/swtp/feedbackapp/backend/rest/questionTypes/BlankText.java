package de.thm.mni.swtp.feedbackapp.backend.rest.questionTypes;

import io.vertx.core.json.JsonObject;

public class BlankText extends Order {
  BlankText(JsonObject question) {
    super(question.getJsonArray("blanks"));
  }
}
