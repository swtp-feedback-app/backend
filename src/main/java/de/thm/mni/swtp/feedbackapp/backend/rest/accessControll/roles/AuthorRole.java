package de.thm.mni.swtp.feedbackapp.backend.rest.accessControll.roles;

import de.thm.mni.swtp.feedbackapp.backend.rest.accessControll.Permission;
import de.thm.mni.swtp.feedbackapp.backend.rest.accessControll.Role;

import java.util.Set;

public class AuthorRole extends ParticipantRole {
  public String getName() {
    return "author";
  }

  @Override
  public Set<Permission> getPermissions() {
    Set<Permission> permissions = super.getPermissions();
    permissions.add(Permission.CREATE_SESSIONS);
    return permissions;
  }
}
