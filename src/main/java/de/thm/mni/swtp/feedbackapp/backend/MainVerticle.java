package de.thm.mni.swtp.feedbackapp.backend;

import de.thm.mni.swtp.feedbackapp.backend.charts.ChartsVerticle;
import de.thm.mni.swtp.feedbackapp.backend.db.DatabaseVerticle;
import de.thm.mni.swtp.feedbackapp.backend.fbsIntegration.FBSIntegrationVerticle;
import de.thm.mni.swtp.feedbackapp.backend.rest.RestVerticle;
import de.thm.mni.swtp.feedbackapp.backend.wordcloud.WordCloudVerticle;
import de.thm.mni.swtp.feedbackapp.backend.wordfilter.WordFilterVerticle;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Promise;

public class MainVerticle extends AbstractVerticle {
    @Override
    public void start(Promise<Void> startPromise) throws Exception {
        Promise<String> dbVerticleDeployment = Promise.promise();
        vertx.deployVerticle(DatabaseVerticle.class.getName(), dbVerticleDeployment);

        dbVerticleDeployment.future().compose(id -> {
            Promise<String> fbsIntegrationVerticleDeployment = Promise.promise();
            vertx.deployVerticle(FBSIntegrationVerticle.class.getName(), fbsIntegrationVerticleDeployment);
            return fbsIntegrationVerticleDeployment.future();
        }).compose(id -> {
            Promise<String> wordFilterVerticleDeployment = Promise.promise();
            vertx.deployVerticle(WordFilterVerticle.class.getName(), wordFilterVerticleDeployment);
            return wordFilterVerticleDeployment.future();
        }).compose(id -> {
            Promise<String> chartsVerticleDeployment = Promise.promise();
            vertx.deployVerticle(ChartsVerticle.class.getName(), chartsVerticleDeployment);
            return chartsVerticleDeployment.future();
        }).compose(id -> {
            Promise<String> wordCloudVerticleDeployment = Promise.promise();
            vertx.deployVerticle(WordCloudVerticle.class.getName(), wordCloudVerticleDeployment);
            return wordCloudVerticleDeployment.future();
        }).compose(id -> {
            Promise<String> restVerticleDeployment = Promise.promise();
            vertx.deployVerticle(RestVerticle.class.getName(), restVerticleDeployment);
            return restVerticleDeployment.future();
        }).onFailure(startPromise::fail).onSuccess(res -> {
            startPromise.complete();
        });
    }
}
