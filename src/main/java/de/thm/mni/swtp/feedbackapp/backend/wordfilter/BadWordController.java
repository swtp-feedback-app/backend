package de.thm.mni.swtp.feedbackapp.backend.wordfilter;

import de.thm.mni.swtp.feedbackapp.backend.services.WordFilterService;
import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.json.JsonArray;

public class BadWordController implements WordFilterService {
    JsonArray badWordList;
    BadWordController(JsonArray list) {
        badWordList = list;
    }

    public void isBadWord(String input, Handler<AsyncResult<Boolean>> result){
        if (input == null) {
            result.handle(Future.succeededFuture(false));
            return;
        }

        input = input.toLowerCase()
            .replaceAll("1","i")
            .replaceAll("!","i")
            .replaceAll("3","e")
            .replaceAll("4","a")
            .replaceAll("@","a")
            .replaceAll("5","s")
            .replaceAll("7","t")
            .replaceAll("0","o")
            .replaceAll("9","g")
            .replaceAll("_","")
            .replaceAll(" ","");

        for (Object badWordObj : this.badWordList) {
            String badWord = (String) badWordObj;
            if (input.contains(badWord)) {
                result.handle(Future.succeededFuture(true));
                return;
            }
        }

        input = input.replaceAll("x","");

        for (Object badWordObj : this.badWordList) {
            String badWord = (String) badWordObj;
            if (input.contains(badWord)) {
                result.handle(Future.succeededFuture(true));
                return;
            }
        }
        result.handle(Future.succeededFuture(false));
    }
}
