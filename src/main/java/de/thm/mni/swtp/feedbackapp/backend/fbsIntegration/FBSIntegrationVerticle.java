package de.thm.mni.swtp.feedbackapp.backend.fbsIntegration;

import de.thm.mni.swtp.feedbackapp.backend.db.DatabaseVerticle;
import de.thm.mni.swtp.feedbackapp.backend.services.DBService;
import de.thm.mni.swtp.feedbackapp.backend.services.DBServiceVertxEBProxy;
import de.thm.mni.swtp.feedbackapp.backend.services.FBSIntegrationService;
import io.vertx.config.ConfigRetriever;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Promise;
import io.vertx.core.json.JsonObject;
import io.vertx.serviceproxy.ServiceBinder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FBSIntegrationVerticle extends AbstractVerticle {
    static String fbsHostname;
    static String fbsUsername;
    static String fbsPassword;
    static long fbsCourseUpdateInterval;
    static boolean insecureTlsTrustAll;
    static DBService db;

    public static final String FBS_INTEGRATION_SERVICE_VERTX_ADDRESS = "feedbackapp.backend.fbsIntegration";

    // 24h = 1 day = 60000*60*24
    private static final long ONE_DAY_IN_MILLISECONDS = 60000*60*24;
    private static final Logger LOGGER = LoggerFactory.getLogger(FBSIntegrationVerticle.class);

    @Override
    public void start(Promise<Void> startPromise) {
        Promise<JsonObject> configPromise = Promise.promise();
        ConfigRetriever.create(vertx).getConfig(configPromise);
        configPromise.future().onSuccess(config -> {
            fbsHostname = config.getString("FBS_HOSTNAME", "feedback.mni.thm.de");
            fbsUsername = config.getString("FBS_ADMIN_USERNAME");
            fbsPassword = config.getString("FBS_ADMIN_PASSWORD");
            fbsCourseUpdateInterval = config.getLong("FBS_COURSE_UPDATE_INTERVAL", ONE_DAY_IN_MILLISECONDS);
            insecureTlsTrustAll = config.getBoolean("FBS_INSECURE_TLS_TRUST_ALL", false);
            db = new DBServiceVertxEBProxy(vertx, DatabaseVerticle.DATABASE_SERVICE_VERTX_ADDRESS);

            FBSIntegrationController fbsIntegrationController = new FBSIntegrationController(vertx);

            ServiceBinder serviceBinder = new ServiceBinder(vertx);
            serviceBinder
                .setAddress(FBS_INTEGRATION_SERVICE_VERTX_ADDRESS)
                .register(FBSIntegrationService.class, fbsIntegrationController);
            startPromise.complete();

            if (fbsUsername != null && fbsPassword != null) {
                LOGGER.info("Fetching courses from fbs");
                fbsIntegrationController.getAndUpdateCourses();

                vertx.setPeriodic(fbsCourseUpdateInterval, aLong -> {
                    LOGGER.info("Fetching courses from fbs");
                    fbsIntegrationController.getAndUpdateCourses();
                });
            } else {
                LOGGER.warn("FBS_ADMIN_USERNAME or FBS_ADMIN_PASSWORD not set. Won't be able to update courses!");
            }
        }).onFailure(startPromise::fail);
    }
}
