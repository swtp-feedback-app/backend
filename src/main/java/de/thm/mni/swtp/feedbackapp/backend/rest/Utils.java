package de.thm.mni.swtp.feedbackapp.backend.rest;

import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import java.util.concurrent.ThreadLocalRandom;

public class Utils {
  public static int getRandomID() {
    return ThreadLocalRandom.current().nextInt(10000000, 100000000);
  }
  public static long getRandomLongID() {
    return ThreadLocalRandom.current().nextLong(100000000000000000L, 1000000000000000000L);
  }
  public static JsonArray shuffle(JsonArray input) {
    JsonArray inp = input.copy();
    JsonArray res = new JsonArray();
    while (!inp.isEmpty()) {
      int i = ThreadLocalRandom.current().nextInt(inp.size());
      res.add(inp.getString(i));
      inp.remove(i);
    }
    return res;
  }


  public static JsonObject castToJsonObject(Object input) {
    if (input instanceof JsonObject) {
      return (JsonObject) input;
    }
    throw new IllegalArgumentException("input is not a JsonObject");
  }
  public static JsonArray castToJsonArray(Object input) {
    if (input instanceof JsonArray) {
      return (JsonArray) input;
    }
    throw new IllegalArgumentException("input is not a JsonArray");
  }
}
