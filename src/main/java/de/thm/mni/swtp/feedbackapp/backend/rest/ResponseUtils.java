package de.thm.mni.swtp.feedbackapp.backend.rest;

import io.vertx.core.buffer.Buffer;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ResponseUtils {
  private static final Logger LOGGER = LoggerFactory.getLogger(RestVerticle.class);

  private static void sendBuffer(RoutingContext context, int status, String contentType, Buffer content) {
    context.response().setStatusCode(status)
        .putHeader("content-type", contentType)
        .putHeader("content-length", Integer.toString(content.length()))
        .write(content).end();
  }
  private static void jsonResponse(RoutingContext context, int status, String encodedBody) {
    Buffer encodedBodyBuffer = Buffer.buffer(encodedBody);
    sendBuffer(context, status, "application/json", encodedBodyBuffer);
  }
  public static void jsonResponse(RoutingContext context, int status, JsonObject body) {
    jsonResponse(context, status, body.encode());
  }
  public static void jsonResponse(RoutingContext context, int status, JsonArray body) {
    jsonResponse(context, status, body.encode());
  }
  public static void errorResponse(RoutingContext context, int statusCode, String statusName, String description) {
    JsonObject response = new JsonObject();
    response
        .put("statusCode", statusCode)
        .put("statusName", statusName);
    if (description != null) {
      response.put("description", description);
    }
    jsonResponse(context, statusCode, response);
  }
  public static void okResponse(RoutingContext context, String description) {
    errorResponse(context, 200, "Ok", description);
  }
  public static void badRequestResponse(RoutingContext context, String description) {
    errorResponse(context, 400, "Bad Request", description);
  }
  public static void unauthorizedResponse(RoutingContext context, String description) {
    errorResponse(context, 401, "Unauthorized", description);
  }
  public static void forbiddenResponse(RoutingContext context, String description) {
    errorResponse(context, 403, "Forbidden", description);
  }
  public static void notFoundResponse(RoutingContext context, String description) {
    errorResponse(context, 404, "Not Found", description);
  }
  public static void conflictResponse(RoutingContext context, String description) {
    errorResponse(context, 409, "Conflict", description);
  }
  public static void internalServerErrorResponse(RoutingContext context, String description) {
    errorResponse(context, 500, "Internal Server Error", description);
  }
  public static void internalServerErrorResponse(RoutingContext context, Throwable error) {
    error.printStackTrace();
    LOGGER.error("An error occurred: {}", error.getMessage());
    errorResponse(context, 500, "Internal Server Error",
        "An error occurred while processing this request");
  }
  public static void pngResponse(RoutingContext context, Buffer png) {
    sendBuffer(context, 200, "image/png", png);
  }
}
