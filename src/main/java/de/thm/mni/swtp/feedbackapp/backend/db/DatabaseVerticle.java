package de.thm.mni.swtp.feedbackapp.backend.db;

import de.thm.mni.swtp.feedbackapp.backend.services.DBService;
import io.vertx.config.ConfigRetriever;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.Promise;
import io.vertx.core.json.JsonObject;
import io.vertx.pgclient.PgConnectOptions;
import io.vertx.pgclient.PgPool;
import io.vertx.serviceproxy.ServiceBinder;
import io.vertx.sqlclient.PoolOptions;

public class DatabaseVerticle extends AbstractVerticle {
    public static final String DATABASE_SERVICE_VERTX_ADDRESS = "feedbackapp.backend.db";

    private PgConnectOptions connectOptions;
    private PoolOptions poolOptions;

    @Override
    public void start(Promise<Void> startPromise) throws Exception {
        Promise<JsonObject> configPromise = Promise.promise();
        ConfigRetriever.create(vertx).getConfig(configPromise);
        configPromise.future().compose(config -> {
            String databaseURL = config
                .getString("DATABASE_URL", "postgres://postgres@localhost/postgres");

            connectOptions = PgConnectOptions.fromUri(databaseURL);
            poolOptions = new PoolOptions().setMaxSize(5);

            Promise<Void> migrationPromise = Promise.promise();
            MigrationController.migrate(vertx, connectOptions, poolOptions, migrationPromise);
            return migrationPromise.future();
        }).compose(res -> {
            PgPool client = PgPool.pool(vertx, connectOptions, poolOptions);

            DatabaseController dbController = new DatabaseController(client);
            ServiceBinder serviceBinder = new ServiceBinder(vertx);
            serviceBinder
                .setAddress(DATABASE_SERVICE_VERTX_ADDRESS)
                .register(DBService.class, dbController);

            return Future.succeededFuture();
        }).onSuccess(res -> startPromise.complete()).onFailure(startPromise::fail);
    }
}
