--
-- PostgreSQL database dump
--

-- Dumped from database version 12.3
-- Dumped by pg_dump version 12.3

-- Started on 2020-06-09 11:34:28

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 1 (class 3079 OID 16384)
-- Name: adminpack; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS adminpack WITH SCHEMA pg_catalog;


--
-- TOC entry 2936 (class 0 OID 0)
-- Dependencies: 1
-- Name: EXTENSION adminpack; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION adminpack IS 'administrative functions for PostgreSQL';


SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 204 (class 1259 OID 16560)
-- Name: answer; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.answer (
    idanswer integer NOT NULL,
    question_idquestion integer,
    person_idperson integer,
    session_idsession integer,
    correct boolean,
    answers json,
    necessarytime timestamp without time zone,
    "position" integer
);


ALTER TABLE public.answer OWNER TO postgres;

--
-- TOC entry 203 (class 1259 OID 16558)
-- Name: answer_idanswer_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.answer_idanswer_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.answer_idanswer_seq OWNER TO postgres;

--
-- TOC entry 2937 (class 0 OID 0)
-- Dependencies: 203
-- Name: answer_idanswer_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.answer_idanswer_seq OWNED BY public.answer.idanswer;


--
-- TOC entry 206 (class 1259 OID 16571)
-- Name: course; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.course (
    idcourse integer NOT NULL,
    name character varying NOT NULL,
    description character varying NOT NULL,
    semester integer,
    lecturer character varying,
    password integer
);


ALTER TABLE public.course OWNER TO postgres;

--
-- TOC entry 205 (class 1259 OID 16569)
-- Name: course_idcourse_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.course_idcourse_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.course_idcourse_seq OWNER TO postgres;

--
-- TOC entry 2938 (class 0 OID 0)
-- Dependencies: 205
-- Name: course_idcourse_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.course_idcourse_seq OWNED BY public.course.idcourse;


--
-- TOC entry 208 (class 1259 OID 16582)
-- Name: courseadmin; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.courseadmin (
    idcourseadmin integer NOT NULL,
    course_idcourse integer,
    person_idperson integer,
    role character varying
);


ALTER TABLE public.courseadmin OWNER TO postgres;

--
-- TOC entry 207 (class 1259 OID 16580)
-- Name: courseadmin_idcourseadmin_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.courseadmin_idcourseadmin_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.courseadmin_idcourseadmin_seq OWNER TO postgres;

--
-- TOC entry 2939 (class 0 OID 0)
-- Dependencies: 207
-- Name: courseadmin_idcourseadmin_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.courseadmin_idcourseadmin_seq OWNED BY public.courseadmin.idcourseadmin;


--
-- TOC entry 210 (class 1259 OID 16594)
-- Name: logs; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.logs (
    idlogs integer NOT NULL,
    person_idperson integer,
    "timestamp" timestamp without time zone,
    action character varying
);


ALTER TABLE public.logs OWNER TO postgres;

--
-- TOC entry 209 (class 1259 OID 16592)
-- Name: logs_idlogs_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.logs_idlogs_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.logs_idlogs_seq OWNER TO postgres;

--
-- TOC entry 2940 (class 0 OID 0)
-- Dependencies: 209
-- Name: logs_idlogs_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.logs_idlogs_seq OWNED BY public.logs.idlogs;


--
-- TOC entry 212 (class 1259 OID 16605)
-- Name: participant; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.participant (
    idparticipant integer NOT NULL,
    course_idcourse integer,
    person_idperson integer
);


ALTER TABLE public.participant OWNER TO postgres;

--
-- TOC entry 211 (class 1259 OID 16603)
-- Name: participant_idparticipant_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.participant_idparticipant_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.participant_idparticipant_seq OWNER TO postgres;

--
-- TOC entry 2941 (class 0 OID 0)
-- Dependencies: 211
-- Name: participant_idparticipant_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.participant_idparticipant_seq OWNED BY public.participant.idparticipant;


--
-- TOC entry 214 (class 1259 OID 16613)
-- Name: person; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.person (
    idperson integer NOT NULL,
    name character varying,
    employee boolean,
    faculty character varying,
    title character varying,
    pseudonym character varying
);


ALTER TABLE public.person OWNER TO postgres;

--
-- TOC entry 213 (class 1259 OID 16611)
-- Name: person_idperson_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.person_idperson_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.person_idperson_seq OWNER TO postgres;

--
-- TOC entry 2942 (class 0 OID 0)
-- Dependencies: 213
-- Name: person_idperson_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.person_idperson_seq OWNED BY public.person.idperson;


--
-- TOC entry 216 (class 1259 OID 16624)
-- Name: question; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.question (
    idquestion integer NOT NULL,
    questiontext character varying,
    type character varying,
    quiz_idquiz integer,
    answers json,
    correctanswers json
);


ALTER TABLE public.question OWNER TO postgres;

--
-- TOC entry 215 (class 1259 OID 16622)
-- Name: question_idquestion_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.question_idquestion_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.question_idquestion_seq OWNER TO postgres;

--
-- TOC entry 2943 (class 0 OID 0)
-- Dependencies: 215
-- Name: question_idquestion_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.question_idquestion_seq OWNED BY public.question.idquestion;


--
-- TOC entry 218 (class 1259 OID 16635)
-- Name: quiz; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.quiz (
    idquiz integer NOT NULL,
    name character varying,
    password integer,
    description character varying,
    person_idperson integer
);


ALTER TABLE public.quiz OWNER TO postgres;

--
-- TOC entry 217 (class 1259 OID 16633)
-- Name: quiz_idquiz_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.quiz_idquiz_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.quiz_idquiz_seq OWNER TO postgres;

--
-- TOC entry 2944 (class 0 OID 0)
-- Dependencies: 217
-- Name: quiz_idquiz_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.quiz_idquiz_seq OWNED BY public.quiz.idquiz;


--
-- TOC entry 220 (class 1259 OID 16646)
-- Name: session; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.session (
    idsession integer NOT NULL,
    start date,
    "end" date,
    anonym boolean,
    quiz_idquiz integer,
    course_idcourse integer,
    person_idperson integer
);


ALTER TABLE public.session OWNER TO postgres;

--
-- TOC entry 219 (class 1259 OID 16644)
-- Name: session_idsession_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.session_idsession_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.session_idsession_seq OWNER TO postgres;

--
-- TOC entry 2945 (class 0 OID 0)
-- Dependencies: 219
-- Name: session_idsession_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.session_idsession_seq OWNED BY public.session.idsession;


--
-- TOC entry 2743 (class 2604 OID 16563)
-- Name: answer idanswer; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.answer ALTER COLUMN idanswer SET DEFAULT nextval('public.answer_idanswer_seq'::regclass);


--
-- TOC entry 2744 (class 2604 OID 16574)
-- Name: course idcourse; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.course ALTER COLUMN idcourse SET DEFAULT nextval('public.course_idcourse_seq'::regclass);


--
-- TOC entry 2745 (class 2604 OID 16585)
-- Name: courseadmin idcourseadmin; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.courseadmin ALTER COLUMN idcourseadmin SET DEFAULT nextval('public.courseadmin_idcourseadmin_seq'::regclass);


--
-- TOC entry 2746 (class 2604 OID 16597)
-- Name: logs idlogs; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.logs ALTER COLUMN idlogs SET DEFAULT nextval('public.logs_idlogs_seq'::regclass);


--
-- TOC entry 2747 (class 2604 OID 16608)
-- Name: participant idparticipant; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.participant ALTER COLUMN idparticipant SET DEFAULT nextval('public.participant_idparticipant_seq'::regclass);


--
-- TOC entry 2748 (class 2604 OID 16616)
-- Name: person idperson; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.person ALTER COLUMN idperson SET DEFAULT nextval('public.person_idperson_seq'::regclass);


--
-- TOC entry 2749 (class 2604 OID 16627)
-- Name: question idquestion; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.question ALTER COLUMN idquestion SET DEFAULT nextval('public.question_idquestion_seq'::regclass);


--
-- TOC entry 2750 (class 2604 OID 16638)
-- Name: quiz idquiz; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.quiz ALTER COLUMN idquiz SET DEFAULT nextval('public.quiz_idquiz_seq'::regclass);


--
-- TOC entry 2751 (class 2604 OID 16649)
-- Name: session idsession; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.session ALTER COLUMN idsession SET DEFAULT nextval('public.session_idsession_seq'::regclass);


--
-- TOC entry 2914 (class 0 OID 16560)
-- Dependencies: 204
-- Data for Name: answer; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.answer (idanswer, question_idquestion, person_idperson, session_idsession, correct, answers, necessarytime, "position") FROM stdin;
\.


--
-- TOC entry 2916 (class 0 OID 16571)
-- Dependencies: 206
-- Data for Name: course; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.course (idcourse, course_name, course_description, semester, lecturer, course_password) FROM stdin;
1	Silascourse	Top	1	Silas Greeb	1234
2	Marvincourse	Top	2	Marvin Seck-Eichhorn	1234
3	Jonascourse	Top	3	Jonas Kuche	1234
4	Danielacourse	Top	4	Daniela Coenen	1234
5	Piacourse	Top	5	Pia Ritzke	1234
6	Brittacourse	Top	6	Britta Fichtl	1234
7	Claudecourse	Top	7	Claude	1234
\.


--
-- TOC entry 2918 (class 0 OID 16582)
-- Dependencies: 208
-- Data for Name: courseadmin; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.courseadmin (idcourseadmin, course_idcourse, person_idperson, role) FROM stdin;
1	1	1	Professor
2	2	2	Professor
3	3	3	Professor
4	4	4	Professor
5	5	5	Professor
6	6	6	Professor
7	7	7	Professor
8	2	1	Dozent
\.


--
-- TOC entry 2920 (class 0 OID 16594)
-- Dependencies: 210
-- Data for Name: logs; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.logs (idlogs, person_idperson, "timestamp", action) FROM stdin;
\.


--
-- TOC entry 2922 (class 0 OID 16605)
-- Dependencies: 212
-- Data for Name: participant; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.participant (idparticipant, course_idcourse, person_idperson) FROM stdin;
9	1	2
10	1	3
11	1	4
12	1	5
13	1	6
14	1	7
15	2	1
16	2	3
17	2	4
18	2	5
19	2	6
20	2	7
21	3	1
22	3	2
23	3	4
24	3	5
25	3	6
26	3	7
27	4	1
28	4	2
29	4	3
30	4	5
31	4	6
32	4	7
33	5	1
34	5	2
35	5	3
36	5	4
37	5	6
38	5	7
39	6	1
40	6	2
41	6	3
42	6	4
43	6	5
44	6	7
45	7	1
46	7	2
47	7	3
48	7	4
49	7	5
50	7	6
\.


--
-- TOC entry 2924 (class 0 OID 16613)
-- Dependencies: 214
-- Data for Name: person; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.person (idperson, name, employee, faculty, title, pseudonym) FROM stdin;
1	Silas	t	MNI	\N	sgrb09
2	Marvin	t	MNI	\N	marvin
3	Jonas	t	MNI	\N	jonas
4	Daniela	t	MNI	\N	daniela
5	Pia	t	MNI	\N	pia
6	Britta	t	MNI	\N	britta
7	Claude	t	MNI	\N	claude
\.


--
-- TOC entry 2926 (class 0 OID 16624)
-- Dependencies: 216
-- Data for Name: question; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.question (idquestion, questiontext, type, quiz_idquiz, answers, correctanswers) FROM stdin;
2	Was ist 1 + 1?	Multiplechoice	1	{"antwort1": 1,"antwort2": 2,"antwort3": 3,"antwort4": 4}	{"antwort1": false,"antwort2": true,"antwort3": false,"antwort4": false}
3	Was ist 2 + 2?	Multiplechoice	1	{"antwort1": 1,"antwort2": 2,"antwort3": 3,"antwort4": 4}	{"antwort1": false,"antwort2": false,"antwort3": false,"antwort4": true}
4	Was ist 1 + 1?	Multiplechoice	2	{"antwort1": 1,"antwort2": 2,"antwort3": 3,"antwort4": 4}	{"antwort1": false,"antwort2": true,"antwort3": false,"antwort4": false}
5	Was ist 2 + 2?	Multiplechoice	2	{"antwort1": 1,"antwort2": 2,"antwort3": 3,"antwort4": 4}	{"antwort1": false,"antwort2": false,"antwort3": false,"antwort4": true}
6	Was ist 1 + 1?	Multiplechoice	3	{"antwort1": 1,"antwort2": 2,"antwort3": 3,"antwort4": 4}	{"antwort1": false,"antwort2": true,"antwort3": false,"antwort4": false}
7	Was ist 2 + 2?	Multiplechoice	3	{"antwort1": 1,"antwort2": 2,"antwort3": 3,"antwort4": 4}	{"antwort1": false,"antwort2": false,"antwort3": false,"antwort4": true}
8	Was ist 1 + 1?	Multiplechoice	4	{"antwort1": 1,"antwort2": 2,"antwort3": 3,"antwort4": 4}	{"antwort1": false,"antwort2": true,"antwort3": false,"antwort4": false}
9	Was ist 2 + 2?	Multiplechoice	4	{"antwort1": 1,"antwort2": 2,"antwort3": 3,"antwort4": 4}	{"antwort1": false,"antwort2": false,"antwort3": false,"antwort4": true}
10	Was ist 1 + 1?	Multiplechoice	5	{"antwort1": 1,"antwort2": 2,"antwort3": 3,"antwort4": 4}	{"antwort1": false,"antwort2": true,"antwort3": false,"antwort4": false}
11	Was ist 2 + 2?	Multiplechoice	5	{"antwort1": 1,"antwort2": 2,"antwort3": 3,"antwort4": 4}	{"antwort1": false,"antwort2": false,"antwort3": false,"antwort4": true}
12	Was ist 1 + 1?	Multiplechoice	6	{"antwort1": 1,"antwort2": 2,"antwort3": 3,"antwort4": 4}	{"antwort1": false,"antwort2": true,"antwort3": false,"antwort4": false}
13	Was ist 2 + 2?	Multiplechoice	6	{"antwort1": 1,"antwort2": 2,"antwort3": 3,"antwort4": 4}	{"antwort1": false,"antwort2": false,"antwort3": false,"antwort4": true}
14	Was ist 1 + 1?	Multiplechoice	7	{"antwort1": 1,"antwort2": 2,"antwort3": 3,"antwort4": 4}	{"antwort1": false,"antwort2": true,"antwort3": false,"antwort4": false}
15	Was ist 2 + 2?	Multiplechoice	7	{"antwort1": 1,"antwort2": 2,"antwort3": 3,"antwort4": 4}	{"antwort1": false,"antwort2": false,"antwort3": false,"antwort4": true}
\.


--
-- TOC entry 2928 (class 0 OID 16635)
-- Dependencies: 218
-- Data for Name: quiz; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.quiz (idquiz, quiz_name, quiz_password, quiz_description, quiz_creator_idperson) FROM stdin;
1	Silastest	1234	Spaß	1
2	Marvintest	1234	Spaß	2
3	Jonastest	1234	Spaß	3
4	Danielatest	1234	Spaß	4
5	Piatest	1234	Spaß	5
6	Brittatest	1234	Spaß	6
7	Claudetest	1234	Spaß	7
\.


--
-- TOC entry 2930 (class 0 OID 16646)
-- Dependencies: 220
-- Data for Name: session; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.session (idsession, start, "end", anonym, quiz_idquiz, course_idcourse, person_idperson) FROM stdin;
\.


--
-- TOC entry 2946 (class 0 OID 0)
-- Dependencies: 203
-- Name: answer_idanswer_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.answer_idanswer_seq', 1, false);


--
-- TOC entry 2947 (class 0 OID 0)
-- Dependencies: 205
-- Name: course_idcourse_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.course_idcourse_seq', 7, true);


--
-- TOC entry 2948 (class 0 OID 0)
-- Dependencies: 207
-- Name: courseadmin_idcourseadmin_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.courseadmin_idcourseadmin_seq', 9, true);


--
-- TOC entry 2949 (class 0 OID 0)
-- Dependencies: 209
-- Name: logs_idlogs_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.logs_idlogs_seq', 1, false);


--
-- TOC entry 2950 (class 0 OID 0)
-- Dependencies: 211
-- Name: participant_idparticipant_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.participant_idparticipant_seq', 50, true);


--
-- TOC entry 2951 (class 0 OID 0)
-- Dependencies: 213
-- Name: person_idperson_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.person_idperson_seq', 8, true);


--
-- TOC entry 2952 (class 0 OID 0)
-- Dependencies: 215
-- Name: question_idquestion_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.question_idquestion_seq', 16, true);


--
-- TOC entry 2953 (class 0 OID 0)
-- Dependencies: 217
-- Name: quiz_idquiz_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.quiz_idquiz_seq', 9, true);


--
-- TOC entry 2954 (class 0 OID 0)
-- Dependencies: 219
-- Name: session_idsession_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.session_idsession_seq', 1, false);


--
-- TOC entry 2753 (class 2606 OID 16568)
-- Name: answer answer_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.answer
    ADD CONSTRAINT answer_pkey PRIMARY KEY (idanswer);


--
-- TOC entry 2755 (class 2606 OID 16579)
-- Name: course course_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.course
    ADD CONSTRAINT course_pkey PRIMARY KEY (idcourse);


--
-- TOC entry 2757 (class 2606 OID 16590)
-- Name: courseadmin courseadmin_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.courseadmin
    ADD CONSTRAINT courseadmin_pkey PRIMARY KEY (idcourseadmin);


--
-- TOC entry 2760 (class 2606 OID 16602)
-- Name: logs logs_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.logs
    ADD CONSTRAINT logs_pkey PRIMARY KEY (idlogs);


--
-- TOC entry 2764 (class 2606 OID 16610)
-- Name: participant participant_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.participant
    ADD CONSTRAINT participant_pkey PRIMARY KEY (idparticipant);


--
-- TOC entry 2766 (class 2606 OID 16621)
-- Name: person person_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.person
    ADD CONSTRAINT person_pkey PRIMARY KEY (idperson);


--
-- TOC entry 2768 (class 2606 OID 16632)
-- Name: question question_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.question
    ADD CONSTRAINT question_pkey PRIMARY KEY (idquestion);


--
-- TOC entry 2770 (class 2606 OID 16643)
-- Name: quiz quiz_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.quiz
    ADD CONSTRAINT quiz_pkey PRIMARY KEY (idquiz);


--
-- TOC entry 2773 (class 2606 OID 16651)
-- Name: session session_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.session
    ADD CONSTRAINT session_pkey PRIMARY KEY (idsession);


--
-- TOC entry 2761 (class 1259 OID 16669)
-- Name: fki_course; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX fki_course ON public.participant USING btree (course_idcourse);


--
-- TOC entry 2762 (class 1259 OID 16680)
-- Name: fki_perso; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX fki_perso ON public.participant USING btree (person_idperson);


--
-- TOC entry 2758 (class 1259 OID 16658)
-- Name: fki_person; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX fki_person ON public.logs USING btree (person_idperson);


--
-- TOC entry 2771 (class 1259 OID 16696)
-- Name: fki_quiz; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX fki_quiz ON public.session USING btree (quiz_idquiz);


--
-- TOC entry 2780 (class 2606 OID 16664)
-- Name: participant course; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.participant
    ADD CONSTRAINT course FOREIGN KEY (course_idcourse) REFERENCES public.course(idcourse) ON UPDATE CASCADE ON DELETE CASCADE NOT VALID;


--
-- TOC entry 2777 (class 2606 OID 16681)
-- Name: courseadmin course; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.courseadmin
    ADD CONSTRAINT course FOREIGN KEY (course_idcourse) REFERENCES public.course(idcourse) ON UPDATE CASCADE ON DELETE CASCADE NOT VALID;


--
-- TOC entry 2785 (class 2606 OID 16697)
-- Name: session course; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.session
    ADD CONSTRAINT course FOREIGN KEY (course_idcourse) REFERENCES public.course(idcourse) ON UPDATE CASCADE ON DELETE CASCADE NOT VALID;


--
-- TOC entry 2779 (class 2606 OID 16653)
-- Name: logs person; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.logs
    ADD CONSTRAINT person FOREIGN KEY (person_idperson) REFERENCES public.person(idperson) ON UPDATE CASCADE ON DELETE CASCADE NOT VALID;


--
-- TOC entry 2778 (class 2606 OID 16686)
-- Name: courseadmin person; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.courseadmin
    ADD CONSTRAINT person FOREIGN KEY (person_idperson) REFERENCES public.person(idperson) ON UPDATE CASCADE ON DELETE CASCADE NOT VALID;


--
-- TOC entry 2786 (class 2606 OID 16702)
-- Name: session person; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.session
    ADD CONSTRAINT person FOREIGN KEY (person_idperson) REFERENCES public.person(idperson) ON UPDATE CASCADE ON DELETE CASCADE NOT VALID;


--
-- TOC entry 2783 (class 2606 OID 16707)
-- Name: quiz person; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.quiz
    ADD CONSTRAINT person FOREIGN KEY (quiz_creator_idperson) REFERENCES public.person(idperson) ON UPDATE CASCADE ON DELETE CASCADE NOT VALID;


--
-- TOC entry 2775 (class 2606 OID 16722)
-- Name: answer person; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.answer
    ADD CONSTRAINT person FOREIGN KEY (person_idperson) REFERENCES public.person(idperson) ON UPDATE CASCADE ON DELETE CASCADE NOT VALID;


--
-- TOC entry 2781 (class 2606 OID 16675)
-- Name: participant person1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.participant
    ADD CONSTRAINT person1 FOREIGN KEY (person_idperson) REFERENCES public.person(idperson) ON UPDATE CASCADE ON DELETE CASCADE NOT VALID;


--
-- TOC entry 2774 (class 2606 OID 16717)
-- Name: answer question; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.answer
    ADD CONSTRAINT question FOREIGN KEY (question_idquestion) REFERENCES public.question(idquestion) ON UPDATE CASCADE ON DELETE CASCADE NOT VALID;


--
-- TOC entry 2784 (class 2606 OID 16691)
-- Name: session quiz; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.session
    ADD CONSTRAINT quiz FOREIGN KEY (quiz_idquiz) REFERENCES public.quiz(idquiz) ON UPDATE CASCADE ON DELETE CASCADE NOT VALID;


--
-- TOC entry 2782 (class 2606 OID 16712)
-- Name: question quiz; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.question
    ADD CONSTRAINT quiz FOREIGN KEY (quiz_idquiz) REFERENCES public.quiz(idquiz) ON UPDATE CASCADE ON DELETE CASCADE NOT VALID;


--
-- TOC entry 2776 (class 2606 OID 16727)
-- Name: answer session; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.answer
    ADD CONSTRAINT session FOREIGN KEY (session_idsession) REFERENCES public.session(idsession) ON UPDATE CASCADE ON DELETE CASCADE NOT VALID;


-- Completed on 2020-06-09 11:34:28

--
-- PostgreSQL database dump complete
--

