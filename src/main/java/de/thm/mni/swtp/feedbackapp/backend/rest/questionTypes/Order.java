package de.thm.mni.swtp.feedbackapp.backend.rest.questionTypes;

import de.thm.mni.swtp.feedbackapp.backend.rest.Utils;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

public abstract class Order extends QuestionType {
  private final JsonArray correctAnswers;

  Order(JsonArray correctAnswers) {
    this.correctAnswers = correctAnswers;
  }

  @Override
  public boolean validateAnswer(JsonArray givenAnswers) {
    if (correctAnswers.size() != givenAnswers.size()) {
      return false;
    }
    for (int i = 0; i < correctAnswers.size(); i++) {
      String correctAnswer = correctAnswers.getString(i);
      String givenAnswer = givenAnswers.getString(i);
      if (!correctAnswer.equals(givenAnswer)) {
        return false;
      }
    }
    return true;
  }

  @Override
  public JsonArray getCorrectAnswers() {
    return correctAnswers;
  }

  @Override
  public Object getPossibleAnswers() {
    return Utils.shuffle(correctAnswers);
  }

  @Override
  public Object computeStats(JsonArray answers) {
    int correct = 0;
    int incorrect = 0;
    for (int i = 0; i < answers.size(); i++) {
      JsonObject answer = answers.getJsonObject(i);
      if (answer.getBoolean("correct")) {
        correct++;
      } else {
        incorrect++;
      }
    }
    return new JsonObject().put("correct", correct).put("incorrect", incorrect);
  }
}
