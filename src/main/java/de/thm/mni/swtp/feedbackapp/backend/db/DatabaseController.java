package de.thm.mni.swtp.feedbackapp.backend.db;

import de.thm.mni.swtp.feedbackapp.backend.services.DBService;
import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.pgclient.PgPool;
import io.vertx.sqlclient.Row;
import io.vertx.sqlclient.RowSet;
import io.vertx.sqlclient.Tuple;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public class DatabaseController implements DBService {

  PgPool client;

  DatabaseController(PgPool client) {
    this.client = client;
  }

  //QUESTION:

  /*
  listquestions
  createquestion
  deletequestion  
  getquestion 
  updatequestion
  
  */

  @Override
  public void listQuestions(int idQuiz, Handler<AsyncResult<JsonArray>> result) {
    JsonArray res = new JsonArray();
    String query = "SELECT * FROM question where quiz_idquiz = $1 ORDER BY position ASC";
    client
        .preparedQuery(query)
        .execute(Tuple.of(idQuiz), ar -> {
          if (ar.succeeded()) {
            RowSet<Row> rows = ar.result();
            for (Row row : rows) {
              JsonObject question = new JsonObject();
              question.put("id", row.getInteger(0));
              question.put("text", row.getString(1));
              question.put("type", row.getString(2));
              question.put("idQuiz", row.getInteger(3));
              question.put("answers", row.getValue(4));
              question.put("position", row.getInteger(5));
              res.add(question);
            }
            result.handle(Future.succeededFuture(res));
          } else {
            result.handle(Future.failedFuture(ar.cause()));
          }
        });
  }

  @Override
  public void createQuestion(String questiontext, String type, int idquiz, JsonObject answersObject,
      JsonArray answersArray, Handler<AsyncResult<JsonObject>> result) {
    String query = "insert into question(questiontext, type, quiz_idquiz, answers, position) " +
        "values ($1,$2,$3,$4, (SELECT COALESCE(max(position), 0)+1 FROM question " +
        "WHERE quiz_idquiz = $3)) RETURNING position";
    Object answers = null;
    if (answersObject != null) {
      answers = answersObject;
    } else if (answersArray != null) {
      answers = answersArray;
    }
    client.preparedQuery(query)
        .execute(Tuple.of(questiontext, type, idquiz, answers), ar -> {
          if (ar.succeeded()) {
            RowSet<Row> rows = ar.result();
            if (!rows.iterator().hasNext()) {
              result.handle(Future.succeededFuture());
              return;
            }
            Row row = rows.iterator().next();
            JsonObject res = new JsonObject().put("position", row.getInteger(0));
            result.handle(Future.succeededFuture(res));
          } else {
            result.handle(Future.failedFuture(ar.cause()));
          }
        });
  }

  @Override
  public void deleteQuestion(int idquiz, int position, Handler<AsyncResult<Void>> result) {
    String query = "delete from question where quiz_idquiz = $1 and position = $2";
    client.preparedQuery(query)
        .execute(Tuple.of(idquiz, position), ar -> {
          if (ar.succeeded()) {
            result.handle(Future.succeededFuture());
          } else {
            result.handle(Future.failedFuture(ar.cause()));
          }
        });
  }

  @Override
  public void getQuestion(int idquiz, int position, Handler<AsyncResult<JsonObject>> result) {
    String query = "select * from question where quiz_idquiz = $1 and position = $2";
    client.preparedQuery(query)
        .execute(Tuple.of(idquiz, position), ar -> {
          if (ar.succeeded()) {
            RowSet<Row> rows = ar.result();
            if (!rows.iterator().hasNext()) {
              result.handle(Future.succeededFuture());
              return;
            }
            Row row = rows.iterator().next();
            JsonObject question = new JsonObject();
            question.put("id", row.getInteger(0));
            question.put("text", row.getString(1));
            question.put("type", row.getString(2));
            question.put("quiz_idquiz", row.getInteger(3));
            question.put("answers", row.getValue(4));
            question.put("position", row.getInteger(5));
            result.handle(Future.succeededFuture(question));
          } else {
            result.handle(Future.failedFuture(ar.cause()));
          }
        });
  }

  @Override
  public void updateQuestion(int idquiz, int position, String questiontext, String type,
      JsonObject answersObject, JsonArray answersArray, Handler<AsyncResult<Void>> result) {
    String query = "update question set questiontext = $1, type = $2, answers = $3 where quiz_idquiz = $4 AND position = $5";
    Object answers = null;
    if (answersObject != null) {
      answers = answersObject;
    } else if (answersArray != null) {
      answers = answersArray;
    }
    client.preparedQuery(query)
        .execute(
            Tuple.of(questiontext, type, answers, idquiz, position),
            ar -> {
              if (ar.succeeded()) {
                result.handle(Future.succeededFuture());
              } else {
                result.handle(Future.failedFuture(ar.cause()));
              }
            });
  }

  //QUIZ:

/*
listquizzes
createquiz
deletequiz
getquiz
updatequiz
listquizsession
createQuizSession
 */

  @Override
  public void listQuizzes(int userID, Handler<AsyncResult<JsonArray>> result) {
    String query = "SELECT idquiz, quiz_name, quiz_password, quiz_description, quiz_creator_idperson, type, " +
        "instant_feedback, (SELECT COUNT(*) FROM question WHERE quiz_idquiz = idquiz) AS question_count " +
        "FROM quiz WHERE quiz_creator_idperson = $1";
    client.preparedQuery(query).execute(Tuple.of(userID), ar -> {
      if (ar.succeeded()) {
        JsonArray res = new JsonArray();
        RowSet<Row> rows = ar.result();
        for (Row row : rows) {
          JsonObject quiz = rowToJson(row, quizFields);
          res.add(quiz);
        }
        result.handle(Future.succeededFuture(res));
      } else {
        result.handle(Future.failedFuture(ar.cause()));
      }
    });
  }

  @Override
  public void createQuiz(int id, String name, int password, String description, String type,
      boolean instantFeedback, int idperson, Handler<AsyncResult<Void>> result) {
    String query = "insert into quiz(idquiz, quiz_name, quiz_password, quiz_description, type, instant_feedback, quiz_creator_idperson) values ($1,$2,$3,$4,$5,$6,$7)";
    client.preparedQuery(query)
        .execute(Tuple.of(id, name, password, description, type, instantFeedback, idperson), ar -> {
          if (ar.succeeded()) {
            result.handle(Future.succeededFuture());
          } else {
            result.handle(Future.failedFuture(ar.cause()));
          }
        });
  }

  @Override
  public void deleteQuiz(int idquiz, Handler<AsyncResult<Void>> result) {
    String query = "delete from quiz where idquiz = $1";
    client.preparedQuery(query)
        .execute(Tuple.of(idquiz), ar -> {
          if (ar.succeeded()) {
            result.handle(Future.succeededFuture());
          } else {
            result.handle(Future.failedFuture(ar.cause()));
          }
        });
  }

  @Override
  public void getQuiz(int idquiz, Handler<AsyncResult<JsonObject>> result) {
    String query = "SELECT idquiz, quiz_name, quiz_password, quiz_description, quiz_creator_idperson, type, " +
        "instant_feedback, (SELECT COUNT(*) FROM question WHERE quiz_idquiz = idquiz) AS " +
        "question_count FROM quiz WHERE idquiz = $1";
    client.preparedQuery(query)
        .execute(Tuple.of(idquiz), ar -> {
          if (ar.succeeded()) {
            RowSet<Row> rows = ar.result();
            if (!rows.iterator().hasNext()) {
              result.handle(Future.succeededFuture());
              return;
            }
            Row row = rows.iterator().next();
            JsonObject quiz = rowToJson(row, quizFields);
            result.handle(Future.succeededFuture(quiz));
          } else {
            result.handle(Future.failedFuture(ar.cause()));
          }
        });
  }

  @Override
  public void updateQuiz(int idquiz, String name, int password, String description, String type,
      boolean instantFeedback, int idperson, Handler<AsyncResult<Void>> result) {
    String query = "update quiz set quiz_name = $1, quiz_password = $2, quiz_description = $3, type = $4, quiz_creator_idperson = $5, instant_feedback = $6 where idquiz = $7";
    client.preparedQuery(query)
        .execute(Tuple.of(name, password, description, type, idperson, instantFeedback, idquiz), ar -> {
          if (ar.succeeded()) {
            result.handle(Future.succeededFuture());
          } else {
            result.handle(Future.failedFuture(ar.cause()));
          }
        });
  }

  @Override
  public void listQuizSessions(int idquiz, Handler<AsyncResult<JsonArray>> result) {
    JsonArray res = new JsonArray();
    String query = "SELECT idsession, start, \"end\", anonym, session_description, show_correct, session_creator_idperson, " +
        "idcourse, course_name, course_description, semester, lecturer_idperson, course_password, fbs, " +
        "d.idperson, d.fbs_id AS \"fbsID\", d.username, d.firstname, d.lastname, d.docent " +
        "FROM session LEFT JOIN course ON course_idcourse = idcourse LEFT JOIN person d ON " +
        "course.lecturer_idperson = d.idperson WHERE quiz_idquiz = $1";
    client.preparedQuery(query)
        .execute(Tuple.of(idquiz), ar -> {
          if (ar.succeeded()) {
            RowSet<Row> rows = ar.result();
            for (Row row : rows) {
              JsonObject session = rowToJson(row, sessionFields);
              if (row.getInteger("idcourse") != null) {
                JsonObject course = rowToJson(row, courseFields);
                JsonObject docent = rowToJson(row, personFields);
                course.put("docent", docent);
                session.put("course", course);
              } else {
                session.putNull("course");
              }
              res.add(session);
            }
            result.handle(Future.succeededFuture(res));
          } else {
            result.handle(Future.failedFuture(ar.cause()));
          }
        });
  }

  private static final ZoneId UTC_TIMEZONE = ZoneId.of("UTC");

  @Override
  public void createQuizSession(int id, long start, long end, boolean anonym, String description, boolean showCorrect,
      int idquiz, Integer idcourse, int idperson, Handler<AsyncResult<Void>> result) {
    LocalDateTime startUTC = Instant.ofEpochSecond(start).atZone(UTC_TIMEZONE).toLocalDateTime();
    LocalDateTime endUTC = Instant.ofEpochSecond(end).atZone(UTC_TIMEZONE).toLocalDateTime();
    if (description == null) {
      description = "";
    }
    String query = "INSERT INTO session(idsession, start, \"end\", anonym, session_description, show_correct, " +
        "quiz_idquiz, course_idcourse, session_creator_idperson) values ($1,$2,$3,$4,$5,$6,$7,$8,$9)";
    client.preparedQuery(query)
        .execute(Tuple.of(id, startUTC, endUTC, anonym, description, showCorrect, idquiz, idcourse, idperson), ar -> {
          if (ar.succeeded()) {
            result.handle(Future.succeededFuture());
          } else {
            result.handle(Future.failedFuture(ar.cause()));
          }
        });
  }

//Course:

/*
createCourse
getCourse
updateCourse
joinCourse
addSession
listSessions
addAdmin
listAdmin
removeAdmin


*/

  @Override
  public void createCourse(int courseID, String name, String description, String semester, int lecturerID,
                           Integer password, boolean fbs, boolean allAuthor, Handler<AsyncResult<Void>> result) {
    String query = "insert into course(idcourse, course_name, course_description, semester, lecturer_idperson, course_password, fbs, all_author) values ($1,$2,$3,$4,$5,$6,$7,$8)";
    client.preparedQuery(query)
        .execute(Tuple.of(courseID, name, description, semester, lecturerID, password, fbs, allAuthor), ar -> {
          if (ar.succeeded()) {
            result.handle(Future.succeededFuture());
          } else {
            result.handle(Future.failedFuture(ar.cause()));
          }
        });
  }

  @Override
  public void browseCourse(String search, int perPage, int page, Integer hideJoined,
      Handler<AsyncResult<JsonArray>> result) {
    if (search == null) {
      search = "";
    }
    if (hideJoined == null) {
      hideJoined = 0;
    }
    String query = "SELECT idcourse, course_name, course_description, semester, lecturer_idperson, course_password, fbs, all_author, " +
        "idperson, fbs_id AS \"fbsID\", username, firstname, lastname, docent " +
        "FROM course LEFT JOIN course_role ON idcourse = course_idcourse JOIN person ON " +
        "lecturer_idperson = idperson WHERE course_name LIKE '%'||$1||'%' AND ($4 = 0 OR " +
        "(SELECT COUNT(*) FROM course_role WHERE course_idcourse = idcourse AND person_idperson = " +
        "$4 AND role != 'none') = 0) GROUP BY idcourse, idperson ORDER BY course_name ASC LIMIT $2 OFFSET $3";
    client.preparedQuery(query)
        .execute(Tuple.of(search, perPage, perPage * page, hideJoined), ar -> {
          if (ar.succeeded()) {
            JsonArray courses = new JsonArray();
            RowSet<Row> rows = ar.result();
            for (Row row : rows) {
              JsonObject course = rowToJson(row, courseFields);
              JsonObject docent = rowToJson(row, personFields);
              course.put("docent", docent);
              courses.add(course);
            }
            result.handle(Future.succeededFuture(courses));
          } else {
            result.handle(Future.failedFuture(ar.cause()));
          }
        });
  }


  @Override
  public void getCourse(int idcourse, Handler<AsyncResult<JsonObject>> result) {
    String query = "SELECT idcourse, course_name, course_description, semester, lecturer_idperson, course_password, fbs, all_author, " +
        "idperson, fbs_id AS \"fbsID\", username, firstname, lastname, docent " +
        "FROM course JOIN person ON lecturer_idperson = idperson WHERE idcourse = $1";
    client.preparedQuery(query)
        .execute(Tuple.of(idcourse), ar -> {
          if (ar.succeeded()) {
            RowSet<Row> rows = ar.result();
            if (!rows.iterator().hasNext()) {
              result.handle(Future.succeededFuture());
              return;
            }
            Row row = rows.iterator().next();
            JsonObject course = rowToJson(row, courseFields);
            JsonObject docent = rowToJson(row, personFields);
            course.put("docent", docent);
            result.handle(Future.succeededFuture(course));
          } else {
            result.handle(Future.failedFuture(ar.cause()));
          }
        });
  }

  @Override
  public void updateCourse(int idcourse, String name, String description, String semester,
                           int lecturer, Integer password, boolean fbs, boolean allAuthor, Handler<AsyncResult<Void>> result) {
    String query = "update course set course_name = $1, course_description = $2, semester = $3, lecturer_idperson = $4, course_password = $5, fbs = $6, all_author = $7 where idcourse = $8";
    client.preparedQuery(query)
        .execute(Tuple.of(name, description, semester, lecturer, password, fbs, allAuthor, idcourse), ar -> {
          if (ar.succeeded()) {
            result.handle(Future.succeededFuture());
          } else {
            result.handle(Future.failedFuture(ar.cause()));
          }
        });
  }

  @Override
  public void deleteCourse(int idcourse, Handler<AsyncResult<Void>> result) {
    String query = "DELETE FROM course WHERE idcourse = $1";
    client.preparedQuery(query)
        .execute(Tuple.of(idcourse), ar -> {
          if (ar.succeeded()) {
            result.handle(Future.succeededFuture());
          } else {
            result.handle(Future.failedFuture(ar.cause()));
          }
        });
  }

  @Override
  public void joinCourse(int idcourse, int idperson, Handler<AsyncResult<Void>> result) {
    String query = "insert into course_role(course_idcourse, person_idperson, role) VALUES ($1, $2, 'participant')";
    client.preparedQuery(query)
        .execute(Tuple.of(idcourse, idperson), ar -> {
          if (ar.succeeded()) {
            result.handle(Future.succeededFuture());
          } else {
            result.handle(Future.failedFuture(ar.cause()));
          }
        });
  }

  //Die Methode klappt noch nicht so ganz, wegen den OffserDateTime Sachen
  @Override
  public void addSession(int idsession, int idcourse, Handler<AsyncResult<Void>> result) {
    String query = "UPDATE session SET course_idcourse = $2 WHERE idsession = $1";
    client.preparedQuery(query)
        .execute(Tuple.of(idsession, idcourse), ar -> {
          if (ar.succeeded()) {
            result.handle(Future.succeededFuture());
          } else {
            result.handle(Future.failedFuture(ar.cause()));
          }
        });
  }


  @Override
  public void listSessions(int idcourse, int idperson, boolean hideNonActive, Handler<AsyncResult<JsonArray>> result) {
    String query = "SELECT idsession, start, \"end\", anonym, quiz_idquiz, session_description, show_correct, " +
        "idcourse, course_name, course_description, semester, lecturer_idperson, course_password, fbs, " +
        "d.idperson, d.fbs_id AS \"fbsID\", d.username, d.firstname, d.lastname, d.docent, " +
        "c.username AS creator_username, c.firstname AS creator_firstname, c.lastname AS creator_lastname, " +
        "c.idperson AS creator_idperson, c.fbs_id AS creator_fbsid, c.docent AS creator_docent, " +
        "course_idcourse, session_creator_idperson, idquiz, quiz_name, quiz_description, " +
        "quiz.type, quiz.instant_feedback, (SELECT COUNT(*) FROM question " +
        "WHERE quiz_idquiz = idquiz) AS question_count, (SELECT COUNT(*) = (SELECT COUNT(*) FROM " +
        "question WHERE quiz_idquiz = idquiz) FROM answer WHERE " +
        "session_idsession = idsession AND person_idperson = $2) AS complete FROM session " +
        "LEFT JOIN course ON course_idcourse = idcourse LEFT JOIN person d ON " +
        "course.lecturer_idperson = d.idperson JOIN quiz ON quiz_idquiz = idquiz JOIN person c ON " +
        "session_creator_idperson = c.idperson WHERE course_idcourse = $1";

    if (hideNonActive) {
      query += " AND \"end\" > NOW() AND start < NOW()";
    }

    client.preparedQuery(query)
        .execute(Tuple.of(idcourse, idperson), ar -> {
          if (ar.succeeded()) {
            handleSessionQueryResult(ar.result(), result);
          } else {
            result.handle(Future.failedFuture(ar.cause()));
          }
        });
  }

  @Override
  public void listParticipants(int idcourse, List<String> roles, Handler<AsyncResult<JsonArray>> result) {
    StringBuilder query = new StringBuilder("SELECT course_idcourse, idperson, username, " +
        "firstname, lastname, fbs_id AS \"fbsID\", docent, role FROM course_role JOIN person ON person_idperson = idperson " +
        "WHERE course_idcourse = $1");
    if (roles.size() > 0) {
      query.append(" AND (");
      for (int i = 0; i < roles.size(); i++) {
        query.append("role = $").append(i+2);
        if (i < roles.size()-1) {
          query.append(" OR ");
        }
      }
      query.append(")");
    }
    List<Object> queryArguments = new LinkedList<>();
    queryArguments.add(idcourse);
    queryArguments.addAll(roles);
    client.preparedQuery(query.toString())
        .execute(Tuple.tuple(queryArguments), ar -> {
          if (ar.succeeded()) {
            JsonArray res = new JsonArray();
            RowSet<Row> rows = ar.result();
            for (Row row : rows) {
              JsonObject person = rowToJson(row, personFields);
              person.put("role", row.getString("role"));
              res.add(person);
            }
            result.handle(Future.succeededFuture(res));
          } else {
            result.handle(Future.failedFuture(ar.cause()));
          }
        });
  }

  @Override
  public void isParticipant(int idcourse, int idperson, Handler<AsyncResult<Boolean>> result) {
    String query = "SELECT COUNT(*) FROM course_role WHERE course_idcourse = $1 AND person_idperson = $2";
    client.preparedQuery(query)
        .execute(Tuple.of(idcourse, idperson), ar -> {
          if (ar.succeeded()) {
            Row row = ar.result().iterator().next();
            result.handle(Future.succeededFuture(row.getInteger(0) == 1));
          } else {
            result.handle(Future.failedFuture(ar.cause()));
          }
        });
  }

  //SESSION: 

    /*
    getsession
    joinsession
    getquestion
    submitanswer
    getsessionstatistics
    getquestionstatistics
    */

  @Override
  public void getSession(int idsession, Handler<AsyncResult<JsonObject>> result) {
    JsonArray res = new JsonArray();
    String query = "SELECT idsession, start, \"end\", anonym, session_description, show_correct, session_creator_idperson, " +
        "idcourse, course_name, course_description, semester, lecturer_idperson, course_password, fbs, " +
        "d.idperson, d.fbs_id AS \"fbsID\", d.username, d.firstname, d.lastname, d.docent, " +
        "c.username AS creator_username, c.firstname AS creator_firstname, c.lastname AS creator_lastname, " +
        "c.idperson AS creator_idperson, c.fbs_id AS creator_fbsid, c.docent AS creator_docent, " +
        "idquiz, quiz_name, quiz_password, quiz_description, quiz_creator_idperson, type, instant_feedback, " +
        "(SELECT COUNT(*) FROM question WHERE quiz_idquiz = idquiz) AS question_count " +
        "FROM session JOIN quiz ON quiz_idquiz = idquiz LEFT JOIN course ON course_idcourse = idcourse LEFT JOIN person d ON " +
        "course.lecturer_idperson = d.idperson JOIN person c ON session_creator_idperson = c.idperson WHERE idsession = $1";
    client.preparedQuery(query)
        .execute(Tuple.of(idsession), ar -> {
          if (ar.succeeded()) {
            RowSet<Row> rows = ar.result();
            if (rows.iterator().hasNext()) {
              Row row = rows.iterator().next();
              JsonObject session = rowToJson(row, sessionFields);
              JsonObject quiz = rowToJson(row, quizFields);
              session.put("quiz", quiz);
              if (row.getInteger("idcourse") != null) {
                JsonObject course = rowToJson(row, courseFields);
                JsonObject docent = rowToJson(row, personFields);
                course.put("docent", docent);
                session.put("course", course);
              } else {
                session.putNull("course");
              }
              JsonObject creator = new JsonObject()
                  .put("username", row.getString("creator_username"))
                  .put("firstname", row.getString("creator_firstname"))
                  .put("lastname", row.getString("creator_lastname"))
                  .put("id", row.getInteger("creator_idperson"))
                  .put("fbsID", row.getInteger("creator_fbsid"))
                  .put("docent", row.getBoolean("creator_docent"));
              session.put("creator", creator);
              result.handle(Future.succeededFuture(session));
            } else {
              result.handle(Future.succeededFuture(null));
            }
          } else {
            result.handle(Future.failedFuture(ar.cause()));
          }
        });
  }

  @Override
  public void getquestion(int idsession, int idquestion, Handler<AsyncResult<Void>> result) {
    JsonArray res = new JsonArray();
    String query = "select * from question where idquestion";
  }


  @Override
  public void submitAnswer(int idquestion, Integer idperson, String anonymIdentity, int idsession, boolean correct,
      JsonArray answers, long necessarytime, Handler<AsyncResult<Void>> result) {
    LocalDateTime necessarytimeUTC = Instant.ofEpochSecond(necessarytime).atZone(UTC_TIMEZONE).toLocalDateTime();
    String identityCondition;
    if (idperson != null) {
      identityCondition = "person_idperson = $2";
    } else if (anonymIdentity != null) {
      identityCondition = "anonym_identity = $3";
    } else {
      throw new IllegalArgumentException("one of idperson or anonymIdentity must not be null");
    }
    //https://www.the-art-of-web.com/sql/upsert/
    String where =  "WHERE question_idquestion = $1 AND session_idsession = $4 AND " + identityCondition;
    String update = "UPDATE answer SET correct = $5, answers = $6 " + where;
    String insert = "INSERT INTO answer (question_idquestion, person_idperson, anonym_identity, " +
        "session_idsession, correct, answers, necessarytime) SELECT $1,$2,$3,$4,$5,$6,$7";
    String query = "WITH upsert AS (" + update + " RETURNING *) " + insert + " WHERE NOT EXISTS " +
        "(SELECT * FROM upsert)";

    client.preparedQuery(query)
        .execute(Tuple.of(idquestion, idperson, anonymIdentity, idsession, correct, answers, necessarytimeUTC), ar -> {
          if (ar.succeeded()) {
            result.handle(Future.succeededFuture());
          } else {
            result.handle(Future.failedFuture(ar.cause()));
          }
        });
  }

  @Override
  public void getMyStatistics(int idsession, Integer idperson, String anonymIdentity,
      Handler<AsyncResult<JsonObject>> result) {
    String query;
    Tuple params;
    if (idperson != null) {
      query = "select * from answer where session_idsession = $1 and person_idperson = $2";
      params = Tuple.of(idsession, idperson);
    } else if (anonymIdentity != null) {
      query = "select * from answer where session_idsession = $1 and anonym_identity = $2";
      params = Tuple.of(idsession, anonymIdentity);
    } else {
      throw new IllegalArgumentException("Either id person or anonymIdentity is required");
    }
    client.preparedQuery(query)
        .execute(params, ar -> {
          if (ar.succeeded()) {
            RowSet<Row> rows = ar.result();
            int correctQuestions = 0;
            JsonObject stats = new JsonObject();
            stats.put("answeredQuestions", rows.rowCount());
            for (Row row : rows) {
              if (row.getBoolean(4)) {
                correctQuestions += 1;
              }
            }
            stats.put("correctQuestions", correctQuestions);
            result.handle(Future.succeededFuture(stats));
          } else {
            result.handle(Future.failedFuture(ar.cause()));
          }
        });
  }


  /*
  "select session_idsession, count(correct), " +
        "count(correct) filter ( where correct = true ) from answer " +
        "where session_idsession = $1 group by session_idsession";
   */
  @Override
  public void getSessionStatistics(int idsession, Handler<AsyncResult<JsonObject>> result) {
    JsonObject res = new JsonObject();
    String countQuery = "SELECT COUNT(*) FROM session JOIN quiz on session.quiz_idquiz = quiz.idquiz " +
        "JOIN question on quiz.idquiz = question.quiz_idquiz WHERE idsession = $1";
    String answersQuery = "SELECT person_idperson, anonym_identity, correct FROM answer WHERE " +
        "session_idsession = $1";
    client.preparedQuery(countQuery)
      .execute(Tuple.of(idsession), ar ->{
        if (ar.succeeded()) {
          RowSet<Row> rows = ar.result();
          Row row = rows.iterator().next();
          res.put("questionCount", row.getInteger(0));
          client.preparedQuery(answersQuery).execute(Tuple.of(idsession), br -> {
            if (br.succeeded()) {
              RowSet<Row> answerRows = br.result();
              JsonArray answers = new JsonArray();
              for (Row answerRow : answerRows) {
                JsonObject answerJson = rowToJson(answerRow, "person_idperson", "anonym_identity", "correct");
                answers.add(answerJson);
              }
              res.put("answers", answers);
              result.handle(Future.succeededFuture(res));
            } else {
              result.handle(Future.failedFuture(br.cause()));
            }
          });
        } else {
          result.handle(Future.failedFuture(ar.cause()));
      }
    });
  }

  @Override
  public void getQuestionStatistics(int idsession, int idquestion, Handler<AsyncResult<JsonObject>> result) {
    String query = "select question_idquestion, count(correct), " +
        "count(correct) filter ( where correct = true ) from answer where " +
        "session_idsession = $1 and question_idquestion = $2 group by question_idquestion";
    client.preparedQuery(query)
      .execute(Tuple.of(idsession, idquestion), ar -> {
        if (ar.succeeded()) {
          RowSet<Row> rows = ar.result();
          if (!rows.iterator().hasNext()) {
            result.handle(Future.succeededFuture());
            return;
          }
          Row row = rows.iterator().next();
          JsonObject statistics = new JsonObject();
          statistics.put("answered", row.getInteger(1));
          statistics.put("correct", row.getInteger(2));
          result.handle(Future.succeededFuture(statistics));
        } else {
          result.handle(Future.failedFuture(ar.cause()));
        }
      });
  }
  //Danielas Abfragen

  @Override
  public void getMyCourses(int idperson, Handler<AsyncResult<JsonArray>> result) {
    JsonArray res = new JsonArray();
    String query1 = "select * from course_role where person_idperson = $1";
    client.preparedQuery(query1)
        .execute(Tuple.of(idperson), ar -> {
          if (ar.succeeded()) {
            RowSet<Row> rows = ar.result();
            JsonObject question = new JsonObject();
            for (Row row : rows) {
              question.put("idcourse", row.getInteger(1));
              res.add(question);
            }
            result.handle(Future.succeededFuture(res));
          } else {
            result.handle(Future.failedFuture(ar.cause()));
          }
        });
  }

  @Override
  public void updateSession(int idsession, long start, long end, String description, boolean showCorrect, Handler<AsyncResult<Void>> result) {
    LocalDateTime startUTC = Instant.ofEpochSecond(start).atZone(UTC_TIMEZONE).toLocalDateTime();
    LocalDateTime endUTC = Instant.ofEpochSecond(end).atZone(UTC_TIMEZONE).toLocalDateTime();
    String query = "UPDATE session SET start = $1, \"end\" = $2, session_description = $3, show_correct = $4 WHERE idsession = $5";
    client.preparedQuery(query)
        .execute(Tuple.of(startUTC, endUTC, description, showCorrect, idsession), ar -> {
          if (ar.succeeded()) {
            result.handle(Future.succeededFuture());
          } else {
            result.handle(Future.failedFuture(ar.cause()));
          }
        });
  }

  @Override
  public void deleteSession(int idsession, Handler<AsyncResult<Void>> result) {
    String query = "DELETE FROM session WHERE idsession = $1";
    client.preparedQuery(query)
        .execute(Tuple.of(idsession), ar -> {
          if (ar.succeeded()) {
            result.handle(Future.succeededFuture());
          } else {
            result.handle(Future.failedFuture(ar.cause()));
          }
        });
  }

  //Person

  @Override
  public void getPersonByUsername(String username, Handler<AsyncResult<JsonObject>> result) {
    String query = "SELECT idperson, fbs_id, username, firstname, lastname, docent, superUser FROM person WHERE username = $1";
    client.preparedQuery(query)
        .execute(Tuple.of(username), ar -> {
          if (ar.succeeded()) {
            RowSet<Row> rows = ar.result();
            if (!rows.iterator().hasNext()) {
              result.handle(Future.succeededFuture(null));
              return;
            }
            Row row = rows.iterator().next();
            JsonObject person = rowToJson(row, personFields);
            result.handle(Future.succeededFuture(person));
          } else {
            result.handle(Future.failedFuture(ar.cause()));
          }
        });
  }

  @Override
  public void getPersonByID(int idperson, Handler<AsyncResult<JsonObject>> result) {
    String query = "SELECT idperson, fbs_id, username, firstname, lastname, docent, superUser FROM person WHERE idperson = $1";
    client.preparedQuery(query)
        .execute(Tuple.of(idperson), ar -> {
          if (ar.succeeded()) {
            RowSet<Row> rows = ar.result();
            if (!rows.iterator().hasNext()) {
              result.handle(Future.succeededFuture(null));
              return;
            }
            Row row = rows.iterator().next();
            JsonObject person = rowToJson(row, personFields);
            result.handle(Future.succeededFuture(person));
          } else {
            result.handle(Future.failedFuture(ar.cause()));
          }
        });
  }


  @Override
  public void getCoursesForUser(int userID, List<String> roles, Handler<AsyncResult<JsonArray>> result) {
    StringBuilder query = new StringBuilder("SELECT idcourse, role, course_name, course_description, semester, " +
        "lecturer_idperson, course_password, fbs, idperson, fbs_id AS \"fbsID\", username, firstname, lastname, docent, all_author " +
        "FROM course JOIN course_role cr ON course.idcourse = cr.course_idcourse " +
        "JOIN person ON lecturer_idperson = idperson WHERE person_idperson = $1");
    if (roles.size() > 0) {
      query.append(" AND (");
      for (int i = 0; i < roles.size(); i++) {
        query.append("role = $").append(i+2);
        if (i < roles.size()-1) {
          query.append(" OR ");
        }
      }
      query.append(")");
    }
    List<Object> queryArguments = new LinkedList<>();
    queryArguments.add(userID);
    queryArguments.addAll(roles);
    client.preparedQuery(query.toString())
        .execute(Tuple.tuple(queryArguments), ar -> {
          if (ar.succeeded()) {
            JsonArray courses = new JsonArray();
            RowSet<Row> rows = ar.result();
            for (Row row : rows) {
              JsonObject course = rowToJson(row, courseFields);
              JsonObject docent = rowToJson(row, personFields);
              course.put("docent", docent);
              courses.add(course);
            }
            result.handle(Future.succeededFuture(courses));
          } else {
            result.handle(Future.failedFuture(ar.cause()));
          }
        });
  }

  @Override
  public void getSessionsForUser(int userID, Handler<AsyncResult<JsonArray>> result) {
    String query = "SELECT idsession, start, \"end\", anonym, session_description, show_correct, quiz_idquiz, " +
        "course_idcourse, session_creator_idperson, idquiz, quiz_name, quiz_description, " +
        "quiz.type, quiz.instant_feedback, (SELECT COUNT(*) FROM question " +
        "WHERE question.quiz_idquiz = session.quiz_idquiz) AS question_count FROM session " +
        "JOIN answer ON idsession = session_idsession JOIN quiz ON quiz_idquiz = idquiz " +
        "WHERE answer.person_idperson = $1";

    client.preparedQuery(query)
        .execute(Tuple.of(userID), ar -> {
          if (ar.succeeded()) {
            handleSessionQueryResult(ar.result(), result);
          } else {
            result.handle(Future.failedFuture(ar.cause()));
          }
        });
  }

  public void createUser(String username, Handler<AsyncResult<Void>> result) {
    String query = "INSERT INTO person (username) VALUES ($1)";
    client.preparedQuery(query).execute(Tuple.of(username), ar -> {
      if (ar.succeeded()) {
        result.handle(Future.succeededFuture());
      } else {
        result.handle(Future.failedFuture(ar.cause()));
      }
      });
  }

  public void flushDatabase(Handler<AsyncResult<Void>> result) {
    String query = "DROP SCHEMA public; CREATE SCHEMA public;";
    client.preparedQuery(query).execute(ar -> {
      if (ar.succeeded()) {
        result.handle(Future.succeededFuture());
      } else {
        result.handle(Future.failedFuture(ar.cause()));
      }
    });
  }

  @Override
  public void createPerson(int fbsUserID, String username, String firstname, String lastname, boolean docent, Handler<AsyncResult<JsonObject>> result) {
    String query = "INSERT INTO person (fbs_id, username, firstname, lastname, docent) VALUES " +
        "($1, $2, $3, $4, $5) RETURNING idperson, fbs_id, username, firstname, lastname, docent";
    client.preparedQuery(query)
        .execute(Tuple.of(fbsUserID, username, firstname, lastname, docent), ar -> {
          if (ar.succeeded()) {
            RowSet<Row> rows = ar.result();
            Row row = rows.iterator().next();
            JsonObject person = rowToJson(row, personFields);
            result.handle(Future.succeededFuture(person));
          } else {
            result.handle(Future.failedFuture(ar.cause()));
          }
        });
  }

  @Override
  public void getBadWords(Handler<AsyncResult<JsonArray>> result) {
    JsonArray res = new JsonArray();
    String query = "select badword from badwordlist";
    client.preparedQuery(query)
      .execute(ar ->{
        if(ar.succeeded()){
          RowSet<Row> rows = ar.result();
          for (Row row : rows) {
            String badWord = row.getString(0);
            res.add(badWord);
          }
          result.handle(Future.succeededFuture(res));
        } else {
          result.handle(Future.failedFuture(ar.cause()));
        }
      });
  }



  //Neue Abfragen

  @Override
  public void countAnswersFromPerson(int idsession,int idperson, Handler<AsyncResult<Integer>> result) {
    String query = "select count(*) from answer where person_idperson = $1 and session_idsession = $2";
    client.preparedQuery(query)
      .execute(Tuple.of(idperson, idsession),ar ->{
        if(ar.succeeded()){
          RowSet<Row> rows = ar.result();
          Row row = rows.iterator().next();
          int answerCount = row.getInteger(0);
          result.handle(Future.succeededFuture(answerCount));
        } else {
          result.handle(Future.failedFuture(ar.cause()));
        }
      });
  }

  @Override
  public void listSessionsForUser(int iduser, boolean hideEnded, boolean hideNonStarted,
      Handler<AsyncResult<JsonArray>> result) {
    String query = "SELECT idsession, start, \"end\", anonym, session_description, show_correct, quiz_idquiz, " +
        "idcourse, course_name, course_description, semester, lecturer_idperson, course_password, fbs, " +
        "d.idperson, d.fbs_id AS \"fbsID\", d.username, d.firstname, d.lastname, d.docent, " +
        "course_idcourse, session_creator_idperson, idquiz, quiz_name, quiz_description, " +
        "quiz.type, quiz.instant_feedback, (SELECT COUNT(*) FROM question " +
        "WHERE question.quiz_idquiz = session.quiz_idquiz) AS question_count FROM session " +
        "LEFT JOIN course ON course_idcourse = idcourse LEFT JOIN person d ON " +
        "course.lecturer_idperson = d.idperson JOIN person ON session_creator_idperson = " +
        "person.idperson JOIN quiz ON quiz_idquiz = idquiz WHERE session_creator_idperson = $1";

    if (hideEnded) {
      query += " AND \"end\" > NOW()";
    }
    if (hideNonStarted) {
      query += " AND \"start\" < NOW()";
    }

    client.preparedQuery(query)
        .execute(Tuple.of(iduser), ar -> {
          if (ar.succeeded()) {
            handleSessionQueryResult(ar.result(), result);
          } else {
            result.handle(Future.failedFuture(ar.cause()));
          }
        });
  }

  @Override
  public void listToDoSessions(int userID, Handler<AsyncResult<JsonArray>> result) {
    String query = "SELECT idsession, start, \"end\", anonym, session_description, show_correct, quiz_idquiz, " +
        "session.course_idcourse, session_creator_idperson, idquiz, quiz_name, quiz_description, " +
        "quiz.type, quiz.instant_feedback, (SELECT COUNT(*) FROM question " +
        "WHERE question.quiz_idquiz = session.quiz_idquiz) AS question_count " +
        "FROM person JOIN course_role ON person.idperson = course_role.person_idperson " +
        "JOIN course ON course_role.course_idcourse = course.idcourse " +
        "JOIN session ON course.idcourse = session.course_idcourse JOIN quiz ON session.quiz_idquiz " +
        "= quiz.idquiz WHERE \"end\" > NOW() AND start < NOW() AND (SELECT COUNT(*) FROM answer " +
        "WHERE answer.person_idperson = idperson AND answer.session_idsession = idsession) = 0 AND "+
        "idperson = $1";

    client.preparedQuery(query)
        .execute(Tuple.of(userID), ar -> {
          if (ar.succeeded()) {
            handleSessionQueryResult(ar.result(), result);
          } else {
            result.handle(Future.failedFuture(ar.cause()));
          }
        });
  }

  @Override
  public void getUserStatistics(int userID, Handler<AsyncResult<JsonObject>> result) {
    String query = "SELECT (SELECT COUNT(*) FROM course_role WHERE person_idperson = $1) AS " +
        "\"courseCount\", (SELECT COUNT(DISTINCT session_idsession) FROM answer WHERE " +
        "person_idperson = $1) AS \"sessionCount\", (SELECT COUNT(*) FROM answer WHERE " +
        "person_idperson = $1) AS \"answeredQuestions\", (SELECT COUNT(*) "+
        "FROM answer WHERE person_idperson = $1 AND correct = true) AS \"correctQuestions\"";
    client.preparedQuery(query)
        .execute(Tuple.of(userID), ar -> {
          if (ar.succeeded()) {
            Row row = ar.result().iterator().next();
            JsonObject stats = rowToJson(row, "courseCount", "sessionCount",
                "answeredQuestions", "correctQuestions");
            result.handle(Future.succeededFuture(stats));
          } else {
            result.handle(Future.failedFuture(ar.cause()));
          }
        });
  }

  @Override
  public void getMyCourseStatistics(int idcourse, int idperson,
      Handler<AsyncResult<JsonObject>> result) {
    String sessionCountQuery = "SELECT COUNT(DISTINCT session_idsession) FROM session JOIN answer " +
        "ON session.idsession = answer.session_idsession WHERE course_idcourse = $1 AND person_idperson = $2";
    String answeredQuestionsQuery = "SELECT COUNT(*) FROM session JOIN answer ON session.idsession = " +
        "answer.session_idsession WHERE course_idcourse = $1 AND person_idperson = $2";
    String correctQuestions = "SELECT COUNT(*) FROM session JOIN answer ON session.idsession = " +
        "answer.session_idsession WHERE course_idcourse = $1 AND person_idperson = $2 AND correct = true";
    String query = "SELECT ("+ sessionCountQuery +") AS \"sessionCount\", " +
        "("+ answeredQuestionsQuery +") AS \"answeredQuestions\", " +
        "("+ correctQuestions +") AS \"correctQuestions\"";
    client.preparedQuery(query)
        .execute(Tuple.of(idcourse, idperson), ar -> {
          if (ar.succeeded()) {
            Row row = ar.result().iterator().next();
            JsonObject stats = rowToJson(row, "sessionCount",
                "answeredQuestions", "correctQuestions");
            result.handle(Future.succeededFuture(stats));
          } else {
            result.handle(Future.failedFuture(ar.cause()));
          }
        });
  }

  @Override
  public void listAnswers(int idsession, int idquestion, Handler<AsyncResult<JsonArray>> result) {
    String query = "SELECT idanswer, person_idperson, anonym_identity, correct, answers FROM answer " +
        "WHERE session_idsession = $1 AND question_idquestion = $2";

    client.preparedQuery(query).execute(Tuple.of(idsession, idquestion), ar -> {
      if (ar.succeeded()) {
        JsonArray res = new JsonArray();
        RowSet<Row> rows = ar.result();
        for (Row row : rows) {
          JsonObject answer = rowToJson(row, "idanswer", "person_idperson", "anonym_identity",
              "correct", "answers");
          res.add(answer);
        }
        result.handle(Future.succeededFuture(res));
      } else {
        result.handle(Future.failedFuture(ar.cause()));
      }
    });
  }

  @Override
  public void getPersonByFbsIdOrCreate(int fbsID, String firstName, String lastName, boolean docent,
      Handler<AsyncResult<Integer>> result) {
    String query = "INSERT INTO person (fbs_id, firstname, lastname, docent) VALUES ($1, $2, $3, $4) " +
        "ON CONFLICT (fbs_id) DO UPDATE SET idperson = person.idperson RETURNING idperson";
    client.preparedQuery(query).execute(Tuple.of(fbsID, firstName, lastName, docent), ar -> {
      if (ar.succeeded()) {
        Row row = ar.result().iterator().next();
        int idperson = row.getInteger(0);
        result.handle(Future.succeededFuture(idperson));
      } else {
        result.handle(Future.failedFuture(ar.cause()));
      }
    });
  }

  @Override
  public void setPersonUsername(int idperson, String username, Handler<AsyncResult<Void>> result) {
    String query = "UPDATE person SET username = $2 WHERE idperson = $1";
    client.preparedQuery(query).execute(Tuple.of(idperson, username), ar -> {
      if (ar.succeeded()) {
        result.handle(Future.succeededFuture());
      } else {
        result.handle(Future.failedFuture(ar.cause()));
      }
    });
  }

  @Override
  public void revokeToken(long tokenID, Handler<AsyncResult<Void>> result) {
    String query = "INSERT INTO revoked_tokens (token_id) VALUES ($1)";
    client.preparedQuery(query).execute(Tuple.of(tokenID), ar -> {
      if (ar.succeeded()) {
        result.handle(Future.succeededFuture());
      } else {
        result.handle(Future.failedFuture(ar.cause()));
      }
    });
  }

  @Override
  public void checkRevoked(long tokenID, Handler<AsyncResult<Boolean>> result) {
    String query = "SELECT * FROM revoked_tokens WHERE token_id = $1";
    client.preparedQuery(query).execute(Tuple.of(tokenID), ar -> {
      if (ar.succeeded()) {
        if (ar.result().iterator().hasNext()) {
          result.handle(Future.succeededFuture(true));
          return;
        }
        result.handle(Future.succeededFuture(false));
      } else {
        result.handle(Future.failedFuture(ar.cause()));
      }
    });
  }

  @Override
  public void getCurseOverview(int courseID, Handler<AsyncResult<JsonArray>> result) {
    String query = "SELECT person.username AS username, session.idsession AS idsession, COUNT(distinct idanswer) AS answered, " +
       "COUNT(distinct idanswer) filter(where correct = true) AS correct, " +
      "(SELECT COUNT(*) FROM question WHERE session.quiz_idquiz = question.quiz_idquiz) AS max, " +
      "start, \"end\", anonym, session_description, show_correct, idquiz, quiz_name, type FROM course " +
      "JOIN course_role on course.idcourse = course_role.course_idcourse " +
      "JOIN person on course_role.person_idperson = person.idperson " +
      "JOIN session on course.idcourse = session.course_idcourse " +
      "JOIN quiz on session.quiz_idquiz = quiz.idquiz " +
      "LEFT JOIN answer on person.idperson = answer.person_idperson and session.idsession = answer.session_idsession " +
      "WHERE course.idcourse = $1 GROUP BY person.idperson, session.idsession, session.quiz_idquiz, person.username, " +
      "start, \"end\", anonym, session_description, idquiz, quiz_name, quiz_description, type;";
    client.preparedQuery(query).execute(Tuple.of(courseID), ar -> {
      if (ar.succeeded()) {
        JsonArray res = new JsonArray();
        RowSet<Row> rows = ar.result();
        for (Row row : rows) {
          JsonObject overview = rowToJson(row, "username", "answered", "correct", "max");
          JsonObject session = rowToJson(row, sessionFields);
          JsonObject quiz = rowToJson(row, quizFields);
          session.put("quiz", quiz);
          overview.put("session", session);
          res.add(overview);
        }
        result.handle(Future.succeededFuture(res));
      } else {
        result.handle(Future.failedFuture(ar.cause()));
      }
    });
  }

  private void handleSessionQueryResult(RowSet<Row> rows, Handler<AsyncResult<JsonArray>> result) {
    JsonArray res = new JsonArray();
    for (Row row : rows) {
      JsonObject session = rowToJson(row, sessionFields);
      JsonObject quiz = rowToJson(row, quizFields);
      session.put("quiz", quiz);
      Boolean complete = row.getBoolean("complete");
      if (complete != null) {
        session.put("complete", complete);
      }
      if (row.getInteger("creator_idperson") != null) {
        JsonObject creator = new JsonObject()
            .put("username", row.getString("creator_username"))
            .put("firstname", row.getString("creator_firstname"))
            .put("lastname", row.getString("creator_lastname"))
            .put("id", row.getInteger("creator_idperson"))
            .put("fbsID", row.getInteger("creator_fbsid"))
            .put("docent", row.getBoolean("creator_docent"));
        session.put("creator", creator);
      }
      if (row.getInteger("idcourse") != null) {
        JsonObject course = rowToJson(row, courseFields);
        JsonObject docent = rowToJson(row, personFields);
        course.put("docent", docent);
        session.put("course", course);
      } else {
        session.putNull("course");
      }
      res.add(session);
    }
    result.handle(Future.succeededFuture(res));
  }

  public void getAllCourses(Handler<AsyncResult<JsonArray>> result) {
    String query = "SELECT idcourse, course_name, course_description, semester, lecturer_idperson, course_password, fbs, " +
        "idperson, fbs_id AS \"fbsID\", username, firstname, lastname, docent " +
        "FROM course JOIN person ON lecturer_idperson = idperson";
    client.preparedQuery(query)
        .execute(Tuple.tuple(), ar -> {
          if (ar.succeeded()) {
            JsonArray courses = new JsonArray();
            RowSet<Row> rows = ar.result();
            for (Row row : rows) {
              JsonObject course = rowToJson(row, courseFields);
              JsonObject docent = rowToJson(row, personFields);
              course.put("docent", docent);
              courses.add(course);
            }
            result.handle(Future.succeededFuture(courses));
          } else {
            result.handle(Future.failedFuture(ar.cause()));
          }
        });
  }

  public void getAllQuizzes(Handler<AsyncResult<JsonArray>> result) {
    String query = "SELECT idquiz, quiz_name, quiz_password, quiz_description, quiz_creator_idperson, type, " +
        "instant_feedback, (SELECT COUNT(*) FROM question WHERE quiz_idquiz = idquiz) AS question_count FROM quiz";
    client.preparedQuery(query).execute(Tuple.tuple(), ar -> {
      if (ar.succeeded()) {
        JsonArray quizzes = new JsonArray();
        RowSet<Row> rows = ar.result();
        for (Row row : rows) {
          JsonObject quiz = rowToJson(row, quizFields);
          quizzes.add(quiz);
        }
        result.handle(Future.succeededFuture(quizzes));
      } else {
        result.handle(Future.failedFuture(ar.cause()));
      }
    });
  }

  public void getAllSessions(Handler<AsyncResult<JsonArray>> result) {
    String query = "SELECT idsession, start, \"end\", anonym, session_description, show_correct, session_creator_idperson, " +
        "idcourse, course_name, course_description, semester, lecturer_idperson, course_password, fbs, " +
        "d.idperson, d.fbs_id AS \"fbsID\", d.username, d.firstname, d.lastname, d.docent " +
        "FROM session LEFT JOIN course ON course_idcourse = idcourse LEFT JOIN person d ON " +
        "course.lecturer_idperson = d.idperson";
    client.preparedQuery(query)
        .execute(Tuple.tuple(), ar -> {
          if (ar.succeeded()) {
            JsonArray sessions = new JsonArray();
            RowSet<Row> rows = ar.result();
            for (Row row : rows) {
              JsonObject session = rowToJson(row, sessionFields);
              if (row.getInteger("idcourse") != null) {
                JsonObject course = rowToJson(row, courseFields);
                JsonObject docent = rowToJson(row, personFields);
                course.put("docent", docent);
                session.put("course", course);
              } else {
                session.putNull("course");
              }
              sessions.add(session);
            }
            result.handle(Future.succeededFuture(sessions));
          } else {
            result.handle(Future.failedFuture(ar.cause()));
          }
        });
  }

  public void getAllPersons(Handler<AsyncResult<JsonArray>> result) {
    String query = "SELECT idperson, fbs_id, username, firstname, lastname, docent, superUser FROM person";
    client.preparedQuery(query)
        .execute(Tuple.tuple(), ar -> {
          if (ar.succeeded()) {
            JsonArray persons = new JsonArray();
            RowSet<Row> rows = ar.result();
            for (Row row : rows) {
              JsonObject person = rowToJson(row, personFields);
              persons.add(person);
            }
            result.handle(Future.succeededFuture(persons));
          } else {
            result.handle(Future.failedFuture(ar.cause()));
          }
        });
  }

  public void setDocent(int idperson, boolean docent, Handler<AsyncResult<Void>> result) {
    String query = "UPDATE person SET docent = $1 WHERE idperson = $2";
    client.preparedQuery(query)
        .execute(Tuple.of(docent, idperson), ar -> {
          if (ar.succeeded()) {
            result.handle(Future.succeededFuture());
          } else {
            result.handle(Future.failedFuture(ar.cause()));
          }
        });
  }

  public void share(int idquiz, int idperson, Handler<AsyncResult<Void>> result) {
    String query = "INSERT INTO quiz_share (quiz_idquiz, person_idperson) VALUES ($1, $2)";
    client.preparedQuery(query)
        .execute(Tuple.of(idquiz, idperson), ar -> {
          if (ar.succeeded()) {
            result.handle(Future.succeededFuture());
          } else {
            result.handle(Future.failedFuture(ar.cause()));
          }
        });
  }
  public void unShare(int idquiz, int idperson, Handler<AsyncResult<Void>> result) {
    String query = "DELETE FROM quiz_share WHERE quiz_idquiz = $1 AND person_idperson = $2";
    client.preparedQuery(query)
        .execute(Tuple.of(idquiz, idperson), ar -> {
          if (ar.succeeded()) {
            result.handle(Future.succeededFuture());
          } else {
            result.handle(Future.failedFuture(ar.cause()));
          }
        });
  }
  public void sharedWith(int idperson, Handler<AsyncResult<JsonArray>> result) {
    String query = "SELECT idquiz, quiz_name, quiz_password, quiz_description, quiz_creator_idperson, type, " +
        "instant_feedback, (SELECT COUNT(*) FROM question WHERE quiz_idquiz = idquiz) AS question_count " +
        "FROM quiz_share JOIN quiz ON quiz_share.quiz_idquiz = quiz.idquiz WHERE person_idperson = $1";
    client.preparedQuery(query).execute(Tuple.of(idperson), ar -> {
      if (ar.succeeded()) {
        JsonArray res = new JsonArray();
        RowSet<Row> rows = ar.result();
        for (Row row : rows) {
          JsonObject quiz = rowToJson(row, quizFields);
          res.add(quiz);
        }
        result.handle(Future.succeededFuture(res));
      } else {
        result.handle(Future.failedFuture(ar.cause()));
      }
    });
  }
  public void isSharedWith(int idquiz, int idperson, Handler<AsyncResult<Boolean>> result) {
    String query = "SELECT COUNT(*) FROM quiz_share WHERE quiz_idquiz = $1 AND person_idperson = $2";
    client.preparedQuery(query)
        .execute(Tuple.of(idquiz, idperson), ar -> {
          if (ar.succeeded()) {
            int count = ar.result().iterator().next().getInteger(0);
            result.handle(Future.succeededFuture(count == 1));
          } else {
            result.handle(Future.failedFuture(ar.cause()));
          }
        });
  }

  @Override
  public void getCorrect(int idperson, int idsession, int position, Handler<AsyncResult<JsonObject>> result) {
    String query = "SELECT type, question.position, questiontext AS text, question.answers, show_correct, " +
        "answer.answers AS given_answers, correct FROM session JOIN question on session.quiz_idquiz = question.quiz_idquiz " +
        "LEFT JOIN answer on session.idsession = answer.session_idsession AND answer.person_idperson = $1 " +
        "WHERE idsession = $2 AND question.position = $3";
    client.preparedQuery(query)
        .execute(Tuple.of(idperson, idsession, position), ar -> {
          if (ar.succeeded()) {
            RowSet<Row> rows = ar.result();
            if (!rows.iterator().hasNext()) {
              result.handle(Future.succeededFuture());
              return;
            }
            Row row = rows.iterator().next();
            JsonObject question = rowToJson(row, questionFields);
            question.put("given", rowToJson(row, answerFields));
            question.put("show_correct", row.getBoolean("show_correct"));
            result.handle(Future.succeededFuture(question));
          } else {
            result.handle(Future.failedFuture(ar.cause()));
          }
        });
  }

  @Override
  public void getRole(int idperson, int idcourse, Handler<AsyncResult<String>> result) {
    String query = "SELECT role FROM course_role WHERE person_idperson = $1 AND course_idcourse = $2";

    client.preparedQuery(query)
        .execute(Tuple.of(idperson, idcourse), ar -> {
          if (ar.succeeded()) {
            RowSet<Row> rows = ar.result();
            if (!rows.iterator().hasNext()) {
              result.handle(Future.succeededFuture());
              return;
            }
            Row row = rows.iterator().next();
            String role = row.getString("role");
            result.handle(Future.succeededFuture(role));
          } else {
            result.handle(Future.failedFuture(ar.cause()));
          }
        });
  }

  @Override
  public void setRole(int idperson, int idcourse, String role, Handler<AsyncResult<Void>> result) {
    String query = "INSERT INTO course_role (person_idperson, course_idcourse, role) VALUES ($1, $2, $3) " +
        "ON CONFLICT ON CONSTRAINT course_role_pk DO UPDATE SET role = $3 WHERE course_role.person_idperson = $1 AND " +
        "course_role.course_idcourse = $2";

    client.preparedQuery(query)
        .execute(Tuple.of(idperson, idcourse, role), ar -> {
          if (ar.succeeded()) {
            result.handle(Future.succeededFuture());
          } else {
            result.handle(Future.failedFuture(ar.cause()));
          }
        });
  }

  @Override
  public void deleteRole(int idperson, int idcourse, Handler<AsyncResult<Void>> result) {
    String query = "DELETE FROM course_role WHERE course_idcourse = $1 AND person_idperson = $2";
    client.preparedQuery(query)
        .execute(Tuple.of(idcourse, idperson),ar ->{
          if(ar.succeeded()){
            if (ar.result().iterator().hasNext()) {
              result.handle(Future.succeededFuture());
            } else {
              result.handle(Future.failedFuture("no participant was deleted"));
            }
          } else {
            result.handle(Future.failedFuture(ar.cause()));
          }
        });
  }

  private static final String[] sessionFields = {"idsession", "start", "end", "anonym", "session_description",
      "show_correct", "session_creator_idperson"};
  private static final String[] quizFields = {"idquiz", "quiz_name", "quiz_password", "quiz_description",
      "quiz_creator_idperson", "type", "instant_feedback", "question_count"};
  private static final String[] questionFields = {"type", "position", "text", "answers"};
  private static final String[] answerFields = {"given_answers", "correct"};
  private static final String[] courseFields = {"idcourse", "course_name", "course_description",
      "semester", "course_password", "fbs", "all_author", "lecturer_idperson", "role"};
  private static final String[] personFields = {"idperson", "fbsID", "username", "firstname",
      "lastname", "docent", "superuser"};

  private JsonObject rowToJson(Row row, String... fields) {
    JsonObject res = new JsonObject();
    for (String field : fields) {
      Object value = row.getValue(field);
      if (value instanceof LocalDateTime) {
        value = ((LocalDateTime) value).toInstant(ZoneOffset.UTC).getEpochSecond();
      }
      res.put(field, value);
    }
    return res;
  }


  //Statistik getBarChartForSingleMultipleChoiceQuiz:
  public void getBarChartForSingleMultipleChoiceQuiz1(int idsession, Handler<AsyncResult<JsonArray>> result){
    String query = "select idquestion from question where quiz_idquiz in (select quiz_idquiz from session"+
        " where idsession = $1) group by idquestion";
    JsonArray res = new JsonArray();
    client.preparedQuery(query)
        .execute(Tuple.of(idsession), ar ->{
          if(ar.succeeded()){
            RowSet<Row> rows = ar.result();
            for (Row row : rows) {
              JsonObject question = new JsonObject();
              question.put("idquestion", row.getInteger(0));
              res.add(question);
            }
            result.handle(Future.succeededFuture(res));
          } else{
            result.handle(Future.failedFuture(ar.cause()));
          }
        });
  }

  public void getBarChartForSingleMultipleChoiceQuiz2(int idquestion, Handler<AsyncResult<JsonArray>> result){
    String query = "select idquestion, questiontext, answers from question where quiz_idquiz In"+
        "(select idquiz from quiz where idquiz IN (select quiz_idquiz from question where idquestion = $1))"+
        " group by idquestion";
    JsonArray res = new JsonArray();
    client.preparedQuery(query)
        .execute(Tuple.of(idquestion), ar ->{
          if(ar.succeeded()){
            RowSet<Row> rows = ar.result();
            for (Row row : rows) {
              JsonObject question = new JsonObject();
              question.put("idquestion", row.getInteger(0));
              question.put("questionText", row.getString(1));
              question.put("answers", row.getValue(2));
              res.add(question);
            }
            result.handle(Future.succeededFuture(res));
          } else{
            result.handle(Future.failedFuture(ar.cause()));
          }
        });
  }

  public void getBarChartForSingleMultipleChoiceQuiz3(int idquestion, int idsession,Handler<AsyncResult<JsonArray>> result){
    String query = "select count(correct), count(correct) filter(where correct = true) from answer where question_idquestion = $1 and session_idsession = $2";
    JsonArray res = new JsonArray();
    client.preparedQuery(query)
        .execute(Tuple.of(idquestion,idsession), ar ->{
          if(ar.succeeded()){
            RowSet<Row> rows = ar.result();
            JsonObject teilnehmer = new JsonObject();
            for (Row row : rows) {
              teilnehmer.put("teilnehmer", row.getValue(0));
              teilnehmer.put("richtigeAntworten", row.getValue(1));
              res.add(teilnehmer);
            }
            result.handle(Future.succeededFuture(res));
          } else{
            result.handle(Future.failedFuture(ar.cause()));
          }
        });
  }

  public void getBarChartForSingleMultipleChoiceQuiz4(int idsession, Handler<AsyncResult<JsonArray>> result){
    String query = "select quiz_name AS name from quiz where idquiz in (select quiz_idquiz from session where idsession = $1)";
    JsonArray res = new JsonArray();
    client.preparedQuery(query)
        .execute(Tuple.of(idsession), ar ->{
          if(ar.succeeded()){
            RowSet<Row> rows = ar.result();
            for (Row row : rows) {
              res.add(row.getString(0));
            }
            result.handle(Future.succeededFuture(res));
          } else{
            result.handle(Future.failedFuture(ar.cause()));
          }
        });
  }

  //getBarChartForMutltipleQuizzes:

  public void getBarChartForMultipleQuizzes1(int idcourse, Handler<AsyncResult<JsonArray>> result){
    String query = "select idsession from session where course_idcourse = $1 group by idsession";
    JsonArray res = new JsonArray();
    client.preparedQuery(query)
        .execute(Tuple.of(idcourse), ar ->{
          if(ar.succeeded()){
            RowSet<Row> rows = ar.result();
            for (Row row : rows) {
              JsonObject question = new JsonObject();
              question.put("idsession", row.getInteger(0));
              res.add(question);
            }
              result.handle(Future.succeededFuture(res));
          } else{
              result.handle(Future.failedFuture(ar.cause()));
          }
        });
  }


  public void getBarChartForMultipleQuizzes2(int idsession, Handler<AsyncResult<JsonArray>> result){
    String query = "select count (idquestion) from question where quiz_idquiz in (select quiz_idquiz from session where idsession = $1)";
    JsonArray res = new JsonArray();
    client.preparedQuery(query)
        .execute(Tuple.of(idsession), ar ->{
          if(ar.succeeded()){
            RowSet<Row> rows = ar.result();
            for (Row row : rows) {
              JsonObject question = new JsonObject();
              question.put("questioncount", row.getInteger(0));
              res.add(question);
            }
            result.handle(Future.succeededFuture(res));
          } else{
            result.handle(Future.failedFuture(ar.cause()));
          }
        });
  }

  public void getBarChartForMultipleQuizzes3(int idsession, int questionCount, Handler<AsyncResult<JsonArray>> result){
    String query = "select person_idperson, count(correct) filter(where correct = true) from answer where session_idsession = $1 group by person_idperson";
    JsonArray res = new JsonArray();
    JsonObject teilnehmer = new JsonObject();
    client.preparedQuery(query)
        .execute(Tuple.of(idsession), ar ->{
          if(ar.succeeded()){
            RowSet<Row> rows = ar.result();
            int count = 0;
            int countTeilnehmer = 0;
            for (Row row : rows) {
                countTeilnehmer++;
              if(row.getInteger(1) == questionCount){
                count++;
              }
            }
            teilnehmer.put("teilnehmer", countTeilnehmer);
            teilnehmer.put("richtigeAntworten", count);
            res.add(teilnehmer);
            result.handle(Future.succeededFuture(res));
          } else{
            result.handle(Future.failedFuture(ar.cause()));
          }
        });
  }
  public void getBarChartForMultipleQuizzes4(int idcourse, Handler<AsyncResult<JsonArray>> result){
    String query =  "select idsession, quiz_name from session s, quiz q "+
                    "where s.quiz_idquiz = q.idquiz and s.course_idcourse = $1 " +
                    "group by idsession, quiz_name";
    JsonArray res = new JsonArray();
    client.preparedQuery(query)
        .execute(Tuple.of(idcourse), ar ->{
          if(ar.succeeded()){
            RowSet<Row> rows = ar.result();
            for (Row row : rows) {
              JsonObject question = new JsonObject();
              question.put("quizname", row.getString(1));
              res.add(question);
            }
            result.handle(Future.succeededFuture(res));
          } else{
              System.out.println(ar.cause().getMessage());
            result.handle(Future.failedFuture(ar.cause()));
          }
        });
  }
  public void getBarChartForMultipleQuizzes5(int idcourse, Handler<AsyncResult<JsonArray>> result){
    String query = "select course_name from course where idcourse = $1";
    JsonArray res = new JsonArray();
    client.preparedQuery(query)
        .execute(Tuple.of(idcourse), ar ->{
          if(ar.succeeded()){
            RowSet<Row> rows = ar.result();
            for (Row row : rows) {
              JsonObject question = new JsonObject();
              question.put("coursename", row.getString(0));
              res.add(question);
            }
            result.handle(Future.succeededFuture(res));
          } else{
            result.handle(Future.failedFuture(ar.cause()));
          }
        });
  }
}

