package de.thm.mni.swtp.feedbackapp.backend.rest;

import de.thm.mni.swtp.feedbackapp.backend.rest.questionTypes.QuestionType;
import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.Promise;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class StatisticUtils {

  private static class StatisticsPerson {

    int answered;
    int correct;

    StatisticsPerson() {
      this.answered = 0;
      this.correct = 0;
    }
  }

  public static JsonObject calculateSessionStatistics(JsonObject input) {
    int questionCount = input.getInteger("questionCount");
    JsonArray answers = input.getJsonArray("answers");
    Map<String, StatisticsPerson> statisticsMap = new HashMap<>();
    for (int i = 0; i < answers.size(); i++) {
      JsonObject answer = answers.getJsonObject(i);
      String identity;

      if (answer.getInteger("person_idperson") != null) {
        identity = answer.getInteger("person_idperson").toString();
      } else {
        identity = answer.getString("anonym_identity");
      }
      StatisticsPerson statPerson = statisticsMap.get(identity);
      if (statPerson == null) {
        statPerson = new StatisticsPerson();
        statisticsMap.put(identity, statPerson);
      }
      statPerson.answered++;
      if (answer.getBoolean("correct")) {
        statPerson.correct++;
      }
    }
    int participantCount = 0;
    int completeCount = 0;
    int allWrong = 0;
    int zeroPlus = 0;
    int fortyPlus = 0;
    int sixtyPlus = 0;
    int eightyPlus = 0;
    for (StatisticsPerson statPerson : statisticsMap.values()) {
      participantCount++;
      if (statPerson.answered == questionCount) {
        completeCount++;
      } else {
        continue;
      }
      if (statPerson.correct > questionCount * 0.8) {
        eightyPlus++;
      } else if (statPerson.correct > questionCount * 0.6) {
          sixtyPlus++;
      } else if (statPerson.correct > questionCount * 0.4) {
          fortyPlus++;
      } else if (statPerson.correct > 0) {
          zeroPlus++;
      } else {
          allWrong++;
      }
    }
    return new JsonObject()
        .put("participantCount", participantCount)
        .put("completeCount", completeCount)
        .put("allWrong", allWrong)
        .put("zeroPlus", zeroPlus)
        .put("fortyPlus", fortyPlus)
        .put("sixtyPlus", sixtyPlus)
        .put("eightyPlus", eightyPlus);
  }

  public static void calculateResultStatistics(JsonArray questions, int sessionID,
      Handler<AsyncResult<JsonArray>> result) {
    calculateResultStatistics(questions.copy(), sessionID, 0, result);
  }

  private static void calculateResultStatistics(JsonArray questions, int sessionID, int i,
      Handler<AsyncResult<JsonArray>> result) {
    if (i >= questions.size()) {
      result.handle(Future.succeededFuture(questions));
      return;
    }
    JsonObject question = questions.getJsonObject(i);
    int questionID = question.getInteger("id");
    String type = question.getString("type");
    Promise<JsonArray> answersPromise = Promise.promise();
    RestVerticle.db.listAnswers(sessionID, questionID, answersPromise);
    answersPromise.future().onSuccess(answers -> {
      question.put("answered", answers.size());
      Object stats = QuestionType.from(type, question.getValue("answers")).computeStats(answers);
      question.put("stats", stats);
      calculateResultStatistics(questions, sessionID, i + 1, result);
    }).onFailure(err -> result.handle(Future.failedFuture(err)));
  }

  public static JsonObject transformWordCloudQuestion(JsonArray answers) {
    Map<String, Integer> answerMap = new HashMap<>();
    for (int i = 0; i < answers.size(); i++) {
      JsonObject answer = answers.getJsonObject(i);
      if (!answer.getBoolean("correct")) {
        continue;
      }
      JsonArray answersGiven = answer.getJsonArray("answers");
      for (int j = 0; j < answersGiven.size(); j++) {
        String answerNormalized = answersGiven.getString(j).toLowerCase().trim();
        answerMap.put(answerNormalized, answerMap.getOrDefault(answerNormalized, 0) + 1);
      }
    }
    List<Entry<String, Integer>> statsEntries = new LinkedList<>();
    for (Entry<String, Integer> entry : answerMap.entrySet()) {
      boolean inserted = false;
      for (int i = 0; i < statsEntries.size(); i++) {
        Entry<String, Integer> listEntry = statsEntries.get(i);
        if (listEntry.getValue() < entry.getValue()) {
          statsEntries.add(i, entry);
          inserted = true;
          break;
        }
      }
      if (!inserted) {
        statsEntries.add(entry);
      }
    }
    JsonObject stats = new JsonObject();
    for (Entry<String, Integer> entry : statsEntries) {
      stats.put(entry.getKey(), entry.getValue());
    }
    return stats;
  }

  public static JsonArray transformOverview(JsonArray overview) {
    JsonObject result = new JsonObject();
    for (int i = 0; i < overview.size(); i++) {
      JsonObject content = overview.getJsonObject(i);
      JsonObject session = DBMapper.mapSession(content.getJsonObject("session"));
      String key = session.getInteger("id").toString();
      String username = content.getString("username");
      int answered = content.getInteger("answered");
      int correct = content.getInteger("correct");
      int max = content.getInteger("max");
      JsonObject sessionOverview = result.getJsonObject(key, new JsonObject());
      sessionOverview.put("session", session);
      JsonObject students = sessionOverview.getJsonObject("students", new JsonObject());
      int correctPercentage = Math.round(((float)correct / (float)max) * 100);
      if (answered > 0) {
        students.put(username, correctPercentage);
      }
      sessionOverview.put("students", students);
      result.put(key, sessionOverview);
    }
    JsonArray resultArray = new JsonArray();
    for (var res : result) {
      resultArray.add(res.getValue());
    }
    return resultArray;
  }
}
