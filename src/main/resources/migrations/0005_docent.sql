BEGIN;

ALTER TABLE person rename COLUMN employee TO docent;

UPDATE person SET docent = false WHERE username = 'user' OR username = 'user1' OR username = 'user2';
UPDATE person SET docent = true WHERE username = 'user3' OR username = 'user4';

UPDATE public._migrations SET migration_number = 5;
COMMIT;
