BEGIN;

ALTER TABLE course ALTER COLUMN semester TYPE varchar USING semester::varchar;
ALTER TABLE course ALTER COLUMN semester SET NOT NULL;

UPDATE public._migrations SET migration_number = 9;
COMMIT;
