BEGIN;

ALTER TABLE quiz
	ADD instant_feedback boolean DEFAULT true;

UPDATE public._migrations SET migration_number = 6;
COMMIT;
