BEGIN;

CREATE TABLE revoked_tokens
(
    token_id   bigint    not null constraint revoked_tokens_pk primary key,
    revoked_at timestamp not null default NOW()
);

UPDATE public._migrations SET migration_number = 16;
COMMIT;
