BEGIN;

CREATE UNIQUE index courseadmin_course_idcourse_person_idperson_uindex
	ON courseadmin (course_idcourse, person_idperson);

CREATE UNIQUE index participant_course_idcourse_person_idperson_uindex
	ON participant (course_idcourse, person_idperson);

UPDATE public._migrations SET migration_number = 8;
COMMIT;
