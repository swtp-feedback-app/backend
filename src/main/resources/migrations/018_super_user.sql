BEGIN;

ALTER TABLE person
    ADD COLUMN superuser boolean DEFAULT false;

INSERT INTO person (username, superuser, docent) VALUES ('su', true, false);

UPDATE public._migrations SET migration_number = 18;
COMMIT;
