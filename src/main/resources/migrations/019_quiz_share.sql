BEGIN;

CREATE TABLE quiz_share
(
    quiz_idquiz INT NOT NULL
        CONSTRAINT quiz_share_quiz_idquiz_fk
            REFERENCES quiz
            ON UPDATE CASCADE ON DELETE CASCADE,
    person_idperson INT NOT NULL
        CONSTRAINT quiz_share_person_idperson_fk
            REFERENCES person
            ON UPDATE CASCADE ON DELETE CASCADE,
    CONSTRAINT quiz_share_pk
        PRIMARY KEY (quiz_idquiz, person_idperson)
);

UPDATE public._migrations SET migration_number = 19;
COMMIT;
