BEGIN;

ALTER TABLE course
    RENAME COLUMN all_admin TO all_author;

UPDATE public._migrations SET migration_number = 23;
COMMIT;
