BEGIN;

alter table course rename column lecturer to lecturer_idperson;
alter table course alter column lecturer_idperson type integer using lecturer_idperson::integer;
alter table course
	add constraint course_person_idperson_fk
		foreign key (lecturer_idperson) references person
			on update cascade on delete cascade;

alter table person
	add username varchar;
create unique index person_username_uindex
	on person (username);

alter table question
	add position int;

UPDATE public._migrations SET migration_number = 2;
COMMIT;
