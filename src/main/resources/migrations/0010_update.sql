BEGIN;

ALTER TABLE person
	ADD firstname varchar;

ALTER TABLE person
	ADD lastname varchar;

ALTER TABLE person
	ADD fbs_id int;

CREATE TABLE badwordlist
(
	idbadword int
		CONSTRAINT badwordlist_pk
			PRIMARY KEY,
	badword varchar NOT NULL
);

UPDATE public._migrations SET migration_number = 10;
COMMIT;
