BEGIN;

CREATE UNIQUE INDEX question_quiz_idquiz_position_uindex
    ON question (quiz_idquiz, position);

UPDATE public._migrations SET migration_number = 17;
COMMIT;
