BEGIN;

ALTER TABLE course
	ADD fbs boolean DEFAULT false;

UPDATE public._migrations SET migration_number = 7;
COMMIT;
