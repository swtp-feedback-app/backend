BEGIN;

CREATE TABLE course_role
(
    person_idperson INT
        CONSTRAINT course_role_person_idperson_fk
            REFERENCES person
            ON UPDATE CASCADE ON DELETE CASCADE,
    course_idcourse INT
        CONSTRAINT course_role_course_idcourse_fk
            REFERENCES course
            ON UPDATE CASCADE ON DELETE CASCADE,
    role VARCHAR(16),
    CONSTRAINT course_role_pk
        PRIMARY KEY (person_idperson, course_idcourse)
);

INSERT INTO course_role (person_idperson, course_idcourse, role)
SELECT course.lecturer_idperson, course.idcourse, 'lecturer' FROM course
ON CONFLICT ON CONSTRAINT course_role_pk DO NOTHING;

INSERT INTO course_role (person_idperson, course_idcourse, role)
SELECT participant.person_idperson, participant.course_idcourse, 'participant' FROM participant
ON CONFLICT ON CONSTRAINT course_role_pk DO NOTHING;

INSERT INTO course_role (person_idperson, course_idcourse, role)
SELECT courseadmin.person_idperson, courseadmin.course_idcourse, 'author' FROM courseadmin
ON CONFLICT ON CONSTRAINT course_role_pk DO NOTHING;

DROP TABLE participant;
DROP TABLE courseadmin;

UPDATE public._migrations SET migration_number = 22;
COMMIT;
