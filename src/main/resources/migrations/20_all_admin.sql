BEGIN;

ALTER TABLE course
    ADD COLUMN all_admin boolean DEFAULT false;

UPDATE public._migrations SET migration_number = 20;
COMMIT;
