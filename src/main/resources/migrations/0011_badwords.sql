BEGIN;

--
-- Data for Name: badwordlist; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.badwordlist VALUES (1, 'anus');
INSERT INTO public.badwordlist VALUES (2, 'anal');
INSERT INTO public.badwordlist VALUES (3, 'arschloch');
INSERT INTO public.badwordlist VALUES (4, 'armleuchter');
INSERT INTO public.badwordlist VALUES (5, 'fotze');
INSERT INTO public.badwordlist VALUES (6, 'arsch');
INSERT INTO public.badwordlist VALUES (7, 'geil');
INSERT INTO public.badwordlist VALUES (8, 'backpfeife');
INSERT INTO public.badwordlist VALUES (9, 'blöd');
INSERT INTO public.badwordlist VALUES (10, 'bloed');
INSERT INTO public.badwordlist VALUES (11, 'sau');
INSERT INTO public.badwordlist VALUES (12, 'bumsen');
INSERT INTO public.badwordlist VALUES (13, 'kacke');
INSERT INTO public.badwordlist VALUES (14, 'scheiß');
INSERT INTO public.badwordlist VALUES (15, 'scheiße');
INSERT INTO public.badwordlist VALUES (16, 'piss');
INSERT INTO public.badwordlist VALUES (17, 'pisse');
INSERT INTO public.badwordlist VALUES (18, 'ficken');
INSERT INTO public.badwordlist VALUES (19, 'hure');
INSERT INTO public.badwordlist VALUES (20, 'masturbieren');
INSERT INTO public.badwordlist VALUES (21, 'masturbation');
INSERT INTO public.badwordlist VALUES (22, 'depp');
INSERT INTO public.badwordlist VALUES (23, 'abschaum');
INSERT INTO public.badwordlist VALUES (24, 'drecksack');
INSERT INTO public.badwordlist VALUES (25, 'dreckskerl');
INSERT INTO public.badwordlist VALUES (26, 'penner');
INSERT INTO public.badwordlist VALUES (27, 'scheisse');
INSERT INTO public.badwordlist VALUES (28, 'schwanz');
INSERT INTO public.badwordlist VALUES (29, 'teufel');
INSERT INTO public.badwordlist VALUES (30, 'möpse');
INSERT INTO public.badwordlist VALUES (31, 'schlampe');
INSERT INTO public.badwordlist VALUES (32, 'titten');
INSERT INTO public.badwordlist VALUES (33, 'bastard');
INSERT INTO public.badwordlist VALUES (34, 'trottel');
INSERT INTO public.badwordlist VALUES (35, 'verdammt');
INSERT INTO public.badwordlist VALUES (36, 'weichei');
INSERT INTO public.badwordlist VALUES (37, 'rosette');
INSERT INTO public.badwordlist VALUES (38, 'fresse');
INSERT INTO public.badwordlist VALUES (39, 'luder');
INSERT INTO public.badwordlist VALUES (40, 'giftzwerg');
INSERT INTO public.badwordlist VALUES (41, 'klappe');
INSERT INTO public.badwordlist VALUES (42, 'onanieren');
INSERT INTO public.badwordlist VALUES (43, 'pestbeule');
INSERT INTO public.badwordlist VALUES (44, 'schwuchtel');
INSERT INTO public.badwordlist VALUES (45, 'nutte');
INSERT INTO public.badwordlist VALUES (46, 'lesbe');

UPDATE public._migrations SET migration_number = 11;
COMMIT;
