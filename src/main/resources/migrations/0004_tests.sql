BEGIN;

INSERT INTO person (username) VALUES ('user'), ('user1'), ('user2'), ('user3'), ('user4');

ALTER TABLE question DROP COLUMN correctanswers;

alter table session alter column start type timestamp using start::timestamp;
alter table session alter column "end" type timestamp using "end"::timestamp;

alter table answer add anonym_identity varchar;

UPDATE public._migrations SET migration_number = 4;
COMMIT;
