BEGIN;

CREATE UNIQUE index answer_session_idsession_person_idperson_uindex
	ON answer (session_idsession, question_idquestion, person_idperson);

CREATE UNIQUE index answer_session_idsession_anonym_identity_uindex
	ON answer (session_idsession, question_idquestion, anonym_identity);

UPDATE public._migrations SET migration_number = 12;
COMMIT;
