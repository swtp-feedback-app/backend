BEGIN;

ALTER TABLE session
    ADD session_description varchar;

UPDATE public._migrations SET migration_number = 14;
COMMIT;
