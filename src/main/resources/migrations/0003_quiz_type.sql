BEGIN;

alter table quiz
	add type varchar;

UPDATE public._migrations SET migration_number = 3;
COMMIT;
