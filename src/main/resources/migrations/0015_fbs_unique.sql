BEGIN;

CREATE UNIQUE INDEX person_fbs_id_uindex
    ON person (fbs_id);

UPDATE public._migrations SET migration_number = 15;
COMMIT;
