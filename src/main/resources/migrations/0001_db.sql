BEGIN;
--
-- PostgreSQL database dump
--

-- Dumped from database version 12.3
-- Dumped by pg_dump version 12.3

-- Started on 2020-06-09 11:34:28

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 1 (class 3079 OID 16384)
-- Name: adminpack; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS adminpack WITH SCHEMA pg_catalog;


--
-- TOC entry 2936 (class 0 OID 0)
-- Dependencies: 1
-- Name: EXTENSION adminpack; Type: COMMENT; Schema: -; Owner:
--

COMMENT ON EXTENSION adminpack IS 'administrative functions for PostgreSQL';


SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 204 (class 1259 OID 16560)
-- Name: answer; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.answer (
    idanswer integer NOT NULL,
    question_idquestion integer,
    person_idperson integer,
    session_idsession integer,
    correct boolean,
    answers json,
    necessarytime timestamp without time zone,
    "position" integer
);


ALTER TABLE public.answer OWNER TO postgres;

--
-- TOC entry 203 (class 1259 OID 16558)
-- Name: answer_idanswer_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.answer_idanswer_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.answer_idanswer_seq OWNER TO postgres;

--
-- TOC entry 2937 (class 0 OID 0)
-- Dependencies: 203
-- Name: answer_idanswer_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.answer_idanswer_seq OWNED BY public.answer.idanswer;


--
-- TOC entry 206 (class 1259 OID 16571)
-- Name: course; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.course (
    idcourse integer NOT NULL,
    name character varying NOT NULL,
    description character varying NOT NULL,
    semester integer,
    lecturer character varying,
    password integer
);


ALTER TABLE public.course OWNER TO postgres;

--
-- TOC entry 205 (class 1259 OID 16569)
-- Name: course_idcourse_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.course_idcourse_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.course_idcourse_seq OWNER TO postgres;

--
-- TOC entry 2938 (class 0 OID 0)
-- Dependencies: 205
-- Name: course_idcourse_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.course_idcourse_seq OWNED BY public.course.idcourse;


--
-- TOC entry 208 (class 1259 OID 16582)
-- Name: courseadmin; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.courseadmin (
    idcourseadmin integer NOT NULL,
    course_idcourse integer,
    person_idperson integer,
    role character varying
);


ALTER TABLE public.courseadmin OWNER TO postgres;

--
-- TOC entry 207 (class 1259 OID 16580)
-- Name: courseadmin_idcourseadmin_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.courseadmin_idcourseadmin_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.courseadmin_idcourseadmin_seq OWNER TO postgres;

--
-- TOC entry 2939 (class 0 OID 0)
-- Dependencies: 207
-- Name: courseadmin_idcourseadmin_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.courseadmin_idcourseadmin_seq OWNED BY public.courseadmin.idcourseadmin;


--
-- TOC entry 210 (class 1259 OID 16594)
-- Name: logs; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.logs (
    idlogs integer NOT NULL,
    person_idperson integer,
    "timestamp" timestamp without time zone,
    action character varying
);


ALTER TABLE public.logs OWNER TO postgres;

--
-- TOC entry 209 (class 1259 OID 16592)
-- Name: logs_idlogs_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.logs_idlogs_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.logs_idlogs_seq OWNER TO postgres;

--
-- TOC entry 2940 (class 0 OID 0)
-- Dependencies: 209
-- Name: logs_idlogs_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.logs_idlogs_seq OWNED BY public.logs.idlogs;


--
-- TOC entry 212 (class 1259 OID 16605)
-- Name: participant; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.participant (
    idparticipant integer NOT NULL,
    course_idcourse integer,
    person_idperson integer
);


ALTER TABLE public.participant OWNER TO postgres;

--
-- TOC entry 211 (class 1259 OID 16603)
-- Name: participant_idparticipant_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.participant_idparticipant_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.participant_idparticipant_seq OWNER TO postgres;

--
-- TOC entry 2941 (class 0 OID 0)
-- Dependencies: 211
-- Name: participant_idparticipant_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.participant_idparticipant_seq OWNED BY public.participant.idparticipant;


--
-- TOC entry 214 (class 1259 OID 16613)
-- Name: person; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.person (
    idperson integer NOT NULL,
    name character varying,
    employee boolean,
    faculty character varying,
    title character varying,
    pseudonym character varying
);


ALTER TABLE public.person OWNER TO postgres;

--
-- TOC entry 213 (class 1259 OID 16611)
-- Name: person_idperson_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.person_idperson_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.person_idperson_seq OWNER TO postgres;

--
-- TOC entry 2942 (class 0 OID 0)
-- Dependencies: 213
-- Name: person_idperson_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.person_idperson_seq OWNED BY public.person.idperson;


--
-- TOC entry 216 (class 1259 OID 16624)
-- Name: question; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.question (
    idquestion integer NOT NULL,
    questiontext character varying,
    type character varying,
    quiz_idquiz integer,
    answers json,
    correctanswers json
);


ALTER TABLE public.question OWNER TO postgres;

--
-- TOC entry 215 (class 1259 OID 16622)
-- Name: question_idquestion_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.question_idquestion_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.question_idquestion_seq OWNER TO postgres;

--
-- TOC entry 2943 (class 0 OID 0)
-- Dependencies: 215
-- Name: question_idquestion_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.question_idquestion_seq OWNED BY public.question.idquestion;


--
-- TOC entry 218 (class 1259 OID 16635)
-- Name: quiz; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.quiz (
    idquiz integer NOT NULL,
    name character varying,
    password integer,
    description character varying,
    person_idperson integer
);


ALTER TABLE public.quiz OWNER TO postgres;

--
-- TOC entry 217 (class 1259 OID 16633)
-- Name: quiz_idquiz_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.quiz_idquiz_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.quiz_idquiz_seq OWNER TO postgres;

--
-- TOC entry 2944 (class 0 OID 0)
-- Dependencies: 217
-- Name: quiz_idquiz_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.quiz_idquiz_seq OWNED BY public.quiz.idquiz;


--
-- TOC entry 220 (class 1259 OID 16646)
-- Name: session; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.session (
    idsession integer NOT NULL,
    start date,
    "end" date,
    anonym boolean,
    quiz_idquiz integer,
    course_idcourse integer,
    person_idperson integer
);


ALTER TABLE public.session OWNER TO postgres;

--
-- TOC entry 219 (class 1259 OID 16644)
-- Name: session_idsession_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.session_idsession_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.session_idsession_seq OWNER TO postgres;

--
-- TOC entry 2945 (class 0 OID 0)
-- Dependencies: 219
-- Name: session_idsession_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.session_idsession_seq OWNED BY public.session.idsession;


--
-- TOC entry 2743 (class 2604 OID 16563)
-- Name: answer idanswer; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.answer ALTER COLUMN idanswer SET DEFAULT nextval('public.answer_idanswer_seq'::regclass);


--
-- TOC entry 2744 (class 2604 OID 16574)
-- Name: course idcourse; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.course ALTER COLUMN idcourse SET DEFAULT nextval('public.course_idcourse_seq'::regclass);


--
-- TOC entry 2745 (class 2604 OID 16585)
-- Name: courseadmin idcourseadmin; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.courseadmin ALTER COLUMN idcourseadmin SET DEFAULT nextval('public.courseadmin_idcourseadmin_seq'::regclass);


--
-- TOC entry 2746 (class 2604 OID 16597)
-- Name: logs idlogs; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.logs ALTER COLUMN idlogs SET DEFAULT nextval('public.logs_idlogs_seq'::regclass);


--
-- TOC entry 2747 (class 2604 OID 16608)
-- Name: participant idparticipant; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.participant ALTER COLUMN idparticipant SET DEFAULT nextval('public.participant_idparticipant_seq'::regclass);


--
-- TOC entry 2748 (class 2604 OID 16616)
-- Name: person idperson; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.person ALTER COLUMN idperson SET DEFAULT nextval('public.person_idperson_seq'::regclass);


--
-- TOC entry 2749 (class 2604 OID 16627)
-- Name: question idquestion; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.question ALTER COLUMN idquestion SET DEFAULT nextval('public.question_idquestion_seq'::regclass);


--
-- TOC entry 2750 (class 2604 OID 16638)
-- Name: quiz idquiz; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.quiz ALTER COLUMN idquiz SET DEFAULT nextval('public.quiz_idquiz_seq'::regclass);


--
-- TOC entry 2751 (class 2604 OID 16649)
-- Name: session idsession; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.session ALTER COLUMN idsession SET DEFAULT nextval('public.session_idsession_seq'::regclass);

ALTER TABLE ONLY public.answer
    ADD CONSTRAINT answer_pkey PRIMARY KEY (idanswer);


--
-- TOC entry 2755 (class 2606 OID 16579)
-- Name: course course_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.course
    ADD CONSTRAINT course_pkey PRIMARY KEY (idcourse);


--
-- TOC entry 2757 (class 2606 OID 16590)
-- Name: courseadmin courseadmin_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.courseadmin
    ADD CONSTRAINT courseadmin_pkey PRIMARY KEY (idcourseadmin);


--
-- TOC entry 2760 (class 2606 OID 16602)
-- Name: logs logs_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.logs
    ADD CONSTRAINT logs_pkey PRIMARY KEY (idlogs);


--
-- TOC entry 2764 (class 2606 OID 16610)
-- Name: participant participant_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.participant
    ADD CONSTRAINT participant_pkey PRIMARY KEY (idparticipant);


--
-- TOC entry 2766 (class 2606 OID 16621)
-- Name: person person_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.person
    ADD CONSTRAINT person_pkey PRIMARY KEY (idperson);


--
-- TOC entry 2768 (class 2606 OID 16632)
-- Name: question question_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.question
    ADD CONSTRAINT question_pkey PRIMARY KEY (idquestion);


--
-- TOC entry 2770 (class 2606 OID 16643)
-- Name: quiz quiz_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.quiz
    ADD CONSTRAINT quiz_pkey PRIMARY KEY (idquiz);


--
-- TOC entry 2773 (class 2606 OID 16651)
-- Name: session session_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.session
    ADD CONSTRAINT session_pkey PRIMARY KEY (idsession);


--
-- TOC entry 2761 (class 1259 OID 16669)
-- Name: fki_course; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX fki_course ON public.participant USING btree (course_idcourse);


--
-- TOC entry 2762 (class 1259 OID 16680)
-- Name: fki_perso; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX fki_perso ON public.participant USING btree (person_idperson);


--
-- TOC entry 2758 (class 1259 OID 16658)
-- Name: fki_person; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX fki_person ON public.logs USING btree (person_idperson);


--
-- TOC entry 2771 (class 1259 OID 16696)
-- Name: fki_quiz; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX fki_quiz ON public.session USING btree (quiz_idquiz);


--
-- TOC entry 2780 (class 2606 OID 16664)
-- Name: participant course; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.participant
    ADD CONSTRAINT course FOREIGN KEY (course_idcourse) REFERENCES public.course(idcourse) ON UPDATE CASCADE ON DELETE CASCADE NOT VALID;


--
-- TOC entry 2777 (class 2606 OID 16681)
-- Name: courseadmin course; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.courseadmin
    ADD CONSTRAINT course FOREIGN KEY (course_idcourse) REFERENCES public.course(idcourse) ON UPDATE CASCADE ON DELETE CASCADE NOT VALID;


--
-- TOC entry 2785 (class 2606 OID 16697)
-- Name: session course; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.session
    ADD CONSTRAINT course FOREIGN KEY (course_idcourse) REFERENCES public.course(idcourse) ON UPDATE CASCADE ON DELETE CASCADE NOT VALID;


--
-- TOC entry 2779 (class 2606 OID 16653)
-- Name: logs person; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.logs
    ADD CONSTRAINT person FOREIGN KEY (person_idperson) REFERENCES public.person(idperson) ON UPDATE CASCADE ON DELETE CASCADE NOT VALID;


--
-- TOC entry 2778 (class 2606 OID 16686)
-- Name: courseadmin person; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.courseadmin
    ADD CONSTRAINT person FOREIGN KEY (person_idperson) REFERENCES public.person(idperson) ON UPDATE CASCADE ON DELETE CASCADE NOT VALID;


--
-- TOC entry 2786 (class 2606 OID 16702)
-- Name: session person; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.session
    ADD CONSTRAINT person FOREIGN KEY (person_idperson) REFERENCES public.person(idperson) ON UPDATE CASCADE ON DELETE CASCADE NOT VALID;


--
-- TOC entry 2783 (class 2606 OID 16707)
-- Name: quiz person; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.quiz
    ADD CONSTRAINT person FOREIGN KEY (person_idperson) REFERENCES public.person(idperson) ON UPDATE CASCADE ON DELETE CASCADE NOT VALID;


--
-- TOC entry 2775 (class 2606 OID 16722)
-- Name: answer person; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.answer
    ADD CONSTRAINT person FOREIGN KEY (person_idperson) REFERENCES public.person(idperson) ON UPDATE CASCADE ON DELETE CASCADE NOT VALID;


--
-- TOC entry 2781 (class 2606 OID 16675)
-- Name: participant person1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.participant
    ADD CONSTRAINT person1 FOREIGN KEY (person_idperson) REFERENCES public.person(idperson) ON UPDATE CASCADE ON DELETE CASCADE NOT VALID;


--
-- TOC entry 2774 (class 2606 OID 16717)
-- Name: answer question; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.answer
    ADD CONSTRAINT question FOREIGN KEY (question_idquestion) REFERENCES public.question(idquestion) ON UPDATE CASCADE ON DELETE CASCADE NOT VALID;


--
-- TOC entry 2784 (class 2606 OID 16691)
-- Name: session quiz; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.session
    ADD CONSTRAINT quiz FOREIGN KEY (quiz_idquiz) REFERENCES public.quiz(idquiz) ON UPDATE CASCADE ON DELETE CASCADE NOT VALID;


--
-- TOC entry 2782 (class 2606 OID 16712)
-- Name: question quiz; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.question
    ADD CONSTRAINT quiz FOREIGN KEY (quiz_idquiz) REFERENCES public.quiz(idquiz) ON UPDATE CASCADE ON DELETE CASCADE NOT VALID;


--
-- TOC entry 2776 (class 2606 OID 16727)
-- Name: answer session; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.answer
    ADD CONSTRAINT session FOREIGN KEY (session_idsession) REFERENCES public.session(idsession) ON UPDATE CASCADE ON DELETE CASCADE NOT VALID;


-- Completed on 2020-06-09 11:34:28

--
-- PostgreSQL database dump complete
--

UPDATE public._migrations SET migration_number = 1;
COMMIT;
