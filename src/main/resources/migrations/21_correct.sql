BEGIN;

ALTER TABLE session
    ADD COLUMN show_correct boolean DEFAULT false;

UPDATE public._migrations SET migration_number = 21;
COMMIT;
