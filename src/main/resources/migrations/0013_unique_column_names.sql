BEGIN;

ALTER TABLE course RENAME COLUMN name TO course_name;
ALTER TABLE course RENAME COLUMN description TO course_description;
ALTER TABLE course RENAME COLUMN password TO course_password;

ALTER TABLE quiz RENAME COLUMN name TO quiz_name;
ALTER TABLE quiz RENAME COLUMN password TO quiz_password;
ALTER TABLE quiz RENAME COLUMN description TO quiz_description;
ALTER TABLE quiz RENAME column person_idperson TO quiz_creator_idperson;

ALTER TABLE session RENAME COLUMN person_idperson TO session_creator_idperson;

UPDATE public._migrations SET migration_number = 13;
COMMIT;
