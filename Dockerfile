FROM ghcr.io/graalvm/graalvm-ce:java11-21 AS build
COPY . /build
WORKDIR /build
RUN ./mvnw package -Dmaven.test.skip=true

FROM ghcr.io/graalvm/graalvm-ce:java11-21
WORKDIR /opt/vertx-backend
COPY --from=build /build/target/vertx-backend-*-fat.jar .
CMD java -jar vertx-backend-*-fat.jar
